# Maniac Verify

## Note: project not manintained actively.
For an improved version please check, aXc: https://gitlab.informatik.uni-bremen.de/arun/axekit/blob/master/README.md
and yise: https://gitlab.informatik.uni-bremen.de/arun/yise/blob/master/README.md


Maniac Verify is an addon in CirKit for formal verification of errors
in Approximate Computing. 

Read maniac_verify.pdf for an overview.

CirKit is a software library and framework for logic synthesis/verification.

This repository is a snapshot of the program explained in the paper
"Precise Error Determination of Approximated Components in Sequential
Circuits with Model Checking", published in DAC-2016.

Note: This is not the full version of CirKit, rather a heavily customized
and trimmed down one.

For further information, please visit
:: http://www.informatik.uni-bremen.de/agra/eng/maniac.php

## Requirements

The following software is required in order to build CirKit

* git
* cmake (at least version 3.0.0)
* g++ (at least version 4.9.0) or clang++ (at least version 3.5.0)
* boost (at least version 1.56.0)
* GNU MP, and its C++ interface GMP++
* GNU readline

In *Ubuntu* the packages can be installed with

    sudo apt-get install build-essential git g++ cmake libboost-all-dev libgmp3-dev libxml2-dev

In *Arch* the packages can be installed with

    sudo pacman -S base-devel git g++ cmake boost boost-libs gmp libxml2

## Build CirKit

After extracting or cloning CirKit perform the following steps

    cd build
    cmake ..
    make external
    make

## Executing CirKit Program
'build/programs/cirkit' is the final executable, when successfully built.

   
Invocation example for Maniac Verify.                         

    cirkit -c "mc_verify -g ./benchmarks/adders/seq_aca_ii_n8_q4.v -1 multiplier -a ./benchmarks/adders/seq_rca_n8.v -2 multiplier -p out --accum_error 1000 --accum_error_rate 5 --clock clk --reset rst --oe oe"


## Benchmark circuits.
The benchmark circuits are provided in 'benchmarks' directory.

