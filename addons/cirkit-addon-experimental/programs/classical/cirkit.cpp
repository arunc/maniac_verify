/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @author Mathias Soeken
 * @author Heinz Riener
 */

#include <memory>

#include <boost/assign/list_inserter.hpp>

#include <core/cli/command.hpp>
#include <core/cli/environment.hpp>
#include <core/cli/rules.hpp>
#include <core/cli/store.hpp>
#include <core/cli/stores.hpp>
#include <core/cli/utils.hpp>
#include <core/cli/commands/bdd.hpp>
#include <core/cli/commands/read_pla.hpp>
#include <core/cli/commands/testbdd.hpp>
#include <core/utils/bdd_utils.hpp>
#include <abc/cli/commands/abc.hpp>
#include <abc/cli/commands/cec.hpp>
#include <classical/aig.hpp>
#include <classical/mig.hpp>
#include <classical/netlist_graphs.hpp>
#include <classical/cli/commands/aig.hpp>
#include <classical/cli/commands/bool_complex.hpp>
#include <classical/cli/commands/cone.hpp>
#include <classical/cli/commands/control_logic.hpp>
#include <classical/cli/commands/controllable.hpp>
#include <classical/cli/commands/cuts.hpp>
#include <classical/cli/commands/dbg_stat.hpp>
#include <classical/cli/commands/depth.hpp>
#include <classical/cli/commands/diagnose.hpp>
#include <classical/cli/commands/dsop.hpp>
#include <classical/cli/commands/feather.hpp>
#include <classical/cli/commands/funchash.hpp>
#include <classical/cli/commands/inject.hpp>
#include <classical/cli/commands/mc_test.hpp>
#include <classical/cli/commands/mc_combo_metrics.hpp>
#include <classical/cli/commands/mc_combo_worst_case.hpp>
#include <classical/cli/commands/mc_compute_max_error.hpp>
#include <classical/cli/commands/mc_write_port_info.hpp>
#include <classical/cli/commands/mc_v_to_blif.hpp>
#include <classical/cli/commands/mc_verify.hpp>
#include <classical/cli/commands/memristor.hpp>
#include <classical/cli/commands/mig.hpp>
#include <classical/cli/commands/mig_rewrite.hpp>
#include <classical/cli/commands/miter.hpp>
#include <classical/cli/commands/npn.hpp>
#include <classical/cli/commands/output_noise.hpp>
#include <classical/cli/commands/propagate.hpp>
#include <classical/cli/commands/read_aiger.hpp>
#include <classical/cli/commands/read_bench.hpp>
#include <classical/cli/commands/read_sym.hpp>
#ifdef ADDON_VERIFIC
#include <classical/cli/commands/read_verilog.hpp>
#endif
#include <classical/cli/commands/read_words.hpp>
#include <classical/cli/commands/rename.hpp>
#include <classical/cli/commands/repair.hpp>
#include <classical/cli/commands/shuffle.hpp>
#include <classical/cli/commands/simgraph.hpp>
#include <classical/cli/commands/simulate.hpp>
#include <classical/cli/commands/strash.hpp>
#include <classical/cli/commands/support.hpp>
#include <classical/cli/commands/tt.hpp>
#include <classical/cli/commands/write_aiger.hpp>
#include <classical/cli/commands/write_verilog.hpp>
#include <classical/cli/commands/words.hpp>
#include <classical/cli/stores.hpp>
#include <classical/utils/truth_table_utils.hpp>
#include <formal/cli/commands/exact_mig.hpp>
#include <formal/cli/commands/lad_spiec.hpp>
#include <formal/cli/commands/modelcheck.hpp>
#include <formal/cli/commands/unate.hpp>

using namespace boost::assign;
using namespace cirkit;

int main( int argc, char ** argv )
{
  cli_main<aig_graph, mig_graph, counterexample_t, simple_fanout_graph_t, std::vector<aig_node>, tt, bdd_function_t> cli( "cirkit" );

  ADD_COMMAND( abc );
  ADD_COMMAND( aig );
  ADD_COMMAND( bdd );
  ADD_COMMAND( bool_complex );
  ADD_COMMAND( cec );
  ADD_COMMAND( cone );
  ADD_COMMAND( control_logic );
  ADD_COMMAND( controllable );
  ADD_COMMAND( cuts );
  ADD_COMMAND( dbg_stat );
  ADD_COMMAND( depth );
  ADD_COMMAND( diagnose );
  ADD_COMMAND( dsop );
  ADD_COMMAND( exact_mig );
  ADD_COMMAND( feather );
  ADD_COMMAND( funchash );
  ADD_COMMAND( inject );
  ADD_COMMAND( lad_spiec );
  ADD_COMMAND( mc_test );
  ADD_COMMAND( mc_compute_max_error );
  ADD_COMMAND( mc_combo_metrics );
  ADD_COMMAND( mc_combo_worst_case );
  ADD_COMMAND( mc_write_port_info );
  ADD_COMMAND( mc_v_to_blif );
  ADD_COMMAND( mc_verify );
  ADD_COMMAND( memristor );
  ADD_COMMAND( mig );
  ADD_COMMAND( mig_rewrite );
  ADD_COMMAND( miter );
  ADD_COMMAND( modelcheck );
  ADD_COMMAND( npn );
  ADD_COMMAND( output_noise );
  ADD_COMMAND( propagate );
  ADD_COMMAND( read_aiger );
  ADD_COMMAND( read_bench );
  ADD_COMMAND( read_pla );
  ADD_COMMAND( read_sym );
  ADD_COMMAND( read_words );
  ADD_COMMAND( rename );
  ADD_COMMAND( repair );
  ADD_COMMAND( shuffle );
  ADD_COMMAND( simgraph );
  ADD_COMMAND( simulate );
  ADD_COMMAND( strash );
  ADD_COMMAND( support );
  ADD_COMMAND( testbdd );
  ADD_COMMAND( tt );
  ADD_COMMAND( unate );
  ADD_COMMAND( write_aiger );
  ADD_COMMAND( write_verilog );
  ADD_COMMAND( words );

#ifdef ADDON_VERIFIC
  ADD_COMMAND( read_verilog );
#endif


  return cli.run( argc, argv );
}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
