/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @author Mathias Soeken
 */

#include <boost/format.hpp>

#include <core/properties.hpp>
#include <core/utils/program_options.hpp>
#include <core/utils/string_utils.hpp>

#include <classical/aig.hpp>
#include <classical/io/read_aiger.hpp>
#include <classical/utils/aig_utils.hpp>

#include <formal/revenge/component_equivalence_check.hpp>
#include <formal/sat/cnf_solver.hpp>
#include <formal/sat/minisat.hpp>
#include <formal/sat/z3.hpp>

using namespace cirkit;

int main( int argc, char ** argv )
{
  using boost::program_options::value;

  std::string target, pattern;
  std::string input_mapping, output_mapping;
  auto solver = 0u;

  program_options opts;
  opts.add_options()
    ( "target",         value( &target ),              "Target circuit (larger one)" )
    ( "pattern",        value( &pattern ),             "Pattern circuit (smaller one)" )
    ( "input_mapping",  value( &input_mapping ),       "Input mapping (pattern=target) (e.g. i1=i2,i3=i3)" )
    ( "output_mapping", value( &output_mapping ),      "Output mapping (pattern=target) (e.g. o1=o2,o3=o3)" )
    ( "solver",         value_with_default( &solver ), "0: minisat\n1: picosat\n2: sat13\n3: minisat (direct)\n4: z3" )
    ( "verbose,v",                                     "Be verbose" )
    ;
  opts.parse( argc, argv );

  if ( !opts.good() || !opts.is_set( "target" ) || !opts.is_set( "pattern" ) )
  {
    std::cout << opts << std::endl;
    return 1;
  }

  /* Create AIGs */
  aig_graph aig_target, aig_pattern;
  try
  {
    std::cout << boost::format( "[i] read target ''%s''" ) % target << '\n';
    read_aiger( aig_target, target );
    std::cout << boost::format( "[i] read pattern ''%s''" ) % pattern << '\n';
    read_aiger( aig_pattern, pattern );
  }
  catch ( const char *msg )
  {
    std::cerr << "[e] " << msg << std::endl;
    return 2;
  }

  const auto& target_info = boost::get_property( aig_target, boost::graph_name );
  const auto& pattern_info = boost::get_property( aig_pattern, boost::graph_name );

  properties::ptr settings = std::make_shared<properties>();
  if ( solver < 3u )
  {
    settings->set( "solver", std::vector<solver_executable_function_t>( {execute_minisat, execute_picosat, execute_sat13} )[solver] );
  }
  settings->set( "verbose", opts.is_set( "verbose" ) );

  properties::ptr statistics = std::make_shared<properties>();

  std::vector<unsigned> vinput_mapping( pattern_info.inputs.size() ), voutput_mapping( pattern_info.outputs.size() );

  foreach_string( input_mapping, ",", [&]( const std::string& p ) {
      auto v = split_string_pair( p, "=" );
      auto pi = aig_input_index( pattern_info, aig_node_by_name( pattern_info, v.first ) );
      auto ti = aig_input_index( target_info, aig_node_by_name( target_info, v.second ) );
      vinput_mapping[pi] = ti;
    });

  foreach_string( output_mapping, ",", [&]( const std::string& p ) {
      auto v = split_string_pair( p, "=" );
      auto pi = aig_output_index( pattern_info, v.first );
      auto ti = aig_output_index( target_info, v.second );
      voutput_mapping[pi] = ti;
    });

  auto equal = false;
  if ( solver < 3u )
  {
    equal = component_equivalence_check<cnf_solver>( aig_target, aig_pattern, vinput_mapping, voutput_mapping, settings, statistics );
  }
  else if ( solver == 3u )
  {
    equal = component_equivalence_check<minisat_solver>( aig_target, aig_pattern, vinput_mapping, voutput_mapping, settings, statistics );
  }
  else if ( solver == 4u )
  {
#ifdef ADDON_FORMAL
    equal = component_equivalence_check<z3_solver>( aig_target, aig_pattern, vinput_mapping, voutput_mapping, settings, statistics );
#else
    std::cout << "[e] z3 solver requires formal addon enabled" << std::endl;
    return 1;
#endif
  }

  std::cout << "[i] component is equivalent: " << ( equal ? "yes" : "no" ) << std::endl;
  std::cout << boost::format( "[i] run-time: %.2f seconds" ) % statistics->get<double>( "runtime" ) << std::endl;

  return 0;
}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
