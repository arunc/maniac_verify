/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @author Mathias Soeken
 */

#define UNIX 1

#include <boost/algorithm/string/predicate.hpp>
#include <boost/format.hpp>

#include <core/utils/program_options.hpp>
#include <core/utils/string_utils.hpp>

#include <classical/aig.hpp>
#include <classical/functions/simulation_graph.hpp>
#include <classical/io/aigmeta.hpp>
#include <classical/io/read_aiger.hpp>
#include <classical/utils/aig_utils.hpp>
#include <classical/verification/abc_cec.hpp>

#ifdef ADDON_VERIFIC
#include <classical/io/read_verilog_to_aig.hpp>
#endif

#include <formal/graph/dynamic_tripartite_subgraph_isomorphism.hpp>
#include <formal/revenge/component_equivalence_check.hpp>
#include <formal/revenge/find_component_candidate.hpp>
#include <formal/sat/cnf_solver.hpp>
#include <formal/sat/minisat.hpp>
#include <formal/sat/z3.hpp>
#include <formal/sat/utils/solver_executables.hpp>

using namespace cirkit;

int main( int argc, char ** argv )
{
  using boost::program_options::value;

  auto block_vectors = true;
  auto functional    = false;
  auto block_support = true;
  auto support_edges = false;
  auto simulation_signatures = false;
  std::string symmetries;
  auto solver = 0u;
  auto method = 0u;
  auto equivalence = 0u;
  std::string target, pattern;
  std::string vectors;
  std::string target_dot, pattern_dot;
  std::string target_graph = "/tmp/target.graph", pattern_graph = "/tmp/pattern.graph";
  std::vector<std::string> defines, pdefines;
  std::string abc_cnf_command = encode_slice_abc_constants::abc_cnf_command;
  std::string logname = "/tmp/log";
  std::string texlogname = "/tmp/log.tex";
  bool verbose;

  { /* these parantheses have an effect on Verific */
  program_options opts;
  opts.add_options()
    ( "target",                value( &target ),                             "Target circuit (larger one)" )
    ( "pattern",               value( &pattern ),                            "Pattern circuit (smaller one)" )
    ( "defines,d",             value( &defines )->composing(),               "Define macros for the Verilog parser" )
    ( "pdefines,p",            value( &pdefines )->composing(),              "Define macros only for the pattern circuit; are overriding 'defines'" )
    ( "method",                value_with_default( &method ),                "Method:\n0: SAT (inverters)\n1: SAT\n2: LAD\n3: VF2\n4: directed LAD\n5: new directed LAD\n6: new directed LAD (from AIG)\n7: new directed LAD (with native simulation graph)" )
    ( "vectors",               value( &vectors ),                            "Simulation vectors (comma separated):\nah: all-hot\n1h: one-hot\n2h: two-hot\nac: all-cold\n1c: one-cold\n2c: two-cold" )
    ( "block_vectors",         value_with_default( &block_vectors ),         "Use blocking clauses based on vectors (only with inverters)" )
    ( "block_support",         value_with_default( &block_support ),         "Use blocking clauses based on support (only with inverters)" )
    ( "functional",            value_with_default( &functional ),            "Compute functional instead of structural support (only with methods 6 and 7)" )
    ( "support_edges",         value_with_default( &support_edges ),         "Use support edges (only with methods 6 and 7)" )
    ( "simulation_signatures", value_with_default( &simulation_signatures ), "Use simulation signatures (only with methods 6 and 7)" )
    ( "symmetries",            value( &symmetries ),                         "Input symmetries (only with SAT)" )
    ( "solver",                value_with_default( &solver ),                "0: minisat\n1: picosat\n2: sat13\n3: minisat (direct)\n4: z3" )
    ( "equivalence",           value_with_default( &equivalence ),           "Check candidate for equivalence:\n0: don't check\n1: built-in\n2: abc" )
    ( "target_dot",            value( &target_dot ) ,                        "Writes target simulation graph to DOT file" )
    ( "pattern_dot",           value( &pattern_dot ),                        "Writes pattern simulation graph to DOT file" )
    ( "target_graph",          value( &target_graph ),                       "Writes target simulation graph to graph file" )
    ( "pattern_graph",         value( &pattern_graph ),                      "Writes pattern simulation graph to graph file" )
    ( "verbose,v",                                                           "Be verbose" )
    ( "abc_cnf_command",       value_with_default( &abc_cnf_command ),       "ABC command for CNF generation" )
    ( "logname",               value_with_default( &logname ),               "Logname if temporary files are written" )
    ( "texlogname",            value_with_default( &texlogname ),            "Logname if temporary LaTeX files are written" )
    ;
  opts.parse( argc, argv );

  if ( !opts.good() || !opts.is_set( "target" ) || !opts.is_set( "pattern" ) || !opts.is_set( "vectors" ) )
  {
    std::cout << opts << std::endl;
    return 1;
  }

  verbose     = opts.is_set( "verbose" );
  }

  /* Create AIGs */
  aig_graph aig_target, aig_pattern;
#ifdef ADDON_VERIFIC
  read_verilog_to_aig_settings rv_settings;
  rv_settings.flatten        = true;
  rv_settings.remove_buffers = true;
  rv_settings.verbose        = verbose;

  for ( const auto& define : defines )
  {
    auto p = split_string_pair( define, "=" );
    rv_settings.macros[p.first] = p.second;
  }
#endif

  if ( boost::ends_with( target, ".v" ) )
  {
    aigmeta meta_target;
#ifdef ADDON_VERIFIC
    read_verilog_to_aig( aig_target, meta_target, target, rv_settings );
#else
    std::cout << "[e] read_verilog_to_aig requires verific addon enabled" << std::endl;
    return 1;
#endif
  }
  else
  {
    read_aiger( aig_target, target );
  }

  if ( boost::ends_with( pattern, ".v" ) )
  {
#ifdef ADDON_VERIFIC
    for ( const auto& define : pdefines )
    {
      auto p = split_string_pair( define, "=" );
      rv_settings.macros[p.first] = p.second;
    }

    aigmeta meta_pattern;
    read_verilog_to_aig( aig_pattern, meta_pattern, pattern, rv_settings );
#else
    std::cout << "[e] read_verilog_to_aig requires verific addon enabled" << std::endl;
    return 1;
#endif
  }
  else
  {
    read_aiger( aig_pattern, pattern );
  }

  /* aig stats */
  if ( verbose )
  {
    aig_print_stats( aig_target );
    aig_print_stats( aig_pattern );
  }

  /* Simulation vectors */
  std::vector<unsigned> types;
  foreach_string( vectors, ",", [&]( const std::string& s ) {
      if      ( s == "ah" ) types += 0u;
      else if ( s == "1h" ) types += 3u;
      else if ( s == "2h" ) types += 5u;
      else if ( s == "ac" ) types += 1u;
      else if ( s == "1c" ) types += 2u;
      else if ( s == "2c" ) types += 4u;
    } );

  properties::ptr settings = std::make_shared<properties>();
  if ( solver < 3u )
  {
    settings->set( "solver", std::vector<solver_executable_function_t>( {execute_minisat, execute_picosat, execute_sat13} )[solver] );
  }
  settings->set( "circuit_dotname", target_dot );
  settings->set( "circuit_graphname", target_graph );
  settings->set( "component_dotname", pattern_dot );
  settings->set( "component_graphname", pattern_graph );
  settings->set( "logname", logname );
  settings->set( "verbose", verbose );

  if ( method <= 1u )
  {
    settings->set( "block_vectors", block_vectors );
    settings->set( "block_support", block_support );
    settings->set( "symmetries",    symmetries );
  }

  properties::ptr statistics = std::make_shared<properties>();

  std::vector<unsigned> input_mapping, output_mapping;
  bool result;

  switch ( method )
  {
  case 0u:
    {
      settings->set( "abc_cnf_command", abc_cnf_command );
      if ( solver < 3u )
      {
        result = find_component_candidate_with_inverters<cnf_solver>( input_mapping, output_mapping, aig_target, aig_pattern, types, settings, statistics );
      }
      else if ( solver == 3u )
      {
        result = find_component_candidate_with_inverters<minisat_solver>( input_mapping, output_mapping, aig_target, aig_pattern, types, settings, statistics );
      }
      else if ( solver == 4u )
      {
#ifdef ADDON_FORMAL
        result = find_component_candidate_with_inverters<z3_solver>( input_mapping, output_mapping, aig_target, aig_pattern, types, settings, statistics );
#else
        std::cout << "[e] z3 solver requires formal addon enabled" << std::endl;
        return 1;
#endif
      }
    } break;
  case 1u:
    {
      if ( solver < 3u )
      {
        result = find_component_candidate<cnf_solver>( input_mapping, output_mapping, aig_target, aig_pattern, types, settings, statistics );
      }
      else if ( solver == 3u )
      {
        result = find_component_candidate<minisat_solver>( input_mapping, output_mapping, aig_target, aig_pattern, types, settings, statistics );
      }
      else if ( solver == 4u )
      {
#ifdef ADDON_FORMAL
        result = find_component_candidate<z3_solver>( input_mapping, output_mapping, aig_target, aig_pattern, types, settings, statistics );
#else
        std::cout << "[e] z3 solver requires formal addon enabled" << std::endl;
        return 1;
#endif
      }
    } break;
  case 2u:
    {
      //result = find_component_candidate_lad( input_mapping, output_mapping, aig_target, aig_pattern, types, settings, statistics );
    } break;
  case 3u:
    {
      //result = find_component_candidate_vf2( input_mapping, output_mapping, aig_target, aig_pattern, types, settings, statistics );
    } break;
  case 4u:
    {
      result = find_component_candidate_directed_lad( input_mapping, output_mapping, aig_target, aig_pattern, types, settings, statistics );
    } break;
  case 5u:
    {
      result = find_component_candidate_directed_lad2( input_mapping, output_mapping, aig_target, aig_pattern, types, settings, statistics );
    } break;
  case 6u:
    {
      settings->set( "functional", functional );
      settings->set( "support_edges", functional && support_edges );
      settings->set( "simulation_signatures", simulation_signatures );
      result = find_component_candidate_directed_lad3( input_mapping, output_mapping, aig_target, aig_pattern, types, settings, statistics );
    } break;
  case 7u:
    {
      settings->set( "functional", functional );
      settings->set( "support_edges", functional && support_edges );
      settings->set( "simulation_signatures", simulation_signatures );
      settings->set( "texlogname", texlogname );
      result = find_component_candidate_directed_lad4( input_mapping, output_mapping, aig_target, aig_pattern, types, settings, statistics );
    } break;
  }

  std::cout << "[i] is pattern contained in target: " << ( result ? "yes" : "no" ) << std::endl;

  if ( result && method == 0u )
  {
    settings->set( "input_inverters", statistics->get<boost::dynamic_bitset<>>( "input_inverters" ) );
    settings->set( "output_inverters", statistics->get<boost::dynamic_bitset<>>( "output_inverters" ) );
    std::cout << "[i] input inverters: " << statistics->get<boost::dynamic_bitset<>>( "input_inverters" ) << std::endl;
    std::cout << "[i] output inverters: " << statistics->get<boost::dynamic_bitset<>>( "output_inverters" ) << std::endl;
  }

  std::cout << boost::format( "[i] run-time: %.2f seconds" ) % statistics->get<double>( "runtime" ) << std::endl;

  if ( verbose )
  {
    if ( method < 2u )
    {
      std::cout << boost::format( "[i] number of variables: %d" ) % statistics->get<unsigned>( "num_vars" ) << std::endl;
      std::cout << boost::format( "[i] number of clauses: %d" ) % statistics->get<unsigned>( "num_clauses" ) << std::endl;
      std::cout << boost::format( "[i] solving run-time: %.2f seconds" ) % statistics->get<double>( "solving_runtime" ) << std::endl;
    }
    if ( method == 0u )
    {
      if ( block_vectors )
      {
        std::cout << boost::format( "[i] number of blocking clauses based on vectors: %d" ) % statistics->get<unsigned>( "num_blocking_clauses_vectors" ) << std::endl;
      }
      if ( block_support )
      {
        std::cout << boost::format( "[i] number of blocking clauses based on support: %d" ) % statistics->get<unsigned>( "num_blocking_clauses_support" ) << std::endl;
      }
      std::cout << boost::format( "[i] number of circuit simulation vectors: %d" ) % statistics->get<unsigned>( "num_circuit_vectors" ) << std::endl;
      std::cout << boost::format( "[i] number of component simulation vectors: %d" ) % statistics->get<unsigned>( "num_component_vectors" ) << std::endl;
    }
    if ( method == 8u )
    {
      std::cout << boost::format( "[i] number of branches: %d" ) % statistics->get<unsigned>( "num_branches" ) << std::endl;
    }
  }

  if ( result && equivalence == 1u )
  {
    bool equal = component_equivalence_check<cnf_solver>( aig_target, aig_pattern, input_mapping, output_mapping, settings, statistics );
    std::cout << "[i] component is equivalent: " << ( equal ? "yes" : "no" ) << std::endl;
    std::cout << boost::format( "[i] run-time: %.2f seconds" ) % statistics->get<double>( "runtime" ) << std::endl;
  }
  else if ( result && equivalence == 2u )
  {
    bool equal = abc_equivalence_check( aig_target, aig_pattern, input_mapping, output_mapping, settings, statistics );
    std::cout << "[i] component is equivalent: " << ( equal ? "yes" : "no" ) << std::endl;
    std::cout << boost::format( "[i] run-time: %.2f seconds" ) % statistics->get<double>( "runtime" ) << std::endl;
  }

  return 0;
}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
