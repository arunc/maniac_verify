/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @author Mathias Soeken
 */

#include <cstdlib>

#include <core/utils/program_options.hpp>
#include <core/utils/string_utils.hpp>
#include <core/utils/system_utils.hpp>

#include <classical/optimization/m_active_sop_optimization.hpp>

using namespace cirkit;

int main( int argc, char ** argv )
{
  using boost::program_options::value;

  std::string filename;
  unsigned    output = 0u;
  std::string active;

  program_options opts;
  opts.add_options()
    ( "filename", value( &filename ),          "PLA filename" )
    ( "output", value_with_default( &output ), "Index of output function" )
    ( "active_set", value( &active ),          "Active set (space separated)" )
    ( "verify,c",                              "Verify (intermediate) results" )
    ( "verbose,v",                             "Be verbose" )
    ;
  opts.parse( argc, argv );

  if ( !opts.good() || !opts.is_set( "filename" ) )
  {
    std::cout << opts << std::endl;
    return 1;
  }

  std::vector<unsigned> active_set;
  parse_string_list( active_set, active, " " );

  properties::ptr settings( new properties );
  settings->set( "output", output );
  settings->set( "verify_buckets", opts.is_set( "verify" ) );
  settings->set( "verbose", opts.is_set( "verbose" ) );
  properties::ptr statistics( new properties );
  m_active_sop_optimization( "/tmp/result.pla", filename, active_set, settings, statistics );

  if ( opts.is_set( "verify" ) )
  {
    system( "abc -c \"cec /tmp/initial.pla /tmp/result.pla\"" );
  }

  std::cout << "[i] initial size: " << statistics->get<unsigned>( "initial_size" ) << std::endl
            << "[i] final size: " << statistics->get<unsigned>( "final_size" ) << std::endl
            << "[i] DSOP size: " << execute_and_return( "espresso -Ddisjoint /tmp/initial.pla | grep -e \"^[01-]\" | wc -l" ).second[0] << std::endl;

  return 0;
}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
