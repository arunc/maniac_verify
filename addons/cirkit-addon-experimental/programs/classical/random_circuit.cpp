/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @author Amr Sayed Ahmed
 * @author Mathias Soeken
 */

#include <chrono>
#include <fstream>
#include <iostream>
#include <random>
#include <string>

#include <boost/assign/std/vector.hpp>
#include <boost/format.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/irange.hpp>

#include <core/utils/program_options.hpp>
#include <core/utils/range_utils.hpp>

using namespace boost::assign;
using namespace cirkit;

typedef std::vector<std::tuple< unsigned, std::string, std::string>> input_operands;
typedef std::vector<std::tuple<std::string, unsigned>> input_vec_t;
typedef std::vector<std::tuple<std::string, unsigned, input_operands, unsigned>> operation_vec_t;
typedef std::vector<std::tuple<std::string, unsigned, unsigned, unsigned>> gate_vec_t;

template<class URNG>
inline unsigned get_num_gates( URNG& g )
{
  return std::uniform_int_distribution<unsigned>( 3u, 10u )( g );
}

template<class URNG>
inline unsigned get_num_inputs( URNG& g, unsigned num_gates )
{
  return std::uniform_int_distribution<unsigned>( num_gates, 2 * num_gates )( g );
}

template<class URNG>
inline void add_input(URNG& g, input_vec_t & used_inputs, input_operands & opr_operands, int opt_width, int input_id, std::string op )
{
    std::string lwidth, mwidth;
    if ( boost::find( used_inputs, std::make_tuple( "{{ width - 1 }}", input_id ) ) == used_inputs.end() ) { used_inputs += std::make_tuple( "{{ width - 1 }}", input_id ); }
    auto bitwise = std::uniform_int_distribution<unsigned>( 0u,  opt_width - 1u )( g );
    lwidth = boost::str( boost::format( "{{%d*width//%d }}" ) % bitwise % opt_width );
    if(op=="LUT")
    	mwidth = boost::str( boost::format( "{{ ( ( [ ((%d*width//%d-1)|abs) - %d*width//%d, 15 ] |sort ) |first) + %d*width//%d }}" ) 
             % (bitwise+1) % opt_width % bitwise % opt_width % bitwise % opt_width);
    else
        mwidth = boost::str( boost::format( "{{(%d*width//%d-1)|abs }}" ) % (bitwise+1) % opt_width);
    opr_operands += std::make_tuple( input_id, mwidth,lwidth );
    return;
}
template<class URNG>
inline operation_vec_t get_operations( URNG& g, unsigned num_gates, unsigned num_inputs, input_vec_t& used_inputs )
{
  std::vector<std::string> ops { "+", "*", "-", "**", "|", "&", "^", "<<", ">>", "MUX", "LUT"};
  std::vector<int> width_div { 1, 2, 4, 8};
  operation_vec_t operations;


  for ( auto i = 0u; i < num_gates; ++i )
  {
    input_operands opr_operands;
    auto op = std::uniform_int_distribution<unsigned>( 0u, ops.size() - 1u )( g );
    auto wdiv = std::uniform_int_distribution<unsigned>( 0u, width_div.size() - 1u )( g );
    /* first input */
    auto i1 = std::uniform_int_distribution<unsigned>( 0u, num_inputs - 1u )( g );
    add_input(g, used_inputs, opr_operands, width_div[wdiv], i1, ops[op]);

    /* second input */
    if(ops[op]!="LUT"){
    	auto i2 = std::uniform_int_distribution<unsigned>( 0u, num_inputs - 2u )( g );
    	if ( i1 == i2 ) { ++i2; }
        add_input(g, used_inputs, opr_operands, width_div[wdiv], i2, ops[op]);
    }
    
    /* third input */
    if(ops[op]=="MUX"){
    	auto i3 = std::uniform_int_distribution<unsigned>( 0u, num_inputs - 3u )( g );
    	if ( boost::find( used_inputs, std::make_tuple( " 0 ", i3 ) ) == used_inputs.end() ) { used_inputs += std::make_tuple( " 0 ", i3 ); }
        auto bitwise = std::uniform_int_distribution<unsigned>( 0u,  width_div[wdiv] - 1u )( g );
        std::string lwidth = boost::str( boost::format( "{{%d*width//%d }}" ) % bitwise % width_div[wdiv] );
        opr_operands += std::make_tuple( i3, lwidth,lwidth );
    }		
    operations += std::make_tuple( ops[op], i, opr_operands, width_div[wdiv]);
  }

  return operations;
}


inline std::function<std::string(unsigned)> make_param( const std::string& type, const std::string& prefix )
{
  return [&type, &prefix]( unsigned i ) { return boost::str( boost::format( "%s [{{ width - 1 }}:0] %s%d" ) % type % prefix % i ); };
}

int main( int argc, char ** argv )
{
  using boost::program_options::value;
  using boost::adaptors::transformed;

  std::string filename;
  std::string modulename = "random";
  bool        dynamic_width = true;
  unsigned    seed;

  program_options opts;
  opts.add_options()
    ( "filename",      value( &filename ),                   "Verilog filename" )
    ( "modulename",    value_with_default( &modulename ),    "Module name" )
    ( "dynamic_width", value_with_default( &dynamic_width ), "If true, output bit-widths are adjusted" )
    ( "seed",          value( &seed ),                       "If given, seed is used instead of current time" )
    ( "verbose,v",                                           "Be verbose" )
    ;
  opts.parse( argc, argv );

  if ( !opts.good() || !opts.is_set( "filename" ) )
  {
    std::cout << opts << std::endl;
    return 1;
  }

  /* Random number generator */
  if ( !opts.is_set( "seed" ) )
  {
    seed = std::chrono::system_clock::now().time_since_epoch().count();
  }
  std::default_random_engine generator( seed );

  unsigned num_gates  = get_num_gates( generator );
  unsigned num_inputs = get_num_inputs( generator, num_gates );
  input_vec_t used_inputs;

  auto     operations      = get_operations( generator, num_gates, num_inputs, used_inputs );

  if ( opts.is_set( "verbose" ) )
  {
    std::cout << "[i] number of gates:  " << num_gates << std::endl
              << "[i] number of inputs: " << num_inputs << std::endl;
  }

  /* Write circuit */
  std::ofstream os( filename.c_str(), std::ofstream::out );

  std::vector<std::string> vinputs;
  for ( const auto& inp :used_inputs )
   {
	std::string width = std::get<0>(inp);
        vinputs += boost::str( boost::format( "input [%s:0] x%d" ) % width % std::get<1>( inp ) );
   }
  auto inputs = boost::join( vinputs, ", " );

 // boost::sort( used_inputs );
 // auto inputs = any_join( used_inputs | transformed( make_param( "input", "x" ) ), ", " );
  std::string outputs;
  std::vector<std::string> voutputs;
  if ( dynamic_width )
  {
    for ( const auto& op :operations )
    {
      std::string width = boost::str( boost::format( "{{ (width//%d - 1)| abs }}" ) % std::get<3>( op ) );
      if ( std::get<0>( op ) == "+" )
      {
        width = boost::str( boost::format( "{{ width//%d }}" ) % std::get<3>( op ) );
      }
      else if ( std::get<0>( op ) == "*" )
      {
        width = boost::str( boost::format( "{{ (2 * width//%d - 1)|abs }}" ) % std::get<3>( op ) );
      }
      voutputs += boost::str( boost::format( "output [%s:0] y%d" ) % width % std::get<1>( op ) );
    }
    outputs = boost::join( voutputs, ", " );
  }
  else
  {
    for ( const auto& op :operations )
    {
    std::string width = boost::str( boost::format( "{{ (width//%d - 1)| abs }}" ) % std::get<3>( op ) );
    voutputs += boost::str( boost::format( "output [%s:0] y%d" ) % width % std::get<1>( op ) );
    outputs = boost::join( voutputs, ", " );
    }
   // outputs = any_join( boost::irange( 0u, num_gates ) | transformed( make_param( "output", "y" ) ), ", " );
  }

  os << boost::format( "// the file is generated  by a seed: %d \n \n" ) % seed << std::endl;
  os << boost::format( "module %s(%s, %s);" ) % modulename % inputs % outputs << std::endl;

  std::vector<int> has_left_shift;
  std::vector<int> has_right_shift;
  std::vector<int> has_look_up_table;
  for ( const auto& op : operations )
  {
    if ( std::get<0>( op ) == "<<" )
    {
      has_left_shift += std::get<3>( op );
      os << boost::format( "  shift_left{{ width//%d }} sl%d( x%d[%s,%s], x%d[%s,%s], y%d ); " )
      % std::get<3>( op ) 
      % std::get<1>( op ) 
      % std::get<0>((std::get<2>( op ))[0]) % std::get<1>((std::get<2>( op ))[0]) % std::get<2>((std::get<2>( op ))[0])
      % std::get<0>((std::get<2>( op ))[1]) % std::get<1>((std::get<2>( op ))[1]) % std::get<2>((std::get<2>( op ))[1]) 
      % std::get<1>( op ) << std::endl;
    }
    else if ( std::get<0>( op ) == ">>" )
    {
      has_right_shift += std::get<3>( op );
      os << boost::format( "  shift_right{{ width//%d }} sr%d( x%d[%s,%s], x%d[%s,%s], y%d ); " )
      % std::get<3>( op )  
      % std::get<1>( op ) 
      % std::get<0>((std::get<2>( op ))[0]) % std::get<1>((std::get<2>( op ))[0]) % std::get<2>((std::get<2>( op ))[0])
      % std::get<0>((std::get<2>( op ))[1]) % std::get<1>((std::get<2>( op ))[1]) % std::get<2>((std::get<2>( op ))[1]) 
      % std::get<1>( op ) << std::endl;
    }
    else if (std::get<0>( op ) == "MUX" )
    {
      os << boost::format( "  assign y%d = x%d[%s,%s] ? x%d[%s,%s] : x%d[%s,%s] ; " )  
        % std::get<1>( op ) 
        % std::get<0>((std::get<2>( op ))[2]) % std::get<1>((std::get<2>( op ))[2]) % std::get<2>((std::get<2>( op ))[2]) 
        % std::get<0>((std::get<2>( op ))[0]) % std::get<1>((std::get<2>( op ))[0]) % std::get<2>((std::get<2>( op ))[0])
        % std::get<0>((std::get<2>( op ))[1]) % std::get<1>((std::get<2>( op ))[1]) % std::get<2>((std::get<2>( op ))[1])  
        << std::endl;
     }
    else if (std::get<0>( op ) == "LUT" )
    {
      has_look_up_table+= std::get<3>( op );
      os << boost::format( "  look_up_table%d LUT%d( x%d[%s , %s], y%d ); " )  
         % (has_look_up_table.size()-1) % std::get<1>( op ) 
         % std::get<0>((std::get<2>( op ))[0]) % std::get<1>((std::get<2>( op ))[0]) % std::get<2>((std::get<2>( op ))[0])
         % std::get<1>( op ) << std::endl;
    }
    else
    {
      os << boost::format( "  assign y%d = x%d[%s,%s] %s x%d[%s,%s];" )
        % std::get<1>( op )
        % std::get<0>((std::get<2>( op ))[0]) % std::get<1>((std::get<2>( op ))[0]) % std::get<2>((std::get<2>( op ))[0])
        % std::get<0>( op )
        % std::get<0>((std::get<2>( op ))[1]) % std::get<1>((std::get<2>( op ))[1]) % std::get<2>((std::get<2>( op ))[1])  
        << std::endl;
    }
  }

  os << "endmodule" << std::endl << std::endl;

  for ( auto &lsh : has_left_shift )
  {
    os << boost::format("module shift_left{{ width//%d }}( input [{{ width//%d - 1 }}:0] a, input [{{ width//%d - 1 }}:0] b, output [{{ width//%d - 1 }}:0] c);") % lsh % lsh % lsh % lsh << std::endl
       << "   assign c = ( b == 0 ) ? a :" << std::endl
       << boost::format("{%%- for i in range( 1, width//%d ) %%}") % lsh << std::endl
       << boost::format("              ( b == {{ i }} ) ? {a[{{ width//%d - i - 1 }}:0], {{ i }}'b0} :") % lsh << std::endl
       << "{%- endfor %}" << std::endl
       << boost::format("              {{ width//%d }}'b0;") % lsh << std::endl
       << "endmodule" << std::endl << std::endl;
  }

  for ( auto &rsh : has_right_shift )
  {
    os << boost::format("module shift_right{{ width//%d }}( input [{{ width//%d - 1 }}:0] a, input [{{ width//%d - 1 }}:0] b, output [{{ width//%d - 1 }}:0] c);") % rsh % rsh % rsh % rsh<< std::endl
       << "   assign c = ( b == 0 ) ? a :" << std::endl
       << boost::format("{%%- for i in range( 1, width//%d ) %%}") % rsh << std::endl
       << boost::format("              ( b == {{ i }} ) ? { {{ i }}'b0, a[{{ width//%d - 1 }}:{{ i }}]} :") % rsh << std::endl
       << "{%- endfor %}" << std::endl
       << boost::format("              {{ width//%d }}'b0;") % rsh << std::endl
       << "endmodule" << std::endl;
  }
 int n=0;
 for (auto &lut : has_look_up_table)
  {
   os << boost::format("module look_up_table%d ( input [{{ ([width//%d - 1, 15] |sort ) |first }}:0] a, output [{{ width//%d - 1 }}:0] c);") % n % lut % lut << std::endl
      << "		case (a)" << std::endl
      << boost::format("{%%- for i in range( 0, 2**(([width//%d, 16] |sort ) |first)  ) %%}") % lut <<std::endl
      << boost::format("              {{ ([width//%d, 16] |sort ) |first}}'d{{ i }} : c =  {{ width//%d }}'b{%%- for j in range( 0, width//%d  ) %%}{{[0,1]|random}}{%%- endfor %%};") % lut % lut % lut <<std::endl
      << "{%- endfor %}" << std::endl
      << boost::format("              default : c = {{ width//%d }}'b{%%- for j in range( 0, width//%d  ) %%}{{0}}{%%- endfor %%};") % lut % lut <<std::endl
      << "endcase" << std::endl
      << "endmodule" << std::endl << std::endl;
   n++;
  }

  os.close();

  return 0;
}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
