/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @author Stefan Frehse
 */

#include <core/utils/program_options.hpp>
#include <classical/dd/bdd.hpp>
#include <classical/dd/count_solutions.hpp>
#include <classical/io/read_pla_to_cirkit_bdd.hpp>

#include <boost/filesystem/path.hpp>
#include <boost/format.hpp>

using namespace cirkit;

int main(int argc, char *argv[])
{
  using boost::program_options::value;

  std::string filename;

  program_options opts;
  opts.add_options()
    ( "filename",  value( &filename ), "Input PLA" )
    ( "verbose,v",                     "Be verbose" )
    ;
  opts.parse( argc, argv );

  if ( !opts.good() || !opts.is_set( "filename" ) )
  {
    std::cout << opts << std::endl;
    return 1;
  }

  boost::filesystem::path file ( filename );

  std::cout << "[i] reading file: " << file.string() << std::endl;
  auto settings = std::make_shared<properties>();
  settings->set( "verbose", opts.is_set( "verbose" ) );
  auto function = read_pla_into_cirkit_bdd( file, settings );

  if ( !function ) {
    std::cerr << "Unable to read pla file." << std::endl;
    return EXIT_FAILURE;
  }

  function->manager()->dump_stats ( std::cout );


  auto sol_printer = [ &function ]
  ( unsigned index, std::string const& name )
  {
    auto bdd = function->lookupOutput ( index );
    std::cout << boost::format ("%10s has %10d solutions.\n")
                 % name
                 % count_solutions( bdd );
  };

  function->for_each_output ( sol_printer );


  return EXIT_SUCCESS;
}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
