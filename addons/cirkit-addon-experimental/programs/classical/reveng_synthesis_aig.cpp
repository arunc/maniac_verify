/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @author Heinz Riener
 */

#include <core/utils/program_options.hpp>
#include <classical/aig.hpp>
#include <classical/io/read_aiger.hpp>
#include <classical/io/write_aiger.hpp>
#include <classical/aig_word.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/format.hpp>
#include <regex>

namespace cirkit {

inline bool split_name( std::string& na, unsigned& idx, const std::string& name )
{
  std::regex re( "([a-zA-Z0-9_]+)\\[([0-9]+)\\]" );
  std::smatch match;
  if ( std::regex_match(name, match, re) )
  {
    na = match[1].str();
    unsigned n;
    sscanf( match[2].str().c_str(), "%u", &n);
    idx = n;
    return true;
  }
  return false;
}

inline unsigned lit2var( const unsigned lit )
{
  return (lit - lit % 2) / 2;
}

void generate_input_words( std::vector< aig_word >& words, aig_graph& aig )
{
  auto& graph_info = boost::get_property( aig, boost::graph_name );
  const auto& indexmap = get( boost::vertex_name, aig );
  const auto& complementmap = boost::get( boost::edge_complement, aig );

  std::map< std::string, aig_word > map;
  for ( auto& input : graph_info.inputs )
  {
    const std::string s = graph_info.node_names[ input ];

    std::string na;
    unsigned idx;
    if ( split_name( na, idx, s ) )
    {
      auto it = map.find( na );
      if ( it == map.end() )
      {
        aig_word w;
        w.resize(idx+1);
        w[idx] = { input,false };
        map.insert( { na,w } );
      }
      else
      {
        aig_word &w = it->second;
        if (w.size() <= idx)
          w.resize(idx+1);
        w[idx] = { input,false };
      }
    }
  }

  // for ( auto it : map )
  // {
  //   std::cout << it.first << ' ';
  //   for ( auto lit : it.second )
  //   {
  //     std::cout << lit.first << ' ';
  //   }
  //   std::cout << std::endl;
  // }

  for ( auto it : map )
  {
    words.push_back( it.second );
  }
}

void apply_template( aig_graph& aig, const std::vector< aig_node >& outputs, const aig_word& templ )
{
  assert( outputs.size() == templ.size() );

  const auto& graph_info = boost::get_property( aig, boost::graph_name );
  const auto& indexmap = get( boost::vertex_name, aig );
  const auto& complementmap = boost::get( boost::edge_complement, aig );

  unsigned index = 0u;
  for ( const auto& output : outputs )
  {
    const auto node = lit2var(output);
    const auto _out_degree = out_degree( node, aig );

    // std::cout << output << std::endl;

    if ( _out_degree )
    {
      assert( out_degree( node, aig ) == 2u );

      const auto& edges = boost::out_edges( node,aig );
      assert( std::distance(edges.first,edges.second) == 2 );
      remove_edge(edges.first, aig);
      remove_edge(edges.second, aig);

      assert( out_degree( node, aig ) == 0u );

      aig_edge le = add_edge( node, templ[index].node, aig ).first;
      boost::get( boost::edge_complement, aig )[le] = templ[index].complemented;
      aig_edge re = add_edge( node, templ[index].node, aig ).first;
      boost::get( boost::edge_complement, aig )[re] = templ[index].complemented;

      // for ( const auto& edge : boost::make_iterator_range( boost::out_edges( node, aig ) ) )
      // {
      //   std::cout << aig_to_literal( aig, {target(edge,aig),complementmap[edge]} ) << ' ';
      // }
      // std::cout << std::endl;
    }

    ++index;
  }
}

void check_templates( const std::vector< aig_node >& out, const std::vector<aig_word>& in , aig_graph& aig )
{
  const unsigned size = out.size();
  unsigned index = 0u;

  /* constant templates */
  {
    aig_graph aig_copy( aig );
    const aig_function &&zero = aig_get_constant( aig_copy, false );
    aig_word zero_word(size,zero);
    std::cout << index++ << " " << zero_word << "   ";
    std::cout.flush();
    // templates.push_back( zero_word );
    apply_template( aig_copy, out, zero_word );
    const std::string&& filename = (boost::format("templ_%u.aag") % index).str();
    write_aiger( aig_copy, filename );
    const std::string&& cmd = (boost::format("blimc %s &> /dev/null; echo $?") % filename).str();
    auto sresult = system( cmd.c_str() );
  }

  {
    aig_graph aig_copy( aig );
    const aig_function &&one = aig_get_constant( aig_copy, true );
    aig_word one_word(size,one);
    std::cout << index++ << " " << one_word << "   ";
    std::cout.flush();
    // templates.push_back( one_word );
    apply_template( aig_copy, out, one_word );
    const std::string&& filename = (boost::format("templ_%u.aag") % index).str();
    write_aiger( aig_copy, filename );
    const std::string&& cmd = (boost::format("blimc %s &> /dev/null; echo $?") % filename).str();
    auto sresult = system( cmd.c_str() );
  }

  {
    for ( const auto& input_word : in )
    {
      aig_graph aig_copy( aig );
      const aig_function &&one = aig_get_constant( aig_copy, true );
      aig_word one_word(size,one);
      std::cout << index++ << " " << input_word << "   ";
      std::cout.flush();
      // templates.push_back( one_word );
      apply_template( aig_copy, out, input_word );
      const std::string&& filename = (boost::format("templ_%u.aag") % index).str();
      write_aiger( aig_copy, filename );
      const std::string&& cmd = (boost::format("blimc %s &> /dev/null; echo $?") % filename).str();
      auto sresult = system( cmd.c_str() );
    }
  }


  /* equality templates */
  {
    aig_graph aig_copy( aig );
    const aig_function &&zero = aig_get_constant( aig_copy, false );
    for ( unsigned i = 0u; i < in.size(); ++i )
    {
      for ( unsigned j = i+1u; j < in.size(); ++j )
      {
        std::cout << index++ << " " << in[i] << " == " << in[j] << "   ";
        std::cout.flush();
        aig_word w(size, zero);
        w[size-1] = aig_create_equal( aig_copy, in[i], in[j] );
        // templates.push_back( w );
        apply_template( aig_copy, out, w );
        const std::string&& filename = (boost::format("templ_%u.aag") % index).str();
        write_aiger( aig_copy, filename );
        const std::string&& cmd = (boost::format("blimc %s &> /dev/null; echo $?") % filename).str();
        auto sresult = system( cmd.c_str() );
      }
    }
  }

  /* ult templates */
  {
    aig_graph aig_copy( aig );
    const aig_function &&zero = aig_get_constant( aig_copy, false );
    for ( unsigned i = 0u; i < in.size(); ++i )
    {
      for ( unsigned j = i+1u; j < in.size(); ++j )
      {
        std::cout << index++ << " " << in[i] << " <u " << in[j] << "   ";
        std::cout.flush();
        aig_word w(size, zero);
        w[size-1] = aig_create_bvult( aig_copy, in[i], in[j] );
        // templates.push_back( w );
        apply_template( aig_copy, out, w );
        const std::string&& filename = (boost::format("templ_%u.aag") % index).str();
        write_aiger( aig_copy, filename );
        const std::string&& cmd = (boost::format("blimc %s &> /dev/null; echo $?") % filename).str();
        auto sresult = system( cmd.c_str() );
      }
    }
  }

  /* ult templates */
  {
    aig_graph aig_copy( aig );
    const aig_function &&zero = aig_get_constant( aig_copy, false );
    for ( unsigned i = 0u; i < in.size(); ++i )
    {
      for ( unsigned j = i+1u; j < in.size(); ++j )
      {
        std::cout << index++ << " " << in[i] << " <u " << in[j] << "   ";
        std::cout.flush();
        aig_word w(size, zero);
        w[size-1] = aig_create_bvult( aig_copy, in[i], in[j] );
        // templates.push_back( w );
        apply_template( aig_copy, out, w );
        const std::string&& filename = (boost::format("templ_%u.aag") % index).str();
        write_aiger( aig_copy, filename );
        const std::string&& cmd = (boost::format("blimc %s &> /dev/null; echo $?") % filename).str();
        auto sresult = system( cmd.c_str() );
      }
    }
  }

  /* ule templates */
  {
    aig_graph aig_copy( aig );
    const aig_function &&zero = aig_get_constant( aig_copy, false );
    for ( unsigned i = 0u; i < in.size(); ++i )
    {
      for ( unsigned j = i+1u; j < in.size(); ++j )
      {
        std::cout << index++ << " " << in[i] << " <=u " << in[j] << "   ";
        std::cout.flush();
        aig_word w(size, zero);
        w[size-1] = aig_create_bvule( aig_copy, in[i], in[j] );
        // templates.push_back( w );
        apply_template( aig_copy, out, w );
        const std::string&& filename = (boost::format("templ_%u.aag") % index).str();
        write_aiger( aig_copy, filename );
        const std::string&& cmd = (boost::format("blimc %s &> /dev/null; echo $?") % filename).str();
        auto sresult = system( cmd.c_str() );
      }
    }
  }

  /* ugt templates */
  {
    aig_graph aig_copy( aig );
    const aig_function &&zero = aig_get_constant( aig_copy, false );
    for ( unsigned i = 0u; i < in.size(); ++i )
    {
      for ( unsigned j = i+1u; j < in.size(); ++j )
      {
        std::cout << index++ << " " << in[i] << " >u " << in[j] << "   ";
        std::cout.flush();
        aig_word w(size, zero);
        w[size-1] = aig_create_bvugt( aig_copy, in[i], in[j] );
        // templates.push_back( w );
        apply_template( aig_copy, out, w );
        const std::string&& filename = (boost::format("templ_%u.aag") % index).str();
        write_aiger( aig_copy, filename );
        const std::string&& cmd = (boost::format("blimc %s &> /dev/null; echo $?") % filename).str();
        auto sresult = system( cmd.c_str() );
      }
    }
  }

  /* uge templates */
  {
    aig_graph aig_copy( aig );
    const aig_function &&zero = aig_get_constant( aig_copy, false );
    for ( unsigned i = 0u; i < in.size(); ++i )
    {
      for ( unsigned j = i+1u; j < in.size(); ++j )
      {
        std::cout << index++ << " " << in[i] << " >=u " << in[j] << "   ";
        std::cout.flush();
        aig_word w(size, zero);
        w[size-1] = aig_create_bvuge( aig_copy, in[i], in[j] );
        // templates.push_back( w );
        apply_template( aig_copy, out, w );
        const std::string&& filename = (boost::format("templ_%u.aag") % index).str();
        write_aiger( aig_copy, filename );
        const std::string&& cmd = (boost::format("blimc %s &> /dev/null; echo $?") % filename).str();
        auto sresult = system( cmd.c_str() );
      }
    }
  }

  /* bvand templates */
  {
    aig_graph aig_copy( aig );
    const aig_function &&zero = aig_get_constant( aig_copy, false );
    for ( unsigned i = 0u; i < in.size(); ++i )
    {
      for ( unsigned j = i+1u; j < in.size(); ++j )
      {
        std::cout << index++ << " " << in[i] << " bvand " << in[j] << "   ";
        std::cout.flush();
        aig_word w = aig_create_bvand( aig_copy, in[i], in[j] );
        // templates.push_back( w );
        apply_template( aig_copy, out, w );
        const std::string&& filename = (boost::format("templ_%u.aag") % index).str();
        write_aiger( aig_copy, filename );
        const std::string&& cmd = (boost::format("blimc %s &> /dev/null; echo $?") % filename).str();
        auto sresult = system( cmd.c_str() );
      }
    }
  }

  /* bvor templates */
  {
    aig_graph aig_copy( aig );
    const aig_function &&zero = aig_get_constant( aig_copy, false );
    for ( unsigned i = 0u; i < in.size(); ++i )
    {
      for ( unsigned j = i+1u; j < in.size(); ++j )
      {
        std::cout << index++ << " " << in[i] << " bvor " << in[j] << "   ";
        std::cout.flush();
        aig_word w = aig_create_bvor( aig_copy, in[i], in[j] );
        // templates.push_back( w );
        apply_template( aig_copy, out, w );
        const std::string&& filename = (boost::format("templ_%u.aag") % index).str();
        write_aiger( aig_copy, filename );
        const std::string&& cmd = (boost::format("blimc %s &> /dev/null; echo $?") % filename).str();
        auto sresult = system( cmd.c_str() );
      }
    }
  }

  /* bvxor templates */
  {
    aig_graph aig_copy( aig );
    const aig_function &&zero = aig_get_constant( aig_copy, false );
    for ( unsigned i = 0u; i < in.size(); ++i )
    {
      for ( unsigned j = i+1u; j < in.size(); ++j )
      {
        std::cout << index++ << " " << in[i] << " bvxor " << in[j] << "   ";
        std::cout.flush();
        aig_word w = aig_create_bvxor( aig_copy, in[i], in[j] );
        // templates.push_back( w );
        apply_template( aig_copy, out, w );
        const std::string&& filename = (boost::format("templ_%u.aag") % index).str();
        write_aiger( aig_copy, filename );
        const std::string&& cmd = (boost::format("blimc %s &> /dev/null; echo $?") % filename).str();
        auto sresult = system( cmd.c_str() );
      }
    }
  }

}

}

using namespace cirkit;

int main( int argc, char ** argv )
{
  using boost::program_options::value;

  std::string filename;
  std::string nodes;
  auto method = 0u;

  program_options opts;
  opts.add_options()
    ( "filename", value( &filename ), "AIG filename (in ASCII AIGER format)" )
    ( "nodes",    value( &nodes ),    "Nodes to replace \"2 6 10 14\"" )
    ;
  opts.parse( argc, argv );

  if ( !opts.good() || !opts.is_set( "filename" ) || !opts.is_set( "nodes" ) )
  {
    std::cout << opts << std::endl;
    return 1;
  }

  std::vector< std::string > a;
  std::vector< aig_node > ids;
  boost::split( a, nodes, boost::is_any_of( " " ), boost::token_compress_on );
  for ( const auto& n : a )
  {
    unsigned id;
    int ok = sscanf( n.c_str(), "%u", &id );
    assert( ok > 0 );
    ids.push_back( id );
  }

  aig_graph aig;
  try
  {
    read_aiger( aig, filename );
  }
  catch (const char *msg)
  {
    std::cerr << msg << std::endl;
    return 2;
  }

  try
  {
    std::cout << "Generating inputs." << std::endl;
    std::vector< aig_word > in;
    generate_input_words( in, aig );
    for ( auto& input_word : in )
    {
      std::cout << input_word << std::endl;
    }
    std::cout.flush();

    std::cout << "Check templates." << std::endl;
    check_templates( ids, in, aig );
  }
  catch (const char *msg)
  {
    std::cerr << msg << std::endl;
    return 2;
  }

  return 0;
}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
