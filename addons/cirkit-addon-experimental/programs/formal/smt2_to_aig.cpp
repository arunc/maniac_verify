/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @author Heinz Riener
 */

#if ADDON_FORMAL

#include <classical/aig.hpp>
#include <classical/aig_word.hpp>
#include <classical/io/write_aiger.hpp>
#include <formal/utils/z3_expr_declaration_collector.hpp>
#include <formal/utils/z3_expr_to_aig.hpp>
#include <formal/utils/z3_utils.hpp>
#include <core/utils/program_options.hpp>

#include <z3++.h>

#include <boost/assign/std/vector.hpp>

#include <stack>

namespace cirkit
{

void smt2_to_aig( const std::string& filename, const bool simplify = false )
{
  z3::context ctx;

  const Z3_ast ast = Z3_parse_smtlib2_file(ctx, filename.c_str(), 0, 0, 0, 0, 0, 0);
  check_error( ctx );

  z3::expr formula( ctx, ast );
  if ( simplify )
  {
    formula = formula.simplify();
  }

  aig_graph aig;
  aig_initialize( aig );

  z3_expr_to_aig conv( aig );
  conv.visit( formula );

  const aig_function& out = conv.get_aig_function( formula );
  aig_create_po( aig, out, "output0" );

  write_aiger( aig, std::cout );
}

}

using namespace cirkit;

int main( int argc, char ** argv )
{
  using boost::program_options::value;

  std::string filename;

  program_options opts;
  opts.add_options()
    ( "filename",     value( &filename ), "SMT-LIB2 file" )
    ( "simplify,s",                       "Simplify instance" )
    ( "no-rewrite,n",                     "Disable instance rewriting" )
    ( "allow-z3,z",                       "Allow non-standard Z3 commands" )
    ;
  opts.parse( argc, argv );

  if ( !opts.good() || !opts.is_set( "filename" ) )
  {
    std::cout << opts << std::endl;
    return 1;
  }

  if ( opts.is_set( "no-rewrite" ) )
  {
    z3::set_param("pp.min_alias_size", 1000000);
    z3::set_param("pp.max_depth", 1000000);
  }

  smt2_to_aig( filename, opts.is_set( "simplify" ) );

  return 0;
}

#else

int main( int argc, char ** argv ) {}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
