/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @author Heinz Riener
 */

#if ADDON_FORMAL

#include <core/utils/program_options.hpp>
#include <z3++.h>

namespace cirkit {

enum class solving_method
{
  check_unsat = 0
, check_sat = 1
, check_validity = 2
};

bool solve_smt2( const std::string& filename, solving_method method, const bool verbose = false )
{
  z3::context ctx;
  const Z3_ast ast = Z3_parse_smtlib2_file(ctx, filename.c_str(), 0, 0, 0, 0, 0, 0);
  const z3::expr e = to_expr( ctx, ast );

  z3::solver solver( ctx );
  if ( method == solving_method::check_unsat )
  {
    if ( verbose )
      std::cout << "[i] Checking unsatisfiability\n";
    solver.add( e );
    return ( solver.check() == z3::unsat );
  }
  else if ( method == solving_method::check_sat )
  {
    if ( verbose )
      std::cout << "[i] Checking satisfiability\n";
    solver.add( e );
    return ( solver.check() == z3::sat );
  }
  else if ( method == solving_method::check_validity )
  {
    if ( verbose )
      std::cout << "[i] Checking validity\n";
    solver.add( !e );
    return ( solver.check() == z3::unsat );
  }
  else
  {
    throw "Unknown solving method\n";
  }

  return false;
}

} /* cirkit */

using namespace cirkit;

int main( int argc, char ** argv )
{
  using boost::program_options::value;

  std::string filename;
  std::string method;

  program_options opts;
  opts.add_options()
    ( "filename",  value( &filename ), "SMT-LIB2 file" )
    ( "verbose,v",                     "Verbose")
    ( "method",    value( &method ),   "Solving method:\nsat Check satisfiability\nunsat Check contradiction\nvalid Check validitiy" )
    ;
  opts.parse( argc, argv );

  if ( !opts.good() || !opts.is_set( "filename" ) )
  {
    std::cout << opts << std::endl;
    return 1;
  }

  solving_method m;
  if ( !opts.is_set( "filename" ) || method == "sat" )
  {
    m = solving_method::check_sat;
  }
  else if ( method == "unsat" )
  {
    m = solving_method::check_unsat;
  }
  else if ( method == "valid" )
  {
    m = solving_method::check_validity;
  }
  else
  {
    std::cerr << "[w] Unknown solving method using sat instead\n";
    m = solving_method::check_sat;
  }

  const bool solved = solve_smt2( filename, m, opts.is_set( "verbose" ) );
  if ( solved )
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

#else

int main( int argc, char ** argv ) {}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
