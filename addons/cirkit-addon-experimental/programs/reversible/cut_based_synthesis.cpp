/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @author Mathias Soeken
 */

#include <iostream>

#include <boost/format.hpp>

#include <classical/aig.hpp>
#include <classical/io/read_aiger.hpp>
#include <reversible/io/print_circuit.hpp>
#include <reversible/io/print_statistics.hpp>
#include <reversible/io/write_realization.hpp>
#include <reversible/synthesis/cut_based_synthesis.hpp>
#include <reversible/utils/reversible_program_options.hpp>

using namespace cirkit;

int main( int argc, char ** argv )
{
  using boost::program_options::value;

  std::string filename;

  reversible_program_options opts;
  opts.add_write_realization_option();
  opts.add_options()
    ( "filename",           value( &filename ),                        "AAG filename" )
    ( "print_circuit,c",                                               "Prints the circuit" )
    ( "verbose,v",                                                     "Be verbose" )
    ;

  opts.parse( argc, argv );

  if ( !opts.good() || !opts.is_set( "filename" ) )
  {
    std::cout << opts << std::endl;
    return 1;
  }

  circuit circ;
  properties::ptr settings( new properties );
  properties::ptr statistics( new properties );
  settings->set( "verbose", opts.is_set( "verbose" ) );

  aig_graph aig;
  read_aiger( aig, filename );
  cut_based_synthesis( circ, aig, settings, statistics );

  if ( opts.is_set( "print_circuit" ) )
  {
    std::cout << circ << std::endl;
  }

  if ( opts.is_write_realization_filename_set() )
  {
    write_realization( circ, opts.write_realization_filename() );
  }

  print_statistics( circ, statistics->get<double>( "runtime" ) );

  std::cout << "[i] # generated:  " << statistics->get<unsigned>( "num_generated_circuits" ) << std::endl
            << "[i] # balanced:   " << statistics->get<unsigned>( "num_balanced_circuits" ) << std::endl
            << boost::format( "[i] exact synth.: %.2f secs" ) % statistics->get<double>( "total_synthesis_runtime" ) << std::endl;

  return 0;
}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
