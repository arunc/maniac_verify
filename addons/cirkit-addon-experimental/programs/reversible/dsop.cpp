/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @author Mathias Soeken
 */

#include <iostream>

#if ADDON_REVERSIBLE

#include <cstdlib>

#include <boost/format.hpp>

#include <core/properties.hpp>
#include <core/io/read_pla_to_bdd.hpp>
#include <core/utils/program_options.hpp>

#include <classical/optimization/compact_dsop.hpp>

#include <reversible/truth_table.hpp>
#include <reversible/functions/extend_pla.hpp>
#include <reversible/io/read_pla.hpp>
#include <reversible/io/write_pla.hpp>

using namespace cirkit;

int main( int argc, char ** argv )
{
  using boost::program_options::value;

  std::string filename;
  std::string planame;
  unsigned    method = 0u;
  unsigned    optfunc = 1u;
  unsigned    sortfunc = 0u;

  program_options opts;
  opts.add_options()
    ( "filename",  value( &filename ),              "PLA filename" )
    ( "planame",   value( &planame ),               "Output filename" )
    ( "method",    value_with_default( &method ),   "Method:\n0: Compact DSOP\n1: Espresso\n2: Extend PLA" )
    ( "optfunc",   value_with_default( &optfunc ),  "Optimization function: 1-5" )
    ( "sortfunc",  value_with_default( &sortfunc ), "Sort function:\n0: dimension first\n1: weight first" )
    ( "verbose,v",                                  "Be verbose" )
    ;
  opts.parse( argc, argv );

  if ( !opts.good() || !opts.is_set( "filename" ) )
  {
    std::cout << opts << std::endl;
    return 1;
  }

  switch ( method )
  {
  case 0u:
    {
      properties::ptr settings( new properties );
      settings->set( "verbose", opts.is_set( "verbose" ) );
      settings->set( "sortfunc", std::vector<sort_cube_meta_func_t>( {sort_by_dimension_first, sort_by_weight_first} )[sortfunc] );
      settings->set( "optfunc", std::vector<opt_cube_func_t>( {opt_dsop_1,opt_dsop_2,opt_dsop_3,opt_dsop_4,opt_dsop_5} )[optfunc - 1u] );
      properties::ptr statistics( new properties );
      compact_dsop( planame, filename, settings, statistics );

      std::cout << boost::format( "[i] Run-time: %.2f" ) % statistics->get<double>( "runtime" ) << std::endl;
    } break;

  case 1u:
    {
      auto sresult = system( boost::str( boost::format( "espresso -Ddisjoint %s > %s" ) % filename % planame ).c_str() );
    } break;

  case 2u:
    {
      binary_truth_table pla, extended;
      read_pla_settings settings;
      settings.extend = false;
      read_pla( pla, filename, settings );
      extend_pla( pla, extended );
      write_pla( extended, planame );
    } break;
  };

  return 0;
}

#else

int main( int argc, char ** argv )
{
  std::cout << "[e] program requires reversible addon enabled" << std::endl;
  return 1;
}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
