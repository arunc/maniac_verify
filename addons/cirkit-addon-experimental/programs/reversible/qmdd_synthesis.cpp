/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @author D. Michael Miller
 * @author Mathias Soeken
 */

#include <iostream>

#if ADDON_REVERSIBLE

#include <fstream>
#include <list>
#include <set>
#include <string>
#include <vector>

#include <boost/assign/std/list.hpp>
#include <boost/assign/std/set.hpp>
#include <boost/assign/std/vector.hpp>
#include <boost/dynamic_bitset.hpp>
#include <boost/format.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/optional.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/algorithm_ext/push_back.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/range/numeric.hpp>
#include <boost/variant.hpp>

#include <cudd.h>

#include <reversible/circuit.hpp>
#include <reversible/functions/add_gates.hpp>
#include <reversible/io/print_circuit.hpp>
#include <reversible/io/print_statistics.hpp>
#include <reversible/io/write_realization.hpp>
#include <core/utils/timer.hpp>

#define __LINUX__

#define VERBOSE 0
#define WRITESTEPS 1

#define GRAYCODE_TRIVIA_CHECK 0
#define BDD_SIFTING 0

// QMDDinclude.h

// includes the files needed to use the QMDD package

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <time.h>
#include<string.h>

/*************************************************
  These routines can be used for approximate CPU
  timing on a WIN system. DMM Sept 2002
************************************************/

long
cpuTime()
/* returns elapsed user CPU time */
{
  return(clock());
}

void
printCPUtime(clock_t x)
/* display a time value in sec. */
{
  printf("%7.2f",(float)x/CLOCKS_PER_SEC);
}

void 
dateToday(char str[])
{
  time_t tt;
  struct tm *tod;
  time(&tt);
  tod=localtime(&tt);
  sprintf(str,"%2.2d/%2.2d/%d",tod->tm_mday,tod->tm_mon +1,tod->tm_year+1900);
}

void 
wallTime(char str[])
{
    time_t tt;
  struct tm *tod;
  time(&tt);
  tod=localtime(&tt);
  sprintf(str,"%2.2d:%2.2d:%2.2d",tod->tm_hour,tod->tm_min,tod->tm_sec);

}

// text file input output routines 

FILE *openTextFile(char *fname,char mode)
{
  FILE *file;
  
    if(mode=='r') file = fopen(fname, "rt"); // open the file for reading
    else file = fopen(fname, "w"); // open the file for writing
    return(file);
}

char getch(FILE *infile)
  {
// fetch one character
  char ch;
  fscanf(infile,"%c",&ch);
  if(ch==13){
    fscanf(infile,"%c",&ch);
  }
  if(ch==11) ch='\n';
  if(ch>='a'&&ch<='z') ch=ch-'a'+'A'; // convert lowercade letters to uppercase
  return(ch);
}

char getnbch(FILE *infile)
{
  char ch;
  
  while(' '==(ch=getch(infile)));
  return(ch);
}

char getstr(FILE *infile,char x[])
{
  char ch;
  int i;
  do
  {
    ch=getch(infile);
  } while(ch==','||ch==' '||ch=='\n');
  i=0;
  while(ch!=','&&ch!=' '&&ch!='\n')
  {
    x[i]=ch;
    i++;
    ch=getch(infile);
  }
  x[i]=0;
  return(ch);
}

int getint(FILE *infile)
{
  char ch;
  int i;
  while(' '==(ch=getch(infile)));
  i=0;
  while(ch!=','&&ch!=' '&&ch!='\n')
  {
    i=i*10+ch-'0';
    ch=getch(infile);
  }
  return(i);
}

void skip2eof(FILE *infile)
{
// skips until it reaches end of file
  char ch;
  while(EOF!=fscanf(infile,"%c",&ch));
}

void skip2eol(FILE *infile)
{
// skips until it reads a '\n'
  char ch;
  do{
    ch=getch(infile);
    if(feof(infile)) return;
  } while(ch!='\n');
}

/**************************************************************************************

    package definitions and types
    
    Michael Miller
    University of Victoria
    mmiller@cs.uvic.ca
    
    Date: July 2008
    
**************************************************************************************/


#define QMDDversion "QMDD Package V3.1 October 2008\n"

// problem parameter limits 


#include <stdint.h>

#define MAXSTRLEN 11
#define MAXN 250		// max no. of inputs     
#define MAXRADIX 2    		// max logic radix                                		   
#define MAXNEDGE 4 		// max no. of edges = MAXRADIX^2                     	   
#define MAXNODECOUNT 2000000 	// max number of nodes in a QMDD for counting 			   
#define GCLIMIT1 250000   	// first garbage collection limit                          
#define GCLIMIT_INC 0 		// garbage collection limit increment                      
					// added to garbage collection limit after each collection 
#define MAXND 6    		// max n for display purposes 							   
#define MAXDIM 64           	// max dimension of matrix for printing, (should be 2^MAXND)				   
#define NBUCKET 8192     	// no. of hash table buckets; must be a power of 2 		   
#define HASHMASK 8191   	// must be nbuckets-1                 					   
#define CTSLOTS 16384  		// no. of computed table slots 							   
#define CTMASK  16383  		// must be CTSLOTS-1   
#define COMPLEXTSIZE 128  	// complex table size  
#define COMPLEXTMASK 127   	// complex table index mask   (not used anywhere?!)
#define TTSLOTS 2048		// Toffoli table slots
#define TTMASK 2047		// must be TTSLOTS-1
#define MAXREFCNT 40000		// max reference count (saturates at this value)    
#define MAXPL 65536		// max size for a permutation recording  	

#define DYNREORDERLIMIT 250000	// minimum value for dynamic reordering limit

// edge and node definitions 

typedef struct QMDDnode *QMDDnodeptr; 

typedef struct QMDDedge
{
   QMDDnodeptr p;  		// edge pointer 											 
   unsigned int w;          	// index of weight edge in complex value table 		
   int sentinel;		// in 64-bit architecture sizeof(QMDDedge) is a multiple of 8 (8 Byte = 64 bit).
				// So sizeof() is 16 with AND without this sentinel. thus when using memcmp identical nodes may not be identified. 
}  QMDDedge;

typedef struct QMDDnode
{
   QMDDnodeptr next;  // link for unique table and available space chain 
   unsigned int ref;  // reference count 												 
   unsigned char v;   // variable index (nonterminal) value (-1 for terminal)
   unsigned int renormFactor; // factor that records renormalization factor
   char ident,diag,block,symm,c01;        // flag to mark if vertex heads a QMDD for a special matrix
   char computeSpecialMatricesFlag;	  // flag to mark whether SpecialMatrices are to be computed
   //QMDDedge e[0]; 	  	// edges out of this node - variable so must be last in structure 
   QMDDedge e[MAXNEDGE];	// when calling malloc in QMDDgetnode
}  QMDDnode;


// list definitions for breadth first traversals (e.g. printing)  
typedef struct ListElement *ListElementPtr;

typedef struct ListElement
{
   int w,cnt;
   int line[MAXN];
   QMDDnodeptr p;
   ListElementPtr next;
}  ListElement;

// computed table definitions 

typedef enum{add,mult,kronecker,reduce,transpose,conjugateTranspose,transform,c0,c1,c2,none,norm,createHdmSign,findCmnSign,findBin,reduceHdm, renormalize} CTkind; // compute table entry kinds 

typedef struct CTentry// computed table entry defn 										 
{			
  QMDDedge a,b,r;     // a and b are arguments, r is the result 						 
  CTkind which;       // type of operation 												 
} CTentry;

typedef struct TTentry // Toffoli table entry defn
{
  int n,m,t,line[MAXN];
  QMDDedge e;
} TTentry;

typedef struct CircuitLine
{
  char input[MAXSTRLEN];
  char output[MAXSTRLEN];
  char variable[MAXSTRLEN];
  char ancillary;
  char garbage;

} CircuitLine;

typedef struct QMDDrevlibDescription // circuit description structure
{
  int n,ngates,qcost,nancillary,ngarbage;
  QMDDedge e,totalDC;
  CircuitLine line[MAXN];
  char version[MAXSTRLEN];
  char inperm[MAXN],outperm[MAXN];
  char ngate,cgate,tgate,fgate,pgate,vgate,kind[7],dc[5],name[32],no[8],modified;
} QMDDrevlibDescription;


/***************************************

    Global variables

***************************************/

double Pi;					// Pi is defined using asin function in QMDDinit routine 

int Radix=2;				// radix (default is 2) 

int Nedge=4;				// no. of edges (default is 4) 

QMDDnodeptr Avail;			// pointer to available space chain 

ListElementPtr Lavail;		// pointer to available list elements for breadth first searchess

QMDDnodeptr QMDDtnode;		// pointer to terminal node 

QMDDedge QMDDone,QMDDzero; 	// edges pointing to zero and one QMDD constants 


long QMDDorder[MAXN];		// variable order initially 0,1,... from bottom up | Usage: QMDDorder[level] := varible at a certain level
long QMDDinvorder[MAXN];	// inverse of variable order (inverse permutation) | Usage: QMDDinvorder[variable] := level of a certain variable

long QMDDnodecount;			// counts active nodes 
long QMDDpeaknodecount;                 // records peak node count in unique table

long Ncount;				// used in QMDD node count - very naive approach 
QMDDnodeptr Nlist[MAXNODECOUNT];

long Nop[6];				// operation counters

long CTlook[20],CThit[20];	// counters for gathering compute table hit stats 

long UTcol, UTmatch;			// counter for collisions / matches in hash tables   

int GCcurrentLimit;			// current garbage collection limit 

int GCswitch = 1;           // set switch to 1 to enable garbage collection 

int ActiveNodeCount;		// number of active nodes 

int Active[MAXN];			// number of active nodes for each variable 

int Smode = 1;				// S mode switch for spectral transformation
							// Smode==1 0->+1 1->-1; Smode==0 0->0 1->1

int RMmode =0;				// Select RM transformation mode
							// forces mod Radix arithmetic
							
QMDDedge QMDDnullEdge;		    // set in QMDDinit routine
							
int MultMode = 0;			// set to 1 for matrix - vector multiplication

int PermList[MAXPL];		// array for recording a permutation

// for sifting

int RenormalizationNodeCount = 0;	// number of active nodes that need renormalization (used in QMDDdecref) 
int blockMatrixCounter = 0;	        // number of active nodes that represent block matrices (used in QMDDincref, QMDDdecref)
char globalComputeSpecialMatricesFlag = 1; // default value for computeSpecialMatricesFlag of newly created nodes (used in QMDDmakeNonterminal)
int dynamicReorderingTreshold = DYNREORDERLIMIT;

int largestRefCount = 0;

/*******************************************

	Unique Tables (one per input variable)
	
*******************************************/

QMDDnodeptr Unique[MAXN][NBUCKET];

/****************************************************

    Compute Table (only one for all operation types)
    
****************************************************/

CTentry CTable[CTSLOTS];

/****************************************************

    Toffoli gate table

*****************************************************/

TTentry TTable[TTSLOTS];

/****************************************************

    Identity matrix table
    
****************************************************/

QMDDedge QMDDid[MAXN];

/****************************************************

Variable labels

****************************************************/

int  Nlabel;		// number of labels
char Label[MAXN][MAXSTRLEN];  // label table

/****************************************************

Output file for writing permutation 

****************************************************/

FILE *outfile;

/***************************************************************

Complex number defnitions and routines for QMDD using
doubles for the real and imaginary part of a complex number.

January 28, 2008
Michael Miller
University of Victoria
Victoria, BC 
CANADA V8W 3P6
mmiller@cs.uvic.ca

****************************************************************/

/****************************************************************

The basic idea is that the required complex values are 
stored in a lookup table.

The value 0 is always in slot 0 and the value 1 is always in slot
1 so that for those two values the index corresponds to the value.

Current implementation uses simple linear searching to find a value.

QMDDinit (in QMDDpackage.c which is the initialization routine that
must be called before the other package routines are used) invokes 
QMDDinitCtable()

*****************************************************************/

/***************************************

    Basic type definition

***************************************/

typedef struct 
{
   long double r,i;
} complex;

/***************************************

	Complex Value Lookup Table
	
	The table holds the unique values needed
	in the QQDD.  It stores one value for each
	conjugate pair (the positive iinary).

***************************************/

int Ctentries;					 // number of complex table entries
complex Ctable[COMPLEXTSIZE];    // value
long double Cmag[COMPLEXTSIZE];  // magnitude to avoid repeated computation
long double Cangle[COMPLEXTSIZE];// angle to avoid repeated computation
int CTa[MAXRADIX];				 // complex table positions for roots of unity

/*********************************************

Complex computation tables.  These tables save
result of computations over complex values to
avoid recomputation later.

  Cta - addition
  Cts - subtraction
  Ctm - multiplication
  Ctd - division

*********************************************/

int cta[COMPLEXTSIZE][COMPLEXTSIZE],cts[COMPLEXTSIZE][COMPLEXTSIZE],ctm[COMPLEXTSIZE][COMPLEXTSIZE],ctd[COMPLEXTSIZE][COMPLEXTSIZE];


/**********************************************

Compute trig functions for angle Pi/div
Note use of cosl and sinl for long double computation

**********************************************/

#define QMDDcos(div) cosl(Pi/(long double)(div))
#define QMDDsin(div) sinl(Pi/(long double)(div))

/***********************************************

Tolerance for testing equality of complex values

***********************************************/

long double Ctol;

#define Cvalue(x) Ctable[x]
#define Dzero 0.0

/**************************************

Prototypes

**************************************/

void QMDDpause(void);

/**************************************

    Routines
    
**************************************/

void Cprint(complex c)
// print a complex value
{
  if(c.r==1.0) printf("1"); else if(c.r==0.0) printf("0"); else printf("%f",c.r);
  if(c.i>0.0) printf("+");
  if(c.i!=0.0) printf("%fi ",c.i);
}

#define Ceq(x,y) ((fabs((x.r)-(y.r))<Ctol)&&(fabs((x.i)-(y.i))<Ctol))

long double angle(int a)
// computes angle for polar coordinate representation of Cvalue(a)
{
  complex ca;
  ca=Cvalue(a);
  if(ca.i>=0-Ctol) return(acos(ca.r/Cmag[a]));
  else return(2*Pi-acos(ca.r/Cmag[a]));
}

int Cdiv(int ai,int bi); /* prototype */

int Cgt(int a, int b)
// returns 1 if |a|>|b|
// returns 0 if |b|>|a|
// returns angle(a)<angle(b)
// where angle is the angle in polar coordinate representation
{  
  complex ca,cb;
  if(a==b) return(0);
  ca=Cvalue(a);
  cb=Cvalue(b);
  if(Cmag[a]>(Cmag[b]+Ctol)) return(1);
  if(Cmag[b]>(Cmag[a]+Ctol)) return(0);
  return((Cangle[a]+Ctol)<Cangle[b]);
}

int Cgt_new(int a, int b)
{  
  complex ca,cb;
  if(a==b) return(0);
  ca=Cvalue(a);
  cb=Cvalue(b);
  if((Cangle[a]+Ctol)<Cangle[b]) return(1);
  return(Cmag[a]>(Cmag[b]+Ctol));
}

int Clt(int a, int b)
// analogous to Cgt
{
  complex ca,cb;
  if(a==b) return(0);
  ca=Cvalue(a);
  cb=Cvalue(b);
  if(Cmag[a]<(Cmag[b]+Ctol)) return(1);
  if(Cmag[b]<(Cmag[a]+Ctol)) return(0);
  return((angle(a)+Ctol)>angle(b));
}

complex Cmake(long double r,long double i)
// make a complex value
{
  complex c;
  c.r=r;
  c.i=i;
  return(c);
}

complex CmakeOne(void)
{
  return(Cmake(1.0,0.0));
}

complex CmakeZero(void)
{
  return(Cmake(0.0,0.0));
}

complex CmakeMOne(void)
{
  return(Cmake(-1.0,0.0));
}

long double Qmake(int a, int b,int c)
// returns the complex number equal to (a+b*sqrt(2))/c
// required to be compatible with quadratic irrational-based 
// complex number package
{
  return(((float)a+((float)b)*sqrt(2.0))/(float)(c));
}

void QMDDinitCtable(void)
// initialize the complex value table and complex operation tables to empty
{
  int i,j;
  
  Ctentries=0;
    
  for(i=0;i<COMPLEXTSIZE;i++)
  {
    for(j=0;j<COMPLEXTSIZE;j++)
      cta[i][j]=cts[i][j]=ctm[i][j]=ctd[i][j]=-1;
  }
  
  if(VERBOSE) printf("\nDouble complex number package initialized\n\n");
}

void QMDDcomplexInit(void)
// initialization
{
  Ctol=1.0e-5;
  Cmag[0]=0;
  Cmag[1]=1;
  QMDDinitCtable();
}

void QMDDcvalue_table_list(void)
// print the complex value table entries
{
  int i;
  
  printf("\nComplex value table: %d entries\n",Ctentries);
  for(i=0;i<Ctentries;i++)
  {
    printf("%d ",i);
    Cprint(Ctable[i]);
    printf("  || %f %f\n",Cmag[i],angle(i));
    if(i!=0&&i%100==0) QMDDpause();
  }
}

int Clookup(complex c)
// lookup a complex value in the complex value table
// if not found add it
// this routine uses linear searching
{
  int i;

  for(i=0;i<Ctentries;i++)
    if(Ceq(Ctable[i],c)) 
    return(i);
  Ctentries++;
  if(Ctentries>COMPLEXTSIZE)
  {
    printf("Complex mapping table overflow.");
    Cprint(c);
    printf("\n");
    QMDDcvalue_table_list();
    exit(0);
  }
  i=Ctentries-1;
  Ctable[i]=c;
  Cmag[i]=sqrt(c.r*c.r+c.i*c.i);
  Cangle[i]=angle(i);
  return(i);
}

complex Conj(complex c)
// return complex conjugate
{
  c.i=-c.i;
  return(c);
}


// basic operations on complex values
// meanings are self-evident from the names
// NOTE arguments are the indices to the values 
// in the complex value table not the values themselves

int Cnegative(int a)
{
  complex c;
  c=Cvalue(a);
  c.r=-c.r;
  c.i=-c.i;
  return(Clookup(c));
}

int Cadd(int ai,int bi)
{
  complex a,b,r;
  int t;
  
  if(ai==0) return(bi); // identity cases
  if(bi==0) return(ai);

  if(0<=(t=cta[ai][bi])) return(t); // look in computation table
  
  a=Cvalue(ai); // if new compute result
  b=Cvalue(bi); 
  r.r=a.r+b.r;
  r.i=a.i+b.i;
  
  t=cta[ai][bi]=cta[bi][ai]=Clookup(r); // save result
  return(t);
}

int Csub(int ai,int bi)
{
  complex a,b,r;
  int t;
  
  if(bi==0) return(ai); // identity case
  
  if(0<=(t=cts[ai][bi])) return(t); // look in computation table
  
  a=Cvalue(ai);  // if new compute result
  b=Cvalue(bi);
  r.r=a.r-b.r;
  r.i=a.i-b.i;
  
  t=cts[ai][bi]=Clookup(r); // save result
  return(t);
}

int Cmul(int ai,int bi)
{
  complex a,b,r;
  int t;
  
  if(ai==1) return(bi); // identity cases
  if(bi==1) return(ai);
  if(ai==0||bi==0) return(0);
  
  if(0<=(t=ctm[ai][bi])) return(t); // look in computation table
  
  a=Cvalue(ai); // if new compute result
  b=Cvalue(bi);
  r.r=a.r*b.r-a.i*b.i;
  r.i=a.r*b.i+a.i*b.r;
  
  t=ctm[ai][bi]=ctm[bi][ai]=Clookup(r); // save result
  return(t);
}

int CintMul(int a,int bi)
{
  complex r;
  r=Cvalue(bi);
  r.r*=a;
  r.i*=a;
  return(Clookup(r));
}

int Cdiv(int ai,int bi)
{
  complex a,b,r;
  int t;
  long double d;
  
  if(ai==bi) return(1); // equal case
  if(ai==0) return(0); // identity cases
  if(bi==1) return(ai);
  
  if(0<=(t=ctd[ai][bi])) return(t); // check computation table
  
  a=Cvalue(ai); // if new compute result
  b=Cvalue(bi);
  if(b.i==0.0)
  {
    r.r=a.r/b.r;
    r.i=a.i/b.r;
  } else {
    d=b.r*b.r+b.i*b.i;
    r.r=(a.r*b.r+a.i*b.i)/d;
    r.i=(a.i*b.r-a.r*b.i)/d;
  }
  t=ctd[ai][bi]=Clookup(r); // save result
  return(t);
}

void QMDDmakeRootsOfUnity(void)
{
  int i;
  CTa[0]=1;
  CTa[1]=Clookup(Cmake(cosl(2*Pi/Radix),sinl(2*Pi/Radix)));
  for(i=2;i<Radix;i++)
    CTa[i]=Cmul(CTa[i-1],CTa[1]);
}

/*********************************************

QMDD package routines 

    Michael Miller
    University of Victoria
    mmiller@cs.uvic.ca
    
    Date: July 2008
    
    extensions by Philipp Niemann, August-November 2012

*********************************************/


/***************************************

	Public Macros

***************************************/


#define QMDDterminal(e) (e.p==QMDDtnode) // checks if an edge points to the terminal node 

#define QMDDedgeEqual(a,b) ((a.p==b.p)&&(a.w==b.w)) // checks if two edges are equal

/***************************************

	Prototypes

***************************************/

void QMDDinitGateMatrices(void);
QMDDedge QMDDtranspose(QMDDedge a); //prototype
void QMDDmatrixPrint2(QMDDedge a); // prototype

/***********************************************

    Private Routines - Used in package - not called by user program.
    
***********************************************/

void QMDDpause(void)
// for debugging purposes - not normally used
{
  char ch;
  scanf("%c",&ch);
}

void QMDDdebugnode(QMDDnodeptr p)
// for debugging purposes - not normally used
{
  int i;
  
  if(p==QMDDzero.p)
  {
    printf("terminal\n");
    return;
  }
   printf("Debug node %d\n",(intptr_t) p);
   printf("node v %d (%d) edges (w,p) ",(int) QMDDorder[p->v],(int) p->v);
  for(i=0;i<Nedge;i++)
  {
    Cprint(Cvalue(p->e[i].w));
     printf(" %d || ", (intptr_t) p->e[i].p);
  }
  printf("ref %d\n",p->ref);
  //for(i=0;i<NEDGE;i++) QMDDdebugnode(p->e[i].p);
}


ListElementPtr QMDDnewListElement(void)
{
  ListElementPtr r,r2;
  int i,j;
      
  if(Lavail!=NULL)	// get node from avail chain if possible
  {
	r=Lavail;
	Lavail=Lavail->next;
  } else {			// otherwise allocate 2000 new nodes
  j=sizeof(ListElement);;
  r=(ListElementPtr)malloc(2000*j);
  r2=(ListElementPtr)((long)r+j);
  Lavail=r2;
  for(i=0;i<1998;i++,r2=(ListElementPtr)((long)r2+j))
  {
    r2->next=(ListElementPtr)((long)r2+j);
  }
     r2->next=NULL;
  }
  return(r);
}

void QMDDprint(QMDDedge e,int limit)
// a slightly better QMDD print utility
{
  ListElementPtr first,q,lastq,pnext;
  int n,i,j;
  
  first=QMDDnewListElement();
  first->p=e.p;
  first->next=NULL;
  first->w=0;
  first->cnt=1;
  n=0;
  i=0;
  printf("top edge weight ");
  Cprint(Cvalue(e.w));
  printf("\n");
  
  pnext=first;
  
  while(pnext!=NULL)
  {
    printf("%3d %3d ",pnext->cnt,pnext->p->ref);

    if(pnext->p->block) printf("B"); else printf(" ");
    if(pnext->p->diag) printf("D"); else printf(" ");
    if(pnext->p->ident) printf("I"); else printf(" ");
    if(pnext->p->symm) printf("S"); else printf(" ");
    if(pnext->p->renormFactor != 1) printf("R=%2d", pnext->p->renormFactor); else printf("    ");
    printf(" %3d| ",i);
    printf(" (%d)",pnext->p->v);

   // if (pnext->cnt != pnext->p->ref)
      //QMDDpause();
      
    
    printf("[");
    if(pnext->p!=QMDDzero.p) 
      for(j=0;j<Nedge;j++)
      {
        if(pnext->p->e[j].p==NULL) printf("NULL ");
        else 
        {
          if(!QMDDterminal(pnext->p->e[j]))
          {
            q=first->next;
            lastq=first;
            while(q!=NULL&&pnext->p->e[j].p!=q->p) {lastq=q; q=q->next;}
            if(q==NULL)
            {
              q=QMDDnewListElement();;
              q->p=pnext->p->e[j].p;
              q->next=NULL;
              q->w=n=n+1;
              q->cnt=1;
              lastq->next=q;
            } else q->cnt=q->cnt+1;
            printf(" %3d:",q->w);
          } else printf("   T:");
          //Cprint(Cvalue(pnext->p->e[j].w));
	  printf(" (%2d)", pnext->p->e[j].w);
          printf(" ");
        }
      }
	

    printf("] %d\n", (intptr_t) pnext->p);
    //printf("] \n");
    i++;
    if(i==limit) 
    {
      printf("Printing terminated at %d vertices\n",limit);
      return;
    }  
pnext=pnext->next; 
  }
}

QMDDedge QMDDnormalize(QMDDedge e)
//  normalize a QMDD node adjusting edge e
//  uses normalization as defined in the ISMVL-2006
//  paper by Miller and Thornton modified for garbage line collapsing
{
  int i,j;
  
  e.w=1;
  for(i=0;(e.p->e[i].p==NULL||e.p->e[i].w==0)&&i<Nedge;i++); //look for the first non-zero entry
  if(i==Nedge) 
  {
    e.w=0;
    return(e); // check validity
  }
  // normalize edge by setting the first non-zero weight as the new incoming weight and adjusting all other weights
  if(e.p->e[i].w==1) return(e);
  e.w=e.p->e[i].w;
  for(j=0;j<Nedge;j++) 
    if(i==j) e.p->e[j].w=1;
    else if(e.p->e[j].p!=NULL&&e.p->e[j].w!=0)
    {
       e.p->e[j].w=Cdiv(e.p->e[j].w,e.w);
    }
  return(e);
}


void QMDDcheckSpecialMatrices(QMDDedge e)
//  check if e points to a block, identity, diagonal, symmetric or 0/1-matrix and
//  marks top node if it does
{
  int i,j, w;
  QMDDedge t;
  
  // only perform checks if flag is set
  if(!e.p->computeSpecialMatricesFlag)
    return;
  
  e.p->ident=0; 	   // assume not identity
  e.p->diag=0;	   	   // assume not diagonal
  e.p->block=0;		   // assume not block
  e.p->symm=1;		   // assume symmetric
  e.p->c01=1;		   // assume 0/1-matrix
  
  /****************** CHECK IF 0-1 MATRIX ***********************/
  
  for(i=0;i<Nedge;i++)  // check if 0-1 matrix
    if(e.p->e[i].w>1||(!e.p->e[i].p->c01)){
        e.p->c01=0;
        break;
    }
    
  /****************** CHECK IF Symmetric MATRIX *****************/
  
 for(i=0;i<Radix;i++)  // check if symmetric matrix (on diagonal)
    if(!(e.p->e[Radix*i+i].p->symm)){
        e.p->symm=0;
        break;
    }

    //printf("specMat for %d. symm=%s", (intptr_t) e.p, e.p->symm?"diag":"nodiag");
    
  for(i=0;e.p->symm&&i<Radix-1;i++){ // check off diagonal entries for transpose properties
    for(j=i+1;j<Radix;j++){
      t=QMDDtranspose(e.p->e[i*Radix+j]);
       if(!QMDDedgeEqual(t,e.p->e[j*Radix+i])){
        e.p->symm=0;
        break;
      }
    }
  }
  
  //printf(",%s\n", e.p->symm?"offdiag":"notOffDiag");
  
  
  
  w=QMDDinvorder[e.p->v];
  if(w!=0) w=QMDDorder[w-1];
  // w:= variable one level below current level or 0 if already at the bottom
  
  /****************** CHECK IF Block MATRIX ***********************/
  
  for(i=0;i<Radix;i++) // check off diagonal entries
    for(j=0;j<Radix;j++)
      if(e.p->e[i*Radix+j].p==NULL||(i!=j&&e.p->e[i*Radix+j].w!=0)) return;
  e.p->block=1;
  
  /****************** CHECK IF Diagonal MATRIX ***********************/
  // will only reach this point if block == 1
  
  e.p->diag=1;
  for(i=0;i<Radix;i++)  // check diagonal entries to verify matrix is diagonal
  {
    // necessary condition: edge points to a diagonal matrix
    e.p->diag=e.p->e[i*Radix+i].p->diag;
    j=Radix*i+i;
    
    // skipped variable: edge pointing to terminal with non-zero weight from level > 0
       if((QMDDterminal(e.p->e[j]))&&e.p->e[j].w!=0&&QMDDinvorder[e.p->v]!=0) /*return; /*/ e.p->diag = 0;
    // skipped variable: edge pointing to an irregular level (non-terminal)
       if((!QMDDterminal(e.p->e[j]))&&e.p->e[j].p->v!=w) /*return; /*/e.p->diag = 0;
    
    if(!e.p->diag) return;
  }
  
  /****************** CHECK IF Identity MATRIX ***********************/
  // will only reach this point if diag == 1
  
  for(i=0;i<Radix;i++)  // check diagonal entries
  {
    j=Radix*i+i;
 // if skipped variable, then matrix cannot be diagonal (and we will not reach this point)!
    if(e.p->e[j].w!=1||e.p->e[j].p->ident==0) return;
  }
  e.p->ident=1;
  return;
}

QMDDedge QMDDutLookup(QMDDedge e)
{
//  lookup a node in the unique table for the appropriate variable - if not found insert it
//  only normalized nodes shall be stored.

  intptr_t key;
  int i,j;
  unsigned int v;
  QMDDnodeptr lastp,p;
  
  if(QMDDterminal(e)) // there is a unique terminal node
  {
	e.p=QMDDzero.p;
	return(e);
  }
  								         
  key=0;
// note hash function shifts pointer values so that order is important
// suggested by Dr. Nigel Horspool and helps significantly
  for(i=0;i<Nedge;i++)
    key+=(((intptr_t)e.p->e[i].p)>>i+e.p->e[i].w);
  key=(key)&HASHMASK;
  
  v=(unsigned int)e.p->v;
  p=Unique[v][key]; // find pointer to appropriate collision chain
  //lastp=NULL;	    // pN: not necessary, don't need to jump back to predecessor
  while(p!=NULL)    // search for a match
  {
	if(memcmp(e.p->e,p->e,Nedge*sizeof(QMDDedge))==0) 
	{
	  // Match found
	  e.p->next=Avail; 	// put node pointed to by e.p on avail chain
	  Avail=e.p;
	  
	  // NOTE: reference counting is to be adjusted by function invoking the table lookup
	  UTmatch++;		// record hash table match
	  
	  e.p=p;		// and set it to point to node found (with weight unchanged)
	  
	  if(p->renormFactor != 1) {
	    printf("Debug: table lookup found a node with active renormFactor with v=%d (id=%d).\n", p->v, (intptr_t) p);
	    if (p->ref != 0) printf("was active!"); else printf("was inactive!");
	    exit(66);
	  e.w = Cdiv(e.w,e.p->renormFactor);   
	  }
	  return(e);       
	}
	
	UTcol++; 		// record hash collision
	//lastp=p;
	p=p->next;
  }
  e.p->next=Unique[v][key]; // if end of chain is reached, this is a new node
  Unique[v][key]=e.p;       // add it to front of collision chain
																							    
  QMDDnodecount++;          // count that it exists
  if(QMDDnodecount>QMDDpeaknodecount) QMDDpeaknodecount=QMDDnodecount;

  if(!QMDDterminal(e)) QMDDcheckSpecialMatrices(e); // check if it is identity or diagonal if nonterminal
 
  return(e);                // and return
}


void QMDDinitComputeTable(void)
// set compute table to empty and
// set toffoli gate table to empty and
// set identity table to empty
{
  int i;
  
  for(i=0;i<CTSLOTS;i++)
  {
    CTable[i].r.p=NULL;
    CTable[i].which=none;
  }
  for(i=0;i<TTSLOTS;i++)
    TTable[i].e.p=NULL;
  for(i=0;i<MAXN;i++) QMDDid[i].p=NULL;
  QMDDnullEdge.p=NULL;
  QMDDnullEdge.w=1;
}

void QMDDgarbageCollect(void)
// a simple garbage collector that removes nodes with 0 ref count from the unique
// tables placing them on the available space chain
{
  int i,j;
  int count,counta;
  QMDDnodeptr p,lastp,nextp;
  
  if(QMDDnodecount<GCcurrentLimit) return; // do not collect if below GCcurrentLimit node count
  count=counta=0;
  //printf("starting garbage collector %d nodes\n",QMDDnodecount);
  for(i=0;i<MAXN;i++)
    for(j=0;j<NBUCKET;j++)
    {
      lastp=NULL;
      p=Unique[i][j];
      while(p!=NULL)
      {
        if(p->ref==0)
        {
          if(p==QMDDtnode) printf("error in garbage collector\n");
          count++;
          nextp=p->next;
          if(lastp==NULL) Unique[i][j]=p->next;
          else lastp->next=p->next;
          p->next=Avail;
          Avail=p;
          p=nextp;
        }
        else
        {
          lastp=p;
          p=p->next;
          counta++;
	    }
      }
    }
  //printf("%d nodes recovered %d nodes active\n",count,counta);
  GCcurrentLimit+=GCLIMIT_INC;
  QMDDnodecount=counta;
  QMDDinitComputeTable();  // IMPORTANT sets compute table to empty after garbage collection
}

QMDDnodeptr QMDDgetNode(void)
{
// get memory space for a node
//
  QMDDnodeptr r,r2;
  int i,j;
      
  if(Avail!=NULL)	// get node from avail chain if possible
  {
	r=Avail;
	Avail=Avail->next;
  } else {			// otherwise allocate 2000 new nodes

  //printf("no space available. allocate 2000 new nodes\n");

  j=sizeof(QMDDnode);//+Nedge*sizeof(QMDDedge);				// estimated value of a QMDDnode ! DANGER ous. pN calculated 44=sizeof(QMDDnode)+Nedge*sizeof(QMDDedge)
  r=(QMDDnodeptr)malloc(2000*j);
  r2=(QMDDnodeptr)((long)r+j);
  Avail=r2;
  for(i=0;i<1998;i++,r2=(QMDDnodeptr)((long)r2+j))
  {
    r2->next=(QMDDnodeptr)((long)r2+j);
  }
	r2->next=NULL;
  }
  r->next=NULL;
  r->ref=0;			// set reference count to 0
  r->ident=r->diag=r->block=0;		// mark as not identity or diagonal
  return(r);
}

void QMDDincref(QMDDedge e)
// increment reference counter for node e points to
// and recursively increment reference counter for 
// each child if this is the first reference
//
// a ref count saturates and remains unchanged if it has reached
// MAXREFCNT
{
  int i;
  
  if (QMDDterminal(e))
    return;
  
  if(e.p->ref==MAXREFCNT) { printf("MAXREFCNT reached\n\n\n"); return; }
  e.p->ref++;
  
  /*if (e.p->ref > largestRefCount) { //record maximum of Reference counts
   largestRefCount = e.p->ref;
   if (largestRefCount % 1000 == 0)
     printf("new MAXREF %d  ... %d, %d\n", largestRefCount, (intptr_t) e.p, (intptr_t) QMDDzero.p);
  }*/
  
  if(e.p->ref==1)
  {  
    if(!QMDDterminal(e)) for(i=0;i<Nedge;i++)
      if(e.p->e[i].p!=NULL) QMDDincref(e.p->e[i]);
      
    Active[e.p->v]++;
    ActiveNodeCount++;

    /******* Part added for sifting purposes ********/
    if(e.p->block)
      blockMatrixCounter++;
    /******* by Niemann, November 2012 ********/
    
  }
}


void QMDDdecref(QMDDedge e)
// decrement reference counter for node e points to
// and recursively decrement reference counter for 
// each child if this is the last reference
//
// a ref count saturates and remains unchanged if it has reached
// MAXREFCNT
{
  int i;
  
  if (QMDDterminal(e))
    return;
  
  if(e.p->ref==MAXREFCNT) return;
  e.p->ref--;
  if(e.p->ref == (unsigned int) -1) // ERROR CHECK 
  {
    printf("error in decref %d\n",e.p->ref);
    QMDDdebugnode(e.p);
    exit(8);
  }
  if(e.p->ref==0)
  {
    if(!QMDDterminal(e)) for(i=0;i<Nedge;i++)
    if(e.p->e[i].p!=NULL) QMDDdecref(e.p->e[i]);
    Active[e.p->v]--;
    if(Active[e.p->v]<0) printf("ERROR in decref\n");
    ActiveNodeCount--;
    
    /******* Part added for sifting purposes ********/
    if(e.p->renormFactor != 1){	    
      RenormalizationNodeCount--;
      e.p->renormFactor = 1;
    }
    if(e.p->block)
      blockMatrixCounter--;
    /******* by Niemann, November 2012 ********/
  }
}

int QMDDnodeCount(QMDDedge e)
// a very simplistic recursive routine for counting
// number of unique nodes in a QMDD
// NEEDS TO BE REDONE
{
  int i,sum;
  
  for(i=0;i<Ncount;i++)
    if(Nlist[i]==e.p) return(0);
  Nlist[Ncount]=e.p;
  Ncount++;
  sum=1;
  if(!QMDDterminal(e))
    for(i=0;i<Nedge;i++)
      if(e.p->e[i].p!=NULL) sum+=QMDDnodeCount(e.p->e[i]);
  if(sum>MAXNODECOUNT) return(MAXNODECOUNT);
  return(sum);
}

void QMDDradixPrint(int p,int n)
// prints p as an n bit Radix number
// with leading 0's and no CR
{
  int i,buffer[MAXN];
  for(i=0;i<n;i++)
  {
    buffer[i]=p%Radix;
    p=p/Radix;
  }
  for(i=n-1;i>=0;i--)
    printf("%d",buffer[i]);
}

#define CThash(a,b) (((((long)a.p+(long)b.p)>>3)+(int)a.w+(int)b.w+(int)which)&CTMASK)

QMDDedge CTlookup(QMDDedge a,QMDDedge b,CTkind which)
{
// Lookup a computation in the compute table
// return NULL if not a match else returns result of prior computation
  int i;
  QMDDedge r;
  
  r.p=NULL;
  CTlook[which]++;
  i=CThash(a,b);
  if(CTable[i].which!=which) return(r);
  if(CTable[i].a.p!=a.p||CTable[i].a.w!=a.w) return(r);
  if(CTable[i].b.p!=b.p||CTable[i].b.w!=b.w) return(r);
  if((CTable[i].r.p)->v==-1) return(r);  /* fix */
  CThit[which]++;
  return(CTable[i].r);
}

void CTinsert(QMDDedge a,QMDDedge b,QMDDedge r,CTkind which)
{
// put an entry into the compute table
  int i;
 
  i=CThash(a,b);
  CTable[i].a=a;
  CTable[i].b=b;
  CTable[i].r=r;
  CTable[i].which=which;
}

int TThash(int n,int m,int t,int line[])
{
  int i,j;
  i=t;
  for(j=0;j<n;j++) if(line[j]==1) i=i<<3+j;
  return(i&TTMASK);
}

QMDDedge TTlookup(int n,int m,int t,int line[])
// does not work with SIFTING!! Idea: TT should be initialized
{
  QMDDedge r;
  int i,j;
  r.p=NULL;
  i=TThash(n,m,t,line);
  if(TTable[i].e.p==NULL||TTable[i].t!=t||TTable[i].m!=m||TTable[i].n!=n) return(r);
  //for(j=0;j<n;j++) if(TTable[i].line[j]!=line[j]) return(r);
  if(0==memcmp(TTable[i].line,line,n*sizeof(int))) return(TTable[i].e);
  return(r);
}

void TTinsert(int n,int m,int t,int line[],QMDDedge e)
{
  int i,j;
  i=TThash(n,m,t,line);
  TTable[i].n=n;
  TTable[i].m=m;
  TTable[i].t=t;
  memcpy(TTable[i].line,line,n*sizeof(int));
  TTable[i].e=e;
}

void QMDDfillmat(short mat[MAXDIM][MAXDIM],QMDDedge a,int r,int c,int dim,short v,char vtype[])
// recursively scan an QMDD putting values in entries of mat
// v is the variable index
{
  int i,j,expand;
  QMDDedge e;
  
  if(a.p==NULL) return;
  
  if(v==-1) // terminal node case
  {
    if (r>=MAXDIM || c >= MAXDIM) {
	printf("out of bounds, r=%d, c=%d\n", r,c); return;
    }
    mat[r][c]=a.w;
  }
  else
  {
    expand=(QMDDterminal(a))||v!=QMDDinvorder[a.p->v];
    for(i=0;i<Nedge;i++)
    {
      if((vtype[v]==0)||(vtype[v]==1&&i<Radix)||(vtype[v]==2&&(i%Radix==0)))
      {
        if(expand)
        {
          QMDDfillmat(mat,a,r+(i/Radix)*dim/Radix,c+(i%Radix)*dim/Radix,dim/Radix,v-1,vtype);
        } else {
          e=a.p->e[i];
          e.w=Cmul(a.w,e.w);
	  //e.w = 1; // pN
          QMDDfillmat(mat,e,r+(i/Radix)*dim/Radix,c+(i%Radix)*dim/Radix,dim/Radix,v-1,vtype);
        }
	  }
    }
  }
}

void recQMDDrcPrint(QMDDedge p,short n,short w)
// called by QMDDcolumnPrint and QMDDrowPrint to do actual printing
// w==2 for column; w==1 for row
{
  int i,j,k,limit;
  QMDDedge e;
 
  if(QMDDterminal(p)) k=n+1;
  else k=n-QMDDinvorder[p.p->v];
  limit=1;
  for(j=0;j<k;j++) limit*=Radix;
  for(j=0;j<limit;j++)
    if(QMDDterminal(p)) 
      {
        if(p.w<=1) printf("%d",p.w);
        else Cprint(Cvalue(p.w));
        printf(" ");
      }
    else 
    {
      k=0;
      for(i=0;i<Radix;i++)
      {
        e=p.p->e[k];
        e.w=Cmul(e.w,p.w);
        recQMDDrcPrint(e,QMDDinvorder[p.p->v]-1,w);
        if(w==1) k++; else k+=Radix;
      }
    }
}

void QMDDpermPrint(QMDDedge e,int row, int col)
{
  int i;
  if(QMDDterminal(e))
  {
    if(e.w!=1) printf("error in permutation printing/n");
    else PermList[col]=row;
  }
  else
    for(i=0;i<Nedge;i++)
      if(e.p->e[i].p!=NULL&&e.p->e[i].w!=0)
        QMDDpermPrint(e.p->e[i],row*Radix+i/Radix,col*Radix+i%Radix);
}

/***************************************

    Public Routines
    
***************************************/

QMDDedge QMDDmakeNonterminal(short v,QMDDedge edge[])
{
// make a QMDD nonterminal node and return an edge pointing to it
// node is not recreated if it already exists
  QMDDedge e;
  int i,redundant;
  
  redundant=1;		// check if redundant node = all edges point to the same node (with same weight)
  e=edge[0];
  
  i=1;
  while(redundant&&(i<Nedge))
  {
    redundant=(edge[i].p==NULL)||((edge[i].w==e.w)&&(edge[i].p==e.p));
    i++;
  }
  if(redundant) return(edge[0]); // return 0 child if redundant
   
  e.p=QMDDgetNode();  // get space and form node
  e.w=1;
  e.sentinel=0;  // see QMDDpackage.h for information about sentinel
  e.p->v=v;
  e.p->renormFactor=1;
  e.p->computeSpecialMatricesFlag = globalComputeSpecialMatricesFlag;
  
  memcpy(e.p->e,edge,Nedge*sizeof(QMDDedge));
  e=QMDDnormalize(e); // normalize it
  e=QMDDutLookup(e);  // look it up in the unique tables
  return(e);		  // return result
}


QMDDedge QMDDmakeTerminal(complex c)
// make a terminal - actually make an edge with appropriate weight 
// as there is only one terminal QMDDone
{
  QMDDedge e;
  
  e.p=QMDDtnode;
  e.w=Clookup(c);
  e.sentinel=0;	// see QMDDpackage.h for information about sentinel
  return(e);
}

void QMDDinit(int verbose)
{
// initialize QMDD package - must be called before other routines are used
//
  int i,j;
  
  if(verbose)
  {
    printf(QMDDversion);
    printf("compiled: %s %s\n\n",__DATE__,__TIME__);
    printf("Edge size %d bytes\n",sizeof(QMDDedge));
    printf("Node size %d bytes\n",sizeof(QMDDnode)+Nedge*sizeof(QMDDedge));
    printf("Max variables %d\nUT buckets / variable %d\nCompute table slots %d\nToffoli table slots %d\nGarbage collection limit %d\nGarbage collection increment %d\nComplex number table size %d\n",MAXN,NBUCKET,CTSLOTS,TTSLOTS,GCLIMIT1,GCLIMIT_INC,COMPLEXTSIZE);
  }
  Pi=2.0*acos(0.0);	   // set value of global Pi
  
  Nedge=Radix*Radix;	   // set number of edges

  QMDDcomplexInit();	   // init complex number package
  QMDDinitComputeTable();  // init computed table to empty
  
  GCcurrentLimit=GCLIMIT1; // set initial garbage collection limit

  UTcol = UTmatch = 0;
  QMDDnodecount=0;			// zero node counter
  QMDDpeaknodecount=0;
  Nlabel=0;                		// zero variable label counter
  Nop[0]=Nop[1]=Nop[2]=0;		// zero op counter
  CTlook[0]=CTlook[1]=CTlook[2]=CThit[0]=CThit[1]=CThit[2]=0;		// zero CTable counters
  Avail=NULL;				// set available node list to empty
  Lavail=NULL;				// set available element list to empty
  QMDDtnode=QMDDgetNode();		// create terminal node - note does not go in unique table
  QMDDtnode->ident=1;
  QMDDtnode->diag=1;
  QMDDtnode->block=0;  // changed by Niemann 121109
  QMDDtnode->symm=1;
  QMDDtnode->c01=1;
  QMDDtnode->renormFactor=1;
  QMDDtnode->computeSpecialMatricesFlag=0;
  for(i=0;i<Nedge;i++)
  { 
    QMDDtnode->e[i].p=NULL;
    QMDDtnode->e[i].w=0;
  }
  QMDDtnode->v=-1;		// pN 120814: if ==1 it counts for Active[1]...bad for sifting
  QMDDzero=QMDDmakeTerminal(CmakeZero());
  QMDDone=QMDDmakeTerminal(CmakeOne());
  Clookup(CmakeMOne());		// (?) guarantees that 0,1,-1 are in CT slots 0,1,2
  for(i=0;i<MAXN;i++)
    for(j=0;j<NBUCKET;j++) // set unique tables to empty
      Unique[i][j]=NULL;
  for(i=0;i<MAXN;i++)      //  set initial variable order to 0,1,2... from bottom up
  {
    QMDDorder[i]=QMDDinvorder[i]=i;
    Active[i]=0;
  } 
  ActiveNodeCount=0;
  QMDDinitGateMatrices();
  if(verbose)printf("QMDD initialization complete\n----------------------------------------------------------\n");
}

QMDDedge QMDDadd(QMDDedge x,QMDDedge y)
// adds two matrices represented by QMDD
// the two QMDD should have the same variable set and ordering
{
  QMDDedge e1,e2,e[MAXNEDGE],r;
  int i,w;
  
  if(x.p==NULL) return(y);  // handles partial matrices i.e.
  if(y.p==NULL) return(x);  // column and row vetors
  Nop[add]++;
  if((!MultMode)&&(QMDDterminal(y)||x.p>y.p))
  {
    e1=x;
    x=y;
    y=e1;
  }

  if(x.w==0) 
  {
    return(y);
  }
  if(y.w==0) 
  {
    return(x);
  }
  if(x.p==y.p)
  {
    r=y;
    r.w=Cadd(x.w,y.w);
    if(r.w==0) r=QMDDzero;
    return(r);
  }
  r=CTlookup(x,y,add);
  if(r.p!=NULL) return(r);

  if(QMDDterminal(x)) w=y.p->v;
  else {
    w=x.p->v;
    if(!QMDDterminal(y))
      if(QMDDinvorder[y.p->v]>QMDDinvorder[w]) w=y.p->v;
  }

  for(i=0;i<Nedge;i++)
  {
    if(!QMDDterminal(x)&&x.p->v==w)
    {
      e1=x.p->e[i];
      e1.w=Cmul(e1.w,x.w);
    } else {
      if((!MultMode)||(i%Radix==0)){
        e1=x;
	if(y.p->e[i].p==0) e1=QMDDnullEdge;
     }
      else {
        e1.p=NULL;
        e1.w=0;
      }
    }
    if(!QMDDterminal(y)&&y.p->v==w)
    {
      e2=y.p->e[i];
      e2.w=Cmul(e2.w,y.w);
    } else {
      if((!MultMode)||(i%Radix==0)) { 
        e2=y;
	if(x.p->e[i].p==0) e2=QMDDnullEdge;
     }
      else {
        e2.p=NULL;
        e2.w=0;
      }
    }
    e[i]=QMDDadd(e1,e2);
  }
  r=QMDDmakeNonterminal(w/*x.p->v*/,e);  /// sept 29
  CTinsert(x,y,r,add);
  return(r);
}

QMDDedge QMDDmultiply2(QMDDedge x,QMDDedge y, int var)

// new multiply routine designed to handle missing variables properly
// var is number of variables
{
  QMDDedge e1,e2,e[MAXNEDGE],r;
  int i,j,k,w;
  
  
  int xweight, yweight; 
  char newCT = 1; 
  
  if(x.p==NULL) return(x);
  if(y.p==NULL) return(y);

  Nop[mult]++;
 
  if(x.w==0||y.w==0)  // the 0 case
  {
    return(QMDDzero);
  }
  
  if(var==0) return(QMDDmakeTerminal(Cvalue(Cmul(x.w,y.w))));

  /// added by Niemann Nov 2012
    // extract and store edge weights of the factors
    // compute /look up product without these factors
    // multiply result with the product of the initial edge weights
    //
    // this gives a higher CT hit rate!
  if(newCT) {
  xweight = x.w;
  yweight = y.w;
  x.w=1;
  y.w=1;
  }
  
  r=CTlookup(x,y,mult);
  if(r.p!=NULL) { 
    if(newCT) {
    r.w=Cmul(r.w, xweight);
    r.w=Cmul(r.w, yweight);
    }
    return(r);
  }
    
  w=QMDDorder[var-1];

  if(x.p->v==w&&x.p->v==y.p->v)
  {
    if(x.p->ident) 
    {
      r=y;
      if(!newCT) r.w=Cmul(r.w,x.w);
      CTinsert(x,y,r,mult);
      if(newCT) r.w = Cmul(xweight, yweight);
      return(r);
    }
    if(y.p->ident)
    {
      r=x;
      if(!newCT) r.w=Cmul(r.w,y.w);
      CTinsert(x,y,r,mult);
      if(newCT) r.w = Cmul(xweight, yweight);
      return(r);
    }

  }
  
  for(i=0;i<Nedge;i+=Radix)
  {
    for(j=0;j<Radix;j++)
    {
      e[i+j].p=NULL;
      e[i+j].w=0;
      for(k=0;k<Radix;k++)
      {
        if(!QMDDterminal(x)&&x.p->v==w)
        {
          e1=x.p->e[i+k];
          e1.w=Cmul(e1.w,x.w);
        } else {
          e1=x;
        }
        if(!QMDDterminal(y)&&y.p->v==w)
        {
          e2=y.p->e[j+Radix*k];
          e2.w=Cmul(e2.w,y.w);
        } else {
          e2=y;
        }
        
        e[i+j]=QMDDadd(e[i+j],QMDDmultiply2(e1,e2,var-1));
      } 
    }
  }
  r=QMDDmakeNonterminal(w,e);
  CTinsert(x,y,r,mult);
  if(newCT) {
   r.w=Cmul(r.w, xweight);
   r.w=Cmul(r.w, yweight);
  }
  return(r);
}

QMDDedge QMDDmultiply(QMDDedge x,QMDDedge y)
{
  int var;
  
  var=0;
  if(!QMDDterminal(x)&&(QMDDinvorder[x.p->v]+1)>var) var=QMDDinvorder[x.p->v]+1;
  if(!QMDDterminal(y)&&(QMDDinvorder[y.p->v]+1)>var) var=QMDDinvorder[y.p->v]+1;
  
  return(QMDDmultiply2(x,y,var));
}

QMDDedge QMDDkron(QMDDedge a,QMDDedge b)
// form Kronecker product of two QMDDs pointed to by a and b
// note Kronecker product is not commutative
{
   QMDDedge e[MAXNEDGE],r;
   int i,j;
   
   if(a.p==NULL) return(a);
   Nop[kronecker]++;
   if(a.w==0) 
   {
     return(QMDDzero);
   }
   if(QMDDterminal(a)&&a.w==1) 
   {
     return(b);
   }
   /********************************
   if(QMDDterminal(a))
   {
     r=b;
     r.w=Cmul(b.w,a.w);
     return(r);
   }
   ********************************/
   r=CTlookup(a,b,kronecker);
   if(r.p!=NULL) return(r);
   
   if(a.p->ident)
   {
     for(i=0;i<Radix;i++)
       for(j=0;j<Radix;j++)
         if(i==j) e[i*Radix+j]=b;
         else e[i*Radix+j]=QMDDzero;
     r=QMDDmakeNonterminal(a.p->v,e);
     r.w=Cmul(r.w,a.w);
     CTinsert(a,b,r,kronecker);
     return(r);
   }

   for(i=0;i<Nedge;i++)
     e[i]=QMDDkron(a.p->e[i],b);
   r=QMDDmakeNonterminal(a.p->v,e);
   r.w=Cmul(r.w,a.w);
   CTinsert(a,b,r,kronecker);
   return(r);
}

QMDDedge QMDDtranspose(QMDDedge a)
// returns a pointer to the transpose of the matrix a points to
{
  QMDDedge r,e[MAXNEDGE];
  int i,j;
  
  if(a.p==NULL) return(a);		 // NULL pointer
  if(QMDDterminal(a) || a.p->symm) return(a); // terminal / or symmetric case   ADDED by Niemann Nov. 2012
  r=CTlookup(a,a,transpose);     // check in compute table
  if(r.p!=NULL) return(r);

  for(i=0;i<Radix;i++)           // transpose submatrices and rearrange as required
    for(j=i;j<Radix;j++)
    {
      e[i*Radix+j]=QMDDtranspose(a.p->e[j*Radix+i]);
      if(i!=j)
        e[j*Radix+i]=QMDDtranspose(a.p->e[i*Radix+j]);
    }

  r=QMDDmakeNonterminal(a.p->v,e);// create new top vertex
  r.w=Cmul(r.w,a.w);		      // adjust top weight
  CTinsert(a,a,r,transpose);      // put in compute table
  return(r);
}


QMDDedge QMDDconjugateTranspose(QMDDedge a)
// returns a pointer to the conjugate transpose of the matrix pointed to by a
{
  QMDDedge r,e[MAXNEDGE];
  int i,j;
  
  if(a.p==NULL) return(a);		  // NULL pointer
  if(QMDDterminal(a)) 			  // terminal case
  {
    a.w=Clookup(Conj(Cvalue(a.w))); // return conjugate
    return(a);
  }
  r=CTlookup(a,a,conjugateTranspose);  // check if in compute table
  if(r.p!=NULL) return(r);
  
  for(i=0;i<Radix;i++)			// conjugate transpose submatrices and rearrange as required
    for(j=i;j<Radix;j++)
    {
      e[i*Radix+j]=QMDDconjugateTranspose(a.p->e[j*Radix+i]);
      if(i!=j)
        e[j*Radix+i]=QMDDconjugateTranspose(a.p->e[i*Radix+j]);
    }
  r=QMDDmakeNonterminal(a.p->v,e);    // create new top node
  r.w=Cmul(r.w,a.w);                  // adjust top weight including conjugate
  r.w=Clookup(Conj(Cvalue(r.w)));
  CTinsert(a,a,r,conjugateTranspose); // put it in the compute table
  return(r);
}


QMDDedge QMDDtrace(QMDDedge a, unsigned char var, char remove[], char all)
// compute the trace or partial trace of the matrix represented by the QMDD with top edge a
// returns an edge pointing to the QMDD representing the result
// var is the index of the 'expected' variable (-1 for a terminal node)
// remove[] determines which variables are to 'removed' / traced out
// all==1 means all variables are to be removed
// CT is only used implicitly (through QMDDadd).
//
// Author: Philipp Niemann Nov. 2012
{
  QMDDedge r, e[MAXNEDGE];
  int i,j;
  int w = QMDDinvorder[a.p->v];
  
  if(QMDDedgeEqual(QMDDzero,a))
    return QMDDzero;
  
  if(var== (unsigned char) -1) // terminal expected
  {
    if(QMDDterminal(a)) return(a);
    printf("Terminal expected - not found - in QMDDtrace/n");
    r.p = NULL;
    return r;
  } else { 			// nonterminal case
    
    if( remove[var] || all == 1 ) // the expected variable is to be removed
    {
      if (var==w){ 		// encounter expected variable
	r=QMDDzero;
	for(i=0; i<Radix; i++) {
	  r = QMDDadd (r, QMDDtrace(a.p->e[i*Radix+i], var-1, remove, all) );
	}
	r.w = Cmul(r.w, a.w);
	return r;
      } else { 			// unexpected variable => skipped expected variable
	r= QMDDtrace(a, var-1, remove, all);
	r.w=CintMul(Radix,r.w);	// have to multiply by Radix to get the correct result
	return r;
      }
    } else { 			// the expected variable is to be retained
        if (var == w){   	// encounter expected variable 
	for(i=0; i<Radix; i++)
	   for(j=0; j<Radix; j++)
	      e[i*Radix+j] = QMDDtrace(a.p->e[i*Radix+j], var-1, remove, all);
	   r = QMDDmakeNonterminal(a.p->v, e);
	   r.w = Cmul(r.w, a.w);
	   return r;
	} else { 		// skipped variable 
	r = QMDDtrace(a, var-1, remove, all);
	return r;
	}
    }
  }
}

QMDDedge QMDDident(int x,int y)
// build a QMDD for the identity matrix for variables x to y (x<y)
{
  int i,j,k;
  QMDDedge e,f,edge[MAXNEDGE];
  
  /// added by Niemann
  if (y<0)
    return QMDDone;
  
  if(x==0&&QMDDid[y].p!=NULL) 
  {
    return(QMDDid[y]);
  }
  if(y>=1&&(f=QMDDid[y-1]).p!=NULL)
  {
    for(i=0;i<Radix;i++)
      for(j=0;j<Radix;j++)
        if(i==j) edge[i*Radix+j]=f;
        else edge[i*Radix+j]=QMDDzero;
     e=QMDDmakeNonterminal(QMDDorder[y],edge);
     QMDDid[y]=e;
     return(e);
  }
  for(i=0;i<Radix;i++)
    for(j=0;j<Radix;j++)
      if(i==j) edge[i*Radix+j]=QMDDone;
      else edge[i*Radix+j]=QMDDzero;
  e=QMDDmakeNonterminal(QMDDorder[x],edge);
  for(k=x+1;k<=y;k++)
  {
    for(i=0;i<Radix;i++)
      for(j=0;j<Radix;j++)
        if(i==j) edge[i*Radix+j]=e;
        else edge[i*Radix+j]=QMDDzero;
    e=QMDDmakeNonterminal(QMDDorder[k],edge);
  }
  if(x==0) QMDDid[y]=e;
  return(e);
}

QMDDedge QMDDmvlgate(complex mat[MAXRADIX][MAXRADIX],int n,int line[])
// build matrix representation for a single gate
// line is the vector of connections
// -1 not connected
// 0...Radix-1 indicates a control by that value
// Radix indicates the line is the target
{
  
  //printf("QMDDmvlgate: %d,%d;%d,%d   %d\n", mat[0][0], mat[0][1], mat[1][0], mat[1][1], n);
  
   QMDDedge e,f,em[MAXNEDGE],fm[MAXNEDGE],temp;
   int i,i1,i2,w,z,j,k,t;
   
   for(i=0;i<Radix;i++)
     for(j=0;j<Radix;j++)
       em[i*Radix+j]=QMDDmakeTerminal(mat[i][j]);
   e=QMDDone;
   for(z=0;line[w=QMDDorder[z]]<Radix;z++) //process lines below target
   {
     if(line[w]>=0) //  control line below target in QMDD
     {
       for(i1=0;i1<Radix;i1++)
         for(i2=0;i2<Radix;i2++)
         {
           i=i1*Radix+i2;
           if(i1==i2) f=e;else f=QMDDzero;
           for(k=0;k<Radix;k++)
             for(j=0;j<Radix;j++)
             {
               t=k*Radix+j;
               if(k==j)
               {
                 if(k==line[w]) 
                 {
                   fm[t]=em[i];
                 }
                 else fm[t]=f;
               } 
               else fm[t]=QMDDzero;
             }
           em[i]=QMDDmakeNonterminal(w,fm);
         }
     } else // not connected
     {
       for(i=0;i<Nedge;i++)
       {
         for(i1=0;i1<Radix;i1++)
           for(i2=0;i2<Radix;i2++)
             if(i1==i2) fm[i1+i2*Radix]=em[i]; else fm[i1+i2*Radix]=QMDDzero;
         em[i]=QMDDmakeNonterminal(w,fm);
       }
     }
     e=QMDDident(0,z);
   }
   e=QMDDmakeNonterminal(QMDDorder[z],em);  // target line
   
   for(z++;z<n;z++) // go through lines above target
     if(line[w=QMDDorder[z]]>=0) //  control line above target in QMDD
     {
       temp=QMDDident(0,z-1);
       for(i=0;i<Radix;i++)
         for(j=0;j<Radix;j++)
           if(i==j)
           {
             if(i==line[w]) em[i*Radix+j]=e;
             else em[i*Radix+j]=temp;
           }
           else em[i*Radix+j]=QMDDzero;
       e=QMDDmakeNonterminal(w,em);
     } else // not connected
     {
       for(i1=0;i1<Radix;i1++)
         for(i2=0;i2<Radix;i2++)
           if(i1==i2) fm[i1+i2*Radix]=e; else fm[i1+i2*Radix]=QMDDzero;
       e=QMDDmakeNonterminal(w,fm);
     }
   return(e);
}


QMDDedge QMDDgate(complex mat[MAXRADIX][MAXRADIX],int n,int c,int t)
// for building 0 or 1 control binary gates
// c is the control variable
// t is the target variable
{
  int line[MAXN],i;
  
  for(i=0;i<n;i++) line[i]=-1;
  if(c>=0) line[c]=Radix-1;
  line[t]=Radix;
  return(QMDDmvlgate(mat,n,line));
}

void QMDDmatrixPrint(QMDDedge a,short v,char vtype[])
// a 0-1 matrix is printed more compactly
//
// Note: 0 entry and 1 entry in complex value table always denote
// the values 0 and 1 respectively, so it is sufficient to print the index
// for a 0-1 matrix.
//
// v is the variable index for the top vertex
{

  short mat[MAXDIM][MAXDIM];
  
  int m,n,i,j,mode,p,perm;
  
  short cTabPrint[COMPLEXTSIZE]; // print only values of used entries of the UniqueTable   // pN 120920
  bool cTabPrintFlag = false;
  
  for (i=0; i<COMPLEXTSIZE; i++)
    cTabPrint[i] = 0;

  
  if(QMDDterminal(a)) n=0; else n=v+1;
  m=1;
  for(i=0;i<n;i++) m*=Radix;
  if(n>MAXND)
  {
    printf("Matrix is too big to print. No. of vars=%d\n",n);
    return;
  }

//printf("DebugPN: Matrix is OK to print. No. of vars=%d. m=%d, v=%d, vtype=%s\n",n,m,v,vtype);

  //v=QMDDinvorder[v]; // convert variable index to position
  for(i=0;i<MAXDIM;i++)
    for(j=0;j<MAXDIM;j++)
      mat[i][j]=-1;

//	printf("DebugPN: No problems so far.");
  QMDDfillmat(mat,a,0,0,m,v,vtype); // convert to matrix
//	printf("DebugPN: No problems so far. Matrix filled.");
	
  mode=0;
  for(i=0;i<m;i++)          // check if 0-1 matrix (mode==1)
    for(j=0;j<m;j++)
      if(mat[i][j]>mode)
      {
         mode=mat[i][j];
         if(mode>2) break;
      }
      
  //DELETE THIS:
  mode=3;
      
  perm=mode==1; // note: assumes 0-1 matrix is a permutation matrix - should add verification



  for(i=0;i<m;i++)          // display matrix
  {
    for(j=0;j<m;j++)
    {
      if(mode>2&&m<=MAXDIM){           // display complex value
        //Cprint(Cvalue(mat[i][j]), os);
        cTabPrint[ mat[i][j] ] = 1;
	cTabPrintFlag = true;
	if (mat[i][j] < 10)
	  printf(" ");
	printf("%d ",mat[i][j]);
      }
      else 
      {
        if(perm&&mat[i][j]==1)    // record permutation value
        {
          p=j;
        }
        if(m<=MAXDIM)
          if(mat[i][j]==0) 
          {
            if(mode==2) printf(" ."); // print 0 as a period for clarity
            else printf(".");
          }
          else if(mat[i][j]==1) 
          {
            if(mode==2) printf(" 1");
            else printf("1");
          }
          else printf("-1");
      }
      if(j==m/2-1) printf("|");
    }
    /*if(perm) {
      printf("  ");
      //QMDDradixPrint(p,n);   // print permutation value if permutation matrix
        printf(" %d %d ", i, p)
	int temp = i;
	std::ostringstream temp2;
	while(temp != 0){
		temp2 << temp % 2;
		temp = temp / 2;
	}
	std::string::reverse_iterator rit;
	for(int j = 0; j < (pow(2,n) - temp2.str().size()); j++) os << "0"; 
	for(rit = temp2.str().rbegin(); rit != temp2.str().rend(); rit++) {
		os << *rit;
	}

	os << " ";
	temp = p;
	temp2.clear();
	while(temp != 0){
		temp2 << temp % 2;
		temp = temp / 2;
	}
	for(int j = 0; j < (pow(2,n) - temp2.str().size()); j++) os << "0"; 
	for(rit = temp2.str().rbegin(); rit != temp2.str().rend(); rit++) {
		os << *rit;
	}
    }*/
    printf("\n");
    if(i==m/2-1) 
    {
      for(j=0;j<m;j++)       printf("-");
            printf("-\n");
    }
  }
  if (cTabPrintFlag) {
    printf("ComplexTable values: (0): 0; (1): 1; ");
  
    for (i=2; i<COMPLEXTSIZE; i++)
      if (cTabPrint[i]) {
	printf("(%d):", i);
	Cprint(Cvalue(i));
	printf("; ");
      }   
  }    
  
  printf("\n");
}

/*void QMDDmatrixPrint(QMDDedge a, short v, char vtype[]) {
	std::ostringstream oss;
	QMDDmatrixPrint(a, v, vtype, oss);
	std::cout << oss.str();
}*/

/*void QMDDmatrixPrint2(QMDDedge a, std::ostream &os)
{
	char v[MAXN];
	int i;
  
	  for(i=0;i<MAXN;i++) v[i]=0;
	  QMDDmatrixPrint(a,a.p->v,v, os);
}

void QMDDmatrixPrint2(QMDDedge a, std::ostream &os, short n)
{
	char v[MAXN];
	int i;
  
	  for(i=0;i<MAXN;i++) v[i]=0;
	  QMDDmatrixPrint(a,n,v, os);
}*/

void QMDDmatrixPrint2(QMDDedge a)
{
  char v[MAXN];
  int i;
  
  for(i=0;i<MAXN;i++) v[i]=0;
  QMDDmatrixPrint(a,QMDDinvorder[a.p->v],v);
}

void QMDDpermutationPrint(QMDDedge a)
// print permutation represented by a QMDD
// it is assumed the QMDD does represent a permutation matrix if it is a 0-1 matrix
// Max vars is MAXND with eMAXDIM set to 2^MAXND
{
   int n,d,i;
   char fname[80],ch1;
   
   do {
      printf("please enter name of output file for permutation data: ");
      scanf("%s", fname);
      scanf("%c",&ch1); // discard end of line
      outfile = fopen(fname, "w"); // open the file for writing
      if(outfile==NULL) printf("Invalid file name, try again: ");
   } while(outfile==NULL);

   if(QMDDterminal(a)) n=0; else n=QMDDinvorder[a.p->v]+1;
   d=1;
   for(i=0;i<n;i++) d*=Radix;
   QMDDpermPrint(a,0,0);
   fprintf(outfile,"%d\n",n);
   for(i=0;i<d;i++) 
   {
     fprintf(outfile," %d",PermList[i]);
     if((i+1)%16==0) fprintf(outfile,"\n");
   }
   fprintf(outfile,"\n");
   fclose(outfile);
}

int QMDDsize(QMDDedge e)
// counts number of unique nodes in a QMDD
{
  Ncount=0;
  return(QMDDnodeCount(e));
}

void QMDDstatistics(void)
// displays QMDD package statistics
{
  printf("\nCurrent # nodes in unique tables: %ld\n\n",QMDDnodecount);
  printf("Total compute table lookups: %ld\n",CTlook[0]+CTlook[1]+CTlook[2]);
  printf("Number of ops: adds %d mults %d Kronecker %d\n",Nop[add],Nop[mult],Nop[kronecker]);
  printf("Compute table hit ratios: \naddition %d/%d %5.2f per cent \nmultiplication %d/%d %5.2f per cent \nKronecker product %d/%d %5.2f per ceent\n",CThit[add],CTlook[add],
        (float)CThit[add]/CTlook[add]*100,CThit[mult],CTlook[mult],(float)CThit[mult]/CTlook[mult]*100,CThit[kronecker],
        CTlook[kronecker],(float)CThit[kronecker]/CTlook[kronecker]*100);
  printf("UniqueTable Collisions: %d, Matches: %d\n", UTcol, UTmatch);
  
}

QMDDedge QMDDmakeColumn(complex c[],int first,int last,int n)
// makes a QMDD representation of a column vector
{
   QMDDedge e[MAXNEDGE],f;
   int d,start,i;
   
   if(first==last) // terminal case
   {
     f=QMDDmakeTerminal(c[first]);
     return(f);
   }
   d=(last-first+1)/Radix;
   start=first;
   for(i=0;i<Nedge;i++)
   {
     if(i%Radix==0)
     {
       e[i]=QMDDmakeColumn(c,start,start+d-1,n-1);
       start+=d;
     } else {
       e[i].p=NULL;
       e[i].w=0;
     }
   }
   f=QMDDmakeNonterminal(QMDDorder[n-1],e);
   return(f);
}

QMDDedge QMDDdiracket(short v,char value)
// build a column vector for variable v to
// represent |value> value = 0 or 1
{
  QMDDedge e[MAXNEDGE],f;
  if(value==0) 
  {
    e[0]=QMDDone;
    e[2]=QMDDzero;
  } else {
    e[0]=QMDDzero;
    e[2]=QMDDone;
  }
  e[1].p=e[3].p=NULL;
  e[1].w=e[3].w=0;
  f=QMDDmakeNonterminal(v,e);
  return(f);
}

void QMDDcolumnPrint(QMDDedge p,int n)
// print a column vector represented as a QMDD
// [values]'
// n is the number of variables
// vector is printed transposed
{
  printf(")[");
  recQMDDrcPrint(p,n-1,2);
  printf("]'\n");
}

QMDDedge QMDDmakeRow(complex c[],int first,int last,int n)
// makes a QMDD representation of a row vector
// STILL UNDER TEST - NOT RELIABLE
{
   QMDDedge e[MAXNEDGE],f;
   int mid;
   
   if(first==last) // terminal case
   {
     f=QMDDmakeTerminal(c[first]);
     return(f);
   }
   mid=(first+last)/2;
   e[0]=QMDDmakeRow(c,first,mid,n-1);
   e[1]=QMDDmakeRow(c,mid+1,last,n-1);
   e[2].p=e[3].p=NULL;
   e[2].w=e[3].w=0;
   f=QMDDmakeNonterminal(QMDDorder[n-1],e);
   return(f);
}

void QMDDrowPrint(QMDDedge p,int n)
// print a row vector represented as a QMDD in form
// [values]
// n is the number of variables
{
  printf(")[");
  recQMDDrcPrint(p,n-1,1);
  printf("]\n");
}

void QMDDprintActive(int n){
// print number of active nodes for variables 0 to n-1
  int i;
  printf("#printActive: %d. ", ActiveNodeCount);
  for (i=0; i<n;i++)
    printf(" %d ", Active[i]);
  printf("\n");
}

/***********************************************************************

This file contains routines for constructing QMDD for gates and
for reading reversible and quantum circuits in extended RevLib real
format.


    Michael Miller
    University of Victoria
    mmiller@uvic.ca

    Date: October 2008

To do list:


1.  currently no error checking of input from circuit files

***********************************************************************/

typedef complex QMDD_matrix[MAXRADIX][MAXRADIX];

/* GLOBALS */

QMDD_matrix Nm,Vm,VPm,Sm,Ss,Sss,C1m,C2m,N3m,Rm,Hm,Zm,ZEROm,Qm;

char input_order[MAXN][MAXSTRLEN],output_order[MAXN][MAXSTRLEN];

/*****************************************************************

    Routines
*****************************************************************/

void QMDDinitGateMatrices(void)
// initializes basic gate matrices
{
  complex v,vc;
  int i,j;


// init binary gate matrices
  v=Cmake(Qmake(1,0,2),Qmake(1,0,2));
  vc=Cmake(Qmake(1,0,2),Qmake(-1,0,2));
  Nm[0][0]=Nm[1][1]=Cmake(Dzero,Dzero);
  Nm[0][1]=Nm[1][0]=Cmake(Qmake(1,0,1),Dzero);
  Vm[0][0]=Vm[1][1]=v;
  Vm[0][1]=Vm[1][0]=vc;
  VPm[0][0]=VPm[1][1]=vc;
  VPm[0][1]=VPm[1][0]=v;
  Hm[0][0]=Hm[0][1]=Hm[1][0]=Cmake(Qmake(0,1,2),Dzero);
  Hm[1][1]=Cmake(Qmake(0,-1,2),Dzero);
  Zm[0][0]=Cmake(Qmake(1,0,1),Dzero);
  Zm[0][1]=Zm[1][0]=Cmake(Dzero,Dzero);
  Zm[1][1]=Cmake(Qmake(-1,0,1),Dzero);
  Sm[0][0]=Cmake(Qmake(1,0,1),Dzero);
  Sm[0][1]=Sm[1][0]=Cmake(Dzero,Dzero);
  Sm[1][1]=Cmake(Dzero,Qmake(1,0,1));
  ZEROm[0][0]=ZEROm[1][0]=Cmake(Qmake(1,0,1),Dzero);
  ZEROm[0][1]=ZEROm[1][0]=Cmake(Dzero,Dzero);
  Qm[0][0]=Cmake(Qmake(1,0,1),Dzero);
  Qm[0][1]=Qm[1][0]=Cmake(Dzero,Dzero);
// init ternary gate matrices
  for(i=0;i<3;i++)
    for(j=0;j<3;j++)
       C1m[i][j]=C2m[i][j]=N3m[i][j]=Ss[i][j]=Sss[i][j]=Cmake(Dzero,Dzero);
  N3m[0][2]=N3m[1][1]=N3m[2][0]=Cmake(Qmake(1,0,1),Dzero);
  C2m[0][1]=C2m[1][2]=C2m[2][0]=Cmake(Qmake(1,0,1),Dzero);
  C1m[0][2]=C1m[1][0]=C1m[2][1]=Cmake(Qmake(1,0,1),Dzero);
  Ss[0][0]=Ss[1][2]=Ss[2][1]=Cmake(Qmake(1,0,1),Dzero);
  Sss[0][2]=Sss[1][1]=Sss[2][0]=Cmake(Qmake(1,0,1),Dzero);
}

int getlabel(FILE *infile,QMDDrevlibDescription circ,int *cont)
// get a label and return its index in a global Label table
// cont is returned as 1 if character after label is not a '\n'

// this is a realy quick and dirty implementation that uses
// linear searching
{
  char ch;
  char lab[MAXSTRLEN];
  int i,k,found;

  ch=getstr(infile,lab);

  *cont=ch!='\n'; 					//set continuation control

  if(lab[0]==' ') return(-1);		// no label

  for(i=0;i<circ.n;i++) 			// lookup up label in table
  {
    if(0==strcmp(lab,circ.line[i].variable))
    {
      return(i); // return if found
    }
  }
  printf("label not found: %s\n",lab);
}

QMDDedge QMDDreadGate(FILE *infile,QMDDrevlibDescription *circ)
{
  int cont,i,j,m,n,t,line[MAXN];

  QMDDedge f,f2;
  char ch1,ch2,ch3;
  int div,pc[MAXN];

  n=(*circ).n;
  f.p=NULL;
  f.w=0;
  ch1=getch(infile);
  while(ch1==' '||ch1=='\n') ch1=getch(infile);
  while(ch1=='#')
  {
    skip2eol(infile);
    ch1=getch(infile);
  }
  if(ch1=='E'||(ch1=='.')) return(f);
  else
  {
    (*circ).ngates++;
    if(ch1!='M') // binary gate
    {

      if(ch1=='V'||ch1=='P'||ch1=='R') ch2=getch(infile); // get gate subtype designation

      // m is number of gate lines
      if(ch1=='N') m=1;
      else if(ch1=='C'||ch1=='V') m=2;
      else if(ch1=='P') m=3;
      else { // read number of lines
        ch3=getch(infile);
        m=ch3-'0';
        ch3=getch(infile);
        while(ch3>='0'&&ch3<='9')
        {
          m=m*10+ch3-'0';
          ch3=getch(infile);
        }
      }

      // for R or Q gate get divisor
      if(ch1=='R'||ch1=='Q')
      {
        if(ch3!=':')
        {
          printf("error in R/Q gate spec (missing or misplaced :)\n");
          exit(0);
        }
        ch3=getch(infile);
        if(ch3!='-') div=ch3-'0';
        else {
          ch3=getch(infile);
          div=-(ch3-'0');
        }
        if(ch1=='R') div*=2;  // Added June 16, 2007
      }


      // define line controls
      for(i=0;i<n;i++) line[i]=-1;
      for(i=0;i<m-1;i++)
      {
        line[j=getlabel(infile,*circ,&cont)]=1; // control line  NOTE embedded assignment to j
        pc[i]=j;
      }
      line[t=getlabel(infile,*circ,&cont)]=2;  // target line  NOTE embedded assignment to t

      // set f to point to QMDD for gate
      if(ch1=='T'||ch1=='C'||ch1=='N') // T, C or N gate
      {
        if(m==1||ch1=='N') circ->ngate=1;
        else if(m==2||ch1=='C') circ->cgate=1;
        else circ->tgate=1;
        f=TTlookup(n,m,t,line);
        if(f.p==NULL)
        {
          f=QMDDmvlgate(Nm,n,line);
          TTinsert(n,m,t,line,f);
        }
      }
      else if(ch1=='F') // Fredkin gate
      {
        circ->fgate=1;
        f=QMDDmvlgate(Nm,n,line);
        for(i=0;i<n;i++) line[i]=-1;
        line[t]=1;
        line[pc[m-2]]=2;
        f2=QMDDmvlgate(Nm,n,line);
        f=QMDDmultiply(f2,QMDDmultiply(f,f2));
      }
      else if(ch1=='P') // Peres gate
      {
        circ->pgate=1;
        f=QMDDmvlgate(Nm,n,line);
        line[t]=-1;
        line[pc[1]]=2;
        f2=QMDDmvlgate(Nm,n,line);
        if(ch2==' ') f=QMDDmultiply(f2,f);
        else if(ch2=='I') f=QMDDmultiply(f,f2);
        else printf("invalid subtype for Peres gate\n");
      }
      else if(ch1=='H') f=QMDDmvlgate(Hm,n,line); // Hadamard gate
      else if(ch1=='Z') f=QMDDmvlgate(Zm,n,line); // Pauli-Z gate
      else if(ch1=='S') f=QMDDmvlgate(Sm,n,line); // Phase gate
      else if(ch1=='0') f=QMDDmvlgate(ZEROm,n,line); // zero pseudo gate
      else if(ch1=='V')	// V or V+ gate
      {
        circ->vgate=1;
        if(ch2==' ') f=QMDDmvlgate(Vm,n,line);
        else if(ch2=='P'||ch2=='+') f=QMDDmvlgate(VPm,n,line);
        else {
        printf("invalid V subtype  '%c'\n",ch2);
        exit(0);
        }
      }
      else if(ch1=='Q')
      {
        Qm[1][1]=Cmake(QMDDcos(div),QMDDsin(div));
        f=QMDDmvlgate(Qm,n,line);
      }
      else if(ch1=='R') // Rotation gate
      {
  	    if(ch2=='X')
        {
		  Rm[0][0]=Rm[1][1]=Cmake(QMDDcos(div),Dzero);
          Rm[0][1]=Rm[1][0]=Cmake(Dzero,QMDDsin(-div));
        } else if(ch2=='Y')
        {
          Rm[0][0]=Rm[1][1]=Cmake(QMDDcos(div),Dzero);
          Rm[0][1]=Cmake(QMDDsin(-div),Dzero);
          Rm[1][0]=Cmake(QMDDsin(div),Dzero);
        } else if(ch2=='Z')
        {
          Rm[0][0]=Cmake(QMDDcos(div),QMDDsin(-div));
          Rm[0][1]=Rm[1][0]=Cmake(Dzero,Dzero);
          Rm[1][1]=Cmake(QMDDcos(div),QMDDsin(div));
        } else {
          printf("invalid rotation type  '%c'\n",ch2);
          exit(0);
        }
        f=QMDDmvlgate(Rm,n,line); // do rotation gate
      } else {
        printf("invalid gate type  '%c'\n",ch1);
        exit(0);
      }
    }
    else { // multiple valued gates
      for(i=0;i<n;i++) line[i]=-1;
      m=getch(infile)-'0';
      line[getlabel(infile,*circ,&cont)]=Radix;
      if(cont) do{
        i=getlabel(infile,*circ,&cont);
        j=getch(infile)-'0';
        line[i]=j;
        ch1=getch(infile);
      } while(ch1==',');
      if(m==1) f=QMDDmvlgate(C1m,n,line);  		// cycle by 1
      else if(m==2) f=QMDDmvlgate(C2m,n,line);  // cycle by 2
      else if(m==3) f=QMDDmvlgate(Ss,n,line);   // self shift
      else if(m==4) f=QMDDmvlgate(Sss,n,line);  // self single shift
      else if(m==5) f=QMDDmvlgate(N3m,n,line);  // self dual shift / negate
    }
  }
  return(f);
}

/*******************************************************************************/

QMDDrevlibDescription QMDDrevlibHeader(FILE *infile)
{
  int cont,header,n,p,i,j,k;
  char cmd[MAXSTRLEN],tLabel[MAXSTRLEN],tInput[MAXSTRLEN];
  char ch,ch1;
  CircuitLine temp;
  QMDDrevlibDescription circ;

  circ.nancillary=circ.ngarbage=0;
  header=1;
  while(header)
  {
    ch=getch(infile);
    if(ch=='#') skip2eol(infile);
    else {
      while(ch==' '||ch=='\n') ch=getch(infile);
      if(ch!='.')
      {
        printf("invalid file\n");
	    circ.n=0;
	    return(circ);
      }
      getstr(infile,cmd);
      if(0==strcmp(cmd,"BEGIN")) header=0;    // end of header information
      else if(0==strcmp(cmd,"VERSION"))
      {
        ch1=getch(infile);
        while(ch1==' ') ch1=getch(infile);
        p=0;
        while(ch1!='\n')
        {
          circ.version[p++]=ch1;
          ch1=getch(infile);
        }
        circ.version[p]=0;
      } else if(0==strcmp(cmd,"NUMVARS"))
      {
        circ.n=n=getint(infile);
        if(VERBOSE) printf("\nnumber of variables %d\n",n);
      } else if(0==strcmp(cmd,"VARIABLES"))
      {
        for(p=n-1;p>=0;p--)
        {
          getstr(infile,circ.line[p].variable);
          strcpy(circ.line[p].input,circ.line[p].variable);				// set inputs to variable names
          strcpy(circ.line[p].input,circ.line[p].variable);				// set output to variable names
          circ.line[p].ancillary=circ.line[p].garbage='-';  // by default no lines are ancillaries or garbage
        }
        Nlabel=n;
      } else if(0==strcmp(cmd,"INPUTS"))
      {
        for(p=n-1;p>=0;p--)
        {
          getstr(infile,circ.line[p].input);
        }
      } else if(0==strcmp(cmd,"OUTPUTS"))
      {
        for(p=n-1;p>=0;p--)
        {
          getstr(infile,circ.line[p].output);
        }
      } else if(0==strcmp(cmd,"CONSTANTS"))
      {
        for(p=n-1;p>=0;p--)
        {
          circ.line[p].ancillary=getnbch(infile);
          if(circ.line[p].ancillary!='-') circ.nancillary++;
        }
        skip2eol(infile);
      } else if(0==strcmp(cmd,"GARBAGE"))
      {
        for(p=n-1;p>=0;p--)
        {
          circ.line[p].garbage=getnbch(infile);
          if(circ.line[p].garbage!='-') circ.ngarbage++;
        }
        skip2eol(infile);
      } else if(0==strcmp(cmd,"DEFINE"))
      {
        while(strcmp(cmd,"ENDDEFINE"))
        {
          skip2eol(infile);
          ch1=getch(infile);
          getstr(infile,cmd);
        }
        //skip2eol(infile);
      }
    }
  }

  for(i=0;i<circ.n;i++) circ.inperm[i]=i;

  return(circ);
}

//////////////////////////////////////////////////////////////////////////////////////////////

QMDDrevlibDescription QMDDcircuitRevlib(char *fname,QMDDrevlibDescription firstCirc,int match)
// reads a circuit in Revlib format: http://www.revlib.org/documentation.php
{

  FILE *infile;

  QMDDrevlibDescription circ;

  int first,i,j,k,p,t,line[MAXN],order[MAXN],invorder[MAXN];
  char ch,tOutput[MAXSTRLEN],flag[MAXN],*cp,*cq,*cr;
  CircuitLine tline;

  QMDDedge e,f,olde;


// get name of input file, open it and attach it to file (a global)

  infile=openTextFile(fname,'r');

  circ=QMDDrevlibHeader(infile);


  circ.ngate=circ.cgate=circ.tgate=circ.fgate=circ.pgate=circ.vgate=0;

  if(match)   //  match input order of this circuit to the one in firctCirc
  {
    for(i=0;i<firstCirc.n;i++)
      if(firstCirc.line[i].ancillary=='-'&&strcmp(firstCirc.line[i].input,circ.line[i].input))
      {
        for(j=i+1;j<circ.n;j++)
          if(0==strcmp(firstCirc.line[i].input,circ.line[j].input)) break;
        if(j==circ.n) printf("error in line match\n");
        tline=circ.line[i];
        circ.line[i]=circ.line[j];
        circ.line[j]=tline;
      }
  }

  circ.ngates=0;

  first=1;
  while(1)
  {
    f=QMDDreadGate(infile,&circ);
    if(f.p==NULL) break;
    if(first) // first gate in circuit
    {
      first=0;
      e=f;
      QMDDincref(e);
    }
    else // second and subsequent gates
    {
      olde=e;
      e=QMDDmultiply(f,e); // multiply QMDD for gate * QMDD for circuit to date
      QMDDincref(e);
      QMDDdecref(olde);
      if(GCswitch) QMDDgarbageCollect();
    }
  }

  for(i=0;i<circ.n;i++) circ.outperm[i]=i;

  skip2eof(infile); // skip rest of input file

  circ.e=e;

  i=0;
  if(circ.ngate) circ.kind[i++]='N';
  if(circ.cgate) circ.kind[i++]='C';
  if(circ.tgate) circ.kind[i++]='T';
  if(circ.fgate) circ.kind[i++]='F';
  if(circ.pgate) circ.kind[i++]='P';
  if(circ.vgate) circ.kind[i++]='V';
  circ.kind[i]=0;

  i=0;
  if(circ.nancillary>0) circ.dc[i++]='C';
  if(circ.ngarbage>0) circ.dc[i++]='G';
  circ.dc[i]=0;

  fclose(infile);
  return(circ);
}


QMDDedge QMDDzeroDiagEntry(QMDDedge a,int k,int v)
{
  QMDDedge r,e[MAXNEDGE];
  int j,i;
  
  if(QMDDterminal(a)) return(QMDDzero);
  i=k;
  for(j=0;j<v;j++) i/=Radix;
  i=i%Radix;
  for(j=0;j<Nedge;j++) e[j]=a.p->e[j];
  i=i+Radix*i;
  e[i]=QMDDzeroDiagEntry(e[i],k,v-1);
  r=QMDDmakeNonterminal(v,e);
  return(r);
}

QMDDedge QMDDmakeDCmask(char *fname,QMDDrevlibDescription *circ)
// reads a function description in Revlib format: http://www.revlib.org/documentation.php 
// and builds a QMDD for the diagonal matrix don't-care mask
{
  
  FILE *infile;
  QMDDrevlibDescription func;
  int i,j,k,m,p,q,s[MAXN],t[MAXN],cnt;
  char ch,temp,out[MAXN],pdc,tdc;

  infile=openTextFile(fname,'r');
  if(infile==NULL)
  {
    func.e.p=NULL;
    return(func.e);
  }
   
  func=QMDDrevlibHeader(infile);
  
  pdc=tdc=0;

  if(func.n==0)
  {
    func.e.p=NULL;
    return(func.e);
  }
  
  for(i=0;i<func.n;i++) func.outperm[i]=i;

  func.e=QMDDident(0,func.n-1);
  m=1;
  for(i=0;i<func.n;i++) m*=Radix;
  
  for(i=0;i<m;i++)
  {
    p=i;
    for(j=0;j<func.n;j++)
    {
      s[j]=p%Radix;
      p/=Radix;
    }
    
    p=0;
    k=0;
    for(j=func.n-1;j>=0;j--)
    {
      t[j]=s[func.inperm[j]];
      if(func.line[j].ancillary!='-'&&t[j]!=func.line[j].ancillary-'0') tdc=p=1;
      k=k*Radix+t[j];
    }
    
    if(p==0)
    {
      cnt=0;
      for(j=func.n-1;j>=0;j--)
      {
        out[j]=getch(infile);
        if(out[j]=='-') cnt++;
      }
      if(cnt==func.n) tdc=1;
      else if(cnt>func.ngarbage) pdc=1;
    }
    
    if(p||cnt==func.n) 
    {
      func.e=QMDDzeroDiagEntry(func.e,k,func.e.p->v);
    }
    skip2eol(infile);
  }

  
  skip2eof(infile); // skip rest of input file

  i=strlen(circ->dc);
  if(tdc) circ->dc[i++]='T';
  if(pdc) circ->dc[i++]='P';
  circ->dc[i]=0;
  
  fclose(infile);
  QMDDincref(func.e);
  return(func.e);
}

/**************************************************************************/
/*   read a spec file                                                     */
/**************************************************************************/

QMDDedge QMDDsetEntry(QMDDedge e,int v,int r,int c,int val)
{
  QMDDedge ed[4];
  int i,j,k;
  if(v==-1)
  {
    if(val==1) return(QMDDone);
    else return(QMDDzero);
  }
  if(r>=(1<<v))
  {
    i=1;
    r=r-(1<<v);
  } else {
    i=0;
  }
  if(c>=(1<<v))
  {
    j=1;
    c=c-(1<<v);
  } else {
    j=0;
  }
  if(e.w==0)
    for(k=0;k<4;k++) ed[k]=e;
  else
    for(k=0;k<4;k++) ed[k]=e.p->e[k];
  k=i*Radix+j;
  ed[k]=QMDDsetEntry(ed[k],v-1,r,c,val);
  return(QMDDmakeNonterminal(v,ed));
}

int map(int k,int invorder[],int n)
{
  int r,i;
  r=0;
  for(i=0;i<n;i++)
  {
    if(k&1) r+=1<<invorder[i];
    k=k/2;
  }
  return(r);
}

QMDDrevlibDescription QMDDspecRevlib(char *fname)
{
  QMDDrevlibDescription func;
  FILE *infile;
  int i,j,k,in,kn;
  int order[MAXN],invorder[MAXN];
  char ch,tdc;
  CircuitLine temp;

  infile=openTextFile(fname,'r');

  func=QMDDrevlibHeader(infile);

  for(i=0;i<func.n;i++) order[i]=invorder[i]=i;

  // sort lines to position constant inputs

  func.e=QMDDzero;

  for(i=0;i<(1<<func.n);i++)
  {
    do{
      ch=getch(infile);
    } while((ch<'0'||ch>'9')&&(ch!='-'));
    k=0;
    tdc=1;
    for(j=0;j<func.n;j++)
    {
      if(ch=='-') ch='0';  // treat don't-care as zero in determining output pattern index
      else tdc=0;
      k=k*2+ch-'0';
      ch=getch(infile);
    }

    kn=map(k,invorder,func.n);
    in=map(i,invorder,func.n);
    //printf("debug i %d k %d in %d kn %d\n",i,k,in,kn);
    //pause();
    if(!tdc) func.e=QMDDsetEntry(func.e,func.n-1,kn,in,1);
  }
  fclose(infile);
  QMDDincref(func.e);
  return(func);
}

using namespace boost::assign;
using namespace cirkit;

typedef boost::variant<std::string, bool> qmdd_vertex;
typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::bidirectionalS, boost::property<boost::vertex_name_t, qmdd_vertex> > qmdd;

boost::graph_traits<qmdd>::vertex_descriptor qmdd_init( qmdd& dd, const QMDDrevlibDescription& circ )
{
  typedef boost::graph_traits<qmdd>::vertex_descriptor vertex_t;

  // number of vertices
  ListElementPtr first, pnext, q, lastq;
  first = QMDDnewListElement();
  first->p = circ.e.p;
  first->next = 0;

  pnext = first;
  while ( pnext )
  {
    vertex_t v = add_vertex( dd );
    //boost::get( boost::vertex_name, dd )[v] = boost::str( boost::format( "x%d" ) % (unsigned)pnext->p->v );
    boost::get( boost::vertex_name, dd )[v] = std::string( circ.line[(unsigned)pnext->p->v].input );

    if ( pnext->p != QMDDzero.p )
    {
      for ( unsigned j = 0u; j < 4u; ++j )
      {
        if ( pnext->p->e[j].p && !QMDDterminal( pnext->p->e[j] ) )
        {
          q = first->next;
          lastq = first;
          while ( q && pnext->p->e[j].p != q->p )
          {
            lastq = q;
            q = q->next;
          }
          if ( !q )
          {
            q = QMDDnewListElement();
            q->p = pnext->p->e[j].p;
            q->next = NULL;
            lastq->next = q;
          }
        }
      }
    }
    else
    {
      std::cout << "Here" << std::endl;
    }

    pnext = pnext->next;
  }

  // Add two vertices for 1 and 0 terminals
  vertex_t term0 = boost::add_vertex( dd );
  vertex_t term1 = boost::add_vertex( dd );

  boost::get( boost::vertex_name, dd )[term0] = false;
  boost::get( boost::vertex_name, dd )[term1] = true;

  // Edges
  unsigned n, i;

  first=QMDDnewListElement();
  first->p = circ.e.p;
  first->next = 0;
  first->w = 0;
  n = 0u;
  i = 0u;

  pnext = first;

  while ( pnext )
  {
    if ( pnext->p != QMDDzero.p )
    {
      for ( unsigned j = 0u; j < 4u; j++ )
      {
        if ( pnext->p->e[j].p )
        {
          unsigned target = 0u;
          if( !QMDDterminal( pnext->p->e[j] ) )
          {
            q = first->next;
            lastq = first;
            while ( q && pnext->p->e[j].p != q->p )
            {
              lastq = q;
              q = q->next;
            }

            if( !q )
            {
              q = QMDDnewListElement();;
              q->p = pnext->p->e[j].p;
              q->next = NULL;
              q->w = n = n + 1u;
              lastq->next = q;
            }
            target = q->w;
          }
          else
          {
            target = Cvalue( pnext->p->e[j].w ).r == 1.0 ? term1 : term0;
          }

          boost::add_edge( i, target, dd );
        }
      }
    }
    ++i;
    pnext = pnext->next;
  }

  // 0 is always the root vertex
  return 0;
}

void write_graphviz( std::ostream& os, const qmdd& dd )
{
  os << "digraph G {" << std::endl;
  os << "  graph [ordering=out];" << std::endl;

  std::string colors[] = { "darkgreen", "blue", "red", "gold" };
  typedef boost::graph_traits<qmdd>::vertex_descriptor vertex_t;
  for ( const auto& v : boost::make_iterator_range( boost::vertices( dd ) ) )
  {
    const qmdd_vertex& vtx = boost::get( boost::vertex_name, dd )[v];
    const bool* value;
    if ( const std::string* label = boost::get<std::string>( &vtx ) )
    {
      os << boost::format( "  v%d [label=\"%s\",style=filled,fillcolor=lightgray];" ) % v % *label << std::endl;
      for ( unsigned j = 0u; j < 4u; ++j )
      {
        os << boost::format( "  v%dp%d [label=\"\",shape=point];" ) % v % j << std::endl;
      }

      os << boost::format( "  {rank=same; v%1%p0 v%1%p1 v%1%p2 v%1%p3}" ) % v << std::endl;
      for ( unsigned j = 0u; j < 4u; ++j )
      {
        os << boost::format( "  v%1% -> v%1%p%2% [arrowhead=none,color=%3%];" ) % v % j % colors[j] << std::endl;
      }
    }
    else if ( ( value = boost::get<bool>( &vtx ) ) && *value )
    {
      os << boost::format( "  v%d [label=\"1\",style=filled,fillcolor=lightgray,shape=box]" ) % v << std::endl;
    }
  }

  for ( const auto& v : boost::make_iterator_range( boost::vertices( dd ) ) )
  {
    if ( v < boost::num_vertices( dd ) - 2u )
    {
      unsigned j = 0u;
      typedef boost::graph_traits<qmdd>::edge_descriptor edge_t;
      for ( const auto& e : boost::make_iterator_range( boost::out_edges( v, dd ) ) )
      {
        const vertex_t& source = boost::source( e, dd );
        const vertex_t& target = boost::target( e, dd );

        if ( target != boost::num_vertices( dd ) - 2u )
        {
          os << boost::format( "  v%dp%d -> v%d;" ) % source % j % target << std::endl;
        }
        ++j;
      }
    }
  }

  os << "}" << std::endl;
}

// Helpers
typedef std::list<boost::optional<bool> > signature_t;
typedef boost::dynamic_bitset<> path_t;

void apply_toffoli( QMDDrevlibDescription& spec, circuit& circ, const std::list<unsigned>& controls, unsigned target )
{
  int line[MAXN];
  unsigned n = spec.n;

  for ( unsigned i = 0u; i < n; ++i ) line[i] = -1;

  unsigned m = controls.size() + 1u;
  for ( auto c : controls )
  {
    line[n - 1 - c] = 1;
  }

  unsigned t = n - 1 - target;
  line[t] = 2;

  QMDDedge f = TTlookup( n, m, t, line );
  if ( !f.p )
  {
    f = QMDDmvlgate( Nm, n, line );
    TTinsert( n, m, t, line, f );
  }

  QMDDedge e = QMDDmultiply( spec.e, f );
  QMDDincref( e );
  QMDDdecref( spec.e );
  if ( GCswitch ) QMDDgarbageCollect();
  spec.e = e;

  //std::vector<unsigned> revkit_controls( controls.begin(), controls.end() );
  //append_toffoli( circ, revkit_controls, target );
}

void apply_gate( QMDDrevlibDescription& spec, circuit& circ, const signature_t& controls, unsigned target )
{
  std::list<unsigned> negative;
  std::list<unsigned> cs;
  gate::control_container pcs;

  unsigned i = 0u;
  for ( signature_t::const_iterator it = controls.begin(); it != controls.end(); ++it, ++i )
  {
    if ( *it )
    {
      pcs += make_var( i, **it );
      cs += i;

      if ( !**it ) // negative control
      {
        negative += i;
      }
    }
  }

  std::list<unsigned> negcontrols;
  for ( auto neg : negative )
  {
    apply_toffoli( spec, circ, negcontrols, neg );
  }

  apply_toffoli( spec, circ, cs, target );
  append_toffoli( circ, pcs, target );

  for ( auto neg : negative )
  {
    apply_toffoli( spec, circ, negcontrols, neg );
  }
}

QMDDnodeptr get_node_by_signature( const QMDDrevlibDescription& spec, const signature_t& sig )
{
  QMDDnodeptr p = spec.e.p;

  for ( signature_t::const_iterator it = sig.begin(); it != sig.end(); ++it )
  {
    if ( !*it || **it )
    {
      p = p->e[3].p;
    }
    else
    {
      p = p->e[0].p;
    }
  }

  return p;
}

void get_one_paths( QMDDnodeptr v, std::set<path_t>& paths )
{
  std::deque<boost::tuple<QMDDnodeptr, path_t> > stubs;

  stubs.push_front( boost::make_tuple( v, path_t() ) );

  while ( !stubs.empty() )
  {
    const boost::tuple<QMDDnodeptr, path_t>& t = stubs.front();
    QMDDnodeptr vtx = boost::get<0>( t );
    const path_t& stub = boost::get<1>( t );

    if ( vtx == QMDDtnode )
    {
      paths += stub;
    }
    else
    {
      for ( unsigned j = 0u; j < 4u; ++j )
      {
        if ( Cvalue( vtx->e[j].w ).r != 0.0 ) // if not zero terminal
        {
          path_t path = stub;
          path.push_back( (bool)( j % 2 ) ); // negative or positive
          stubs.push_back( boost::make_tuple( vtx->e[j].p, path ) );
        }
      }
    }

    stubs.pop_front();
  }
}

DdNode* get_one_paths2( QMDDnodeptr v, DdManager* dd, unsigned offset )
{
  std::deque<boost::tuple<QMDDnodeptr, path_t> > stubs;

  stubs.push_front( boost::make_tuple( v, path_t() ) );

  DdNode *paths = Cudd_ReadLogicZero( dd );
  Cudd_Ref( paths );

  while ( !stubs.empty() )
  {
    const boost::tuple<QMDDnodeptr, path_t>& t = stubs.front();
    QMDDnodeptr vtx = boost::get<0>( t );
    const path_t& stub = boost::get<1>( t );

    if ( vtx == QMDDtnode )
    {
      DdNode *tmp;
      DdNode *cube = Cudd_ReadOne( dd );
      Cudd_Ref( cube );

      for ( unsigned i = 0u; i < stub.size(); ++i )
      {
        DdNode *var = stub.test( i ) ? Cudd_bddIthVar( dd, offset + i ) : Cudd_Not( Cudd_bddIthVar( dd, offset + i ) );
        Cudd_Ref( var );

        tmp = Cudd_bddAnd( dd, cube, var );
        Cudd_Ref( tmp );
        Cudd_RecursiveDeref( dd, var );
        Cudd_RecursiveDeref( dd, cube );
        cube = tmp;
      }

      tmp = Cudd_bddOr( dd, paths, cube );
      Cudd_Ref( tmp );
      Cudd_RecursiveDeref( dd, cube );
      Cudd_RecursiveDeref( dd, paths );
      paths = tmp;
    }
    else
    {
      for ( unsigned j = 0u; j < 4u; ++j )
      {
        if ( Cvalue( vtx->e[j].w ).r != 0.0 ) // if not zero terminal
        {
          path_t path = stub;
          path.push_back( (bool)( j % 2 ) ); // negative or positive
          stubs.push_back( boost::make_tuple( vtx->e[j].p, path ) );
        }
      }
    }

    stubs.pop_front();
  }

  return paths;
}

boost::dynamic_bitset<>& inc( boost::dynamic_bitset<>& bitset )
{
  for ( boost::dynamic_bitset<>::size_type i = 0; i < bitset.size(); ++i )
  {
    bitset.flip( i );
    if ( bitset.test( i ) ) break;
  }
  return bitset;
}

DdNode* node_from_bitset( DdManager *dd, const path_t& nu_path, unsigned offset )
{
  DdNode* tmp;
  DdNode* cube = Cudd_ReadOne( dd );
  Cudd_Ref( cube );

  for ( unsigned i = 0u; i < nu_path.size(); ++i )
  {
    DdNode* var = nu_path.test( i ) ? Cudd_bddIthVar( dd, i + offset ) : Cudd_Not( Cudd_bddIthVar( dd, i + offset ) );
    Cudd_Ref( var );

    tmp = Cudd_bddAnd( dd, cube, var );
    Cudd_Ref( tmp );
    Cudd_RecursiveDeref( dd, cube );
    Cudd_RecursiveDeref( dd, var );
    cube = tmp;
  }

  return cube;
}

// Real Synthesis procedure
void qmdd_synthesis( circuit& circ, QMDDrevlibDescription& spec )
{
  DdManager *dd = Cudd_Init( spec.n, 0u, 0u, 0u, 0ul );
  char* cube_str = new char[spec.n];

  // Metadata
  circ.set_lines( spec.n );

  // This describes the queue for the BFS to process
  std::deque<signature_t> vertices;

  // We start with the first root vertex
  vertices.push_front( signature_t() );

  unsigned step = 0u;

  while ( !vertices.empty() )
  {
    const signature_t& sig = vertices.front();
    QMDDnodeptr v = get_node_by_signature( spec, sig );

    if ( v != QMDDtnode && !v->ident )
    {
#if VERBOSE
      std::cout << "Process node " << (unsigned)v->v << std::endl;
#endif

      while ( Cvalue( v->e[1].w ).r != 0.0 )
      {
        // Do something
        ++step;

#if WRITESTEPS
        qmdd q;
        qmdd_init( q, spec );

        std::ofstream file( boost::str( boost::format( "/tmp/qmdd_debug%d.dot" ) % step ).c_str() );
        write_graphviz( file, q );
        file.close();

        std::cout << "++++++++++++++++++++++++++++++++++++++++" << std::endl;
        std::cout << "Step " << step << std::endl;
        std::cout << circ << std::endl;
        std::cout << "++++++++++++++++++++++++++++++++++++++++" << std::endl;

        //if ( step == 4u ) return;
#endif

        // Special case: n is 0 node
        if ( Cvalue( v->e[0].w ).r == 0.0 )
        {
#if VERBOSE
          std::cout << "Trivial case: Switch Paths" << std::endl;
#endif

          apply_gate( spec, circ, sig, sig.size() );
          v = get_node_by_signature( spec, sig ); // recalculate
        }
#if GRAYCODE_TRIVIA_CHECK
        else if ( Cvalue( v->e[0].p->e[1].w ).r == 0.0 && Cvalue( v->e[0].p->e[3].w ).r == 0.0 &&
                  Cvalue( v->e[1].p->e[0].w ).r == 0.0 && Cvalue( v->e[1].p->e[2].w ).r == 0.0 )
        {
          signature_t sigc = sig;
          sigc += boost::optional<bool>(),true;
          apply_gate( spec, circ, sigc, sig.size() );
          v = get_node_by_signature( spec, sig ); // recalculate
        }
#endif
        else
        {
          // Cudd Manager
          unsigned offset = sig.size() + 1u;

          DdNode *ddn = get_one_paths2( v->e[0].p, dd, offset );
          DdNode *ddp = get_one_paths2( v->e[1].p, dd, offset );

#if VERBOSE
          std::cout << "Case: Move paths (Size: " << Cudd_CountPathsToNonZero( ddn ) << "/" << Cudd_CountPathsToNonZero( ddp ) << ")" << std::endl;
#endif

          if ( Cudd_CountPathsToNonZero( ddp ) > Cudd_CountPathsToNonZero( ddn ) )
          {
            //assert( false );
            apply_gate( spec, circ, sig, sig.size() );
            DdNode *tmp = ddn;
            ddn = ddp;
            ddp = tmp;
          }

          // Sifting
#if BDD_SIFTING
          Cudd_ReduceHeap( dd, CUDD_REORDER_SIFT, 0 );
#endif

          // Unique paths
          DdNode *unique_paths = Cudd_bddAnd( dd, ddp, Cudd_Not( ddn ) );
          Cudd_Ref( unique_paths );

          DdNode *non_unique_paths = Cudd_bddAnd( dd, ddp, ddn );
          Cudd_Ref( non_unique_paths );

#if VERBOSE
          std::cout << "N Paths" << std::endl;
          Cudd_PrintMinterm( dd, ddn );

          std::cout << "Unique Paths" << std::endl;
          Cudd_PrintMinterm( dd, unique_paths );
#endif

          // Move Unique paths
          while ( unique_paths != Cudd_ReadLogicZero( dd ) )
          {
            DdNode *tmp;
            DdNode *cube = Cudd_ReadOne( dd );
            Cudd_Ref( cube );

            signature_t sigc = sig;
            sigc += boost::optional<bool>();

            Cudd_bddPickOneCube( dd, unique_paths, cube_str );
            for ( unsigned i = offset; i < spec.n; ++i )
            {
              DdNode* var = 0;
              switch ( (unsigned)cube_str[i] )
              {
              case 0:
                var = Cudd_Not( Cudd_bddIthVar( dd, i ) );
                sigc += false;
                break;
              case 1:
                var = Cudd_bddIthVar( dd, i );
                sigc += true;
                break;
              case 2:
                sigc += boost::optional<bool>();
                break;
              default:
                assert( false );
              }

              if ( var )
              {
                Cudd_Ref( var );
                tmp = Cudd_bddAnd( dd, cube, var );
                Cudd_Ref( tmp );
                Cudd_RecursiveDeref( dd, cube );
                Cudd_RecursiveDeref( dd, var );
                cube = tmp;
              }
            }

            apply_gate( spec, circ, sigc, sig.size() );

            tmp = Cudd_bddAnd( dd, unique_paths, Cudd_Not( cube ) );
            Cudd_Ref( tmp );
            Cudd_RecursiveDeref( dd, unique_paths );
            unique_paths = tmp;

            tmp = Cudd_bddOr( dd, ddn, cube );
            Cudd_Ref( tmp );
            Cudd_RecursiveDeref( dd, ddn );
            Cudd_RecursiveDeref( dd, cube );
            ddn = tmp;
          }

          Cudd_RecursiveDeref( dd, unique_paths );

          // Sifting
#if BDD_SIFTING
          Cudd_ReduceHeap( dd, CUDD_REORDER_SIFT, 0 );
#endif

          while ( non_unique_paths != Cudd_ReadLogicZero( dd ) )
          {
            // TODO expensive solution?

            // Get a non unique path (- is 0)
            Cudd_bddPickOneCube( dd, non_unique_paths, cube_str );
            boost::dynamic_bitset<> nu_path( spec.n - offset );

            for ( unsigned i = offset; i < spec.n; ++i )
            {
              nu_path.set( i - offset, (unsigned)cube_str[i] == 1 );
            }
            DdNode* nu_node = node_from_bitset( dd, nu_path, offset );

#if VERBOSE
            std::cout << "N Paths" << std::endl;
            Cudd_PrintMinterm( dd, ddn );

            std::cout << "NOT(N) Paths" << std::endl;
            Cudd_PrintMinterm( dd, Cudd_Not( ddn ) );

            std::cout << "Non Unique Paths" << std::endl;
            Cudd_PrintMinterm( dd, non_unique_paths );

            std::cout << "Chosen path: " << nu_path << std::endl;
#endif

            // Get a path that does not exist in ddn (adjust - to nu_cube)
            Cudd_bddPickOneCube( dd, Cudd_Not( ddn ), cube_str );

            boost::dynamic_bitset<> b_new_cube( spec.n - offset );

            for ( unsigned i = offset; i < spec.n; ++i )
            {
              switch ( (unsigned)cube_str[i] )
              {
              case 0:
              case 1:
                b_new_cube.set( i - offset, (unsigned)cube_str[i] );
                break;
              case 2:
                b_new_cube.set( i - offset, nu_path.test( i - offset ) );
                break;
              default:
                std::cout << "Value: " << (unsigned)cube_str[i] << std::endl;
                assert( false );
              }
            }
            DdNode* new_node = node_from_bitset( dd, b_new_cube, offset );

#if VERBOSE
            std::cout << "Picked Cube from NOT(N): " << b_new_cube << std::endl;
            //int cancel;
            //std::cin >> cancel;
#endif

            // Remove nu_cube from non_unique_paths
            DdNode* tmp = Cudd_bddAnd( dd, non_unique_paths, Cudd_Not( nu_node ) );
            Cudd_Ref( tmp );
            Cudd_RecursiveDeref( dd, non_unique_paths );
            non_unique_paths = tmp;

            // Which bits are different?
            std::vector<unsigned> to_flip;
            for ( unsigned b = 0u; b < nu_path.size(); ++b )
            {
              if ( b_new_cube.test( b ) != nu_path.test( b ) )
              {
                to_flip += b;
              }
            }

            // Adjust path by flipping different bits
            for ( auto flip : to_flip )
            {
              // uniquify
              signature_t sigc = sig;
              sigc += true;
              for ( unsigned i = 0u; i < nu_path.size(); ++i )
              {
                if ( i != flip )
                {
                  sigc += (bool)nu_path.test( i );
                }
                else
                {
                  sigc += boost::optional<bool>();
                }
              }

              apply_gate( spec, circ, sigc, sig.size() + 1u + flip );

              nu_path.flip( flip );

              DdNode *next_nu_node = node_from_bitset( dd, nu_path, offset );

              // Did I change another path?
              DdNode *tmp;
              tmp = Cudd_bddAnd( dd, non_unique_paths, next_nu_node );
              Cudd_Ref( tmp );
              bool found = ( tmp != Cudd_ReadLogicZero( dd ) );
              Cudd_RecursiveDeref( dd, tmp );
              if ( found )
              {
                // remove next_nu_node
                tmp = Cudd_bddAnd( dd, non_unique_paths, Cudd_Not( next_nu_node ) );
                Cudd_Ref( tmp );
                Cudd_RecursiveDeref( dd, non_unique_paths );
                non_unique_paths = tmp;

                // put old one
                tmp = Cudd_bddOr( dd, non_unique_paths, nu_node );
                Cudd_Ref( tmp );
                Cudd_RecursiveDeref( dd, non_unique_paths );
                non_unique_paths = tmp;
              }

              Cudd_RecursiveDeref( dd, nu_node );
              nu_node = next_nu_node;
            }

            assert( nu_node == new_node );

            // Now we can apply a unique gate
            signature_t sigc = sig;
            sigc += boost::optional<bool>();
            for ( unsigned b = 0u; b < nu_path.size(); ++b )
            {
              sigc += nu_path.test( b );
            }
            apply_gate( spec, circ, sigc, sig.size() );

            // Add this path to the N side
            tmp = Cudd_bddOr( dd, ddn, nu_node );
            Cudd_Ref( tmp );
            Cudd_RecursiveDeref( dd, ddn );
            ddn = tmp;

            Cudd_RecursiveDeref( dd, new_node );
            Cudd_RecursiveDeref( dd, nu_node );

            //assert( false );
          }

          Cudd_RecursiveDeref( dd, ddn );
          Cudd_RecursiveDeref( dd, ddp );

          //assert( false );

          v = get_node_by_signature( spec, sig ); // recalculate
        }
      }

      // Successors
      if ( v->e[0].p != v->e[3].p )
      {
        signature_t sig0 = sig;
        sig0 += false;
        signature_t sig1 = sig;
        sig1 += true;
        vertices.push_back( sig0 );
        vertices.push_back( sig1 );
      }
      else
      {
        signature_t sigb = sig;
        sigb += boost::optional<bool>();
        vertices.push_back( sigb );
      }
    }

    vertices.pop_front();
  }

  delete[] cube_str;
  Cudd_Quit( dd );
}

struct not_gate
{
  typedef int result_type;

  int operator()( const gate& g ) const
  {
    return ( g.controls().size() == 0u ) ? 1 : 0;
  }
};

int main( int argc, char ** argv )
{
  QMDDinit( 0 );

  QMDDrevlibDescription desc;

  {
    std::cout << "Reading QMDD" << std::endl;
    print_timer t;

    std::string filename( argv[1] );
    if ( filename.substr( filename.size() - 4u ) == "spec" )
    {
      desc = QMDDspecRevlib( argv[1] );
    }
    else
    {
      desc = QMDDcircuitRevlib( argv[1], desc, 0 );
    }
  }

  circuit circ;

#if VERBOSE
  QMDDprint( desc.e, 100 );
#endif

  long node_count = ActiveNodeCount;

  double runtime;
  {
    reference_timer t( &runtime );
    qmdd_synthesis( circ, desc );
  }
#if VERBOSE
  QMDDprint( desc.e, 100 );
#endif

  // Adjust negative controls calculation (Number of NOT gates)
  using boost::adaptors::transformed;
  int not_gates = boost::accumulate( circ | transformed( not_gate() ), 0 );

  print_statistics_settings settings;
  settings.main_template += boost::str( boost::format( "NOT:              %d\nNode Count:       %d\n" ) % not_gates % node_count );
  print_statistics( circ, runtime, settings );

#if WRITESTEPS
  write_realization( circ, "/tmp/qmdd.real" );
#endif

  return 0;
}

#else

int main( int argc, char ** argv )
{
  std::cout << "[e] program requires reversible addon enabled" << std::endl;
  return 1;
}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
