/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @author Mathias Soeken
 */

#include <iostream>

#include <boost/format.hpp>

#include <reversible/functions/extend_pla.hpp>
#include <reversible/io/print_circuit.hpp>
#include <reversible/io/print_statistics.hpp>
#include <reversible/io/read_pla.hpp>
#include <reversible/io/read_realization.hpp>
#include <reversible/io/write_pla.hpp>
#include <reversible/io/write_realization.hpp>
#include <reversible/synthesis/embed_pla.hpp>
#include <reversible/synthesis/symbolic_transformation_based_synthesis.hpp>
#include <reversible/utils/reversible_program_options.hpp>

using namespace cirkit;

int main( int argc, char ** argv )
{
  using boost::program_options::value;

  std::string filename;

  reversible_program_options opts;
  opts.add_write_realization_option();
  opts.add_options()
    ( "filename",        value( &filename ), "PLA filename" )
    ( "print_circuit,c",                     "Prints the circuit" )
    ( "verbose,v",                           "Be verbose" )
    ;

  opts.parse( argc, argv );

  if ( !opts.good() )
  {
    std::cout << opts << std::endl;
    return 1;
  }

  binary_truth_table pla, extended;
  rcbdd cf;
  circuit circ;

  read_pla_settings settings;
  settings.extend = false;
  if ( opts.is_set( "verbose" ) ) { std::cout << "[i] read PLA" << std::endl; }
  read_pla( pla, filename, settings );
  if ( opts.is_set( "verbose" ) ) { std::cout << "[i] extend PLA" << std::endl; }
  extend_pla( pla, extended );
  write_pla( extended, "/tmp/extended.pla" );

  auto ep_settings = std::make_shared<properties>();
  if ( opts.is_set( "verbose" ) ) { std::cout << "[i] embed PLA" << std::endl; }
  embed_pla( cf, "/tmp/extended.pla", ep_settings );

  properties::ptr tbs_settings( new properties );
  properties::ptr tbs_statistics( new properties );
  tbs_settings->set( "verbose", opts.is_set( "verbose" ) );

  if ( opts.is_set( "verbose" ) )
  {
    std::cout << "[i] number of variables: " << cf.num_vars() << std::endl;
  }

  symbolic_transformation_based_synthesis( circ, cf, tbs_settings, tbs_statistics );

  if ( opts.is_set( "print_circuit" ) )
  {
    std::cout << circ << std::endl;
  }

  if ( opts.is_write_realization_filename_set() )
  {
    write_realization( circ, opts.write_realization_filename() );
  }

  print_statistics( circ, tbs_statistics->get<double>( "runtime" ) );

  return 0;
}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
