/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "abc_cec.hpp"

#include <abc/abc_api.hpp>
#include <abc/utils/abc_run_command.hpp>
#include <classical/functions/simulate_aig.hpp>
#include <classical/utils/aig_utils.hpp>
#include <core/utils/range_utils.hpp>
#include <core/utils/timer.hpp>

#include <boost/format.hpp>

namespace cirkit
{

void abc_write_aig( const aig_graph& aig, const std::string& filename )
{
  abc_run_command_no_output( aig, ( boost::format( "&w %s;" ) % filename ).str() );
}

boost::optional< counterexample_t > abc_cec( const aig_graph& aig1, const aig_graph& aig2,
                                             properties::ptr settings, properties::ptr statistics )
{
  /* Timer */
  properties_timer t( statistics );

  const std::string aig_file1 = ( boost::format( "/tmp/aig1-%s.aig" ) % getpid() ).str();
  const std::string aig_file2 = ( boost::format( "/tmp/aig2-%s.aig" ) % getpid() ).str();
  abc_write_aig( aig1, aig_file1 );
  abc_write_aig( aig2, aig_file2 );

  /*** note that the assignment only contains the inputs ***/
  boost::optional< boost::dynamic_bitset<> > assignment;
  assignment = abc_run_command_get_counterexample( ( boost::format("&r %s; &miter %s; &cec -s -m") % aig_file1 % aig_file2 ).str() );

  if ( assignment )
  {
    assert( aig_info( aig1 ).inputs.size() == aig_info( aig2 ).inputs.size() );
    assert( aig_info( aig1 ).inputs.size() == assignment->size() );
    assert( aig_info( aig1 ).outputs.size() == aig_info( aig2 ).outputs.size() );

    const auto num_inputs = aig_info( aig1 ).inputs.size();
    const auto num_outputs = aig_info( aig1 ).outputs.size();

    counterexample_t cex( num_inputs, num_outputs );

    /* input assignment */
    /*
      cex.in.bits[ 0u ] === first input
      ...
      cex.in.bits[ num_inputs-1u ] === last input
    */
    for ( auto i = 0u; i < num_inputs; ++i )
    {
      cex.in.bits[i] = (*assignment)[num_inputs-i-1u ];
      cex.in.mask[i] = 1u;
    }

    /* resimulate to determine outputs */
    auto settings = std::make_shared<properties>();
    settings->set( "verbose", false );

    simple_assignment_simulator::aig_name_value_map m;
    for ( auto i = 0u; i < aig_info( aig1 ).inputs.size(); ++i )
    {
      m.insert( { aig_info( aig1 ).node_names.at( aig_info( aig1 ).inputs.at( i ) ), cex.in.bits[i] == 1 } );
    }
    simple_assignment_simulator sim( m );

    /* output assignment */
    /*
      cex.out.bits[ 0u ] === first output
      ...
      cex.out.bits[ num_outputs-1u ] === last output
    */
    auto result = simulate_aig( aig1, sim, settings );
    for ( const auto& p : index( aig_info( aig1 ).outputs ) )
    {
      // std::cout << boost::format( "[i] %s : %d" ) % p.value.second % result.at( p.value.first ) << std::endl;
      cex.out.bits[ p.index ] = result.at( p.value.first );
      cex.out.mask[ p.index ] = 1u;
    }

    /* expected output assignment */
    result = simulate_aig( aig2, sim, settings );
    for ( const auto& p : index( aig_info( aig2 ).outputs ) )
    {
      // std::cout << boost::format( "[i] %s : %d" ) % p.value.second % result.at( p.value.first ) << std::endl;
      cex.expected_out.bits[ p.index ] = result.at( p.value.first );
      cex.expected_out.mask[ p.index ] = 1u;
    }

    return boost::optional< counterexample_t >( cex );
  }

  return boost::optional< counterexample_t >();
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
