/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cirkit_to_gia.hpp"

#include <cstring>

#include <classical/utils/aig_utils.hpp>

namespace cirkit
{

abc::Gia_Man_t* cirkit_to_gia( const aig_graph& aig )
{
  const auto& info = aig_info( aig );
  const auto& complement_map = boost::get( boost::edge_complement, aig );

  const unsigned _num_inputs = info.inputs.size();
  const unsigned _num_outputs = info.outputs.size();
  const unsigned _num_latches = info.cis.size();
  const unsigned _num_vertices = num_vertices( aig ) - 1u;
  const unsigned _num_gates = _num_vertices - _num_latches - _num_inputs;

  assert( _num_vertices == _num_inputs + _num_latches + _num_gates );

  /* allocate an empty aig in abc */
  abc::Gia_Man_t * gia = abc::Gia_ManStart( _num_vertices + _num_latches + _num_outputs + 1u );
  gia->nConstrs = 0;
  gia->pName = strcpy( (char*)malloc( sizeof( char ) * ( info.model_name.size() + 1u ) ), info.model_name.c_str() );

  std::map< int, int > nodes;

  /* inputs */
  for ( const auto& input : info.inputs )
  {
    const int obj = abc::Gia_ManAppendCi( gia );
    nodes.insert( { obj,input } );
  }

  /* latches */
  assert( info.cis.size() == info.cos.size() );
  assert( _num_latches == 0u );

  /* and gates */
  for ( const auto& node : boost::make_iterator_range( vertices( aig ) ) )
  {
    const auto _out_degree = out_degree( node, aig );
    if ( _out_degree )
    {
      assert( _out_degree == 2u );

      typename boost::graph_traits< aig_graph >::out_edge_iterator it, ie;
      boost::tie( it, ie ) = out_edges( node, aig );

      /* zero node */
      const auto zero_id = target( *it, aig );
      const auto zero_compl = complement_map( *it );
      ++it;

      /* one node */
      const auto one_id = target( *it, aig );
      const auto one_compl = complement_map( *it );
      assert( ++it == ie );

      const int obj = abc::Gia_ManAppendAnd( gia, abc::Abc_Var2Lit( zero_id, zero_compl ), abc::Abc_Var2Lit( one_id, one_compl ) );
      nodes.insert( { obj, node } );
    }
  }

  /* outputs */
  for ( const auto& output : info.outputs )
  {
    abc::Gia_ManAppendCo( gia, abc::Abc_Var2Lit( output.first.node, output.first.complemented ) );
  }

  return gia;
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
