/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "aig.hpp"

#include <core/utils/program_options.hpp>
#include <classical/functions/mig_to_aig.hpp>
#include <classical/utils/aig_utils.hpp>

using namespace boost::program_options;

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

aig_command::aig_command( const environment::ptr& env )
  : mig_base_command( env, "Creates an MIG from an AIG" ),
    aigs( env->store<aig_graph>() )
{
  opts.add_options()
    ( "new,n",                  "Add a new entry to the store; if not set, the current entry is overriden" )
    ( "name",   value( &name ), "Override default name" )
    ;
  be_verbose();
}

bool aig_command::execute()
{
  if ( aigs.empty() || opts.is_set( "new" ))
  {
    aigs.extend();
  }

  auto settings = make_settings();
  aigs.current() = mig_to_aig( mig(), settings );

  if ( opts.is_set( "name" ) )
  {
    aig_info( aigs.current() ).model_name = name;
  }

  return true;
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
