/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "control_logic.hpp"

#include <iostream>

#include <boost/format.hpp>

#include <core/utils/bitset_utils.hpp>
#include <core/utils/program_options.hpp>
#include <core/utils/range_utils.hpp>
#include <classical/functions/aig_control_logic.hpp>

using namespace boost::program_options;
using boost::format;

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

control_logic_command::control_logic_command( const environment::ptr& env ) : aig_base_command( env, "Extract MUXS of an AIG" )
{
  opts.add_options()
    ( "muxs,x",           "Prints the inputs and ouputs of MUXS when verbose" )
    ( "control_inputs,i", "Prints control inputs when verbose" )
    ( "extract_words,w",  "Extracts words to AIG for slicing" )
    ;
  be_verbose();
}

bool control_logic_command::execute()
{
  auto settings = make_settings();
  auto statistics = std::make_shared<properties>();
  std::vector<aig_node> control_inputs;

  const auto allmuxs = aig_control_logic( aig(), control_inputs, settings, statistics );
  const auto print   = opts.is_set( "muxs" );

  for ( const auto& mux : allmuxs )
  {
    if ( print ) { std::cout << "[i] selections: " << any_join( mux.selections, " " ) << std::endl; }

    for ( const auto& win : mux.word_inputs )
    {
      if ( print ) { std::cout << "[i] input word: " << any_join( win, " " ) << std::endl; }
      info().trans_words.push_back( win );
    }

    if ( print ) { std::cout << "[i] output word: " << any_join( mux.word_output, " " ) << std::endl; }
    info().trans_words.push_back( mux.word_output );
  }

  if ( opts.is_set( "control_inputs" ) )
  {
    std::cout << "[i] control inputs: {" << any_join( control_inputs, " " ) << "}" << std::endl;
  }

  std::cout << boost::format( "[i] run-time: %.2f seconds" ) % statistics->get<double>( "runtime" ) << std::endl;

  return true;
}


}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
