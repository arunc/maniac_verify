/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "controllable.hpp"

#include <core/utils/program_options.hpp>
#include <classical/cli/stores.hpp>
#include <classical/debug/aig_make_controllable.hpp>
#include <classical/io/read_aiger.hpp>

using namespace boost::program_options;

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

controllable_command::controllable_command( const environment::ptr & env )
  : aig_base_command( env, "Replace gates with a controllable input in current AIG" ),
    store( env->store<std::vector<aig_node>>() )
{
  opts.add_options()
    ( "gates,g", value( &gates )->composing(), "Gates to repair" )
    ;
  be_verbose();
}

command::rules_t controllable_command::validity_rules() const
{
  return {
    {[&]() { return !store.empty() || !gates.empty(); }, "no gates to modify specified" },
  };
}

bool controllable_command::execute()
{
  /* vector of indices to replace with controllable inputs */
  std::vector<unsigned> gates_to_replace;

  if ( opts.is_set( "gates" ) )
  {
    /* convert literal to indices */
    for ( const auto g : gates )
    {
      gates_to_replace.push_back( aiger_lit2var( g ) );
    }
  }
  else if ( !store.empty() )
  {
    for ( const auto g : store.current() )
    {
      gates_to_replace.push_back( g );
    }
  }

  aig() = aig_make_controllable( aig(), gates_to_replace );
  return true;
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
