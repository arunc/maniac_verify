/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cuts.hpp"

#include <iostream>

#include <boost/format.hpp>

#include <core/utils/bitset_utils.hpp>
#include <core/utils/program_options.hpp>
#include <classical/utils/cut_enumeration.hpp>
#include <classical/utils/truth_table_utils.hpp>

using namespace boost::program_options;
using boost::format;

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

cuts_command::cuts_command( const environment::ptr& env ) : aig_mig_command( env, "Computes cuts of an AIG", "Enumerate cuts for %s" )
{
  opts.add_options()
    ( "node_count,k", value_with_default( &node_count ), "Number of nodes in a cut" )
    ( "truthtable,t",                                    "Prints truth tables when verbose" )
    ( "cone_count,c",                                    "Prints nodes in cut cone when verbose" )
    ;
  be_verbose();
}

bool cuts_command::execute_aig()
{
  auto settings = make_settings();
  auto statistics = std::make_shared<properties>();
  auto map = structural_cut_enumeration( aig(), node_count, settings, statistics );

  if ( is_verbose() )
  {
    for ( const auto& p : map )
    {
      std::cout << boost::format( "[i] node %d has %d cuts" ) % p.first % p.second.size() << std::endl;
      for ( const auto& cut : p.second )
      {
        std::cout << "[i] - ";
        print_as_set( std::cout, cut );

        if ( opts.is_set( "cone_count" ) )
        {
          std::cout << format( " (%d)" ) % cut_cone_size( p.first, cut, aig() );
        }

        if ( opts.is_set( "truthtable" ) )
        {
          std::cout << " " << tt_to_hex( simulate_cut_tt<aig_graph>( p.first, cut, aig() ) );
        }

        std::cout << std::endl;
      }
    }
  }

  std::cout << boost::format( "[i] run-time: %.2f seconds" ) % statistics->get<double>( "runtime" ) << std::endl;

  return true;
}

bool cuts_command::execute_mig()
{
  using cuts_type_t = structural_cut;
  //using cuts_type_t = std::vector<std::set<unsigned>>;
  //using cuts_type_t = std::vector<std::vector<unsigned>>;

  auto settings = make_settings();
  auto statistics = std::make_shared<properties>();
  auto map = structural_cut_enumeration<mig_graph, cuts_type_t>( mig(), node_count, settings, statistics );

  if ( is_verbose() )
  {
    for ( const auto& p : map )
    {
      std::cout << boost::format( "[i] node %d has %d cuts" ) % p.first % p.second.size() << std::endl;
      for ( const auto& cut : p.second )
      {
        std::cout << "[i] - ";
        print_cut( std::cout, cut );

        if ( opts.is_set( "cone_count" ) )
        {
          std::cout << format( " (%d)" ) % cut_cone_size<mig_graph, cuts_type_t>( p.first, cut, mig() );
        }

        if ( opts.is_set( "truthtable" ) )
        {
          std::cout << " " << tt_to_hex( simulate_cut_tt<mig_graph, cuts_type_t>( p.first, cut, mig() ) );
        }

        std::cout << std::endl;
      }
    }
  }

  std::cout << boost::format( "[i] run-time: %.2f seconds" ) % statistics->get<double>( "runtime" ) << std::endl;

  return true;
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
