/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "diagnose.hpp"

#include <iostream>
#include <boost/format.hpp>

#include <core/utils/program_options.hpp>
#include <classical/cli/stores.hpp>

#ifdef ADDON_METASMT
#include <classical/metasmt/aig_diagnose.hpp>
#endif

using namespace boost::program_options;

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

diagnose_command::diagnose_command( const environment::ptr& env )
  : command( env, "Compute diagnoses from first-principle" ),
    aigs( env->store<aig_graph>() ),
    cexs( env->store<counterexample_t>() ),
    gates( env->store<std::vector<aig_node>>() )
{
  opts.add_options()
    ( "circuit,c",       value_with_default( &circ ),    "Store-ID of faulty circuit" )
    ( "specification,s", value_with_default( &spec ),    "Store-ID of specification" )
    ( "pattern,p",       value_with_default( &pattern ), "Store-ID of counterexample" )
    ( "new,n",                                           "Write gates into new store entry" )
    ( "size",            value_with_default( &size ),    "Maximal size of diagnosis" )
    ;
  be_verbose();
}

command::rules_t diagnose_command::validity_rules() const
{
  return {
    {[&]() { return circ < aigs.size(); },    "store-ID of faulty circuit is invalid" },
    {[&]() { return spec < aigs.size(); },    "store-ID of specification is invalid" },
    {[&]() { return pattern < cexs.size(); }, "store-ID of counterexample is invalid" }
  };
}

bool diagnose_command::execute()
{
  const auto& aig_circ = aigs[circ];
  const auto& aig_spec = aigs[spec];
  const auto& cex = cexs[pattern];

#ifdef ADDON_METASMT
  auto settings = make_settings();
  auto statistics = std::make_shared<properties>();

  settings->set( "cardinality", size );

  auto nodes = aig_diagnose( aig_circ, aig_spec, cex, settings, statistics );
  std::cout << boost::format( "[i] diagnose run-time: %.2f secs" ) % statistics->get<double>( "runtime" ) << std::endl;

  if ( nodes )
  {
    if ( gates.empty() || opts.is_set( "new" ) )
    {
      gates.extend();
    }
    gates.current() = *nodes;

    std::cout << "[i] diagnoses: ";
    for ( const auto& n : *nodes )
    {
      std::cout << aig_to_literal( aig_circ, n ) << ' ';
    }
    std::cout << std::endl;
  }
  else
  {
    std::cout << "[i] no diagnosis found." << std::endl;
  }
#else
  std::cout << "[w] no modelchecker available." << std::endl;
#endif
  return true;
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
