/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "inject.hpp"

#include <core/cli/rules.hpp>
#include <core/utils/program_options.hpp>
#include <classical/cli/stores.hpp>
#include <classical/functions/aig_fault_injection.hpp>

using namespace boost::program_options;

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

inject_command::inject_command( const environment::ptr& env )
  : command( env, "Fault injection for AIGs" )
  , aigs( env->store<aig_graph>() )
{
  opts.add_options()
    ( "circuit,c", value_with_default( &circuit ),      "Store-ID of AIG to inject" )
    ( "faults,f",  value_with_default( &fault_number ), "Number of injected faults" )
    ( "seed,s",    value( &seed ),                      "Random seed" )
    ( "new,n",                                          "Add a new entry to the store; if not set, the current entry is overriden" )
    ;
  be_verbose();
}

command::rules_t inject_command::validity_rules() const
{
  return {
    {[&]() { return !aigs.empty(); },            "no AIG in store" },
    {[&]() { return !(circuit < aigs.size()); }, "store-ID of circuit is invalid" },
  };
}

bool inject_command::execute()
{
  auto aig_circuit = circuit < 0 ? aigs.current() : aigs[circuit];

  if ( is_verbose() )
  {
    std::cout << ( boost::format( "[i] inject %s fault into %s ''" ) % fault_number % aig_info( aig_circuit ).model_name ).str() << std::endl;
  }

  auto settings = make_settings();
  if ( opts.is_set( "seed" ) )
  {
    settings->set( "seed", seed );
  }

  if ( opts.is_set( "new" ))
  {
    aigs.extend();
  }

  /* inject fault and update AIG store */
  auto new_aig = aig_random_fault_injection( aig_circuit, fault_number, settings );
  if ( circuit < 0 )
  {
    aigs.current() = new_aig;
  }
  else
  {
    aigs[circuit] = new_aig;
  }
  return true;
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
