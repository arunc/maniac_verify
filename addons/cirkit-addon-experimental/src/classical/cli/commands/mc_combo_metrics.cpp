/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mc_combo_metrics.hpp"

#include <iostream>
#include <vector>
#include <fstream>
#include <cassert>
#include <cstdio>
#include <algorithm>
#include <ctime>

#include <boost/filesystem.hpp> 

#include <classical/cli/stores.hpp>
#include <classical/functions/strash.hpp>
#include <classical/utils/aig_utils.hpp>
#include <abc/abc_api.hpp>
#include <classical/dd/bdd.hpp>
#include <classical/io/read_into_bdd.hpp>
#include <classical/approximate/error_metrics.hpp>
#include <classical/dd/size.hpp>
#include <classical/dd/visit_solutions.hpp>
#include <classical/io/read_into_bdd.hpp>
#include <boost/format.hpp>

#include <classical/utils/gcc_version.hpp>

using namespace boost::program_options;

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

//------------------------------------------------------------------------------

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

mc_combo_metrics_command::mc_combo_metrics_command( const environment::ptr& env )
  : command( env, "MANIAC Simple Error Metrics calculator for Combinational Circuits." )
{
    opts.add_options()
    ( "golden,g", value( &golden ), "Golden Design file (aig)" )
    ( "approx,a", value( &approx ), "Approximated Design file (aig)" )
    ( "report",   value_with_default( &report_file ), "Output report [Default = maniac.rpt]" )
    ( "csv",   value_with_default( &csv_file ), "Output csv [Default = maniac.csv]" )
    ( "signed_outputs", value_with_default( &signed_outputs ), "NOT Implemented. Incorporate in the input design." )

    ;

}
//------------------------------------------------------------------------------
bool mc_combo_metrics_command::execute() {
  read_golden();
  read_approx();
  wc = worst_case( fs, fshat );
  ac = average_case ( fs, fshat );
  err_count = error_rate ( fs, fshat );
  er = ( (double)err_count / (1ull << manager->num_vars()) * 100.0 );
  bit_flips = count_bit_flips (fs, fshat);
  
  std::cout << "[i] worst case    :  " << wc << std::endl;
  std::cout << "[i] average case  :  " << ac << std::endl;
  std::cout << "[i] error rate    :  " << er << "%" << std::endl;
  std::cout << "[i] error count   :  " << err_count << std::endl;
  std::cout << "[i] bit flips     :  " << bit_flips << std::endl;
  write_report (report_file);
  write_csv (csv_file);
  return true;
}

//-------------------------------------------------------------------------
//-------------------------------------------------------------------------
//
// TODO : parse and store the golden inputs and outputs.
inline void mc_combo_metrics_command::read_golden ()  {
  auto golden_settings = std::make_shared<properties>();
  golden_settings->set("log_max_objs", 24u);
  std::tie( manager, fs ) = read_into_bdd( golden, golden_settings );
}
inline void mc_combo_metrics_command::read_approx ()  { // with same bdd-manager.
  aig_graph aig;
  read_aiger (aig, approx);
  fshat = aig_to_bdd (aig, manager);
}

void mc_combo_metrics_command::write_report (const std::string &filename) {
    std::ofstream of;
    of.open (filename.c_str(), std::ios_base::out);
    auto t = std::time(nullptr);
    auto tm = *std::localtime(&t);
     of << "MANIAC Simple Combo Error Report :: "
       << std::put_time(&tm, "( at %H:%M on %d-%m-%Y )") << std::endl
       << "---------------------------------------------------------"
       << "--------" << std::endl;
    of << "Golden           : " << golden << std::endl;
    of << "Approx           : " << approx << std::endl;
    of << "Worst Case Error : "  << wc << std::endl;
    of << "Avg Case Error   : "  << ac << std::endl;
    of << "Error Rate       : "  << er << std::endl;
    of << "Error Count      : "  << err_count << std::endl;
    of << "Bit Flips        : "  << bit_flips << std::endl;
    of << "Golden Module    : NA " << std::endl;
    of << "Approx Module    : NA " << std::endl;
    of << "Ports Checked    : NA " << std::endl;
    of << "Signed Interpretation : NA " << std::endl;
    of.close();

}
void mc_combo_metrics_command::write_csv (const std::string &filename) {
    std::ofstream of;
    of.open (filename.c_str(), std::ios_base::out);
    of << "golden,approx,worst-case"<<std::endl;
    of << golden << "," << approx << "," << wc << std::endl;
    of.close();
}

unsigned short mc_combo_metrics_command::count_bit_flips ( const std::vector<bdd>& f,
							  const std::vector<bdd>& fhat )
{
  //assert_valid (f, fhat); some inline function. cant be used.
  auto zero = f.front().manager->bdd_bot();
  auto count = 0u;
  for (auto i=0u; i < f.size(); i++)
  {
    auto fdash = f[i] ^ fhat[i];
    // if f & fhat are the same stuff result is bottom, else some function will be there.
    if ( fdash.equals (zero) ) count++;
    //if (fdash == zero)  count++;  operator == is not implemented.
  }
  return ( f.size() - count ); 
}

//-------------------------------------------------------------------------
//-------------------------------------------------------------------------
}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:


