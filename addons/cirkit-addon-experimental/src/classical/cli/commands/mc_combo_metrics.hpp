/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file mc_combo_metrics.hpp
 *
 * @brief Formal verifier for Approximate ckts.
 *
 * @author Arun
 * @since  2.3
 */

#ifndef MC_COMBO_METRICS_HPP
#define MC_COMBO_METRICS_HPP

#include <string>
#include <core/cli/command.hpp>
#include <yosys/yosys_api.hpp>
#include <utility>
#include <core/properties.hpp>

#include <boost/dynamic_bitset.hpp>

#include <classical/io/read_into_bdd.hpp>
#include <core/utils/range_utils.hpp>
#include <core/utils/timer.hpp>
#include <classical/aig.hpp>
#include <classical/dd/aig_to_cirkit_bdd.hpp>
#include <classical/approximate/error_metrics.hpp>
#include <classical/io/read_aiger.hpp>
#include <classical/io/read_pla_to_cirkit_bdd.hpp>
#include <classical/utils/aig_utils.hpp>

namespace cirkit
{

//-------------------------------------------------------------------------
//-------------------------------------------------------------------------
class mc_combo_metrics_command : public command
{
public:
  mc_combo_metrics_command( const environment::ptr& env );
    
protected:
  bool execute();

private:
  // command arguments.
  std::string golden;
  std::string approx;
  
  std::string report_file = "maniac_wc.rpt";
  std::string csv_file = "maniac_wc.csv";

  unsigned signed_outputs = 0u; // To indidcate if inputs are signed or not.

  //----------------------------
  // Private Variables.
  //unsigned long wc = 0u;
  boost::multiprecision::uint256_t wc;
  boost::multiprecision::cpp_dec_float_100  ac;
  boost::multiprecision::uint256_t err_count;
  unsigned long bit_flips;
  float er;
  bdd_manager_ptr  manager;
  std::vector<bdd> fs, fshat;

  // TODO - implement all these features sometime!.
  std::vector <std::string> golden_inputs, approx_inputs;
  std::vector <std::string> golden_outputs, approx_outputs;
  
  inline void read_golden ();
  inline void read_approx ();
  void write_report (const std::string &filename);
  void write_csv (const std::string &filename);
  unsigned short count_bit_flips ( const std::vector<bdd>& f, const std::vector<bdd>& fhat );

};

}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:


