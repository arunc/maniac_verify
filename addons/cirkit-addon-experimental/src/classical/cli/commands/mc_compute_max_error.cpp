/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mc_compute_max_error.hpp"

#include <iostream>

#include <classical/cli/stores.hpp>
#include <classical/functions/strash.hpp>
#include <classical/utils/aig_utils.hpp>
#include <abc/abc_api.hpp>

using namespace boost::program_options;

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

mc_compute_max_error_command::mc_compute_max_error_command( const environment::ptr& env )
  : command( env, "MANIAC compute_max_error command" )
{
  opts.add_options()
    ( "blif_file", value( &blif_file ), "Input BLIF file" )
    ( "max_cycles", value_with_default( &max_cycles ), "Number to cycles to be checked. [Default = 100]" )
    ( "log", value( &log_file ), "Log file [Default = cirkit_maniac.log]" )
    ;

}

bool mc_compute_max_error_command::execute() {
  
  abc::Abc_Start();
  abc::Abc_Frame_t *frame = abc::Abc_FrameGetGlobalFrame();
  if (!frame) {
    abc::Abc_Stop();
    std::cout << "[e] not able to invoke the ABC frame" << std::endl;
    return false;
  }

  // lets define the commands here and then run ABC
  // (1) read_blif blif_file
  // (2) fraig
  // (3) bmc2 -F max_cycles -L log_file
  std::string cmd_read_blif = "read_blif " + blif_file;
  std::string cmd_fraig = "fraig";
  std::string cmd_bmc2 = "bmc -F " + std::to_string(max_cycles) 
    + " -L " + log_file;
  auto status = abc::Cmd_CommandExecute (frame, cmd_read_blif.c_str());
  status = abc::Cmd_CommandExecute (frame, cmd_fraig.c_str());
  status = abc::Cmd_CommandExecute (frame, cmd_bmc2.c_str());

  abc::Abc_Stop();
  
  return true;
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
