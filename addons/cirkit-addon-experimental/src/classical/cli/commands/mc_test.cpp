/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mc_test.hpp"

#include <iostream>

#include <classical/cli/stores.hpp>
#include <classical/functions/strash.hpp>
#include <classical/utils/aig_utils.hpp>
#include <yosys/yosys_api.hpp>
#include <vector>

using namespace boost::program_options;
using namespace Yosys;

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

mc_test_command::mc_test_command( const environment::ptr& env )
  : command( env, "MANIAC Test command" )
{
  opts.add_options()
    ( "blif_file", value( &blif_file ), "Input BLIF file" )
    ( "max_cycles", value_with_default( &max_cycles ), "Number to cycles to be checked. [Default = 100]" )
    ( "log", value( &log_file ), "Log file [Default = cirkit_maniac.log]" )
    ;

}

bool mc_test_command::execute() {
  //Yosys::yosys_banner();
  //Yosys::yosys_setup();
  //Yosys::run_pass("help");
  //Yosys::yosys_shutdown();
  yosys_banner();
  yosys_setup();
  std::cout << "version " << yosys_version_str << std::endl;
  run("read_verilog /media/arun/Academics/phd/Bremen/works/approxComputing/newIdeas/accumulative_error/multiplier/multiplier_wc.v");
  run("hierarchy -check -top multiply_wc");
  run("proc");
  run("flatten");
  run("clean");
  run("opt");
  run("memory");
  run("opt");
  run("fsm");
  run("opt");
  run("techmap");
  run("opt");
  run("fsm");
  run("techmap");
  run("opt");
  run("flatten");
  run("clean");
  run("write_blif multiplier_wc.blif");


  // Lets try to do something custom here.
  auto design = yosys_get_design();
  auto my_module = design->top_module();
  for (auto mod:design->modules())
    my_module = mod;
  
  std::vector<RTLIL::Wire*> all_inputs;
  std::vector<RTLIL::Wire*> all_outputs;
 
  for (auto &w : my_module->wires_) { // _wires is a dict in Module
    if (w.second->port_input) {
      all_inputs.push_back(w.second);
    }
    if (w.second->port_output) all_outputs.push_back(w.second);
    
  }
  //shell(yosys_design);
  std::cout << "All Inputs : ";
  for (auto &p : all_inputs) {
    std::cout << (p->name).str() << " [" << p->width << "]   ";
  }
  std::cout << std::endl;

  std::cout << "All Outputs : ";
  for (auto &p : all_outputs) {
    std::cout << (p->name).str() << " [" << p->width << "]   ";
  }
  std::cout << std::endl;
  
  yosys_shutdown();
  return true;
}

void mc_test_command::run (const char *command) {
    	int selSize = GetSize(yosys_get_design()->selection_stack);
	std::cout << "run " << command << std::endl;
	try {
		log_last_error = "Internal error (see JavaScript console for details)";
		run_pass(command);
		log_last_error = "";
	} catch (...) {
		while (GetSize(yosys_get_design()->selection_stack) > selSize)
			yosys_get_design()->selection_stack.pop_back();
		throw;
	}
  }

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:


