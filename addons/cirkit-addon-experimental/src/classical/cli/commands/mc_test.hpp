/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file mc_test.hpp
 *
 * @brief trial and error code for experimentation.
 *
 * @author Arun
 * @since  2.3
 */

#ifndef MC_TEST_HPP
#define MC_TEST_HPP

#include <string>
#include <sys/stat.h>
#include <sys/types.h>

#include <core/cli/command.hpp>

namespace cirkit
{

//-------------------------------------------------------------------------
//-------------------------------------------------------------------------
class mc_test_command : public command
{
public:
  mc_test_command( const environment::ptr& env );
  void run(const char *command);
protected:
  bool execute();

private:
  std::string blif_file;
  unsigned max_cycles = 100u;
  std::string log_file = "cirkit_maniac.log";
};

}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:


