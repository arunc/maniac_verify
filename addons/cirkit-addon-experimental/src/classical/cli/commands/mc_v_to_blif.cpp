/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mc_v_to_blif.hpp"

#include <iostream>

#include <classical/cli/stores.hpp>
#include <classical/functions/strash.hpp>
#include <classical/utils/aig_utils.hpp>
#include <yosys/yosys_api.hpp>
#include <vector>

using namespace boost::program_options;

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

mc_v_to_blif_command::mc_v_to_blif_command( const environment::ptr& env )
  : command( env, "MANIAC Verilog to BLIF converter" )
{
  opts.add_options()
    ( "v_file", value( &v_file ), "Input Verilog file" )
    ( "blif_file", value( &blif_file ), "Output BLIF file" )
    ( "top_module", value( &top_module ), "Top Module Name [Default = last read module]" )
    ;
}

bool mc_v_to_blif_command::execute() {
  Yosys::yosys_setup();
  run("read_verilog  " + v_file);
  run("hierarchy -check");
  run("proc");
  run("flatten");
  run("clean");
  run("opt");
  run("memory");
  run("opt");
  run("fsm");
  run("opt");
  run("techmap");
  run("opt");
  run("fsm");
  run("techmap");
  run("opt");
  run("flatten");
  run("clean");
  run("write_blif " + blif_file);


  Yosys::yosys_shutdown();
  return true;
}


void mc_v_to_blif_command::run (const std::string &command) {
  try {    Yosys::run_pass(command); }
  catch (...) { throw; }
}


}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:


