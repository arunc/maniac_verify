/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mc_verify.hpp"

#include <iostream>
#include <vector>
#include <fstream>
#include <cassert>
#include <cstdio>
#include <algorithm>
#include <iterator>
#include <ctime>

#include <boost/filesystem.hpp> 

#include <classical/cli/stores.hpp>
#include <classical/functions/strash.hpp>
#include <classical/utils/aig_utils.hpp>
#include <abc/abc_api.hpp>
#include <classical/utils/mc_results.hpp>
#include <classical/utils/mc_worst_case.hpp>
#include <classical/utils/mc_design.hpp>

using namespace boost::program_options;

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

mc_verify_command::mc_verify_command( const environment::ptr& env )
  : command( env, "MANIAC Formal Verifier for Approximate circuits :: Golden and Approximate Modules should have same ports " )
{
  
  opts.add_options()
    ( "golden,g", value( &golden ), "Golden Design file (verilog)" )
    ( "approx,a", value( &approx ), "Approximated Design file (verilog)" )
    ( "golden_module,1", value( &golden_module ), "Golden Module to be verified" )
    ( "approx_module,2", value( &approx_module ), "Approx Module to be verified" )
    ( "port,p",   value( &port ),   "Output to be verified" )
    ( "avg_error", value( &avg_error ),"Max average error to verify" )
    ( "accum_error_rate",  value( &accum_error_rate ),   "Accum error-rate [count (bit_flips()]" )
    ( "accum_error", value( &accum_error ),"Max accumulated error " )
    ( "cycle_limit,c", value_with_default( &cycle_limit ),
      "Number of clock cycles to check for accumulated error. [Default = 100]" )
    ( "error_rate_cycle_limit", value_with_default( &error_rate_cycle_limit ),
      "Number of clock cycles to check for accumulated error-rate. [Default = 100]" )
    ( "log,l",    value( &log_file ), "Log file [Default = cirkit_maniac.log]" )
    ( "debug,d",  value_with_default( &debug ),
      "Extra debug information [Default = 1 (true), Recommended to keep true]" )
    ( "clock",    value_with_default( &clock ), "Clock signal name [Default = clock]" )
    ( "reset",    value_with_default( &reset ), "Reset signal name [Default = rst]" )
    ( "oe",    value_with_default( &oe ),
      "Output-Enable signal, 1bit output signal  [Default = __NA__ (No output-enable signal)]" ) 
    ( "report",   value_with_default( &report_file ), "Output report [Default = maniac.rpt]" )
    ( "csv",   value_with_default( &csv_file ), "Output csv [Default = maniac.csv]" )
     ( "signed_outputs", value_with_default( &signed_outputs ),
       "If the outputs are signed numbers [Default = false]" )
    ( "worst_case_en", value_with_default( &worst_case_en ),"worst-case enable/disable " )
    ( "accum_error_en", value_with_default( &accum_error_en ),"accum-error enable/disable " )
    ( "worst_er_en", value_with_default( &worst_er_en ),"worst-error-rate enable/disable " )
    ( "accum_er_en", value_with_default( &accum_er_en ),"accum-error-rate enable/disable " )
    ( "avg_case_en", value_with_default( &avg_case_en ),"avg-case enable/disable " )
    ( "cex_en", value_with_default( &cex_en ),"counter-example printing enable/disable " )
    ( "cross_verify", value_with_default( &cross_verify ),"cross verify accum computations result" )
    ( "avg_case_effort_level", value_with_default( &avg_case_effort_level ),
      "Effort put into the avg-error computation" )
    ( "cycles_for_avg_error", value_with_default( &cycles_for_avg_error ),
      "Clock cycles for which avg-error is computed. This does not apply for effort-level 2 or above" )
    ( "opt", value_with_default( &opt ),"heavy optimization procedures " )
    ( "err_upper_bound", value_with_default( &err_upper_bound ),"Upper bound for worst error-rate " )
    ( "err_lower_bound", value_with_default( &err_lower_bound ),"Lower bound for worst error-rate " )
    ;

}
//------------------------------------------------------------------------------
bool mc_verify_command::execute() {
  auto start_time = curr_time();
  auto print_cex = cex_en;
  
  // Initialize stuffs.
  //set_stats(); // Initialize the statistics collector.
  //auto results = new mc_results( mc_stats ); // results object
  std::cout <<"[i] Maniac Circuit Verifier - started on " << curr_time();
  create_work_dir( work ); // Create the work directory.
  
  Yosys::yosys_setup(); // initialize yosys
  if (debug) std::cout << "[i] " << Yosys::yosys_version_str << std::endl;
  auto localdebug = false;

  auto yosys_debug  = false;
  auto gold = mc_design (golden, golden_module, clock, reset, oe, yosys_debug);
  auto appx = mc_design (approx, approx_module, clock, reset, oe, yosys_debug);
  port_to_check = std::make_pair( port, gold.get_width (port) );

  // Do a first round of sanity checks.
  assert ( check_sanity (gold, appx) );
  
  std::ofstream file;
  // saving the result whenever available. anticipating long run-times/crashes.
  file.open (report_file);
  file << "Maniac Approximation Verifier Report " << std::endl;
  file << "Start Time  :: "  << start_time;
  file << "------------------------------------------------" << std::endl;
  print_program_options(file);
  file << "------------------------------------------------" << std::endl;
  file << "Results" << std::endl;
  file.close();
  
  abc::Abc_Start();

  // 1. Worst Case Type 2
  unsigned long wc = 0u;
  if (worst_case_en > 0)
  {
    std::cout << "----------------- Checking WORST CASE ----------------- :: " << curr_time();
    create_work_dir( worst_case_work ); // Create the work directory.
    const clock_t begin_time = std::clock();
    wc = mc_worst_case (gold, appx, port_to_check, signed_outputs,
			worst_case_work, debug, opt);
    wc_time = float ( std::clock() - begin_time ) / CLOCKS_PER_SEC;
  }
  file.open (report_file, std::ios::app);
  file << "Worst Case   :: " << wc << std::endl;
  file << "Time for WC  :: " << wc_time << std::endl;
  file.close();
  
  // 2. Accumulated Worst Case Type 1
  int acc_cycles = 0;
  if (accum_error_en > 0)
  {
    std::cout << "----------------- Checking ACCUM ERROR ----------------- :: " << curr_time();
    create_work_dir( accum_case_work ); // Create the work directory.
    auto verify_wc = true_or_false (cross_verify);
    auto print_cex_wc = true_or_false (print_cex);
    const clock_t begin_time = std::clock();
    acc_cycles =
      mc_cycles_for_max_accum_error ( gold, appx, port_to_check,
				      accum_error, cycle_limit,
				      signed_outputs, accum_case_work,
				      debug, verify_wc, print_cex_wc );
    // NOTE :: For the accumulated case, there is an extra cycle needed for accumulation.
    // Need to subtract this number.
    acc_cycles --;
    acc_time = float ( std::clock() - begin_time ) / CLOCKS_PER_SEC;
  }
  file.open (report_file, std::ios::app);
  file << "Cycles for Max Accumulated Error of " << accum_error << "  :: "
       << acc_cycles << std::endl;
  file << "Time for ACC CASE  :: " << acc_time << std::endl;
  file.close();

  // 3. Worst Error Rate Type 4
  unsigned long er = 0u;
  if (worst_er_en > 0)
  {
    std::cout << "----------------- Checking WORST ERROR RATE ----------------- :: " << curr_time();
    const clock_t begin_time = std::clock();
    create_work_dir( worst_er_work ); // Create the work directory.
    er = mc_worst_error_rate (gold, appx, port_to_check, signed_outputs,
			      worst_er_work, debug, opt, err_lower_bound, err_upper_bound);
    err_time = float ( std::clock() - begin_time ) / CLOCKS_PER_SEC;
  }
  file.open (report_file, std::ios::app);
  file << "Worst Error Rate   :: " << er << std::endl;
  file << "Time for WERR      :: " << err_time << std::endl;
  file.close();

  // 4. Accumulated Error Rate Type 3
  int acc_cycles_er = 0;
  if (accum_er_en > 0)
  {
    std::cout << "----------------- Checking ACCUM ERROR RATE ----------------- :: " << curr_time();
    create_work_dir( accum_er_work ); // Create the work directory.
    auto verify_er = true_or_false (cross_verify);
    auto print_cex_er = true_or_false (print_cex);
    const clock_t begin_time = std::clock();
    acc_cycles_er =
      mc_cycles_for_accum_error_rate ( gold, appx, port_to_check,
				       accum_error_rate, error_rate_cycle_limit,
				       signed_outputs, accum_er_work,
				       debug, verify_er, print_cex_er );
    // NOTE :: For the accumulated case, there is an extra cycle needed for accumulation.
    // Need to subtract this number.
    acc_cycles_er --;
    acc_err_time = float ( std::clock() - begin_time ) / CLOCKS_PER_SEC;
  }
  file.open (report_file, std::ios::app);
  file << "Cycles for Accumulated Error Rate of " << accum_error_rate << "  :: "
       << acc_cycles_er << std::endl;
  file << "Time for ACCERR      :: " << acc_err_time << std::endl;
  file.close();
  
//---  // 5. Average Case Type 5
  if (avg_case_en > 0)
  {
    std::cout << "----------------- Checking AVG CASE ----------------- :: " << curr_time();
    std::cout << "NOT YET READY TO DEAL WITH AVG-ERROR" << std::endl;
//---    update_avg_case_clock_cycles ();
//---  create_work_dir( avg_case_work ); // Create the work directory.
//---  auto verify = true;
//---  auto print_cex = false;
//---  auto status = mc_check_average_case (gold, appx, port_to_check, signed_outputs,
//---				       avg_error, avg_case_work, avg_case_effort_level,
//---    debug);
  }
  auto finish_time = curr_time();
  
  abc::Abc_Stop();
  file.open (report_file, std::ios::app);
  file << "Finish Time :: " << finish_time;
  file.close ();
  
  std::cout << std::endl;
  if (worst_case_en > 0)  std::cout << "RESULT: SEQ WORST CASE = " << wc << std::endl;
  if (accum_error_en > 0)
    std::cout << "RESULT: CYCLES FOR MAX ACCUMULATED ERROR  " << accum_error
	      << " = " << acc_cycles << std::endl;
  if (worst_er_en > 0)   std::cout << "RESULT: SEQ WORST ERROR RATE = " << er << std::endl;
  if (accum_er_en > 0)
    std::cout << "RESULT: CYCLES FOR ACCUMULATED ERROR RATE " << accum_error_rate
	      << " = " << acc_cycles_er << std::endl;
  

  return true;
}

//-------------------------------------------------------------------------
//-------------------------------------------------------------------------
//---// setup the statistics
//---// TODO: add run time details.
//---inline void mc_verify_command::set_stats() {
//---  /*
//---  mc_stats->set ("golden", golden);
//---  mc_stats->set ("approx", approx);
//---  mc_stats->set ("golden_module", golden_module);
//---  mc_stats->set ("approx_module", approx_module);
//---  mc_stats->set ("port", port);
//---  mc_stats->set ("clock", clock);
//---  mc_stats->set ("reset", reset);
//---  mc_stats->set ("max_cycles", (unsigned) max_cycles);
//---  mc_stats->set ("max_error", (unsigned) max_error);
//---  mc_stats->set ("accum_error", (unsigned)accum_error);
//---  mc_stats->set ("log_file", log_file);
//---  mc_stats->set ("report_file", report_file);
//---  mc_stats->set ("csv_file", csv_file);
//---  mc_stats->set ("debug", (unsigned)debug);
//---  */
//---}

void mc_verify_command::print_program_options (std::ofstream &file) {
  assert (file.is_open());
  // TODO: get the correct method for this.
  file << "Program Options" << std::endl;
  file << "golden,g         :: " << golden << std::endl;
  file << "approx,a         :: " << approx << std::endl;
  file << "golden_module,1  :: " << golden_module << std::endl;
  file << "approx_module,2  :: " << approx_module << std::endl;
  file << "accum_error_rate   :: " << accum_error_rate << std::endl;
  file << "avg_error          :: " << avg_error << std::endl;
  file << "accum_error        :: " << accum_error << std::endl;
  file << "cycle_limit,c           :: " << cycle_limit << std::endl;
  file << "error_rate_cycle_limit  :: " << error_rate_cycle_limit << std::endl;
  file << "port,p   :: "     << port << std::endl;
  file << "clock    :: "   << clock << std::endl;
  file << "reset    :: "   << reset << std::endl;
  file << "oe       :: "   << oe << std::endl;
  file << "log,l    :: "   << log_file << std::endl;
  file << "debug,d  :: "   << debug << std::endl;
  file << "report   :: "   << report_file << std::endl;
  file << "csv      :: "   << csv_file << std::endl;
  file << "signed_outputs  :: " << signed_outputs << std::endl;
  file << "worst_case_en   :: " << worst_case_en << std::endl;
  file << "accum_error_en  :: " << accum_error_en << std::endl;
  file << "worst_er_en     :: " << worst_er_en << std::endl;
  file << "accum_er_en     :: " << accum_er_en << std::endl;
  file << "avg_case_en     :: " << avg_case_en << std::endl;
  file << "cex_en          :: " << cex_en << std::endl;
  file << "cross_verify    :: " << cross_verify << std::endl;
  //file << "avg_case_effort_level :: " << avg_case_effort_level << std::endl;
  //file << "cycles_for_avg_error  :: " << cycles_for_avg_error  << std::endl;
    
}


// TODO: use std::move 
bool mc_verify_command::check_sanity (const mc_design &golden, const mc_design &approx)
{
  auto g_inputs = golden.get_input_ports ();
  auto a_inputs = approx.get_input_ports ();
  auto g_outputs = golden.get_output_ports ();
  auto a_outputs = approx.get_output_ports ();

  //---// Try this later. TODO
  //---// http://stackoverflow.com/questions/9778238/move-two-vectors-together
  //---mc_ports g_ports, a_ports;
  //---g_ports.reserve ( g_inputs.size() + g_outputs.size() );
  //---a_ports.reserve ( a_inputs.size() + a_outputs.size() );
  //---// http://stackoverflow.com/questions/9778238/move-two-vectors-together

  if ( ( g_inputs.size() + g_outputs.size() ) !=  ( a_inputs.size() + a_outputs.size() ) ) 
  {
    std::cout << "[e] Number of ports for golden and approx must be the same " << std::endl;
    return false;
  }

  assert ( g_inputs.size() == a_inputs.size() );
  for (auto i=0u; i < g_inputs.size(); i++)
  {
    auto g = g_inputs[i];
    auto a = a_inputs[i];
    if ( g.first != a.first )
    {
      std::cout << "[e] Name or order mismatch for the input port :: "  ;
      std::cout << " golden port = " << g.first << " VS approx port = " << a.first << std::endl;
      return false;
    }
    if ( g.second != a.second )
    {
      std::cout << "[e] Width mismatch for the input port :: " << g.first  ;
      std::cout << " golden width = " << g.second << " VS approx width = " << a.second << std::endl;
      return false;
    }
  }
  
  assert ( g_outputs.size() == a_outputs.size() );
  for (auto i=0u; i < g_outputs.size(); i++)
  {
    auto g = g_outputs[i];
    auto a = a_outputs[i];
    if ( g.first != a.first )
    {
      std::cout << "[e] Name or order mismatch for the output port :: "  ;
      std::cout << " golden port = " << g.first << " VS approx port = " << a.first << std::endl;
      return false;
    }
    if ( g.second != a.second )
    {
      std::cout << "[e] Width mismatch for the output port :: " << g.first  ;
      std::cout << " golden width = " << g.second << " VS approx width = " << a.second << std::endl;
      return false;
    }
  }

  // Check the specified acc error-rate is less than or equal to the width of port_to_check.
  if (accum_error_rate > port_to_check.second )
  {
    std::cout << "[w] Specified accumulated error-rate (--accum_error_rate) is higher than "
	      << "the width of output port to verify (--port)." << std::endl
	      << "[w] accum_error_rate is reset to width of " << port_to_check.first
	      << " [" << port_to_check.second << "]" << std::endl;
    accum_error_rate = port_to_check.second;
      
  }

  if (avg_case_effort_level > 0u)
    std::cout << "[w] Average Case Effort is more than 0u. Going to take a long time." << std::endl;

  return true;
}


// Getting paranoid with unsigned vs bool conversion!
inline bool mc_verify_command::true_or_false (unsigned value) {
  if (value == 0u) return false;
  else return true;
}

inline void mc_verify_command::update_avg_case_clock_cycles () {
  // Assert not a combinational case.
  assert (cycles_for_avg_error > 0);
  return;

  // REST OF THIS LOGIC IS WRONG.
  
  if (avg_case_effort_level > 0u) // Warn in case of purposeful sabotage.
  { 
    std::cout << "[w] Average Case Effort level is " << avg_case_effort_level << std::endl
	      << "[w] This will take a LoooooooooooNG time to conclude."
	      << " Highly recommended to keep --avg_case_effort_level 0" << std::endl;
    return;
  }

  // Adjust to the nearest multiple of 2^n
  auto msb_pos = msb_pos_of_unsigned (cycles_for_avg_error);
  assert (msb_pos < 32); // Not going to support clk cycles more than 2^31
  auto cycles = ( 1u << (msb_pos + 1) ) ;
  if (cycles != cycles_for_avg_error) {
    cycles_for_avg_error = cycles;
    std::cout << "[w] In avg-case computations cycles_for_avg_error updated to the value :: "
  	      << cycles << std::endl;
  }
}
  
}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:


