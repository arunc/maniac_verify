/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file mc_verify.hpp
 *
 * @brief Formal verifier for Approximate ckts.
 *
 * @author Arun
 * @since  2.3
 */

#ifndef MC_VERIFY_HPP
#define MC_VERIFY_HPP

#include <string>
#include <core/cli/command.hpp>
#include <yosys/yosys_api.hpp>
#include <utility>
#include <fstream>
#include <core/properties.hpp>
#include <classical/utils/mc_design.hpp>
#include <classical/utils/mc_curr_time.hpp>
#include <classical/utils/mc_make_miter.hpp>
#include <classical/utils/mc_common_utils.hpp>
#include <boost/dynamic_bitset.hpp>
#include <classical/utils/mc_cycles_for_max_accum_error.hpp>
#include <classical/utils/mc_cycles_for_accum_error_rate.hpp>
#include <classical/utils/mc_worst_error_rate.hpp>

namespace cirkit
{

//-------------------------------------------------------------------------
//-------------------------------------------------------------------------
class mc_verify_command : public command
{
public:
  mc_verify_command( const environment::ptr& env );
    
protected:
  bool execute();

private:
  // command arguments.
  std::string golden;
  std::string approx;
  std::string golden_module;
  std::string approx_module;
  std::string port;
  std::string clock = "clock";
  std::string reset = "rst";
  std::string oe = "__NA__";
  unsigned cycle_limit = 100u;
  unsigned error_rate_cycle_limit = 100u;
  unsigned accum_error_rate = 0u;
  unsigned accum_error = 0u;
  unsigned avg_error = 0u;
  std::string log_file = "cirkit_maniac.log";
  std::string report_file = "maniac.rpt";
  std::string csv_file = "maniac.csv";
  unsigned debug = 1u; // Default in debug mode.
  unsigned signed_outputs = 0u; // To indidcate if inputs are signed or not.

  unsigned worst_case_en = 1u; // If worst-case computations are enabled.
  unsigned accum_error_en = 1u; // If acc error computations are enabled.
  unsigned worst_er_en = 1u; // If worst-error-rate computations are enabled.
  unsigned accum_er_en = 1u; // If accum-error-rate computations are enabled.
  unsigned avg_case_en = 1u; // If avg-case computations are enabled.
  unsigned cex_en = 0u;  //  counter-example writeout (default false)
  unsigned cross_verify = 0u;  //  cross-verify the accum computations results.

  // avg_error = accumulated_error / cycles_for_avg_error;
  // if its 0u, then division will be rounded to nxt shifting.
  // if its 1u, then division will be with a constant
  // if its 2u, then generic division will 
  unsigned avg_case_effort_level = 0u;
  unsigned cycles_for_avg_error = 256u;

  unsigned opt = 0u;
  unsigned err_upper_bound = 0u;
  unsigned err_lower_bound = 0u;
  
  // Private Variables.
  //mc_design gold, appx;
  const std::string work = "work";
  const std::string worst_case_work = "work/worst_case";
  const std::string accum_case_work = "work/accum_case";
  const std::string worst_er_work = "work/worst_er";
  const std::string accum_er_work = "work/accum_er";
  const std::string avg_case_work = "work/avg_case";

  float wc_time = 0, acc_time = 0, err_time = 0, acc_err_time = 0;
  
  // Not for CLI. Private variables.
  mc_port port_to_check = std::make_pair("__NA__", 0);

  void print_program_options (std::ofstream &file);
  inline bool true_or_false (unsigned value);
  bool check_sanity (const mc_design &golden, const mc_design &approx);
  inline void update_avg_case_clock_cycles ();
};

}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:


