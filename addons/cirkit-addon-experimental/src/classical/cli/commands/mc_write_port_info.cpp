/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mc_write_port_info.hpp"

#include <iostream>

#include <classical/cli/stores.hpp>
#include <classical/functions/strash.hpp>
#include <classical/utils/aig_utils.hpp>
#include <yosys/yosys_api.hpp>
#include <vector>

using namespace boost::program_options;

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

mc_write_port_info_command::mc_write_port_info_command( const environment::ptr& env )
  : command( env, "MANIAC Prints the port info" )
{
  opts.add_options()
    ( "v_file", value( &v_file ), "Input Verilog file" )
    ( "top_module", value( &top_module ), "Top Module Name [Default = last read module]" )
    ;
}

bool mc_write_port_info_command::execute() {
  Yosys::yosys_setup();
  run("read_verilog  " + v_file);

  auto design = Yosys::yosys_get_design();
  auto my_module = design->top_module();

  // TODO: top_module() is not defined. Seg fault.
  for (auto mod:design->modules())
    my_module = mod;

  std::vector<Yosys::Wire*> all_inputs;
  std::vector<Yosys::Wire*> all_outputs;

  // TODO: Simplify all these iterations and write to a file
  for (auto &w : my_module->wires_) { // _wires is a dict in Module
    if (w.second->port_input) {
      all_inputs.push_back(w.second);
    }
    if (w.second->port_output) all_outputs.push_back(w.second);
    
  }

  std::cout << "All Inputs : ";
  for (auto &p : all_inputs) {
    std::cout << (p->name).str() << " [" << p->width << "]   ";
  }
  std::cout << std::endl;

  std::cout << "All Outputs : ";
  for (auto &p : all_outputs) {
    std::cout << (p->name).str() << " [" << p->width << "]   ";
  }
  std::cout << std::endl;
  
  Yosys::yosys_shutdown();
  return true;
}

// TODO: Move this to the api.
void mc_write_port_info_command::run (const std::string &command) {
  std::cout << "running command" << std::endl;
  try {    Yosys::run_pass(command); }
  catch (...) { throw; }
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:


