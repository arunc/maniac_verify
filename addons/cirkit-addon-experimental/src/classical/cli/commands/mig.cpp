/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mig.hpp"

#include <core/cli/store.hpp>
#include <core/utils/program_options.hpp>
#include <classical/mig.hpp>
#include <classical/functions/aig_to_mig.hpp>
#include <classical/functions/mig_from_string.hpp>
#include <classical/io/mig_verilog.hpp>
#include <classical/utils/mig_utils.hpp>

using namespace boost::program_options;

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

mig_command::mig_command( const environment::ptr& env )
  : aig_base_command( env, "Creates an MIG from an AIG" )
{
  opts.add_options()
    ( "expr,e", value( &expr ),   "Create MIG from string expression" )
    ( "mighty", value( &mighty ), "Creates MIG from an auto-generated Verilog file from Mighty" )
    ( "new,n",                    "Add a new entry to the store; if not set, the current entry is overriden" )
    ( "name",   value( &name ),   "Override default name" )
    ;
  be_verbose();
}

command::rules_t mig_command::validity_rules() const
{
  return {
    { [&]() { return opts.is_set( "expr" ) || opts.is_set( "mighty" ) || env->store<aig_graph>().current_index() >= 0; }, "no current AIG available" }
  };
}

bool mig_command::execute()
{
  auto& migs = env->store<mig_graph>();

  if ( migs.empty() || opts.is_set( "new" ))
  {
    migs.extend();
  }

  if ( opts.is_set( "expr" ) )
  {
    migs.current() = mig_from_string( expr );
  }
  else if ( opts.is_set( "mighty" ) )
  {
    migs.current() = read_mighty_verilog( mighty );
  }
  else
  {
    auto settings = make_settings();
    migs.current() = aig_to_mig( aig(), settings );
  }

  if ( opts.is_set( "name" ) )
  {
    mig_info( migs.current() ).model_name = name;
  }

  return true;
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
