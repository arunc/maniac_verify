/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mig_rewrite.hpp"

#include <boost/format.hpp>

#include <core/utils/program_options.hpp>
#include <classical/optimization/mig_rewriting.hpp>

using namespace boost::program_options;

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

mig_rewrite_command::mig_rewrite_command( const environment::ptr& env ) : mig_base_command( env, "MIG rewriting" )
{
  opts.add_options()
    ( "area,a",                                  "Optimize for area instead of delay" )
    ( "memristor",                               "Optimize for memristor costs" )
    ( "nodist",                                  "Don't use distributivity rule" )
    ( "noassoc",                                 "Don't use associativity rule" )
    ( "nocassoc",                                "Don't use complementary associativity rule" )
    ( "effort,e", value_with_default( &effort ), "Number of optimization cycles" )
    ;
  be_verbose();
}

bool mig_rewrite_command::execute()
{
  const auto settings = make_settings();
  settings->set( "effort", effort );
  settings->set( "use_distributivity", !opts.is_set( "nodist" ) );
  settings->set( "use_associativity", !opts.is_set( "noassoc" ) );
  settings->set( "use_compl_associativity", !opts.is_set( "nocassoc" ) );
  const auto statistics = std::make_shared<properties>();

  if ( opts.is_set( "area" ) )
  {
    mig() = mig_area_rewriting( mig(), settings, statistics );
  }
  else if ( opts.is_set( "memristor" ) )
  {
    mig() = mig_memristor_rewriting( mig(), settings, statistics );
  }
  else
  {
    mig() = mig_depth_rewriting( mig(), settings, statistics );
  }

  std::cout << boost::format( "[i] run-time: %.2f secs" ) % statistics->get<double>( "runtime" ) << std::endl;

  if ( is_verbose() )
  {
    std::cout << boost::format( "[i] distributivity: %d" ) % statistics->get<unsigned>( "distributivity_count" ) << std::endl
              << boost::format( "[i] associativity: %d" ) % statistics->get<unsigned>( "associativity_count" ) << std::endl
              << boost::format( "[i] complementary associativity: %d" ) % statistics->get<unsigned>( "compl_associativity_count" ) << std::endl;
  }

  return true;
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
