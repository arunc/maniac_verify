/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "miter.hpp"

#include <iostream>
#include <core/utils/program_options.hpp>
#include <classical/cli/stores.hpp>
#include <classical/functions/aig_miter.hpp>
#include <classical/utils/aig_utils.hpp>

using namespace boost::program_options;

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

miter_command::miter_command( const environment::ptr & env )
  : command( env, "Constructs the miter of the circuits" )
  , aigs( env->store<aig_graph>() )
{
  opts.add_options()
    ( "circuit1",     value_with_default( &circ1 ),  "Store-ID of circuit1" )
    ( "circuit2",     value_with_default( &circ2 ),  "Store-ID of circuit2" )
    ( "new,n",                                       "Add a new entry to the store; if not set, the current entry is overriden" )
    ;
  be_verbose();
}

command::rules_t miter_command::validity_rules() const
{
  return {
    {[&]() { return (circ1 == -1 && aigs.size() >= 2u) || (circ1 >= 0 && circ1 < int(aigs.size())); }, "store-ID of circuit1 is invalid" },
    {[&]() { return (circ2 == -1 && aigs.size() >= 2u) || (circ2 >= 0 && circ2 < int(aigs.size())); }, "store-ID of circuit2 is invalid" }
  };
}

bool miter_command::execute()
{
  const auto aig_circ1 = circ1 >= 0 ? aigs[circ1] : aigs[aigs.size()-2u];
  const auto aig_circ2 = circ2 >= 0 ? aigs[circ2] : aigs[aigs.size()-1u];

  if ( is_verbose() )
  {
    std::cout << "[i] construct miter of ''" << aig_info( aig_circ1 ).model_name << "'' and ''" << aig_info( aig_circ2 ).model_name << "''" << std::endl;
  }

  if ( opts.is_set( "new" ))
  {
    aigs.extend();
  }

  aigs.current() = aig_miter( aig_circ1, aig_circ2 );

  return true;
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
