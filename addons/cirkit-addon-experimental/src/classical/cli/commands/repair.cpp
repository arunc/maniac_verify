/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "repair.hpp"

#include <boost/format.hpp>

#include <core/utils/program_options.hpp>

#include <classical/debug/aig_repair.hpp>

using namespace boost::program_options;

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

repair_command::repair_command( const environment::ptr & env ) : aig_base_command( env, "Repair a circuit utilizing AIG synthesis" )
{
  opts.add_options()
    ( "strategy",  value_with_default( &strategy ),  "Repair strategy\n0: Bdd-bAsed Safety synthesIs tooL (BASIL)\n1: SAT-based safety synthesis (Demiurge)" )
    ;
}

bool repair_command::execute()
{
  const auto& aig_circ = aig();

  auto settings = make_settings();
  auto statistics = std::make_shared<properties>();
  aig_repair( aig_circ, aig_synthesizer_t(strategy), settings, statistics );
  std::cout << boost::format( "[i] repair run-time: %.2f secs" ) % statistics->get<double>( "runtime" ) << std::endl;

  return true;
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
