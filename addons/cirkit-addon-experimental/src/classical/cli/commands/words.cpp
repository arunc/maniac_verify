/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "words.hpp"

#include <boost/range/algorithm.hpp>

#include <core/utils/range_utils.hpp>
#include <core/utils/string_utils.hpp>
#include <classical/cli/stores.hpp>
#include <classical/functions/slice.hpp>

using namespace boost::program_options;

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

words_command::words_command( const environment::ptr& env )
  : aig_base_command( env, "Transparent words" )
{
  opts.add_options()
    ( "print,p",                      "Print all words" )
    ( "slice,s", value( &slicedesc ), "Slices based on words (e.g. 1,3:4 creates a slice with inputs from word 1 and 3 and outputs from word 4)" )
    ;
}

bool words_command::execute()
{
  const auto& words = info().trans_words;

  if ( opts.is_set( "print" ) )
  {
    for ( const auto& word : index( words ) )
    {
      std::cout << "[i] word " << word.index << ": " << any_join( word.value, " " ) << std::endl;
    }
  }

  if ( opts.is_set( "slice" ) )
  {
    const auto io = split_string_pair( slicedesc, ":" );
    std::vector<unsigned> in_ids, out_ids;
    parse_string_list( in_ids, io.first, "," );
    parse_string_list( out_ids, io.second, "," );

    std::vector<aig_node> slice_inputs, slice_outputs;

    for ( auto id : in_ids )
    {
      for ( auto node : words[id] )
      {
        if ( boost::find( slice_inputs, node ) == slice_inputs.end() ) { slice_inputs.push_back( node ); }
      }
    }

    for ( auto id : out_ids )
    {
      for ( auto node : words[id] )
      {
        if ( boost::find( slice_outputs, node ) == slice_outputs.end() ) { slice_outputs.push_back( node ); }
      }
    }

    const auto sliced_aig = slice( aig(), slice_inputs, slice_outputs );

    auto& aigs = env->store<aig_graph>();
    aigs.extend();
    aigs.current() = sliced_aig;
  }

  return true;
}


}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
