/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "write_verilog.hpp"

#include <boost/program_options.hpp>

#include <classical/io/mig_verilog.hpp>

using namespace boost::program_options;

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

write_verilog_command::write_verilog_command( const environment::ptr& env ) : aig_mig_command( env, "Writes circuit to Verilog", "Write current %s" )
{
  opts.add_options()
    ( "filename", value( &filename ), "Verilog filename" )
    ;
  be_verbose();
}

command::rules_t write_verilog_command::validity_rules() const
{
  return {
    { [&]() { return opts.is_set( "filename" ); }, "no filename set" }
  };
}

bool write_verilog_command::execute_aig()
{
  return false;
}

bool write_verilog_command::execute_mig()
{
  write_verilog( mig(), filename );
  return true;
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
