/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "aig_debug_utils.hpp"

#include <classical/utils/aig_utils.hpp>

#include <boost/format.hpp>

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

void aig_print_debug_stats( const aig_graph& aig, std::ostream& os )
{
  const auto& info = aig_info( aig );
  auto n = info.inputs.size();

  std::string name = info.model_name;
  if ( name.empty() )
  {
    name = "(unnamed)";
  }

  /*** count annotations ***/
  unsigned pure_nodes; /* not annotated */
  std::map< std::string, unsigned > annotation_counter;
  auto annotation = get( boost::vertex_annotation, aig );
  for ( const auto& node : boost::make_iterator_range( vertices( aig ) ) )
  {
    std::set< std::string > string_annotations;
    if ( annotation[ node ].find( "miter_id" ) != annotation[ node ].end() )
    {
      if ( annotation[ node ].at( "miter_id" ) != "" )
      {
        std::istringstream iss( annotation[ node ].at( "miter_id" ) );
        std::copy(std::istream_iterator<std::string>(iss),
                  std::istream_iterator<std::string>(),
                  std::inserter(string_annotations, string_annotations.begin()));
      }
    }

    if ( string_annotations.empty() )
    {
      ++pure_nodes;
    }

    for ( const auto& s : string_annotations )
    {
      annotation_counter[s]++;
    }
  }

  const unsigned total_nodes = num_vertices( aig );
  const unsigned zero_nodes = annotation_counter["0"];
  const unsigned one_nodes = annotation_counter["1"];
  os << ( boost::format( "[i] aig:%20s %d 0:%d 1:%d r:%d" )
          % name % total_nodes % zero_nodes % one_nodes % pure_nodes ).str()<< '\n';
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
