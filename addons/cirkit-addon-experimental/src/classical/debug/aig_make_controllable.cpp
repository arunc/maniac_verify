/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "aig_make_controllable.hpp"

#include <classical/utils/aig_utils.hpp>

#include <boost/format.hpp>

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/
void aig_assign_name_pi( aig_graph& aig, const aig_node& input, const std::string &name )
{
  auto info = aig_info( aig );
  auto it = info.node_names.find( input );
  if ( it != info.node_names.end() )
  {
    it->second = name;
  }
  else
  {
    info.node_names.insert( {input,name} );
  }
}

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

aig_graph aig_make_controllable( const aig_graph &aig, const std::vector<unsigned>& gates )
{
  const auto& info = aig_info( aig );
  const auto& complementmap = boost::get( boost::edge_complement, aig );

  aig_graph result_aig;
  aig_initialize( result_aig, info.model_name );
  aig_info( result_aig ).enable_strashing = false;
  aig_info( result_aig ).enable_local_optimization = false;

  std::map< aig_node, aig_node > node_map;

  /* constant */
  if ( aig_is_constant_used( aig ) )
  {
    aig_function f = aig_get_constant( result_aig, false );
    node_map.insert( { info.constant, f.node } );
  }

  /* iterate through nodes in order */
  for ( const auto& node : boost::make_iterator_range( vertices( aig ) ) )
  {
    const auto _out_degree = out_degree( node, aig );

    /* input */
    if ( boost::find( info.inputs, node ) != info.inputs.end() )
    {
      // std::cout << "in " << node << '\n';
      std::string name = ( boost::format("in%s") % node ).str();
      auto it = info.node_names.find( node );
      if ( it != info.node_names.end() )
      {
        name = it->second;
      }
      aig_function f = aig_create_pi( result_aig, name );
      node_map.insert( { node, f.node } );
    }

    /* latches */
    if ( _out_degree == 1u )
    {
      assert( false && "latches are not supported" );
    }

    /* and gates */
    else if ( _out_degree == 2u )
    {
      /* check if gate should be transformed to controllable input */
      auto it = boost::find( gates, node );
      if ( it != gates.end() )
      {
        aig_function f = aig_create_pi( result_aig, ( boost::format("controllable_%s") % aig_to_literal( aig, {node,false}) ).str() );
        // std::cout << "control_in " << aig_to_literal( aig, {node,false} ) << '\n';
        node_map.insert( { node, f.node } );
      }
      else
      {
        const auto& edges = boost::make_iterator_range( out_edges( node, aig ) );

        const auto& r_index = target( edges[ 0u ], aig );
        const auto r_compl = complementmap[ edges[ 0u ] ];

        const auto& l_index = target( edges[ 1u ], aig );
        const auto l_compl = complementmap[ edges[ 1u ] ];

        // std::cout << "and " << aig_to_literal( aig, {r_index, r_compl} ) << ' ' << aig_to_literal( aig, {l_index, l_compl} ) << '\n';

        auto rhs = node_map.at( r_index );
        auto lhs = node_map.at( l_index );
        aig_function f = aig_create_and( result_aig, { rhs, r_compl }, { lhs, l_compl } );
        node_map.insert( { node, f.node } );
      }

      /* output */
      for ( const auto &o : info.outputs )
      {
        if ( o.first.node == node )
        {
          auto it = node_map.find( node );
          assert( it != node_map.end() );
          aig_create_po( result_aig, { it->second, o.first.complemented }, o.second );
          // std::cout << "out " << aig_to_literal( aig, o.first ) << '\n';
        }
      }
    }
  }

  aig_info( result_aig ).enable_strashing = true;
  aig_info( result_aig ).enable_local_optimization = true;

  return result_aig;
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
