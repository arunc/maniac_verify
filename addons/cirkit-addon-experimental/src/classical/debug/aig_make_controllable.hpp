/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file aig_make_controllable.hpp
 *
 * @brief Replace a gate with a controllable input
 *
 * @author Heinz Riener
 * @since  2.3
 */

#ifndef AIG_MAKE_CONTROLLABLE_HPP
#define AIG_MAKE_CONTROLLABLE_HPP

#include <classical/aig.hpp>

#include <vector>

namespace cirkit
{

aig_graph aig_make_controllable( const aig_graph &aig, const std::vector<unsigned>& gates = {} );

}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
