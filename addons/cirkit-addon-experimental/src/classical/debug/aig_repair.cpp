/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "aig_repair.hpp"

#include <classical/io/write_aiger.hpp>
#include <core/utils/system_utils.hpp>
#include <core/utils/timer.hpp>

#include <boost/format.hpp>

#include <algorithm>
#include <fstream>
#include <unistd.h>

namespace cirkit
{

std::vector< std::string > read_file( const std::string& filename )
{
  std::vector< std::string > result;
  std::ifstream is( filename.c_str() );
  std::string line;
  while ( !is.eof() )
  {
    std::getline( is, line );
    result.push_back( line );
  }
  return result;
}

void diff( std::vector< std::string >& out, const std::vector< std::string > &in )
{
  for ( const auto& i : in )
  {
    out.erase( std::remove(out.begin(), out.end(), i), out.end() );
  }

  // cut comment
  auto it = std::find( out.begin(), out.end(), "c" );
  if ( it != out.end() )
  {
    const unsigned pos = std::distance( out.begin(), it );
    out.resize( pos );
  }
}

aig_synthesis_result_t aig_repair( const aig_graph& circuit, const aig_synthesizer_t repair_strategy,
                                   properties::ptr settings, properties::ptr statistics )
{
  /* Timer */
  properties_timer t( statistics );

  const std::string in_file = ( boost::format( "/tmp/repair-%s.aag" ) % getpid() ).str();
  const std::string out_file = ( boost::format( "/tmp/result-%s.aag" ) % getpid() ).str();

  write_aiger( circuit, in_file );

  /* call synthesizer */
  result_t result;
  if ( repair_strategy == aig_synthesizer_t::demiurge )
  {
    /* Demiurge 1.2.0 */
    result = execute_and_return( ( boost::format( "demiurge-bin -i %s -o %s -b lp1 -c lp1" ) % in_file % out_file ).str() );
  }
  else if ( repair_strategy == aig_synthesizer_t::basil )
  {
    result = execute_and_return( ( boost::format( "basil %s --extract 1 2> /dev/null > %s" ) % in_file % out_file ).str() );
  }
  else
  {
    std::cout << "[e] unsupported synthesizer selection" << '\n';
    return { aig_synthesizer_retval_t::error, std::vector<std::string>() };
  }

  /* interpret results */
  if ( result.first == 10 )
  {
    auto in = read_file( in_file );
    auto out = read_file( out_file );
    diff( out, in );
    for ( const auto& o : out )
    {
      std::cout << o << '\n';
    }
    std::cout << ( boost::format("[i] successfully computed repair: %s gates") % (out.size()-1u) ) << std::endl;
    return { aig_synthesizer_retval_t::realizable, out };
  }
  else if ( result.first == 20 )
  {
    std::cout << "[i] synthesis problem is unrealizable" << std::endl;
    return { aig_synthesizer_retval_t::unrealizable, std::vector<std::string>() };
  }
  else
  {
    std::cout << "[e] unknown error code from AIG synthesizer" << std::endl;
    return { aig_synthesizer_retval_t::error, std::vector<std::string>() };
  }
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
