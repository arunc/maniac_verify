/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file aig_repair.hpp
 *
 * @brief Repair AIG utilizing an AIG synthesizer
 *
 * @author Heinz Riener
 * @since  2.3
 */

#ifndef AIG_REPAIR_HPP
#define AIG_REPAIR_HPP

#include <classical/aig.hpp>
#include <core/properties.hpp>

#include <vector>

namespace cirkit
{

enum class aig_synthesizer_t
{
  basil = 0
, demiurge = 1
};

enum class aig_synthesizer_retval_t
{
  realizable = 0
, unrealizable = 1
, error = 2
};

using aig_synthesis_result_t = std::pair< aig_synthesizer_retval_t, std::vector< std::string > >;

aig_synthesis_result_t aig_repair( const aig_graph& circuit, aig_synthesizer_t repair_strategy = aig_synthesizer_t::basil,
                                   properties::ptr settings = properties::ptr(), properties::ptr statistics = properties::ptr() );

}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
