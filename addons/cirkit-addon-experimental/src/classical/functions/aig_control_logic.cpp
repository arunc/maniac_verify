/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "aig_control_logic.hpp"

#include <iostream>
#include <unordered_map>
#include <vector>

#include <boost/dynamic_bitset.hpp>
#include <boost/format.hpp>
#include <boost/graph/topological_sort.hpp>
#include <boost/optional.hpp>

#include <core/graph/coi.hpp>
#include <core/graph/depth.hpp>
#include <core/utils/bitset_utils.hpp>
#include <core/utils/graph_utils.hpp>
#include <core/utils/range_utils.hpp>
#include <core/utils/timer.hpp>
#include <classical/functions/fanout_free_regions.hpp>
#include <classical/functions/npn_canonization.hpp>
#include <classical/functions/simulate_aig.hpp>
#include <classical/utils/cut_enumeration.hpp>
#include <classical/utils/cut_enumeration_traits.hpp>
#include <classical/utils/aig_utils.hpp>
#include <classical/utils/truth_table_utils.hpp>
#include <classical/functions/compute_levels.hpp>
#include <boost/range/algorithm_ext/iota.hpp>

#define timer timer_class
#include <boost/progress.hpp>
#undef timer

namespace cirkit
{


/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/


struct npn_hash_table_entry_t
{
  npn_hash_table_entry_t() {}
  npn_hash_table_entry_t( unsigned long long tt, unsigned long long npn, const std::vector<unsigned>& perm, const boost::dynamic_bitset<>& phase )
    : tt( tt ), npn( npn ), perm( perm ), phase( phase ) {}

  unsigned long long   tt  = 0;
  unsigned long long   npn = 0;
  std::vector<unsigned>   perm;
  boost::dynamic_bitset<> phase;
};

using npn_hash_table_t = std::vector<npn_hash_table_entry_t>;


class aig_control_logic_manager
{
public:
 aig_control_logic_manager( const aig_graph& aig, std::vector<aig_node>& control_inputs,
				unsigned npn_hash_table_size, bool verbose );
 netlist_muxs run();

private:

  tt compute_npn( const tt& tt, boost::dynamic_bitset<>& phase, std::vector<unsigned>& perm );
  std::map<aig_node, structural_cut>  muxs_structural_cut_enumeration( unsigned k );

  inline std::vector<unsigned> inv( const std::vector<unsigned>& perm ) const
  {
    std::vector<unsigned> invperm( perm.size() );
    for ( auto i = 0u; i < perm.size(); ++i ) { invperm[perm[i]] = i; }
    return std::move( invperm );
  }

  void insert_control_node(aig_node, structural_cut, mux_structure &);
  void remove_muxs_redundancy();

  tt mux1s2i_tt = tt( 64u, 0x000000FFFF00FFFF);// one selection lines two inputs
  tt mux1s4i_tt= tt( 64u,  0xCC00A0A0CC00A0A0);// one selection lines four inputs
  tt mux2s4i_tt = tt( 64u, 0x000F3355FF0F3355);// two selection lines four inputs
  tt mux2s3i_tt = tt( 64u, 0x000003CFFFFF03CF);// two selection lines three inputs
  tt mux2s3i_v1_tt = tt( 64u, 0x000003CF33FF03CF);// two selection lines three inputs

public:
  const aig_graph&                   aig;
  const aig_graph_info&              info;
  aig_node_vec_t 	  	     control_nodes;
  aig_node_vec_t& 	     	     control_inputs;
  netlist_muxs                       allmuxs;
  npn_hash_table_t                   npn_table;
  bool                               verbose;
  double                             runtime_cut = 0.0;
  double                             runtime_npn = 0.0;
  unsigned long                      cache_hit = 0ul;
  unsigned long                      cache_miss = 0ul;
  properties::ptr                    statistics;
  properties::ptr                    settings;
};

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

std::vector<tt> npn_class( const tt& t)
{
  /* initialize */
  auto n = tt_num_vars( t );

  assert( n <= 6u );

  const auto& swap_array = tt_store::i().swaps( n );
  const auto& flip_array = tt_store::i().flips( n );
  const auto total_swaps = swap_array.size();
  const auto total_flips = flip_array.size();

  std::vector<tt> npnclass;

  auto t1 = t;
  auto t2 = ~t; /* inversion */
  npnclass.push_back(t1);
  npnclass.push_back(t2);

  if ( false /* verbose */ )
  {
    std::cout << "[i] swaps: " << any_join( swap_array, " " ) << std::endl
              << "[i] flips: " << any_join( flip_array, " " ) << std::endl;
  }

  int best_flip = total_flips;
  int best_swap = total_swaps;

  for ( int i = total_swaps - 1; i >= 0; --i )
  {
    const auto pos = swap_array[i];
    t1 = tt_permute( t1, pos, pos + 1 );
    t2 = tt_permute( t2, pos, pos + 1 );
    tt_shrink( t1, n ); tt_shrink( t2, n );
    npnclass.push_back(t1);
    npnclass.push_back(t2);
  }

  for ( int j = total_flips - 1; j >= 0; --j )
  {
    t1 = tt_flip( tt_permute( t1, 0u, 1u ), flip_array[j] );
    t2 = tt_flip( tt_permute( t2, 0u, 1u ), flip_array[j] );
    tt_shrink( t1, n ); tt_shrink( t2, n );
    npnclass.push_back(t1);
    npnclass.push_back(t2);

    for ( int i = total_swaps - 1; i >= 0; --i )
    {
      const auto pos = swap_array[i];
      t1 = tt_permute( t1, pos, pos + 1 );
      t2 = tt_permute( t2, pos, pos + 1 );
      tt_shrink( t1, n ); tt_shrink( t2, n );
      npnclass.push_back(t1);
      npnclass.push_back(t2);
    }
  }

  return npnclass;
}
/**********************************************************/

std::map<aig_node, structural_cut> aig_control_logic_manager::muxs_structural_cut_enumeration( unsigned k)
{
  /* settings */
  const auto include_constant = get( settings, "include_constant", false );

  /* timer */
  properties_timer t( statistics );

  const auto n = num_vertices( aig );
  std::map<aig_node, structural_cut> cut_map;

  std::vector<aig_node> topsort( n );
  boost::topological_sort( aig, topsort.begin() );
  //const auto levels = compute_levels( aig );

  for ( auto node : topsort )
  {
    if ( node == 0u && !include_constant )
    {
      cut_map.insert( {0u, {boost::dynamic_bitset<>( n )}} );
    }
    else if ( out_degree( node, aig ) == 0u )
    {
      cut_map.insert( {node, {onehot_bitset( n, node )}} );
    }
    else
    {
      structural_cut cuts;
      const auto children = get_children( aig, node );
      for ( const auto& v1 : cut_map[children[0u].node] )
      {
	if ( v1.count() >= k ) { continue; }
        for ( const auto& v2 : cut_map[children[1u].node] )
        {
          auto cut = v1 | v2;
          if ( cut.count() > k || boost::find(cuts,cut)!=cuts.end() ) { continue; }
	  //auto cut_nodes=get_index_vector(cut);
	 // if(levels.at(cut_nodes[0])>k) { continue; }
	  cuts += cut;
        }
      }

      cuts += onehot_bitset( n, node );
      cut_map.insert( {node, cuts} );
    }
  }

  return cut_map;
}


aig_control_logic_manager::aig_control_logic_manager( const aig_graph& aig, std::vector<aig_node>& control_inputs,
							unsigned npn_hash_table_size, bool verbose )
  : aig( aig ),
    control_inputs(control_inputs),
    info( aig_info( aig ) ),
    npn_table( npn_hash_table_size ),
    verbose( verbose )
{


    settings = std::make_shared<properties>();
    statistics = std::make_shared<properties>();
    settings->set( "verbose",      verbose );
}

netlist_muxs aig_control_logic_manager::run()
{

  auto mux1s2i_class = npn_class( mux1s2i_tt );
  auto mux1s4i_class = npn_class( mux1s4i_tt);
  auto mux2s4i_class = npn_class( mux2s4i_tt );
  auto mux2s3i_class = npn_class( mux2s3i_tt );
  auto mux2s3i_v1_class = npn_class( mux2s3i_v1_tt);

  auto map = muxs_structural_cut_enumeration( 6u );

  const auto levels = compute_levels( aig );

  for ( const auto& p : map )
  {
      for ( const auto& cut : p.second )
      {
	tt sim_tt= simulate_cut_tt<aig_graph>( p.first, cut, aig ) ;
	auto mux_inputs=get_index_vector(cut);
	unsigned mux_flag=0u;
	if(mux_inputs.size()==3&&boost::find(mux1s2i_class, sim_tt)!=mux1s2i_class.end()){
		mux_flag=1u;
	}
	else if(mux_inputs.size()==5&&boost::find(mux1s4i_class, sim_tt)!=mux1s4i_class.end()){
		mux_flag=2u;
	}
	else if(mux_inputs.size()==5&&boost::find(mux2s3i_class, sim_tt)!=mux2s3i_class.end()){
		mux_flag=3u;
	}
	else if(mux_inputs.size()==5&&boost::find(mux2s3i_v1_class, sim_tt)!=mux2s3i_v1_class.end()){
		mux_flag=3u;
	}
	else if(mux_inputs.size()==6&&boost::find(mux2s4i_class, sim_tt)!=mux2s4i_class.end()){
		mux_flag=4u;
	}


	if(mux_flag!=0){

		boost::dynamic_bitset<> phase;
		std::vector<unsigned> perm;
		mux_structure mux1;
		tt nbnex_tt= compute_npn( sim_tt, phase, perm );

    if ( verbose )
    {
      std::cout << "Mux: ";
      for( const auto& in: perm )
      {
        if( in < mux_inputs.size() )
        {
          std::cout << " " << mux_inputs[in];
        }
      }
      std::cout << " " << tt_to_hex(sim_tt)<<" "<<tt_to_hex(nbnex_tt)<<std::endl;
    }

		auto sb=perm.size()-mux_inputs.size();
		auto in1=mux_inputs[perm[sb]];
		auto in2=mux_inputs[perm[sb+1]];
		std::vector<aig_node> wordinput1, wordinput2, wordinput3, wordinput4;
		if( mux_flag==1){
			wordinput1+=mux_inputs[perm[sb+1]];
			wordinput2+=mux_inputs[perm[sb+2]];
		}
		else if( mux_flag==2) {
			in1=mux_inputs[perm[sb+1]];
			wordinput1+=mux_inputs[perm[sb]];
			wordinput2+=mux_inputs[perm[sb+2]];
			wordinput3+=mux_inputs[perm[sb+3]];
			wordinput4+=mux_inputs[perm[sb+4]];
		}
		else if( mux_flag==3){
			in2=mux_inputs[perm[sb+3]];
			wordinput1+=mux_inputs[perm[sb+1]];
			wordinput2+=mux_inputs[perm[sb+2]];
			wordinput3+=mux_inputs[perm[sb+4]];
		}
		else if( mux_flag==4) {
			in1=mux_inputs[perm[sb+3]];
			in2=mux_inputs[perm[sb+4]];
			wordinput1+=mux_inputs[perm[sb]];
			wordinput2+=mux_inputs[perm[sb+1]];
			wordinput3+=mux_inputs[perm[sb+2]];
			wordinput4+=mux_inputs[perm[sb+5]];
		}

		mux1.word_inputs+=wordinput1;
		mux1.word_inputs+=wordinput2;

		if(mux_flag==2){
			mux1.word_inputs+=wordinput3;
			mux1.word_inputs+=wordinput4;
		}
		else if(mux_flag==3){
			mux1.word_inputs+=wordinput3;
		}
		else if(mux_flag==4){
			mux1.word_inputs+=wordinput3;
			mux1.word_inputs+=wordinput4;
		}
		insert_control_node(in1, map.at(in1), mux1);
		if(mux_flag>2)
			insert_control_node(in2, map.at(in2), mux1);

		mux1.word_output+=p.first;
		boost::sort(mux1.selections);

		bool exist_mux=false;
		for(auto &mux2: allmuxs){
			bool same_mux=true;
			if(mux1.selections==mux2.selections && mux1.word_inputs.size()==mux2.word_inputs.size()){
				for(auto i=0; i<mux1.word_inputs.size(); i++){
					if(levels.at(mux1.word_inputs[i].at(0))>=levels.at(mux2.word_output[0])){
						same_mux=false;
						break;
					}
				}
				if(!same_mux) continue;
				for(auto i=0; i<mux2.word_inputs.size(); i++){
					mux2.word_inputs[i].insert(mux2.word_inputs[i].end(),mux1.word_inputs[i].begin(),mux1.word_inputs[i].end());
				}
				mux2.word_output.insert(mux2.word_output.end(),mux1.word_output.begin(),mux1.word_output.end());
				exist_mux=true;
				break;
			}
		}
		if(!exist_mux){
			allmuxs+=mux1;
		}
	 }
      }
  }


   remove_muxs_redundancy();

   runtime_cut += statistics->get<double>( "runtime" );

  return allmuxs;

}

tt aig_control_logic_manager::compute_npn( const tt& tt, boost::dynamic_bitset<>& phase, std::vector<unsigned>& perm )
{
  boost::dynamic_bitset<> npn;

  /* compute NPN and use hash table if possible */
  if ( !npn_table.empty() )
  {
    const auto ttu      = tt.to_ulong();
    const auto hash_key = ttu % npn_table.size();
    const auto& entry   = npn_table[hash_key];
    if ( entry.tt == ttu )
    {
      ++cache_hit;
      npn = boost::dynamic_bitset<>( 64u, entry.npn );
      perm = entry.perm;
      phase = entry.phase;
    }
    else
    {
      ++cache_miss;
      increment_timer t( &runtime_npn );
      npn = exact_npn_canonization( tt, phase, perm );
      npn_table[hash_key] = npn_hash_table_entry_t( ttu, npn.to_ulong(), perm, phase );
    }
  }
  else
  {
    increment_timer t( &runtime_npn );
    npn = exact_npn_canonization( tt, phase, perm );
  }

  return npn;
}


void aig_control_logic_manager::insert_control_node(aig_node node, structural_cut core_node, mux_structure & mux1){

	mux1.selections.push_back(node);
	if(boost::find(control_nodes, node)==control_nodes.end()){
		if(boost::find(info.inputs, node)!=info.inputs.end()){
			control_inputs.push_back(node);
			control_nodes.push_back(node);
			mux1.primary_control_inputs.push_back(node);
		}
		else{
			//auto core_node = map.at(node);
			auto last_cut = core_node[0];
			print_as_set( std::cout, last_cut );
			std::cout << " : are inputs of " << node<<std::endl;
			auto cut_inputs = get_index_vector(last_cut);
			for(const auto& cut_in: cut_inputs){
				if(boost::find(info.inputs, cut_in)==info.inputs.end()){
					control_nodes.push_back(node);
					return;
				}
			}
			for(const auto& cut_in: cut_inputs){
				if(boost::find(control_nodes, cut_in)==control_nodes.end()){
					if(boost::find(info.inputs, cut_in)!=info.inputs.end()){
						control_inputs.push_back(cut_in);
						mux1.primary_control_inputs.push_back(cut_in);
					}
					control_nodes.push_back(cut_in);
				}
			}
		}
	}
	return;
}

void aig_control_logic_manager::remove_muxs_redundancy(){

	std::vector<int> removed_muxs;
	for(auto m=0; m<allmuxs.size()-1; m++){
		auto mux1=allmuxs[m];
		auto rm=m;
		if(boost::find(removed_muxs,m)!=removed_muxs.end()){
			mux1=allmuxs[m-1];
			rm=m-1;
		}
		auto mux2=allmuxs[m+1];
		if(mux1.word_output == mux2.word_output){
			if(mux1.selections.size()>mux2.selections.size()
				||mux1.word_inputs.size()<mux2.word_inputs.size()){
				removed_muxs+=m+1;
			}
			else{
				removed_muxs+=rm;
			}
		}
	}

	std::sort (removed_muxs.begin(), removed_muxs.end());
	size_t i=0;
	for(auto & mind: removed_muxs){
		allmuxs.erase(allmuxs.begin()+mind-i);
		i++;
	}

	for(auto &mux : allmuxs){
		for(auto &win : mux.word_inputs){
			boost::sort(win);
			win.erase(std::unique (win.begin(), win.end()), win.end());
		}
		boost::sort(mux.word_output);
		mux.word_output.erase(std::unique (mux.word_output.begin(), mux.word_output.end()),mux.word_output.end());
	}

	return;

}

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

 netlist_muxs aig_control_logic( const aig_graph& aig, std::vector<aig_node>& control_inputs,
                                  const properties::ptr& settings,
                                  const properties::ptr& statistics)
{
  /* settings */
  const auto npn_hash_table_size = get( settings, "npn_hash_table_size", 200u );
  const auto verbose             = get( settings, "verbose",             false );

  /* timing */
  properties_timer t( statistics );

  /* AIG info */
  const auto& info = aig_info( aig );

  aig_control_logic_manager mgr( aig,  control_inputs, npn_hash_table_size, verbose );

  auto allmuxs=mgr.run();

  std::cout<<" cache_miss: "<<mgr.cache_miss<< " cache_hit: "<<mgr.cache_hit<<std::endl;
  set( statistics, "runtime_cut", mgr.runtime_cut );
  set( statistics, "runtime_npn", mgr.runtime_npn );
  set( statistics, "cache_hit",   mgr.cache_hit );
  set( statistics, "cache_miss",  mgr.cache_miss );
  return allmuxs;
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
