/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file aig_control_logic.hpp
 *
 * @brief Extraction of control logic
 *
 * @author Amr Sayed-Ahmed
 * @since  2.3
 */

#ifndef AIG_CONTROL_LOGIC_HPP
#define AIG_CONTROL_LOGIC_HPP

#include <core/properties.hpp>
#include <classical/aig.hpp>

namespace cirkit
{

using aig_node_vec_t = std::vector<aig_node>;

struct mux_structure
{
  aig_node_vec_t  selections;
  aig_node_vec_t  primary_control_inputs;
  std::vector<aig_node_vec_t> word_inputs;
  aig_node_vec_t word_output;
};

using netlist_muxs = std::vector<mux_structure>;

netlist_muxs aig_control_logic( const aig_graph& aig, aig_node_vec_t& control_inputs,
                              const properties::ptr& settings = properties::ptr(),
                              const properties::ptr& statistics = properties::ptr());
}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
