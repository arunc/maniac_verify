/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "aig_fault_injection.hpp"

#include <classical/utils/aig_utils.hpp>

#include <boost/assign/std/vector.hpp>
#include <boost/assign/std/set.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <boost/format.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int_distribution.hpp>

#include <chrono>
#include <set>

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

using node_set_t     = std::set<aig_node>;
using edge_set_t     = std::set<aig_edge>;
using filter_graph_t = boost::filtered_graph<aig_graph, boost::is_not_in_subset<edge_set_t>, boost::is_not_in_subset<node_set_t>>;

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

aig_graph aig_fault_injection( const aig_graph& aig, const std::map< aig_node, fault_type_t >& faults,
                               const properties::ptr& settings, const properties::ptr& statistics )
{
  node_set_t vertex_modify;
  edge_set_t edge_remove, edge_added;

  const auto& info = aig_info( aig );

  /* cannot deal with latches yet */
  assert( info.latch.size() == 0u );

  /* find all edges that need to be removed */
  unsigned gate_count = 0u;
  std::map< aig_edge, fault_type_t > edge_faults;
  for ( const auto& v : boost::make_iterator_range( vertices( aig ) ) )
  {
    if ( boost::find( info.inputs, v ) != info.inputs.end() )
    {
      /* ignore */
      continue;
    }
    else
    {
      auto it = faults.find( gate_count );
      if ( it != faults.end() )
      {
        // std::cout << "[i] inject fault type " << it->second << " in gate " << gate_count << '\n';
        auto edges = out_edges( v, aig );

        auto edge_it = edges.first;
        if ( it->second == stuck_at_zero_left || it->second == stuck_at_one_left || it->second == negate_left )
        {
          edge_remove += *edge_it;
          edge_faults.insert( { *edge_it, it->second } );
        }

        ++edge_it;
        if ( it->second == stuck_at_zero_right || it->second == stuck_at_one_right || it->second == negate_right )
        {
          edge_remove += *edge_it;
          edge_faults.insert( { *edge_it, it->second } );
        }
      }
      ++gate_count;
    }
  }

  /* create filtered graph */
  filter_graph_t fg( aig, boost::is_not_in_subset<edge_set_t>( edge_remove ), boost::is_not_in_subset<node_set_t>( vertex_modify ) );

  /* copy graph */
  std::vector<aig_node> copy_map;
  auto new_aig = copy_from_filtered<aig_graph, filter_graph_t>( fg, copy_map );

  /* restore info */
  auto& new_info = aig_info( new_aig );
  const auto& old_complement = boost::get( boost::edge_complement, aig );
  const auto& complement = boost::get( boost::edge_complement, new_aig );

  if ( info.constant_used )
  {
    new_info.constant = copy_map[info.constant];
    new_info.constant_used = true;
  }

  if ( info.model_name != "" )
  {
    new_info.model_name = info.model_name + "-injected";
  }

  /* re-assign edges */
  for ( const auto& e : edge_faults )
  {
    const auto& s = boost::source( e.first, aig );
    const auto& t = boost::target( e.first, aig );

    if ( e.second == stuck_at_zero_left )
    {
      new_info.constant = copy_map[info.constant];
      new_info.constant_used = true;
      auto en = boost::add_edge( copy_map[s], new_info.constant, new_aig ).first;
      std::cout << "[i] injected s-a-0[left] @ " << aig_to_literal( aig, s ) << '\n';
      complement[en] = false;
    }
    else if ( e.second == stuck_at_zero_right )
    {
      new_info.constant = copy_map[info.constant];
      new_info.constant_used = true;
      auto en = boost::add_edge( copy_map[s], new_info.constant, new_aig ).first;
      std::cout << "[i] injected s-a-0[right] @ " << aig_to_literal( aig, s ) << '\n';
      complement[en] = false;
    }
    else if ( e.second == stuck_at_one_left )
    {
      new_info.constant = copy_map[info.constant];
      new_info.constant_used = true;
      auto en = boost::add_edge( copy_map[s], new_info.constant, new_aig ).first;
      std::cout << "[i] injected s-a-1[left] @ " << aig_to_literal( aig, s ) << '\n';
      complement[en] = true;
    }
    else if ( e.second == stuck_at_one_right )
    {
      new_info.constant = copy_map[info.constant];
      new_info.constant_used = true;
      auto en = boost::add_edge( copy_map[s], new_info.constant, new_aig ).first;
      std::cout << "[i] injected s-a-1[right] @ " << aig_to_literal( aig, s ) << '\n';
      complement[en] = true;
    }
    else if ( e.second == negate_left )
    {
      auto en = boost::add_edge( copy_map[s], copy_map[t], new_aig ).first;
      std::cout << "[i] injected negate[left] @ " << aig_to_literal( aig, s ) << '\n';
      complement[en] = !old_complement[e.first];
    }
    else if ( e.second == negate_right )
    {
      auto en = boost::add_edge( copy_map[s], copy_map[t], new_aig ).first;
      std::cout << "[i] injected negate[right] @ " << aig_to_literal( aig, s ) << '\n';
      complement[en] = !old_complement[e.first];
    }
    else
    {
      assert( false && "Yet not implemented" );
    }
  }

  /* restore PIs */
  for ( const auto& v : info.inputs )
  {
    new_info.inputs += copy_map[v];
    new_info.node_names[copy_map[v]] = info.node_names.at( v );
  }

  /* restore POs */
  for ( const auto& output : info.outputs )
  {
    aig_function f = { copy_map[ output.first.node ], output.first.complemented };
    new_info.outputs += std::make_pair(f, output.second);
  }

  /* restore internal IDs */
  const auto& vertex_name = boost::get( boost::vertex_name, new_aig );
  for ( const auto& v : boost::make_iterator_range( boost::vertices( new_aig ) ) )
  {
    vertex_name[ v ] = ( v << 1u );
  }

  return new_aig;
}

aig_graph aig_random_fault_injection( const aig_graph& aig, const unsigned fault_number,
                                      const properties::ptr& settings, const properties::ptr& statistics )
{
  /* generate random fault information */
  const auto max_number_of_tries = get( settings, "max_number_of_tries", 1000u );
  const unsigned seed = get( settings, "seed", unsigned(std::chrono::system_clock::now().time_since_epoch().count()) );
  boost::random::mt19937 gen( seed );

  const auto& info = aig_info( aig );
  const auto num_gates = num_vertices( aig ) - info.inputs.size() - info.latch.size() - 1u;
  assert( num_gates > 0u );

  boost::random::uniform_int_distribution<> gate_random_dist( 1u, num_gates );
  boost::random::uniform_int_distribution<> fault_random_dist( 0u, fault_type_t::max_fault_type-1u );

  std::map< aig_node, fault_type_t > faults;

  /* try to populate map with random faults */
  unsigned tries = 0u;
  while ( faults.size() < fault_number && tries++ < max_number_of_tries )
  {
    faults.insert( std::make_pair( aig_node(gate_random_dist( gen )), fault_type_t(fault_random_dist( gen )) ) );
  }

  if ( faults.size() != fault_number )
  {
    std::cout << ( boost::format( "[w] populating fault map failed: not enough gates for the desired number of faults --- use %s instead\n"  ) % faults.size() ).str();
  }

  return aig_fault_injection( aig, faults, settings, statistics );
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
