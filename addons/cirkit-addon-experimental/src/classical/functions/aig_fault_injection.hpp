/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file aig_fault_injection.hpp
 *
 * @brief Injects random faults to AIG
 *
 * @author Heinz Riener
 * @since  2.3
 */

#ifndef AIG_FAULT_INJECTION_HPP
#define AIG_FAULT_INJECTION_HPP

#include <core/properties.hpp>
#include <classical/aig.hpp>

#include <map>

namespace cirkit
{

enum fault_type_t
{
  stuck_at_zero_left = 0
, stuck_at_zero_right
, stuck_at_one_left
, stuck_at_one_right
, negate_left
, negate_right
, max_fault_type
}; /* fault_type_t */

/**
 * @brief Injects faults into an AIG.
 *
 * @param aig AIG graph
 * @param fault_info A map from AIG gate to fault type.
 */
aig_graph aig_fault_injection( const aig_graph& aig, const std::map< aig_node, fault_type_t >& fault_info,
                               const properties::ptr& settings = properties::ptr(),
                               const properties::ptr& statistics = properties::ptr() );

aig_graph aig_random_fault_injection( const aig_graph& aig, const unsigned fault_number,
                                      const properties::ptr& settings = properties::ptr(),
                                      const properties::ptr& statistics = properties::ptr() );

}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End: