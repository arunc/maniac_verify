/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "aig_functional_support.hpp"

#include <boost/assign/std/vector.hpp>
#include <boost/dynamic_bitset.hpp>
#include <boost/format.hpp>

#include <core/utils/bitset_utils.hpp>
#include <core/utils/range_utils.hpp>
#include <classical/io/write_verilog.hpp>
#include <classical/functions/simulate_aig.hpp>

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

class aig_structural_support_simulator : public aig_simulator<boost::dynamic_bitset<>>
{
public:
  aig_structural_support_simulator( unsigned num_inputs ) : num_inputs( num_inputs ) {}

  boost::dynamic_bitset<> get_input( const aig_node& node, const std::string& name, unsigned pos, const aig_graph& aig ) const
  {
    boost::dynamic_bitset<> b( num_inputs );
    b.set( pos );
    return b;
  }

  boost::dynamic_bitset<> get_constant() const
  {
    return boost::dynamic_bitset<>( num_inputs );
  }

  boost::dynamic_bitset<> invert( const boost::dynamic_bitset<>& v ) const
  {
    return v;
  }

  boost::dynamic_bitset<> and_op( const aig_node& node, const boost::dynamic_bitset<>& v1, const boost::dynamic_bitset<>& v2 ) const
  {
    return v1 | v2;
  }

private:
  unsigned num_inputs;
};

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

support_map_t aig_functional_support( const aig_graph& aig, properties::ptr settings, properties::ptr statistics )
{
  using boost::format;

  /* settings */
  unsigned    seed    = get( settings, "seed",        0xCAFEu );
  unsigned    runs    = get( settings, "runs",        10u );
  bool        verbose = get( settings, "verbose",     false );
  std::string tmpname = get( settings, "verilogname", std::string( "/tmp/test" ) );

  /* compute structural support as upper bound */
  support_map_t f_support;
  auto s_support = simulate_aig( aig, aig_structural_support_simulator( boost::get_property( aig, boost::graph_name ).inputs.size() ) );

  const auto& aig_info = boost::get_property( aig, boost::graph_name );

  /* initialize functional support */
  for ( const auto& o : aig_info.outputs )
  {
    f_support.insert( {o.first, boost::dynamic_bitset<>( aig_info.inputs.size() )} );
  }

  /* under approximate functional support with random simulation */
  std::default_random_engine generator( seed );
  for ( unsigned run = 0u; run < runs; ++run )
  {
    /* create random simulation vectors with bit flips */
    std::vector<boost::dynamic_bitset<>> vectors;
    for ( unsigned i = 0u; i < aig_info.inputs.size(); ++i )
    {
      auto vector = random_bitset( aig_info.inputs.size(), generator );
      vectors += vector;
      vector.flip( i );
      vectors += vector;
    }

    /* simulate */
    auto sim_vectors_t = transpose( vectors );
    word_node_assignment_simulator::aig_node_value_map m;
    for ( auto word : index( sim_vectors_t ) )
    {
      m[aig_info.inputs[word.index]] = word.value;
    }

    auto results = simulate_aig( aig, word_node_assignment_simulator( m ) );

    /* for each output */
    for ( const auto& o : aig_info.outputs )
    {
      const auto& f = o.first;
      for ( unsigned i = 0; i < aig_info.inputs.size(); ++i )
      {
        if ( !f_support[f][i] && s_support[f][i] && results[f][i << 1u] != results[f][(i << 1u) + 1u] )
        {
          f_support[f].set( i );
        }
      }
    }
  }

  /* we only want to write a verilog file if we need to do ATPG */
  bool verilog_written = false;

  /* check remaining from structural support */
  for ( const auto& o : aig_info.outputs )
  {
    auto rem = s_support[o.first] & ~f_support[o.first];
    if ( rem.count() > 0u )
    {
      if ( verbose ) { std::cout << format( "[i] ATPG for %d inputs in code of output %s" ) % rem.count() % o.second << std::endl; }
      if ( !verilog_written )
      {
        if ( verbose ) { std::cout << format( "[i] write verilog netlist to %s.v" ) % tmpname << std::endl; }
        write_verilog( aig, tmpname + ".v" );
        verilog_written = true;
      }
    }
  }

  return f_support;
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
