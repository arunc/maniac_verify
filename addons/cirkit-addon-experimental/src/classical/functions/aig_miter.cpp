/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "aig_miter.hpp"

#include <classical/utils/aig_utils.hpp>
#include <core/utils/range_utils.hpp>
#include <core/utils/zip.hpp>

#include <boost/algorithm/string.hpp>

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

using mapping_t = std::map< std::pair< unsigned, aig_node >, aig_node >;

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

void assign_names_to_inputs( aig_graph& aig )
{
  auto& info = aig_info( aig );
  auto& node_names = info.node_names;

  /* inputs */
  for ( auto i : index( info.inputs ) )
  {
    auto it = node_names.find( i.value );
    if ( it == node_names.end() )
    {
      node_names.insert( { i.value, ( boost::format( "in%s" ) % i.index ).str() } );
    }
    else
    {
      auto& name = it->second;
      name = ( boost::format( "in%s" ) % i.index ).str();
    }
  }
}

void annotate_node( aig_graph& aig, const aig_node& n, const std::vector< unsigned >& ids )
{
  auto annotation = get( boost::vertex_annotation, aig );

  std::set< std::string > string_annotations;
  if ( annotation[ n ][ "miter_id" ] != "" )
  {
    std::istringstream iss( annotation[ n ][ "miter_id" ] );
    std::copy(std::istream_iterator<std::string>(iss),
              std::istream_iterator<std::string>(),
              std::inserter(string_annotations, string_annotations.begin()));
  }

  for ( const auto& id : ids )
  {
    string_annotations.insert( ( boost::format("%s") % id ).str() );
  }

  std::string a;
  for ( const auto& s : string_annotations )
  {
    a += s + " ";
  }
  boost::algorithm::trim_right( a );

  annotation[ n ][ "miter_id" ] = a;
}

void aig_add( aig_graph& result_aig, const aig_graph& aig, const unsigned id, mapping_t& mapping )
{
  auto info = aig_info( aig );
  const auto& complementmap = boost::get( boost::edge_complement, aig );

  /* constant */
  if ( aig_is_constant_used( aig ) )
  {
    aig_function f = aig_get_constant( result_aig, false );
    annotate_node( result_aig, f.node, {id} );
    mapping.insert( { {id, info.constant}, f.node } );
  }

  /* iterate through nodes in order */
  for ( const auto& node : boost::make_iterator_range( vertices( aig ) ) )
  {
    const auto _out_degree = out_degree( node, aig );

#if 0
    /* input */
    if ( boost::find( info.inputs, node ) != info.inputs.end() )
    {
      // std::cout << "in " << node << '\n';
      aig_function f = aig_create_pi( result_aig, "" );
      mapping.insert( { {id, node}, f.node } );
    }
#endif

    /* latches */
    if ( _out_degree == 1u )
    {
      assert( false && "latches are not supported" );
    }
    /* and gates */
    else if ( _out_degree == 2u )
    {
      const auto& edges = boost::make_iterator_range( out_edges( node, aig ) );

      const auto& r_index = target( edges[ 0u ], aig );
      const auto r_compl = complementmap[ edges[ 0u ] ];

      const auto& l_index = target( edges[ 1u ], aig );
      const auto l_compl = complementmap[ edges[ 1u ] ];

      // std::cout << "and " << aig_to_literal( aig, {r_index, r_compl} ) << ' ' << aig_to_literal( aig, {l_index, l_compl} ) << '\n';

      auto rhs = mapping.at( {id, r_index} );
      auto lhs = mapping.at( {id, l_index} );
      aig_function f = aig_create_and( result_aig, { rhs, r_compl }, { lhs, l_compl } );
      annotate_node( result_aig, f.node, {id} );
      mapping.insert( { {id, node}, f.node } );
    }

#if 0
    /* output */
    for ( const auto &o : info.outputs )
    {
      if ( o.first.node == node )
        {
        auto it = mapping.find( {id, node} );
        assert( it != mapping.end() );
        aig_create_po( result_aig, { it->second, o.first.complemented }, o.second );
        // std::cout << "out " << aig_to_literal( aig, o.first ) << '\n';
      }
    }
#endif
  }
}

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

aig_graph aig_miter( const aig_graph &aig1, const aig_graph &aig2 )
{
  const auto& info1 = aig_info( aig1 );
  const auto& info2 = aig_info( aig2 );

  const std::string model_name =
    ( boost::format("miter of ''%s'' and ''%s''")
      % (info1.model_name == "" ? "(unnamed)" : info1.model_name)
      % (info2.model_name == "" ? "(unnamed)" : info2.model_name) ).str();

  aig_graph result_aig;
  aig_initialize( result_aig, model_name );
  aig_info( result_aig ).enable_strashing = false;
  aig_info( result_aig ).enable_local_optimization = false;

  mapping_t mapping;

  /* equal inputs */
  for ( auto i : zip( info1.inputs, info2.inputs ) )
  {
    aig_function f = aig_create_pi( result_aig, "" );
    annotate_node( result_aig, f.node, {0u,1u} );
    mapping.insert( { { 0u, boost::get<0u>( i ) }, f.node } );
    mapping.insert( { { 1u, boost::get<1u>( i ) }, f.node } );
  }

  aig_add( result_aig, aig1, 0u, mapping );
  aig_add( result_aig, aig2, 1u, mapping );

  std::vector< aig_function > outs;

  /* different outputs */
  for ( auto o : zip( info1.outputs, info2.outputs ) )
  {
    const auto left = mapping.at( { 0u, boost::get<0u>( o ).first.node } );
    const auto right = mapping.at( { 1u, boost::get<1u>( o ).first.node } );
    outs += aig_create_xor( result_aig, {left,boost::get<0u>( o ).first.complemented}, {right,boost::get<1u>( o ).first.complemented} );
  }

  aig_function o = aig_create_nary_or( result_aig, outs );
  aig_create_po( result_aig, o, "miter_out" );

  aig_info( result_aig ).enable_strashing = true;
  aig_info( result_aig ).enable_local_optimization = true;

  assign_names_to_inputs( result_aig );
  return result_aig;
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
