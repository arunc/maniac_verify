/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cut_netlist.hpp"

#include <core/utils/range_utils.hpp>
#include <boost/format.hpp>
#include <boost/graph/boykov_kolmogorov_max_flow.hpp>
#include <boost/graph/copy.hpp>
#include <boost/graph/filtered_graph.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/algorithm_ext/push_back.hpp>

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

using color_map_t = std::map<simple_fanout_vertex_t, boost::default_color_type>;
using cap_map_t   = std::map<simple_fanout_edge_t, double>;
using rev_map_t   = std::map<simple_fanout_edge_t, simple_fanout_edge_t>;

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

class cut_netlist_manager
{
public:
  explicit cut_netlist_manager( simple_fanout_graph_t& g, bool verbose ) : g( g ), verbose( verbose ) {}

  std::vector<simple_fanout_edge_t> find_minimum_cut();
  void add_new_ports( const std::vector<simple_fanout_edge_t>& cut );
  std::pair<simple_fanout_graph_t, simple_fanout_graph_t> split();

private:
  void construct_flow_graph();
  void break_infinite_paths();
  inline void add_reverse_edge( const simple_fanout_vertex_t& s,
                                const simple_fanout_vertex_t& t,
                                const simple_fanout_edge_t& e,
                                double capacity )
  {
    auto re = boost::add_edge( t, s, g ).first;
    cap[e] = capacity; cap[re] = 0.0;
    rev[e] = re;       rev[re] = e;
  }

private:
  cap_map_t   cap;
  rev_map_t   rev;
  color_map_t color;
  simple_fanout_vertex_t src, tgt;
  simple_fanout_graph_t& g;
  bool verbose;
};

void cut_netlist_manager::break_infinite_paths()
{
  using boost::format;

  const auto& vertex_name = boost::get( boost::vertex_name, g );
  const auto& gate_type   = boost::get( boost::vertex_gate_type, g );

  for ( const auto& v : boost::make_iterator_range( boost::vertices( g ) ) )
  {
    if ( gate_type[v] == gate_type_t::pi )
    {
      if ( boost::out_degree( v, g ) != 1u )
      {
        std::cout << format( "[e] unexpected degree of %d for primary input %s" ) % boost::out_degree( v, g ) % vertex_name[v] << std::endl;
        assert( false );
      }
      auto e = *boost::out_edges( v, g ).first;
      auto w = boost::target( e, g );

      if ( gate_type[w] == gate_type_t::po )
      {
        /* Direct case
         *
         *    □    □
         *   ∞│ → 0│
         *    □    □
         *
         */
        cap[e] = 0.0;

        if ( verbose )
        {
          std::cout << format( "[i] broke direct infinite path between %s and %s" ) % vertex_name[v] % vertex_name[w] << std::endl;
        }
      }
      else if ( gate_type[w] != gate_type_t::fanout )
      {
        /* Gate case
         *
         *    □       □
         *   ∞│      0│
         *    ○──┐ →  ○──┐
         *   ∞│  │   ∞│  │
         *    □  ☁    □  ☁
         *
         */
        if ( boost::out_degree( w, g ) == 1u )
        {
          std::cout << "[e] unexpected degree of " << boost::out_degree( w, g ) << std::endl;
          assert( false );
        }
        auto e2 = *boost::out_edges( w, g ).first;
        auto x = boost::target( e2, g );
        if ( gate_type[x] == gate_type_t::po )
        {
          cap[e2] = 0.0;

          if ( verbose )
          {
            std::cout << format( "[i] broke infinite path (gate) between %s and %s" ) % vertex_name[v] % vertex_name[x] << std::endl;
          }
        }
      }
      else
      {
        /* Fanout case
         *
         *    □  ☁    □  ☁
         *   ∞│  │   ∞│  │
         *    ●──┘ →  ●──┘
         *   ∞│      0│
         *    □       □
         *
         */
        for ( const auto& e2 : boost::make_iterator_range( boost::out_edges( w, g ) ) )
        {
          auto x = boost::target( e2, g );
          if ( gate_type[x] == gate_type_t::po )
          {
            cap[e] = 0.0;

            if ( verbose )
            {
              std::cout << format( "[i] broke infinite path (fanout) between %s and %s" ) % vertex_name[v] % vertex_name[x] << std::endl;
            }
          }
        }
      }
    }
  }
}

void cut_netlist_manager::construct_flow_graph()
{
  using vertex_t    = simple_fanout_vertex_t;
  using edge_t      = simple_fanout_edge_t;

  const auto& gate_type = boost::get( boost::vertex_gate_type, g );

  std::vector<edge_t> the_edges;
  boost::push_back( the_edges, boost::make_iterator_range( edges( g ) ) );
  for ( const auto& e : the_edges )
  {
    auto s = boost::source( e, g );
    auto t = boost::target( e, g );
    auto capacity = ( gate_type[s] == gate_type_t::pi || gate_type[t] == gate_type_t::po ) ? std::numeric_limits<double>::infinity() : 1.0;

    add_reverse_edge( s, t, e, capacity );
  }

  break_infinite_paths();

  src = boost::add_vertex( g );
  tgt = boost::add_vertex( g );
  gate_type[src] = gate_type[tgt] = gate_type_t::internal;

  for ( const auto& v : boost::make_iterator_range( vertices( g ) ) )
  {
    if ( gate_type[v] == gate_type_t::pi )
    {
      auto e = add_edge( src, v, g ).first;
      add_reverse_edge( src, v, e, std::numeric_limits<double>::infinity() );
    }
    else if ( gate_type[v] == gate_type_t::po )
    {
      auto e = add_edge( v, tgt, g ).first;
      add_reverse_edge( v, tgt, e, std::numeric_limits<double>::infinity() );
    }
  }
}

std::vector<simple_fanout_edge_t> cut_netlist_manager::find_minimum_cut()
{
  using vertex_t    = simple_fanout_vertex_t;
  using edge_t      = simple_fanout_edge_t;
  using boost::make_assoc_property_map;

  construct_flow_graph();

  std::map<edge_t, double> rcap;

  auto acap   = make_assoc_property_map( cap );
  auto arcap  = make_assoc_property_map( rcap );
  auto arev   = make_assoc_property_map( rev );
  auto acolor = make_assoc_property_map( color );
  boost::boykov_kolmogorov_max_flow( g, acap, arcap, arev, acolor, get( boost::vertex_index, g ), src, tgt );

  std::vector<simple_fanout_edge_t> cut, to_remove;

  for ( const auto& e : boost::make_iterator_range( boost::edges( g ) ) )
  {
    auto s = boost::source( e, g );
    auto t = boost::target( e, g );

    if ( cap[e] == 0.0 || s == src || t == tgt )
    {
      to_remove += e;
    }
    else
    {
      if ( color[s] == boost::black_color && color[t] != boost::black_color )
      {
        cut += e;
      }
    }
  }

  for ( const auto& e : to_remove )
  {
    boost::remove_edge( e, g );
  }
  boost::remove_vertex( tgt, g );
  boost::remove_vertex( src, g );

  return cut;
}

void cut_netlist_manager::add_new_ports( const std::vector<simple_fanout_edge_t>& cut )
{
  const auto& vertex_name = boost::get( boost::vertex_name, g );
  const auto& gate_type   = boost::get( boost::vertex_gate_type, g );
  const auto& edge_name   = boost::get( boost::edge_name, g );

  for ( auto e : index( cut ) )
  {
    auto s = boost::source( e.value, g );
    auto t = boost::target( e.value, g );

    auto pi = boost::add_vertex( g );
    vertex_name[pi] = boost::str( boost::format( "%d-cut-%d" ) % pi % e.index ); /* prefix is to avoid same names, last value is for cut id */
    gate_type[pi] = gate_type_t::pi;
    color[pi] = color[t];
    auto epi = boost::add_edge( pi, t, g ).first;

    auto po = boost::add_vertex( g );
    vertex_name[pi] = boost::str( boost::format( "%d-cut-%d" ) % po % e.index ); /* prefix is to avoid same names, last value is for cut id */
    gate_type[po] = gate_type_t::po;
    color[po] = color[s];
    auto epo = boost::add_edge( s, po, g ).first;

    edge_name[epi] = edge_name[epo] = edge_name[e.value];

    boost::remove_edge( e.value, g );
  }
}

struct keep_vertices
{
  keep_vertices() {}
  keep_vertices( const color_map_t& color, bool keep_source ) : color( &color ), keep_source( keep_source ) {}

  template<class Vertex>
  bool operator()( const Vertex& v ) const
  {
    auto is_black = color->find( v )->second == boost::black_color;
    return keep_source == is_black;
  }

private:
  color_map_t const* color = nullptr;
  bool keep_source;
};

struct keep_edges
{
  keep_edges() {}
  keep_edges( const simple_fanout_graph_t& g, const color_map_t& color, bool keep_source ) : g( &g ), color( &color ), keep_source( keep_source ) {}

  template<class Edge>
  bool operator()( const Edge& e ) const
  {
    auto is_black = color->find( source( e, *g ) )->second == boost::black_color && color->find( target( e, *g ) )->second == boost::black_color;
    return keep_source == is_black;
  }

private:
  simple_fanout_graph_t const* g = nullptr;
  color_map_t const* color = nullptr;
  bool keep_source;
};

std::pair<simple_fanout_graph_t, simple_fanout_graph_t> cut_netlist_manager::split()
{
  using split_graph_t = boost::filtered_graph<simple_fanout_graph_t, keep_edges, keep_vertices>;
  split_graph_t fg_black( g, keep_edges( g, color, true ), keep_vertices( color, true ) );
  split_graph_t fg_white( g, keep_edges( g, color, false ), keep_vertices( color, false ) );

  simple_fanout_graph_t g_black, g_white;
  boost::copy_graph( fg_black, g_black );
  boost::copy_graph( fg_white, g_white );

  return {g_black, g_white};
}

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

std::pair<simple_fanout_graph_t, simple_fanout_graph_t> cut_netlist( simple_fanout_graph_t& netlist )
{
  cut_netlist_manager mgr( netlist, true );

  auto cut = mgr.find_minimum_cut();
  std::cout << "[i] cut size: " << cut.size() << std::endl;
  mgr.add_new_ports( cut );

  return mgr.split();
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
