/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file is_tree_like.hpp
 *
 * @brief Checks whether a node is root of a tree-like circuit
 *
 * @author Mathias Soeken
 * @since  2.3
 */

#ifndef IS_TREE_LIKE_HPP
#define IS_TREE_LIKE_HPP

#include <stack>

#include <boost/range/iterator_range.hpp>

#include <core/utils/graph_utils.hpp>

namespace cirkit
{

template<typename Graph>
bool is_tree_like( const vertex_t<Graph>& node, const Graph& g )
{
  const auto indegrees = precompute_in_degrees( g );

  std::stack<vertex_t<Graph>> stack;
  stack.push( node );

  while ( !stack.empty() )
  {
    const auto n = stack.top();

    if ( n != node && boost::out_edges( n, g ) > 0u && indegrees[n] > 1u )
    {
      return false;
    }

    stack.pop();

    for ( const auto& c : boost::make_iterator_range( boost::adjacent_vertices( n, g ) ) )
    {
      stack.push( c );
    }
  }

  return true;
}

}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
