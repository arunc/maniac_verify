/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mig_from_string.hpp"

#include <map>
#include <stack>

#include <boost/algorithm/string/replace.hpp>
#include <boost/range/algorithm.hpp>

#include <core/utils/range_utils.hpp>
#include <classical/utils/mig_utils.hpp>

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

using bracket_map_t = std::map<unsigned, unsigned>;
using input_map_t = std::map<char, mig_function>;

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

bracket_map_t find_bracket_pairs( const std::string& s, char open_bracket, char closed_bracket )
{
  std::stack<unsigned> open;
  bracket_map_t pairs;

  for ( const auto& c : index( s ) )
  {
    if ( c.value == open_bracket )
    {
      open.push( c.index );
    }
    else if ( c.value == closed_bracket )
    {
      pairs.insert( {open.top(), c.index} );
      open.pop();
    }
  }

  assert( open.empty() );

  return pairs;
}

mig_function function_from_string( mig_graph& mig, const std::string& expr, unsigned offset,
                                   const bracket_map_t& brackets, input_map_t& inputs )
{
  assert( expr[offset] != '!' );

  if ( expr[offset] == '<' )
  {
    mig_function fs[3];

    auto child_pos = offset + 1u;
    auto to = 0u;
    auto inv = false;

    for ( auto i = 0u; i < 3u; ++i )
    {
      child_pos += to;
      inv = ( expr[child_pos] == '!' );
      if ( inv ) { ++child_pos; }

      to = ( expr[child_pos] == '<' ) ? brackets.at( child_pos ) - child_pos + 1u : 1u;

      fs[i] = function_from_string( mig, expr, child_pos, brackets, inputs );
      if ( inv ) { fs[i] = !fs[i]; }
    }

    return mig_create_maj( mig, fs[0], fs[1], fs[2] );
  }
  else if ( expr[offset] == '0' || expr[offset] == '1' )
  {
    return mig_get_constant( mig, expr[offset] == '1' );
  }
  else
  {
    const auto it = inputs.find( expr[offset] );
    if ( it == inputs.end() )
    {
      auto f = mig_create_pi( mig, expr.substr( offset, 1u ) );
      inputs.insert( {expr[offset], f} );
      return f;
    }
    else
    {
      return it->second;
    }
  }
}

std::string mig_to_string_rec( const mig_graph& mig, const mig_graph_info& info, const mig_function& f )
{
  std::string expr;

  if ( f.complemented )
  {
    expr += "!";
  }

  if ( boost::out_degree( f.node, mig ) == 0u )
  {
    if ( f.node == info.constant )
    {
      expr += "0";
    }
    else
    {
      expr += ( 'a' + std::distance( info.inputs.begin(), boost::find( info.inputs, f.node ) ) );
    }
  }
  else
  {
    auto complemented = boost::get( boost::edge_complement, mig );

    auto it = boost::out_edges( f.node, mig ).first;
    auto expra = mig_to_string_rec( mig, info, {boost::target( *it, mig ), complemented[*it]} ); ++it;
    auto exprb = mig_to_string_rec( mig, info, {boost::target( *it, mig ), complemented[*it]} ); ++it;
    auto exprc = mig_to_string_rec( mig, info, {boost::target( *it, mig ), complemented[*it]} );
    expr += "<" + expra + exprb + exprc + ">";
  }

  return expr;
}

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

mig_graph mig_from_string( const std::string& expr,
                           const properties::ptr& settings,
                           const properties::ptr& statistics )
{
  /* settings */
  const auto model_name   = get( settings, "model_name",   expr );
  const auto output_name  = get( settings, "output_name",  std::string( "f" ) );

  mig_graph mig;
  mig_initialize( mig, model_name );

  mig_create_po( mig, mig_from_string( mig, expr, settings, statistics ), output_name );

  return mig;
}

mig_function mig_from_string( mig_graph& mig, const std::string& expr,
                              const properties::ptr& settings,
                              const properties::ptr& statistics )
{
  /* settings */
  auto variable_map = get( settings, "variable_map", input_map_t() );

  auto nexpr = boost::replace_all_copy( expr, "!!", "" );

  const auto brackets = find_bracket_pairs( nexpr, '<', '>' );

  mig_function f;

  if ( nexpr[0] == '!' )
  {
    f = !function_from_string( mig, nexpr, 1u, brackets, variable_map );
  }
  else
  {
    f = function_from_string( mig, nexpr, 0u, brackets, variable_map );
  }

  return f;
}

std::string mig_to_string( const mig_graph& mig, const mig_function& f,
                           const properties::ptr& settings,
                           const properties::ptr& statistics )
{
  const auto& info = mig_info( mig );

  return mig_to_string_rec( mig, info, f );
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
