/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "simulation_graph2.hpp"

#include <boost/range/combine.hpp>

#include <core/utils/bitset_utils.hpp>
#include <core/utils/graph_utils.hpp>
#include <core/utils/range_utils.hpp>
#include <core/utils/timer.hpp>
#include <classical/functions/simulate_aig.hpp>
#include <classical/utils/aig_utils.hpp>

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

using simulation_node2  = vertex_t<simulation_graph2>;
using simulation_edge2  = edge_t<simulation_graph2>;

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

simulation_graph2 create_simulation_graph2( const aig_graph& aig, const std::vector<boost::dynamic_bitset<>>& sim_vectors,
                                            const properties::ptr& settings,
                                            const properties::ptr& statistics )
{
  /* Timing */
  properties_timer t( statistics );

  /* AIG */
  const auto& info = aig_info( aig );
  auto n = info.inputs.size();
  auto m = info.outputs.size();

  simulation_graph2 g( n + sim_vectors.size() + m );

  /* edges from inputs to simulation vectors */
  for ( auto it : index( sim_vectors ) )
  {
    foreach_bit( it.value, [&]( unsigned pos ) {
        add_edge( pos, n + it.index, g );
      } );
  }

  /* simulate */
  word_node_assignment_simulator::aig_node_value_map map;
  auto sim_vectors_t = transpose( sim_vectors );
  for ( const auto& p : boost::combine( info.inputs, sim_vectors_t ) )
  {
    map[boost::get<0>( p )] = boost::get<1>( p );
  }

  auto results = simulate_aig( aig, word_node_assignment_simulator( map ) );

  /* create edges */
  for ( auto i = 0u; i < sim_vectors.size(); ++i )
  {
    for ( auto j = 0u; j < m; ++j )
    {
      if ( results[info.outputs[j].first][i] )
      {
        add_edge( n + i, n + sim_vectors.size() + j, g );
      }
    }
  }

  return g;
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
