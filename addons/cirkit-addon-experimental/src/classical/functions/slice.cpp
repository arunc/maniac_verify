/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "slice.hpp"

#include <boost/format.hpp>
#include <boost/range/algorithm.hpp>

#include <core/utils/range_utils.hpp>
#include <core/utils/timer.hpp>
#include <classical/functions/simulate_aig.hpp>
#include <classical/utils/aig_utils.hpp>

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

class slice_simulator : public aig_simulator<aig_function>
{
public:
  slice_simulator( aig_graph& aig_new, const std::vector<aig_node>& slice_inputs )
    : aig_new( aig_new ),
      slice_inputs( slice_inputs ) {}

  aig_function get_input( const aig_node& node, const std::string& name, unsigned pos, const aig_graph& aig ) const
  {
    return aig_create_pi( aig_new, name );
  }

  aig_function get_constant() const
  {
    return aig_get_constant( aig_new, false );
  }

  aig_function invert( const aig_function& v ) const
  {
    return !v;
  }

  aig_function and_op( const aig_node& node, const aig_function& v1, const aig_function& v2 ) const
  {
    return aig_create_and( aig_new, v1, v2 );
  }

  virtual bool terminate( const aig_node& node, const aig_graph& aig ) const
  {
    return boost::find( slice_inputs, node ) != slice_inputs.end();
  }

private:
  aig_graph& aig_new;
  const std::vector<aig_node>& slice_inputs;
};

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

aig_graph slice( const aig_graph& aig,
                 const std::vector<aig_node>& slice_inputs,
                 const std::vector<aig_node>& slice_outputs,
                 const properties::ptr& settings,
                 const properties::ptr& statistics )
{
  using boost::format;
  using boost::str;

  /* settings */
  const auto verbose = get( settings, "verbose", false );

  /* timer */
  properties_timer t( statistics );

  std::cout << "input slice: " << slice_inputs.size() << std::endl;
  std::cout << "output slice: " << slice_outputs.size() << std::endl;

  aig_graph aig_new;
  aig_initialize( aig_new );

  auto& info = aig_info( aig_new );

  /* simulate */
  slice_simulator sim( aig_new, slice_inputs );
  aig_node_color_map colors;
  std::map<aig_node, aig_function> node_values;

  for ( const auto& o : index( slice_outputs ) )
  {
    aig_create_po( aig_new,
                   simulate_aig_node( aig, o.value, sim, colors, node_values ),
                   str( format( "slice_po%d" ) % o.index ) );

  }

  /* dangling inputs */
  auto index = 0u;
  for ( const auto& node : boost::make_iterator_range( boost::vertices( aig_new ) ) )
  {
    if ( boost::out_degree( node, aig_new ) == 0u && node != 0u )
    {
      if ( boost::find( info.inputs, node ) == info.inputs.end() )
      {
        info.inputs.push_back( node );
        info.node_names[node] = str( format( "slice_pi%d" ) % index++ );
      }
    }
  }

  return aig_new;
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
