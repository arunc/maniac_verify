/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "split_aig.hpp"

#include <boost/assign/std/vector.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/connected_components.hpp>
#include <boost/range/iterator_range.hpp>

using namespace boost::assign;

namespace cirkit
{

/******************************************************************************
 * Split graph                                                                *
 ******************************************************************************/

using split_graph_t       = boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS>;
using split_graph_node_t  = boost::graph_traits<split_graph_t>::vertex_descriptor;
using split_graph_edge_t  = boost::graph_traits<split_graph_t>::edge_descriptor;
using components_map_t    = std::map<split_graph_node_t, unsigned>;
using components_vector_t = std::vector<std::vector<split_graph_node_t>>;

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

split_graph_t create_split_graph( const aig_graph& aig )
{
  split_graph_t g;

  /* graph info */
  auto aig_info = boost::get_property( aig, boost::graph_name );

  /* storing data */
  std::map<aig_node, split_graph_node_t> node2node;
  std::map<aig_edge, split_graph_edge_t> edge2edge;

  /* copy almost all vertices */
  for ( const auto& v : boost::make_iterator_range( vertices( aig ) ) )
  {
    if ( v == aig_info.constant ) continue;
    node2node[v] = add_vertex( g );
  }

  /* copy almost all edges */
  for ( const auto& e : boost::make_iterator_range( edges( aig ) ) )
  {
    if ( boost::target( e, aig ) == aig_info.constant ) continue;
    edge2edge[e] = add_edge( node2node[boost::source( e, aig )], node2node[boost::target( e, aig )], g ).first;
  }

  return g;
}

components_vector_t make_components_vector( const components_map_t& mcomponents, unsigned num_components )
{
  components_vector_t v( num_components );

  for ( const auto& p : mcomponents )
  {
    v[p.second] += p.first;
  }

  return v;
}

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

std::vector<aig_graph> split_aig( const aig_graph& aig )
{
  std::vector<aig_graph> v;

  /* create split graph */
  auto g = create_split_graph( aig );

  /* compute connected components */
  components_map_t mcomponents;
  auto num_components = boost::connected_components( g, boost::make_assoc_property_map( mcomponents ) );
  auto components = make_components_vector( mcomponents, num_components );

  std::cout << "[i] number of connected components: " << num_components << std::endl;

  return v;
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
