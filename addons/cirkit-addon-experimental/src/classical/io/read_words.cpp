/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "read_words.hpp"

#include <boost/range/algorithm.hpp>
#include <boost/regex.hpp>

#include <core/utils/range_utils.hpp>
#include <core/utils/string_utils.hpp>
#include <classical/utils/aig_utils.hpp>

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

namespace detail
{

bool operator==( const std::vector<aig_node>& w1, const std::vector<aig_node>& w2 )
{
  if ( w1.size() != w2.size() ) { return false; }

  for ( auto i = 0u; i < w1.size(); ++i )
  {
    if ( w1[i] != w2[i] ) { return false; }
  }

  return true;
}

}

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

void read_words( aig_graph& aig, const std::string& filename )
{
  using namespace detail;

  auto& words = aig_info( aig ).trans_words;

  line_parser( filename, {
      {boost::regex( "^\\s*(Input|Output) word:\\s*(.*)$" ), [&]( const boost::smatch& m ) {
          std::vector<aig_node> word;
          parse_string_list( word, m[2], " \t" );

          if ( boost::find( words, word ) == words.end() )
          {
            words.push_back( word );
          }
        }}
    } );
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
