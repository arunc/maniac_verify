/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "find_mincut.hpp"

#include <boost/assign/std/list.hpp>
#include <boost/graph/boykov_kolmogorov_max_flow.hpp>
#include <boost/range/iterator_range.hpp>

using namespace boost::assign;

namespace cirkit
{

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

using cap_map_t = std::map<aig_edge, double>;
using rev_map_t = std::map<aig_edge, aig_edge>;

void add_reverse_edge( aig_graph& aig, cap_map_t& cap, rev_map_t& rev, const aig_edge& edge, double capacity = 1.0 )
{
  auto redge = add_edge( boost::target( edge, aig ), boost::source( edge, aig ), aig ).first;

  cap[edge] = capacity;
  cap[redge] = 0.0;
  rev[edge] = redge;
  rev[redge] = edge;
}

aig_edge add_reverse_edge( aig_graph& aig, cap_map_t& cap, rev_map_t& rev,
                           const aig_node& from, const aig_node& to, double capacity = std::numeric_limits<double>::infinity() )
{
  auto edge = add_edge( from, to, aig ).first;
  auto redge = add_edge( to, from, aig ).first;

  cap[edge] = capacity;
  cap[redge] = 0.0;
  rev[edge] = redge;
  rev[redge] = edge;

  return edge;
}

void prepare_graph( aig_graph& aig, cap_map_t& cap, rev_map_t& rev, aig_node& source, aig_node& target, std::list<aig_edge>& original_edges )
{
  const auto& graph_info = boost::get_property( aig, boost::graph_name );
  auto complementmap = get( boost::edge_complement, aig );

  /* reverse edges */
  for ( const auto& edge : boost::make_iterator_range( edges( aig ) ) )
  {
    add_reverse_edge( aig, cap, rev, edge );
    original_edges += edge;
  }

  /* source and target */
  source = add_vertex( aig );
  target = add_vertex( aig );

  for ( const auto& input : graph_info.inputs )
  {
    add_reverse_edge( aig, cap, rev, input, target );
  }

  for ( const auto& output : graph_info.outputs )
  {
    auto output_node = add_vertex( aig );
    auto edge = add_reverse_edge( aig, cap, rev, output_node, output.first.node, 1.0 );
    complementmap[edge] = output.first.complemented;
    original_edges += edge;

    add_reverse_edge( aig, cap, rev, source, output_node );
  }

  /* constant */
  add_reverse_edge( aig, cap, rev, graph_info.constant, target );
}

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

bool find_mincut( std::list<std::list<aig_function>>& cuts, aig_graph& aig, unsigned count, properties::ptr settings, properties::ptr statistics )
{
  if ( count == 0u )
  {
    return true;
  }

  aig_node source, target;
  std::list<aig_edge> original_edges;
  std::list<aig_edge> cut_edges;

  cap_map_t cap;
  rev_map_t rev;

  prepare_graph( aig, cap, rev, source, target, original_edges );

  for ( auto i = 0u; i < count; ++i )
  {
    std::map<aig_edge, double> rcap;
    std::map<aig_node, boost::default_color_type> color;

    auto acap   = make_assoc_property_map( cap );
    auto arcap  = make_assoc_property_map( rcap );
    auto arev   = make_assoc_property_map( rev );
    auto acolor = make_assoc_property_map( color );

    boykov_kolmogorov_max_flow( aig, acap, arcap, arev, acolor, get( boost::vertex_index, aig ), source, target );

    auto complement = boost::get( boost::edge_complement, aig );

    std::list<aig_function> cut;

    for ( const auto& e : boost::make_iterator_range( edges( aig ) ) )
    {
      if ( cap[e] > 0 )
      {
        if ( color[boost::source(e, aig)] != color[boost::target(e, aig)] )
        {
          const aig_function f = { boost::target(e, aig), complement[e] };
          cut += f;
          cut_edges += e;
        }
      }
    }

    for ( const auto& e : original_edges )
    {
      cap[e] = 1.0;
    }

    for ( const auto& e : cut_edges )
    {
      cap[e] = std::numeric_limits<double>::infinity();
    }

    cuts += cut;
  }

  return true;
}

mincut_by_edge_func find_mincut_func( properties::ptr settings, properties::ptr statistics )
{
  mincut_by_edge_func f = [&]( std::list<std::list<aig_function>>& cuts, aig_graph& aig, unsigned count ) {
    return find_mincut( cuts, aig, count, settings, statistics );
  };
  f.init( settings, statistics );
  return f;
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
