/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mincut.hpp"

#include <boost/format.hpp>
#include <boost/graph/depth_first_search.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/property_map/property_map.hpp>
#include <boost/range/iterator_range.hpp>

namespace cirkit
{

class has_one_weight_edges_visitor : public boost::default_dfs_visitor
{
public:
  has_one_weight_edges_visitor( bool& found ) : found( found )
  {
    found = false;
  }

  template<typename Edge>
  void examine_edge( Edge e, const mc_graph_t& graph )
  {
    if ( get( boost::edge_capacity, graph )[e] == 1.0 )
    {
      found = true;
    }
  }

  bool& found;
};

bool has_one_weight_edges( const mc_graph_t& graph, const mc_vertex_t& source, bool verbose )
{
  if ( verbose )
  {
    std::cout << boost::format( "[I] determine reachable nodes from %d" ) % source << std::endl;
  }

  bool found;
  std::map<mc_vertex_t, boost::default_color_type> colors;
  boost::depth_first_visit( graph, source, has_one_weight_edges_visitor( found ), boost::make_assoc_property_map( colors ) );

  return found;
}

bool has_infinity_path( const mc_graph_t& graph, const mc_vertex_t& source, const mc_vertex_t& target )
{
  auto capacity = boost::get( boost::edge_capacity, graph );
  std::map<mc_edge_t, double> weight;
  std::map<mc_vertex_t, double> distance;

  for ( const auto& e : boost::make_iterator_range( edges( graph ) ) )
  {
    if ( capacity[e] == 0.0 )
    {
      weight[e] = std::numeric_limits<double>::infinity();
    }
    else if ( capacity[e] == 1.0 )
    {
      weight[e] = 1.0;
    }
    else if ( capacity[e] == std::numeric_limits<double>::infinity() )
    {
      weight[e] = 0.0;
    }
    else
    {
      assert( false );
    }
  }

  boost::dijkstra_shortest_paths( graph, source,
                                  boost::weight_map( boost::make_assoc_property_map( weight ) ).distance_map( boost::make_assoc_property_map( distance ) ) );

  return distance[target] == 0.0;
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
