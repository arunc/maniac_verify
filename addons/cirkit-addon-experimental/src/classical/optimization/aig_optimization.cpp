/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "aig_optimization.hpp"

#include <cuddObj.hh>

#include <boost/assign/std/vector.hpp>
#include <boost/dynamic_bitset.hpp>
#include <boost/optional.hpp>

#include <core/utils/bitset_utils.hpp>
#include <core/utils/timer.hpp>
#include <classical/functions/simulate_aig.hpp>
#include <classical/utils/aig_utils.hpp>

using namespace boost::assign;

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

using simulation_value = std::pair<boost::optional<BDD>, boost::dynamic_bitset<>>;
using simulation_map   = std::map<aig_node, simulation_value>;
using function_map     = std::map<DdNode*, std::vector<aig_node>>;
using support_map      = std::map<DdNode*, boost::dynamic_bitset<>>;
using key_vector       = std::vector<DdNode*>;

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

class aig_functional_hashing_simulator : public aig_simulator<simulation_value>
{
public:
  aig_functional_hashing_simulator( const aig_graph& aig, unsigned max_support = 16u )
    : max_support( max_support )
  {
    const auto& info = aig_info( aig );
    num_inputs = info.inputs.size();

    for ( auto i = 0u; i < num_inputs; ++i ) { mgr.bddVar(); }
  }

  simulation_value get_input( const aig_node& node, const std::string& name, unsigned pos, const aig_graph& aig ) const
  {
    return std::make_pair( mgr.bddVar( pos ), onehot_bitset( num_inputs, pos ) );
  }

  simulation_value get_constant() const
  {
    return std::make_pair( mgr.bddZero(), boost::dynamic_bitset<>( num_inputs ) );
  }

  simulation_value invert( const simulation_value& v ) const
  {
    if ( v.first == boost::none )
    {
      return v;
    }
    else
    {
      return std::make_pair( !v.first.get(), v.second );
    }
  }

  simulation_value and_op( const aig_node& node, const simulation_value& v1, const simulation_value& v2 ) const
  {
    if ( v1.first == boost::none ) { return v1; }
    if ( v2.first == boost::none ) { return v2; }

    auto new_bitset = v1.second | v2.second;
    if ( new_bitset.count() > max_support )
    {
      return std::make_pair( boost::none, boost::dynamic_bitset<>() );
    }

    return std::make_pair( v1.first.get() & v2.first.get(), new_bitset );
  }

private:
  Cudd     mgr;
  unsigned num_inputs;
  unsigned max_support = 16u;
};

std::tuple<function_map, support_map, key_vector> find_equivalent_nodes( const simulation_map& node_values, unsigned& node_count )
{
  function_map fm;
  support_map  sm;
  key_vector   keys;
  for ( const auto& value : node_values )
  {
    const auto& node = value.first;
    const auto& sim_bdd = value.second.first;

    if ( sim_bdd != boost::none )
    {
      auto key = sim_bdd.get().getNode();
      fm[key] += node;
      if ( fm[key].size() == 2u )
      {
        sm[key] = value.second.second;
        keys += key;
      }
      node_count++;
    }
  }

  return std::make_tuple( fm, sm, keys );
}

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

aig_graph aig_functional_hashing( const aig_graph& aig,
                                  const properties::ptr& settings,
                                  const properties::ptr& statistics )
{
  /* settings */
  auto verbose     = get( settings, "verbose",     false );
  auto max_support = get( settings, "max_support", 16u );

  /* statistics */
  properties_timer t( statistics );

  /* simulation */
  aig_functional_hashing_simulator simulator( aig, max_support );
  auto sa_settings   = std::make_shared<properties>();
  auto sa_statistics = std::make_shared<properties>();

  simulate_aig( aig, simulator, sa_settings, sa_statistics );
  set( statistics, "simulation_runtime", sa_statistics->get<double>( "runtime" ) );

  /* find equivalent nodes */
  function_map fm;
  support_map  sm;
  key_vector   keys;

  auto node_count = 0u;
  std::tie( fm, sm, keys ) = find_equivalent_nodes( sa_statistics->get<simulation_map>( "node_values" ), node_count );

  set( statistics, "node_count", node_count );

  /* go through all keys that have more than one node */
  auto reduction = 0u;
  std::vector<aig_node> to_be_removed;
  for ( const auto& k : keys )
  {
    if ( verbose )
    {
      std::cout << boost::format( "[i] node %x is shared by %d nodes (support size: %d)." ) % k % fm[k].size() % sm[k].count() << std::endl;
    }
    std::copy( fm[k].begin() + 1, fm[k].end(), std::back_inserter( to_be_removed ) );
    reduction += ( fm[k].size() - 1 );
  }

  set( statistics, "reduction", reduction );

  /* TODO */
  /* 1. DFS for all nodes in to_be_removed that stops until a node with more than one parent is reached */

  return aig;
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
