/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "m_active_sop_optimization.hpp"

#include <cstdlib>
#include <fstream>

#include <boost/assign/std/vector.hpp>
#include <boost/format.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/range/algorithm_ext/push_back.hpp>
#include <boost/range/numeric.hpp>

#include <cudd.h>
#include <cuddObj.hh>

#include <core/io/extract_pla.hpp>
#include <core/io/pla_parser.hpp>
#include <core/utils/range_utils.hpp>

using namespace boost::assign;

namespace cirkit
{

class m_active_processor : public pla_processor
{
public:
  m_active_processor( Cudd& cudd )
    : cudd( cudd )
  {
  }

  void on_num_inputs( unsigned n )
  {
    if ( !append_cubes )
    {
      num_inputs = n;
      ntimes( n, [this]() { this->cudd.bddVar(); } );
    }
  }

  void on_num_outputs( unsigned num_outputs )
  {
    assert( num_outputs == 1u );
  }

  void on_input_labels( const std::vector<std::string>& labels )
  {
    if ( !append_cubes )
    {
      boost::push_back( input_labels, labels );
    }
  }

  void on_output_labels( const std::vector<std::string>& output_labels )
  {
    if ( !append_cubes )
    {
      output = output_labels.front();
    }
  }

  void on_cube( const std::string& in, const std::string& out )
  {
    assert( out == "1" );
    scubes += in;

    if ( !append_cubes )
    {
      BDD cube = cudd.bddOne();
      for ( auto it : index( in ) )
      {
        if ( it.value == '1' )
        {
          cube &= cudd.bddVar( it.index );
        }
        else if ( it.value == '0' )
        {
          cube &= ~cudd.bddVar( it.index );
        }
      }
      cubes += cube;
    }
  }

  void on_end()
  {
    append_cubes = true;
  }

  void write_pla( const std::string& filename )
  {
    using boost::adaptors::transformed;

    std::ofstream os( filename.c_str(), std::ofstream::out );
    os << ".i " << num_inputs << std::endl
       << ".o 1" << std::endl
       << ".ilb " << any_join( input_labels, " " ) << std::endl
       << ".ob " << output << std::endl
       << any_join( scubes | transformed( []( const std::string& s ) { return s + " 1"; } ), "\n" ) << std::endl
       << ".e" << std::endl;

    os.close();
  }

public:
  std::vector<std::string> scubes;
  std::vector<BDD> cubes;
  bool append_cubes = false;
  unsigned num_inputs;
  std::vector<std::string> input_labels;
  std::string output;

private:
  Cudd& cudd;
};

void dump_pla( BDD f, const std::string& filename )
{
  unsigned n = Cudd_ReadSize( f.manager() );

  std::ofstream out( filename.c_str(), std::ofstream::out );
  out << ".i " << n << std::endl
      << ".o " << 1 << std::endl;

  int *cube;
  CUDD_VALUE_TYPE value;
  DdGen *gen;

  Cudd_ForeachCube( f.manager(), f.getNode(), gen, cube, value )
  {
    char v;

    for (unsigned i = 0u; i < n; ++i)
    {
      v = cube[i];
      if (v != 2)
      {
        out << ((v == 0) ? "0" : "1");
      }
      else
      {
        out << "-";
      }
    }

    out << " 1" << std::endl;
  }

  out << ".e" << std::endl;
  out.close();
}

bool verify( Cudd& cudd, const std::vector<BDD>& cubes1, const std::vector<BDD>& cubes2 )
{
  auto combine = []( BDD l, BDD r ) { return l | r; };
  BDD f = boost::accumulate( cubes1, cudd.bddZero(), combine );
  BDD fb = boost::accumulate( cubes2, cudd.bddZero(), combine );

  return f == fb;
}

void m_active_sop_optimization( const std::string& destination, const std::string& filename, const std::vector<unsigned>& active_set,
                                properties::ptr settings, properties::ptr statistics )
{
  /* Settings */
  std::string extractname     = get( settings, "extractname",     std::string( "/tmp/extracted.pla" ) );
  std::string initialname     = get( settings, "initialname",     std::string( "/tmp/initial.pla" ) );
  std::string bucketname      = get( settings, "bucketname",      std::string( "/tmp/bucket%d.pla" ) );
  std::string dsopname        = get( settings, "bucketname",      std::string( "/tmp/bucket_dsop%d.pla" ) );
  unsigned    output          = get( settings, "output",          0u );
  std::string espresso_option = get( settings, "espresso_option", std::string( "fast" ) );
  bool        verify_buckets  = get( settings, "verify_buckets",  false );
  bool        verbose         = get( settings, "verbose",         false );

  /* Extract PLA */
  if ( verbose )
  {
    std::cout << boost::format( "[i] extract PLA for output %d" ) % output << std::endl;
  }
  extract_pla( filename, extractname, output );

  /* Create initial cover */
  if ( verbose )
  {
    std::cout << "[i] create initial cover" << std::endl;
  }
  auto sresult = system( boost::str( boost::format( "espresso -e%s %s > %s" ) % espresso_option % extractname % initialname ).c_str() );

  /* Cudd manager */
  Cudd cudd;

  /* Parse initial cover */
  m_active_processor processor( cudd );
  pla_parser( initialname, processor );

  if ( statistics )
  {
    statistics->set( "initial_size", (unsigned)processor.cubes.size() );
  }

  /* Iterate through all cubes */
  std::vector<BDD> buckets;
  for ( auto cube : processor.cubes )
  {
    if ( verbose )
    {
      std::cout << "[i] process cube:" << std::endl;
      cube.PrintMinterm();
    }

    /* Iterate through all buckets */
    for ( unsigned b = 0u; b < buckets.size(); ++b )
    {
      if ( verbose )
      {
        std::cout << "[i] update bucket " << ( b + 1u ) << ", before:" << std::endl;
        buckets[b].PrintMinterm();
      }
      BDD common = cube & buckets[b];
      buckets[b] ^= cube;
      cube = common;
      if ( verbose )
      {
        std::cout << "[i] after:" << std::endl;
        buckets[b].PrintMinterm();
        std::cout << "[i] cube:" << std::endl;
        cube.PrintMinterm();
      }

      if ( cube == cudd.bddZero() ) break;
    }

    /* Is there something left? */
    if ( cube != cudd.bddZero() )
    {
      buckets += cube;
    }
  }

  /* Buckets */
  if ( verbose )
  {
    std::cout << boost::format( "[i] %s bucket(s) created" ) % buckets.size() << std::endl;
    for ( const auto& b : index( buckets ) )
    {
      std::cout << "[i] bucket " << ( b.index + 1u ) << ":" << std::endl;
      b.value.PrintMinterm();
    }
  }
  if ( verify_buckets )
  {
    if ( !verify( cudd, processor.cubes, buckets ) )
    {
      assert( false );
    }
    else if ( verbose )
    {
      std::cout << "[i] buckets verified successfully" << std::endl;
    }
  }

  /* Create DSOPs for incompatible buckets */
  auto next_active = active_set.begin();
  for ( unsigned b = 0u; b < buckets.size(); ++b )
  {
    if ( ( b + 1u ) == *next_active )
    {
      /* everything is okay, this bucket is allowed */
      ++next_active;
    }
    else
    {
      if ( verbose )
      {
        std::cout << boost::format( "create DSOP for bucket %d:" ) % ( b + 1u ) << std::endl;
      }
      auto bname = boost::str( boost::format( bucketname ) % ( b + 1u ) );
      auto dname = boost::str( boost::format( dsopname ) % ( b + 1u ) );
      dump_pla( buckets[b], boost::str( boost::format( bucketname ) % ( b + 1u ) ) );
      auto sresult = system( boost::str( boost::format( "espresso -e%s -Ddisjoint %s > %s" ) % espresso_option % bname % dname ).c_str() );
      pla_parser( dname, processor );

      if ( ( b + 1u ) == buckets.size() )
      {
        buckets += buckets[b];
      }
      else
      {
        buckets[b + 1u] |= buckets[b];
      }
      buckets[b] = cudd.bddZero();
    }
  }

  /* Verify buckets again */
  if ( verbose )
  {
    std::cout << boost::format( "[i] %s bucket(s) created" ) % buckets.size() << std::endl;
  }
  if ( verify_buckets )
  {
    if ( !verify( cudd, processor.cubes, buckets ) )
    {
      assert( false );
    }
    else if ( verbose )
    {
      std::cout << "[i] buckets verified successfully" << std::endl;
    }
  }

  processor.write_pla( destination );

  if ( statistics )
  {
    statistics->set( "final_size", (unsigned)processor.scubes.size() );
  }
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
