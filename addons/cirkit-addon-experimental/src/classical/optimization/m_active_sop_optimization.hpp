/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file m_active_sop_optimization.hpp
 *
 * @brief M-active SOP optimization
 *
 * @author Mathias Soeken
 * @since 2.0
 */

#ifndef M_ACTIVE_SOP_OPTIMIZATION_HPP
#define M_ACTIVE_SOP_OPTIMIZATION_HPP

#include <string>
#include <vector>

#include <core/properties.hpp>

namespace cirkit
{

/**
 * @param active_set Must be sorted and the last element must be larger than
                     the number of buckets. */
void m_active_sop_optimization( const std::string& destination, const std::string& filename, const std::vector<unsigned>& active_set,
                                properties::ptr settings = properties::ptr(), properties::ptr statistics = properties::ptr() );

}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
