#ifndef GCC_TIME_HEADER_HPP
#define GCC_TIME_HEADER_HPP

// Problem : Only in GCC 5.0 onwards std::put_time() is defined.
//           Cant break the existing setup which (submitted for publishing).
// Solution : This header checks if the version is less than 5 and includes
//            dummy defintions for std::put_time() is if it is!
//            http://stackoverflow.com/questions/259248/how-do-i-test-the-current-version-of-gcc
//
// Other interesting side notes:
// To get all the macros in GCC use  :: g++ -E -dM - < /dev/null 


#ifdef __GNUC__
#  include <features.h>

#  if __GNUC_PREREQ(5,0)
//      If  gcc_version >= 5.0
//      Nothing for now.
#  elif __GNUC_PREREQ(4,0)
//       If gcc_version >= 4.0
#  include <iomanip>
#  include <ctime>
#  include <iostream>
namespace std {
  template< class CharT >
  std::string put_time( const std::tm* tmb, const CharT* fmt ) {
    return "Time-NA";
  }

}
#  else
#     error Sorry GCC version is not supported.
#  endif
#else
//    If not gcc
#endif





#endif

