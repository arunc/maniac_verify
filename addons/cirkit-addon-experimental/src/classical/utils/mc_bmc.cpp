/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mc_bmc.hpp"

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/


/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/
inline void ___run_abc_cmd (abc::Abc_Frame_t *frame,
			   std::string cmd, bool debug)
{
  
  if (debug) std::cout << "[i] Abc :: " << cmd << std::endl;
  auto status = abc::Cmd_CommandExecute ( frame, cmd.c_str() );
  // Interpretation of status is wrong. Its not a boolean for true or false.
  //if (debug && !status)  std::cout << "[e] Abc :: command " << cmd
  //				   << " failed " << std::endl;
  
}
/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

int mc_bmc( const std::string &blif_file, const std::string &out_cex_file,
	    const std::string &results_file, unsigned cycle_limit, bool debug,
	    bool print_cex)
{
  abc::Abc_Frame_t *frame = abc::Abc_FrameGetGlobalFrame();
  if (!frame) {
    abc::Abc_Stop();
    std::cout << "[e] not able to invoke the ABC frame" << std::endl;
    assert (false ); 
  }

  // redirect printf() output to a string.
  // http://stackoverflow.com/questions/19485536/
  //        redirect-output-of-an-function-printing-to-console-to-string
  auto org_stdout = stdout; 
  char buffer_results[4096];
  auto tmp_stdout = fmemopen (buffer_results, 4096, "w");
  stdout = tmp_stdout;

  std::string read_blif = "read_blif " + blif_file;
  std::string strash = "strash ";
  std::string fraig  = "ifraig ";
  std::string bmc = "bmc3 -F " + std::to_string (cycle_limit);
  std::string cex = "write_cex -n  " + out_cex_file;

  auto abc_debug = false;
  ___run_abc_cmd (frame, read_blif, abc_debug);
  ___run_abc_cmd (frame, strash,    abc_debug);
  ___run_abc_cmd (frame, fraig,     abc_debug);
  ___run_abc_cmd (frame, bmc,       abc_debug);
  //if (debug) ___run_abc_cmd (frame, cex, abc_debug);
  
  std::fclose (tmp_stdout);
  stdout = org_stdout;
  std::string results = buffer_results;
  std::ofstream of;
  of.open (results_file.c_str(), std::ios_base::out);
  of << results;
  of.close();

  std::string no_output =  "No output asserted";
  std::string proved = "Explored all reachable states";
  std::string satisfied = "was asserted in frame";

  if (debug)
  {
    std::cout << "[i] Abc Info begins :: " << std::endl
	      << results << "[i] Abc Info ends " << std::endl;
  }
  if ( results.find (no_output) != std::string::npos ) {
    if (debug) std::cout << "[i] BMC Result -  No output asserted after "
			 << cycle_limit << " Frames" << std::endl;
    return -2;
  }
  if ( results.find (proved) != std::string::npos  ) {
    //if (debug) std::cout << "[i] PDR Result -  NO" << std::endl;
    return -1;
  }
  if ( results.find (satisfied) != std::string::npos  ) {
    auto str = results;
    auto my_regex = boost::regex (
      "^.*Output 0.*was asserted.*frame");
    auto tmp1 = boost::regex_replace ( str, my_regex, std::string("") );
    auto cycles = std::stoi ( boost::regex_replace (
				tmp1, boost::regex("\\.*Time.*=.*"),
				std::string("")
				) );
    if (print_cex)   ___run_abc_cmd (frame, cex,       abc_debug);
    return cycles;
  }

  return -3;
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
