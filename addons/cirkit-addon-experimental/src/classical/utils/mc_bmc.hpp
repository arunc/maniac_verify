
/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file mc_bmc.hpp
 *
 * @brief 
 *
 * @author Arun <arun@informatik.uni-bremen.de>
 * @since  2.3
 */

#ifndef MC_BMC_HPP
#define MC_BMC_HPP

#include <abc/abc_api.hpp>
#include <string>
#include <iostream>
#include <boost/dynamic_bitset.hpp>
#include <boost/regex.hpp>
#include <fstream>
#include <classical/utils/mc_common_utils.hpp>

namespace cirkit
{
int mc_bmc( const std::string &blif_file, const std::string &out_cex_file,
	    const std::string &results_file, unsigned cycle_limit, bool debug,
	    bool print_cex);

}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
