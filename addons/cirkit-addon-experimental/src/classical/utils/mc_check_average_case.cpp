/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mc_check_average_case.hpp"


namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/
//------------------------------------------------------------------------------
inline std::string __pdr_result_to_string (const unsigned short result) {
  if (result == 3u) return " NO ";
  if (result == 2u) return " YES ";
  if (result == 1u) return " UNDECIDED ";
  return " ERROR - IMPOSSIBLE CONDITION ";
  
}
  
/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/
// Create the miter and simply run the PDR
unsigned long mc_check_average_case ( const mc_design &golden, const mc_design &approx,
				      const mc_port &port_to_check, unsigned signed_outputs,
				      const unsigned long &avg_error, std::string work,
				      const unsigned long &clk_cycles, const unsigned effort_level,
				      bool debug )
{
  const unsigned short NO = 3u, YES = 2u, UNDECIDED = 1u;
  
  auto golden_vlog = work + "/golden.v";
  auto approx_vlog = work + "/approx.v";
  golden.write ( golden_vlog, "mc_golden", "verilog" );
  approx.write ( approx_vlog, "mc_approx", "verilog" );

  // TODO: ABC may need to be start/stop cycled for memory buildup.
  std::string miter_vlog =  work + "/miter.v";
  std::string miter_blif =  work + "/miter.blif";
  std::string miter_cex  =  work + "/miter.cex";
  std::string miter_summary =  work + "/miter.summary";
  std::string miter_snippet = work + "/m.v";

  // 1. Create the MITER.
  // a. create the miter snippet.
  if (debug) std::cout << "[i] Started  creating avg-error miter :: "
		       << curr_time();
  
  if (effort_level == 0u)  {
    assert (false); // LOGIC IS WRONG;
    mc_make_miter ( miter_snippet, golden, approx, "avg_error_miter", port_to_check,
		    avg_error, signed_outputs, clk_cycles, mc_miter_type::type5_0 );
  }
  else if (effort_level == 1u) {
    assert (false); // LOGIC IS WRONG;
    mc_make_miter ( miter_snippet, golden, approx, "avg_error_miter", port_to_check,
		    avg_error, signed_outputs, clk_cycles, mc_miter_type::type5_1 );
  }
  else
    mc_make_miter ( miter_snippet, golden, approx, "avg_error_miter", port_to_check,
		    avg_error, signed_outputs, mc_miter_type::type5_2 );
  

  // b. cat golden.v + approx.v + miter.v
  cat ( miter_vlog, golden_vlog, approx_vlog, miter_snippet );
  auto miter = mc_design ( miter_vlog, "avg_error_miter", "clock",
			   golden.get_reset(), golden.get_oe(), false  );
  miter.write (miter_blif, "blif");
  if (debug) std::cout << "[i] Finished creating avg-error miter :: "
		       << curr_time();
  
  // 2. Run PDR.
  if (debug) std::cout << "[i] Starting PDR :: "<< port_to_check.first << " >= "
		       << avg_error << "?   :: " << curr_time();
  auto result = mc_pdr (miter_blif, miter_cex, miter_summary, debug);
  if (debug) std::cout << "[i] Finished PDR :: " << port_to_check.first << " >= "
		       << avg_error << "?   :: " << __pdr_result_to_string (result)
		       << "  :: " << curr_time();
  
  // 3. And return the results.
  if (result == NO)  return 3u;
  if (result == YES) return 2u;
  if (result == UNDECIDED) return 1u;

  std::cout << "[e] Error PDR Result is wrong." << std::endl;
  return 10u; // something strange happened.
  
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
