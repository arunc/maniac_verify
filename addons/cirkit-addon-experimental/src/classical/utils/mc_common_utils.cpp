/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mc_common_utils.hpp"

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/


/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

//------------------------------------------------------------------------------
// Return the MSB position of the number.
// NOTE: WORKS ONLY FOR UNSIGNED. FOR SIGNED NEED TO MASK SIGN BIT (32nd or 64th)
unsigned msb_pos_of_unsigned (const unsigned long &num) {
  auto length = sizeof(num) * 8u;
  // it is 0 to N-1, alloting 1 + size-required.
  boost::dynamic_bitset<> num_bs (length + 1, num); 
  for (auto i = length; i > 0u; i--)
    if ( num_bs.test(i) ) return i;
  return 0;
}

//------------------------------------------------------------------------------
void create_work_dir (const std::string &dir_name) {
  boost::filesystem::path p(dir_name);
  if ( boost::filesystem::exists (p) ) {
    std::cout << "[w] Directory \"" + dir_name + "\" exists. "
	      << "Contents will be overwritten"  << std::endl;
    return;
  }
  auto status = boost::filesystem::create_directory (p);
  if (!status)  std::cout << "[e] Cannot create work directory "
			  << "(Permissions? Disk full?)" << std::endl;
  assert (status);
}
void delete_work_dir (const std::string &dir_name) {
  boost::filesystem::path p(dir_name);
  boost::filesystem::remove_all (p);
}

// Open and Copy all files to ofstream, in the order
void cat ( std::ofstream &of, std::string file1, std::string file2, std::string file3)
{
  std::ifstream if1 (file1, std::ios_base::binary);
  std::ifstream if2 (file2, std::ios_base::binary);
  std::ifstream if3 (file3, std::ios_base::binary);
  of << if1.rdbuf() << if2.rdbuf() << if3.rdbuf() << std::endl;
  if1.close(); if2.close(); if3.close();
}

void cat ( std::string out_file, std::string file1, std::string file2, std::string file3)
{
  std::ofstream of;
  of.open (out_file);
  std::ifstream if1 (file1, std::ios_base::binary);
  std::ifstream if2 (file2, std::ios_base::binary);
  std::ifstream if3 (file3, std::ios_base::binary);
  of << if1.rdbuf() << if2.rdbuf() << if3.rdbuf() << std::endl;
  if1.close(); if2.close(); if3.close();
  of.close();
}
//------------------------------------------------------------------------------

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
