/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file mc_common_utils.hpp
 *
 * @brief 
 *
 * @author Arun <arun@informatik.uni-bremen.de>
 * @since  2.3
 */

#ifndef MC_COMMON_UTILS_HPP
#define MC_COMMON_UTILS_HPP

#include <fstream>
#include <iostream>
#include <string>
#include <boost/filesystem.hpp>
#include <boost/dynamic_bitset.hpp>

namespace cirkit
{
void create_work_dir (const std::string &dir_name);
void delete_work_dir (const std::string &dir_name);

void cat ( std::ofstream &of, std::string file1, std::string file2, std::string file3 );
void cat ( std::string out_file, std::string file1, std::string file2, std::string file3 ); 
unsigned msb_pos_of_unsigned (const unsigned long &num);
}
#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
