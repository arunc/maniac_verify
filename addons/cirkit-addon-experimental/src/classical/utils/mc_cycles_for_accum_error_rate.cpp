/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mc_cycles_for_accum_error_rate.hpp"

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/


/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

int mc_cycles_for_accum_error_rate (
  const mc_design &golden, const mc_design &approx,
  const mc_port &port_to_check, unsigned long acc_error_rate,
  unsigned cycle_limit, unsigned signed_outputs,
  std::string work, bool debug, bool verify, bool print_cex)
{
  auto golden_vlog = work + "/golden.v";
  auto approx_vlog = work + "/approx.v";
  golden.write ( golden_vlog, "mc_golden", "verilog" );
  approx.write ( approx_vlog, "mc_approx", "verilog" );

  std::string miter_vlog =  work + "/miter.v";
  std::string miter_blif =  work + "/miter.blif";
  std::string miter_cex  =  work + "/miter.cex";
  std::string miter_summary =  work + "/miter.summary";
  std::string miter_snippet = work + "/m.v";

  // 1. Create the MITER.
  // a. create the miter snippet.
  if (debug) std::cout << "[i] Started  creating accum-error-rate miter :: "
		       << curr_time();
  mc_make_miter ( miter_snippet, golden, approx, "acc_error_rate_miter", port_to_check,
		  acc_error_rate, signed_outputs, mc_miter_type::type3 );
  // b. golden.v + approx.v + miter.v
  cat ( miter_vlog, golden_vlog, approx_vlog, miter_snippet );
  auto miter = mc_design ( miter_vlog, "acc_error_rate_miter", "clock",
			   golden.get_reset(), golden.get_oe(), false  );
  miter.write (miter_blif, "blif");
  if (debug) std::cout << "[i] Finished creating accum-error-rate miter :: "
		       << curr_time();

  // 2. Run BMC
  auto cycles = mc_bmc ( miter_blif, miter_cex, miter_summary, cycle_limit, debug, print_cex );
  if (!verify) return cycles;
  if (cycles < 1) return cycles;
  auto new_limit = cycles - 1;
  auto new_result = mc_bmc ( miter_blif, miter_cex, miter_summary, new_limit, debug, false );
  if ( new_result == -2 ) {
    std::cout << "[i] Result Cross Verified. BMC run for cylces " << new_limit
	      << " is UNSAT" << std::endl;
    return cycles;
  }

  std::cout << "[e] Result verified to be FALSE. BMC did not report UNSAT for "
	    << new_limit << " number of cycles " << std::endl;
  return cycles;
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
