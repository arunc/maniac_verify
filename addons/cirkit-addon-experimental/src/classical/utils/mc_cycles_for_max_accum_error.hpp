
/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file mc_cycles_for_max_accum_error.hpp
 *
 * @brief 
 *
 * @author Arun <arun@informatik.uni-bremen.de>
 * @since  2.3
 */

#ifndef MC_CYCLES_FOR_MAX_ACCUM_ERROR_HPP
#define MC_CYCLES_FOR_MAX_ACCUM_ERROR_HPP

#include <classical/utils/mc_common_utils.hpp>
#include <classical/utils/mc_design.hpp>
#include <classical/utils/mc_curr_time.hpp>
#include <classical/utils/mc_make_miter.hpp>
#include <classical/utils/mc_bmc.hpp>
#include <boost/dynamic_bitset.hpp>
#include <boost/regex.hpp>

namespace cirkit
{

int mc_cycles_for_max_accum_error (
  const mc_design &golden, const mc_design &approx,
  const mc_port &port_to_check, unsigned long acc_error,
  unsigned cycle_limit, unsigned signed_outputs,
  std::string work, bool debug, bool verify, bool print_cex);


}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
