/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file mc_design.hpp
 *
 * @brief Data class to hold the design info
 *
 * @author Arun 
 * @since  2.3
 */

#ifndef MC_DESIGN_HPP
#define MC_DESIGN_HPP

#include <yosys/yosys_api.hpp>
#include <string>
#include <boost/regex.hpp>
#include <iomanip>
#include <ctime>
#include <utility>
#include <classical/utils/mc_common_utils.hpp>

namespace cirkit
{

// TODO : move this out to its own header file.
typedef std::vector < std::pair < std::string, unsigned > > mc_ports;
typedef std::pair <std::string, unsigned> mc_port;
//-------------------------------------------------------------------------
//-------------------------------------------------------------------------
class mc_design
{
public:
  mc_design( const std::string &vlog_file,
	     const std::string &top_module_name,
	     const std::string &clk, const std::string &rst,
	     const std::string &out_enable, bool debug = false );

  // Getters.
  mc_ports get_input_ports() const { return input_ports; };
  mc_ports get_output_ports() const { return output_ports; }
  std::string get_filename () const { return filename; }
  std::string get_top_module () const { return top_module; }
  std::string get_clock () const { return clock; }
  std::string get_reset () const { return reset; }
  std::string get_oe () const { return oe; }

  
  void write ( const std::string &out_filename, const std::string &format ) const;
  void write ( const std::string &out_filename, const std::string &format,
	       const unsigned &opt) const;
  void write ( const std::string &out_filename,
	       const std::string &new_module, const std::string &format,
	       const unsigned &opt) const ;
  void write ( const std::string &out_filename,
	       const std::string &new_module, const std::string &format) const ;
  void write_ports ( const std::string &out_filename );
  unsigned get_width ( const std::string &port_name);
  
private:
  mc_ports input_ports, output_ports;
  std::string top_module;
  std::string filename;
  std::string clock = "__NA__";
  std::string reset = "__NA__";
  std::string oe = "__NA__";
  void extract_ports();
  void get_ports (Yosys::RTLIL::Module *module);
  bool localdebug = false;
};

}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:


