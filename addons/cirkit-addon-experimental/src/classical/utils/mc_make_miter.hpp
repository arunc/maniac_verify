
/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file mc_make_miter.hpp
 *
 * @brief 
 *
 * @author Arun <arun@informatik.uni-bremen.de>
 * @since  2.3
 */

#ifndef MC_MAKE_MITER_HPP
#define MC_MAKE_MITER_HPP

#include <classical/utils/mc_design.hpp>
#include <classical/utils/mc_curr_time.hpp>
#include <classical/utils/mc_common_utils.hpp>
#include <cassert>

namespace cirkit
{
// TOOD give meaningful names.
enum class mc_miter_type {type1, type2, type3, type4, type5_0, type5_1, type5_2};

void mc_make_miter( const std::string &out_filename,
		    const mc_design &golden, const mc_design &approx,
		    std::string miter_module_name, const mc_port &port_to_check,
		    const unsigned long &max_error, const unsigned signed_outputs,
		    const mc_miter_type &miter_type );

void mc_make_miter( const std::string &out_filename,
		    const mc_design &golden, const mc_design &approx,
		    std::string miter_module_name, const mc_port &port_to_check,
		    const unsigned long &max_error, const unsigned signed_outputs,
		    const unsigned long &clk_cycles, const mc_miter_type &miter_type );


}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
