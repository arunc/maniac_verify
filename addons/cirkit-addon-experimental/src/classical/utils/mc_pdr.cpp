/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mc_pdr.hpp"

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/


/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/
inline void __run_abc_cmd (abc::Abc_Frame_t *frame,
			   std::string cmd, bool debug)
{
  
  if (debug) std::cout << "[i] Abc :: " << cmd << std::endl;
  auto status = abc::Cmd_CommandExecute ( frame, cmd.c_str() );
  // Interpretation of status is wrong. Its not a boolean for true or false.
  //if (debug && !status)  std::cout << "[e] Abc :: command " << cmd
  //				   << " failed " << std::endl;
  
}
/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

unsigned mc_pdr( const std::string &blif_file, const std::string &out_cex_file,
		 const std::string &results_file, bool debug )
{
  abc::Abc_Frame_t *frame = abc::Abc_FrameGetGlobalFrame();
  if (!frame) {
    abc::Abc_Stop();
    std::cout << "[e] not able to invoke the ABC frame" << std::endl;
    assert (false ); 
  }

  // redirect printf() output to a string.
  // http://stackoverflow.com/questions/19485536/
  //        redirect-output-of-an-function-printing-to-console-to-string
  auto org_stdout = stdout; 
  char buffer_results[4096];
  auto tmp_stdout = fmemopen (buffer_results, 4096, "w");
  stdout = tmp_stdout;

  std::string read_blif = "read_blif " + blif_file;
  std::string strash = "strash ";
  std::string pdr = "pdr ";
  std::string cex = "write_cex -n  " + out_cex_file;

  auto abc_debug = false;
  __run_abc_cmd (frame, read_blif, abc_debug);
  __run_abc_cmd (frame, strash,    abc_debug);
  __run_abc_cmd (frame, pdr,       abc_debug);
  //if (debug) __run_abc_cmd (frame, cex, abc_debug);
  
  std::fclose (tmp_stdout);
  stdout = org_stdout;
  std::string results = buffer_results;
  std::ofstream of;
  of.open (results_file.c_str(), std::ios_base::out);
  of << results;
  of.close();
  
  std::string undecided = "Property UNDECIDED";
  std::string proved = "Property proved";
  std::string satisfied = "was asserted in frame";

  if (debug)
  {
    std::cout << "[i] Abc Info begins :: " << std::endl
	      << results << "[i] Abc Info ends " << std::endl;
  }
  if ( results.find (undecided) != std::string::npos ) {
    if (debug) std::cout << "[i] PDR Result -  UNDECIDED after 100000 Frames" << std::endl;
    return 1u;
  }
  if ( results.find (proved) != std::string::npos  ) {
    //if (debug) std::cout << "[i] PDR Result -  NO" << std::endl;
    return 3u;
  }
  if ( results.find (satisfied) != std::string::npos  ) {
    //if (debug) std::cout << "[i] PDR Result -  YES " << std::endl;
    return 2u;
  }

  if (debug) std::cout << "[i] PDR Error - Could not complete" << std::endl;
  return 10u; // Should not come here normally.
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
