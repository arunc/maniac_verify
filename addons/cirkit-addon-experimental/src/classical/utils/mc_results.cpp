/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <fstream>
#include "mc_results.hpp"

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

void mc_results::update (const std::string &str, mc_results::info &my_info) {
  my_info.cycles = 0;  my_info.abc_time = 0;  
  // 1. check if there is "No output asserted"
  if (str.find("No output asserted") != std::string::npos)
    my_info.unsat = true;
  else my_info.unsat = false;
  // 2. check if its impossible condition.
  auto combo_flag = false;
  if (str.find("Does not work for combinational networks") != std::string::npos)
  {
    combo_flag = true;     my_info.unsat = true;
    std::cout << "[i] Maniac :: Impossible condition. Reporting as UNSAT"
	      << std::endl;
  }
  if (combo_flag) return;
  // 3. Extract Abc time.
  std::string tmp1 = boost::regex_replace ( // TODO: make this oneliner.
    str, boost::regex(".*Time.*="), std::string(""));
  my_info.abc_time =  std::stof (
    boost::regex_replace ( tmp1, boost::regex("sec"), std::string("") ));
  // 4. Extract cycles.
  if (!my_info.unsat) {
    auto my_regex = boost::regex (
      "^.*Output 0.*was asserted.*frame");
    tmp1 = boost::regex_replace ( str, my_regex, std::string("") );
    my_info.cycles = std::stoi (boost::regex_replace (
				  tmp1, boost::regex("\\.*Time.*=.*"),
				  std::string("") ) );
  }
    
}

void mc_results::write ( std::ofstream &of, const mc_report &format,
			 const mc_type &err_type ) {
  assert (of.is_open()); // Only send an open stream here.
  auto type_str = (err_type == mc_type::accum_error) ?
    "accumulated" : "maximum";
  auto status = (err_type == mc_type::accum_error) ?
    ( accum_error_info.unsat ? "UNSAT" : "SOLVED" )  :
    ( max_error_info.unsat   ? "UNSAT" : "SOLVED" )  ;
  auto __cycles = (err_type == mc_type::accum_error) ?
    accum_error_info.cycles : max_error_info.cycles;

  if (format == mc_report::report) { // TODO: use boost::format
    of << "Status  (" << type_str << ")    :   " << status << std::endl;
    of << "Cycles  (" << type_str << ")    :   " << __cycles << std::endl;
  }
  else if (format == mc_report::csv) {
    of << type_str << "," << status << ","
       << stats->get<unsigned> ("max_cycles")
       << "," << __cycles << "," << stats->get<unsigned> ("max_error")
       << "," << stats->get<unsigned> ("accum_error") << std::endl;
  }
  else {
    std::cout << "[e] Unknown report format. "
	      << "( provide mc_report::csv or mc_report::report )"
	      << std::endl;
    assert(false);
  }
}

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/
mc_results::mc_results( const std::string &str,
			const properties::ptr &mc_stats, mc_type error_type ) {
  stats = mc_stats;
  max_error_info.unsat = true; accum_error_info.unsat = true;
  if (error_type == mc_type::max_error) update (str, max_error_info);
  else update (str, accum_error_info);
}

mc_results::mc_results( const properties::ptr &mc_stats ) {
  stats = mc_stats;
  max_error_info.unsat = true; accum_error_info.unsat = true;
}

void mc_results::update (const std::string &str,
			 const mc_type &error_type) {
  if (error_type == mc_type::max_error) update (str, max_error_info);
  else update (str, accum_error_info);
}

// TODO: try-catch and cover opening errors.
void mc_results::write(const std::string &filename,
		       const mc_report &format) {
  write (filename, false, format);
}

void mc_results::write(const std::string &filename, bool to_append,
		       const mc_report &format) {
  std::ofstream of;
  if (to_append) of.open (filename.c_str(), std::ios_base::app);
  else of.open (filename.c_str(), std::ios_base::out);
  
  if (format == mc_report::csv)
    of << "type,sat_status,max_cycles,cycles,max_error,accum_error"
       << std::endl;
  else if (format == mc_report::report) {
    auto t = std::time(nullptr);
    auto tm = *std::localtime(&t);
    of << std::endl << "MANIAC Approximate verifier report :: "
       << std::put_time(&tm, "( at %H:%M on %d-%m-%Y )") << std::endl
       << "---------------------------------------------------------"
       << "--------" << std::endl;
    of << "Command Line Parameters & Inputs :: " << std::endl;
    of << "Golden      :   " << stats->get<std::string> ("golden")
       << std::endl;
    of << "Approx      :   " << stats->get<std::string> ("approx")
       << std::endl;
    of << "Clock Port  :   " << stats->get<std::string> ("clock")
       << std::endl;
    of << "Reset Port  :   " << stats->get<std::string> ("reset")
       << std::endl;
    of << "Golden Module  :   " << stats->get<std::string> ("golden_module")
       << std::endl;
    of << "Approx Module  :   " << stats->get<std::string> ("golden_module")
       << std::endl;
    of << "Ports Checked  :   " << stats->get<std::string> ("port")
       << std::endl;
    of << "Max Limit   :   " << stats->get<unsigned> ("max_cycles")
       << " (clock cycles specified) " << std::endl;
    of << "Max Error   :   " << stats->get<unsigned> ("max_error")
       << std::endl;
    of << "Accum Error :   " << stats->get<unsigned> ("accum_error")
       << std::endl;
    of << std::endl << "Results :: " << std::endl;
  }
  write (of, format, mc_type::accum_error);
  write (of, format, mc_type::max_error);
  of.close();
}


void mc_results::print() {
  auto t = std::time(nullptr);
  auto tm = *std::localtime(&t);

  std::cout << std::endl << "MANIAC Approximate verifier report :: "
	    << std::put_time(&tm, "( at %H:%M on %d-%m-%Y )") << std::endl
	    << "---------------------------------------------------------"
	    << "--------" << std::endl;
  std::cout << "Golden Module      :  "
	    << get <std::string> (stats, "golden_module","UNDEF")
  	    << std::endl;
  std::cout << "Approx Module      :  "
	    << get <std::string> (stats, "approx_module","UNDEF")
 	    << std::endl;
  std::cout << "Golden             :  "
	    << get <std::string> (stats, "golden","UNDEF") << std::endl;
  std::cout << "Approx             :  "
	    << get <std::string> (stats, "approx","UNDEF") << std::endl;
  std::cout << "Clock Port         :  "
	    << get <std::string> (stats, "clock","UNDEF") << std::endl;
  std::cout << "Reset Port         :  "
	    << get <std::string> (stats, "reset","UNDEF") << std::endl;
  std::cout << "Ports Checked      :  "
	    << get <std::string> (stats, "port","UNDEF") << std::endl;
  std::cout << "Max Limit          :  "
	    << get <unsigned> (stats, "max_cycles", 0)
	    << " (clock cycles spcified) " << std::endl;
  std::cout << "Max Error          :  "
	    << get  <unsigned> (stats, "max_error", 0) << std::endl;
  std::cout << "Accum Error        :  "
	    << get <unsigned> (stats, "accum_error", 0) << std::endl;

  auto status = max_error_info.unsat ? "UNSAT" : "SOLVED"  ;
  std::cout << "Max Error Status   :  " << status << std::endl;
  std::cout << "Max Error Cycles   :  " << max_error_info.cycles
	    << std::endl;
  status = accum_error_info.unsat ? "UNSAT" : "SOLVED" ;
  std::cout << "Accum Error Status :  " << status << std::endl;
  std::cout << "Accum Error Cycles :  " << accum_error_info.cycles
	    << std::endl;

}

float mc_results::get_Abc_time ( const mc_type &err_type ) {
  if (err_type == mc_type::max_error) return max_error_info.abc_time;
  else return accum_error_info.abc_time;
}
unsigned mc_results::get_cycles( const mc_type &err_type ) {
  if (err_type == mc_type::max_error) return max_error_info.cycles;
  else return accum_error_info.cycles;
}
bool mc_results::is_unsat( const mc_type &err_type ) {
  if (err_type == mc_type::max_error) return max_error_info.unsat;
  else return accum_error_info.unsat;
}

}
// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:


