/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file mc_results.hpp
 *
 * @brief Results class for mc_verify(). 
 *
 * @author Arun
 * @since  2.3
 */

#ifndef MC_RESULTS_HPP
#define MC_RESULTS_HPP

#include <string>
#include <boost/regex.hpp>
#include <iomanip>
#include <ctime>

#include <core/utils/program_options.hpp>
#include <core/properties.hpp>
#include <classical/utils/gcc_version.hpp>

namespace po = boost::program_options;

namespace cirkit
{

//-------------------------------------------------------------------------
//-------------------------------------------------------------------------
// TODO: move this out to a more general class.
enum class mc_type {max_error, accum_error};
enum class mc_report {csv, report};
class mc_results
{
public:
  mc_results( const std::string &str,
	      const properties::ptr &mc_stats, mc_type error_type );
  mc_results( const properties::ptr &mc_stats );
  void update (const std::string &str,
			   const mc_type &error_type);
  float get_Abc_time( const mc_type &error_type );
  unsigned get_cycles( const mc_type &error_type );
  bool is_unsat( const mc_type &error_type );
  const bool append = true;
  void write(const std::string &filename, bool to_append=false,
	     const mc_report &format = mc_report::report);
  void write(const std::string &filename,
	     const mc_report &format = mc_report::report);
  void print();
  
private:
  class info { // Data Class to wrap up the info.
  public:
    bool unsat; unsigned cycles = 0u; unsigned abc_time = 0u;
  } max_error_info, accum_error_info;
  void update ( const std::string &str, info &my_info );
  properties::ptr stats;
  void write( std::ofstream &of,
	      const mc_report &format, const mc_type &err_type );

};

}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:


