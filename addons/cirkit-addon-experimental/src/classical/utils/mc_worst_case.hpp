/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file mc_worst_case.hpp
 *
 * @brief 
 *
 * @author Arun <arun@informatik.uni-bremen.de>
 * @since  2.3
 */

#ifndef MC_WORST_CASE_HPP
#define MC_WORST_CASE_HPP

#include <iostream>
#include <string>
#include <abc/abc_api.hpp>
#include <classical/utils/mc_common_utils.hpp>
#include <classical/utils/mc_design.hpp>
#include <classical/utils/mc_curr_time.hpp>
#include <classical/utils/mc_make_miter.hpp>
#include <classical/utils/mc_pdr.hpp>

namespace cirkit
{

unsigned long mc_worst_case (const mc_design &golden, const mc_design &approx,
			     const mc_port &port_to_check, unsigned signed_outputs,
			     std::string work, bool debug, const unsigned &opt);


}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
