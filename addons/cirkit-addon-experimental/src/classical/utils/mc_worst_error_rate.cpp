/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mc_worst_error_rate.hpp"


namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/
//------------------------------------------------------------------------------
inline std::string _pdr_result_to_string (const unsigned short result) {
  if (result == 3u) return " NO ";
  if (result == 2u) return " YES ";
  if (result == 1u) return " UNDECIDED ";
  return " ERROR - IMPOSSIBLE CONDITION ";
  
}
  
/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

unsigned long mc_worst_error_rate (const mc_design &golden, const mc_design &approx,
				   const mc_port &port_to_check, unsigned signed_outputs,
				   std::string work, bool debug, const unsigned &opt,
				   const unsigned &l_bound, const unsigned &u_bound)
{
  const unsigned short NO = 3u, YES = 2u, UNDECIDED = 1u;
  
  auto golden_vlog = work + "/golden.v";
  auto approx_vlog = work + "/approx.v";
  golden.write ( golden_vlog, "mc_golden", "verilog", opt );
  approx.write ( approx_vlog, "mc_approx", "verilog", opt );

  auto max_iterations = 200u;

  auto lower_bound = 0u;
  auto curr_error = port_to_check.second; // max-case, all bits flipped.
  auto upper_bound = curr_error;

  if (l_bound != 0u)  lower_bound = l_bound; // Provide the custom upper&lower bounds.
  if (u_bound != 0u)  upper_bound = u_bound;
  
  if (debug) std::cout << "[i] BinSearch PDR on output :: " << port_to_check.first
		       << "  [width = " << port_to_check.second << "]" << std::endl;
  // TODO: ABC may need to be start/stop cycled for memory buildup.
  // Binary Search.
  auto step = 1u;
  while (lower_bound < upper_bound)
  {
    std::string miter_vlog =  work + "/miter" + std::to_string(step) + ".v";
    std::string miter_blif =  work + "/miter" + std::to_string(step) + ".blif";
    std::string miter_cex  =  work + "/miter" + std::to_string(step) + ".cex";
    std::string miter_summary =  work + "/miter" + std::to_string(step) + ".summary";
    std::string miter_snippet = work + "/m" + std::to_string(step) + ".v";
    
    auto sum = upper_bound + lower_bound;
    auto lsb = (sum & 1u);
    curr_error = lsb ? ( (sum >> 1) + 1 ) : (sum >> 1) ; // (ceil (u+l)/2)
    //std::cout << "sum = " << sum << "  ::  lsb = " << lsb << std::endl;
    //curr_error = (sum >> 1u) | lsb; // (ceil (u+l)/2)

    // 1. Create the MITER.
    // a. create the miter snippet.
    if (debug) std::cout << "[i] Step " << step << " Started  creating max-error-rate miter :: "
			 << curr_time();
    mc_make_miter ( miter_snippet, golden, approx, "max_error_rate_miter", port_to_check,
		    curr_error, signed_outputs, mc_miter_type::type4 );
    // b. cat golden.v + approx.v + miter.v
    cat ( miter_vlog, golden_vlog, approx_vlog, miter_snippet );
    auto miter = mc_design ( miter_vlog, "max_error_rate_miter", "clock",
			     golden.get_reset(), golden.get_oe(), false  );
    miter.write (miter_blif, "blif", opt);
    if (debug) std::cout << "[i] Step " << step << " Finished creating max-error-rate miter :: "
			 << curr_time();

    // 2. Run PDR.
    if (debug) std::cout << "[i] Step " << step << " Starting PDR :: BinSearch bounds ["
			 << lower_bound << "," << upper_bound
			 << "] :: ##bit_flip(" << port_to_check.first << ") >= "
			 << curr_error << "?   :: " << curr_time();
    auto result = mc_pdr (miter_blif, miter_cex, miter_summary, debug);
    if (debug) std::cout << "[i] Step " << step << " Finished PDR :: BinSearch bounds ["
			 << lower_bound << "," << upper_bound
			 << "] :: ##bit_flip(" << port_to_check.first << ") >= "
			 << curr_error << "?   :: " << _pdr_result_to_string (result)
			 << "  :: " << curr_time();

    // 3. Adjust the bounds
    if (result == NO) { // lower_bound remains the same.
      upper_bound = curr_error - 1; // since we are verifying >= 
    }
    else if (result == YES) { // upper bound remains the same.
      lower_bound = curr_error;
    }
    else if (result == UNDECIDED) { // Cant figure out by this method.
      std::cout << "[e] Inference :: worst-case is between " << lower_bound
		<< " and " << upper_bound << std::endl;
      std::cout << "[e] Cannot process further. Returning 0 " << std::endl;
      return 0u;
    }

    //auto lsb = curr_error & 1u;
    //curr_error = lsb ? ( (curr_error >> 1u) + 1 ) : (curr_error >> 1u) ;
    //---auto sum = curr_error;
    //---auto lsb = (sum & 1u);
    //---curr_error = lsb ? ( (sum >> 1) + 1 ) : (sum >> 1) ; // (ceil (u+l)/2)
    //---std::cout << "sum = " << sum << "  ::  lsb = " << lsb << std::endl;
    //---//curr_error = (sum >> 1u) | lsb; // (ceil (u+l)/2)
    
    if (step >= max_iterations) {
      std::cout << "[e] Inference :: worst-case is between " << lower_bound
		<< " and " << upper_bound << std::endl;
      std::cout << "[e] Aborting after " << step << "th step. Returning 0 "
		<< std::endl;
      return 0u;
    }
    step++;
    
  } // while loop
  
  assert (lower_bound == upper_bound); // Never fails.
  return lower_bound;
  
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
