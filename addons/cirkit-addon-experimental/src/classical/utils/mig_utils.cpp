/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mig_utils.hpp"

#include <boost/assign/std/vector.hpp>
#include <boost/format.hpp>

#include <core/graph/depth.hpp>

using namespace boost::assign;

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

void mig_print_stats( const mig_graph& mig, std::ostream& os )
{
  const auto& info = mig_info( mig );
  auto n = info.inputs.size();

  std::string name = info.model_name;
  if ( name.empty() )
  {
    name = "(unnamed)";
  }

  std::vector<mig_node> outputs;
  for ( const auto& output : info.outputs )
  {
    outputs += output.first.node;
  }

  std::vector<unsigned> depths;
  const auto depth = compute_depth( mig, outputs, depths );

  os << boost::format( "[i] %20s: i/o = %7d / %7d  maj = %7d  lev = %4d" ) % name % n % info.outputs.size() % ( boost::num_vertices( mig ) - n - 1u ) % depth << std::endl;
}

std::vector<mig_function> get_children( const mig_graph& mig, const mig_node& node )
{
  std::vector<mig_function> children;

  for ( const auto& edge : boost::make_iterator_range( boost::out_edges( node, mig ) ) )
  {
    children += mig_to_function( mig, edge );
  }

  return children;
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
