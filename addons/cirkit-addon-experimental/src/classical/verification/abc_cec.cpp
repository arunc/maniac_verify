/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "abc_cec.hpp"

#include <cstdlib>
#include <map>

#include <boost/format.hpp>
#include <boost/regex.hpp>

#include <core/utils/range_utils.hpp>
#include <core/utils/string_utils.hpp>
#include <core/utils/timer.hpp>
#include <classical/functions/aig_cone.hpp>
#include <classical/functions/aig_rename.hpp>
#include <classical/io/write_aiger.hpp>
#include <classical/utils/aig_utils.hpp>

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

using mapping_t = std::map<std::string, std::string>;

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

const abc_equivalence_check_commands::type abc_equivalence_check_commands::cec = std::make_tuple( "cec %1% %2%", "^Networks are equivalent.*$" );

bool abc_equivalence_check( const aig_graph& c1, const aig_graph& c2,
                            const std::map<std::string, std::string>& input_mapping,
                            const std::map<std::string, std::string>& output_mapping,
                            const properties::ptr& settings,
                            const properties::ptr& statistics )
{
  using boost::format;
  using boost::str;

  /* settings */
  auto tmpname1   = get( settings, "tmpname1",   std::string( "/tmp/c1.aag" ) );
  auto tmpname2   = get( settings, "tmpname2",   std::string( "/tmp/c2.aag" ) );
  auto abccommand = get( settings, "abccommand", abc_equivalence_check_commands::cec );
  auto abclogname = get( settings, "abclogname", std::string( "/tmp/abc.log" ) );
  auto verbose    = get( settings, "verbose",    false );

  /* statistics */
  properties_timer t( statistics );

  auto info1 = aig_info( c1 );
  auto info2 = aig_info( c2 );

  /* c1 cannot be smaller */
  assert( info1.inputs.size() >= info2.inputs.size() );
  assert( info1.outputs.size() >= info2.outputs.size() );
  assert( info2.inputs.size() == input_mapping.size() );
  assert( info2.outputs.size() == output_mapping.size() );

  /* strip larger circuit */
  aig_graph aig_cone_result;
  if ( info1.outputs.size() > info2.outputs.size() )
  {
    auto output_names1 = get_map_keys( output_mapping );
    aig_cone_result = aig_cone( c1, output_names1, settings );
    info1 = aig_info( aig_cone_result );

    if ( verbose && info1.inputs.size() != info2.inputs.size() )
    {
      std::cout << "[w] c1 has more inputs than c2 after cone stripping" << std::endl;
    }
  }
  else
  {
    aig_cone_result = c1;
  }

  /* rename inputs */
  aig_rename( aig_cone_result, input_mapping, output_mapping );

  write_aiger( aig_cone_result, tmpname1 );
  write_aiger( aig_cone_result, tmpname2 );

  system( str( format( "aigtoaig %1% %1%.aig" ) % tmpname1 ).c_str() );
  system( str( format( "aigtoaig %1% %1%.aig" ) % tmpname2 ).c_str() );

  auto name1   = str( format( "%1%.aig" ) % tmpname1 );
  auto name2   = str( format( "%1%.aig" ) % tmpname2 );
  auto command = str( format( std::get<0>( abccommand ) ) % name1 % name2 );
  system( str( format( "abc -c \"%s\" > %s" ) % command % abclogname ).c_str() );

  return any_line_contains( abclogname, boost::regex( std::get<1>( abccommand ) ) );
}

bool abc_equivalence_check( const aig_graph& c1, const aig_graph& c2,
                            const std::vector<unsigned>& input_mapping,
                            const std::vector<unsigned>& output_mapping,
                            const properties::ptr& settings,
                            const properties::ptr& statistics )
{
  std::map<std::string, std::string> sinput_mapping, soutput_mapping;

  const auto& info1 = aig_info( c1 );
  const auto& info2 = aig_info( c2 );

  for ( const auto& in : index( input_mapping ) )
  {
    sinput_mapping[info1.node_names.at( info1.inputs[in.value] )] = info2.node_names.at( info2.inputs[in.index] );
  }

  for ( const auto& out : index( output_mapping ) )
  {
    soutput_mapping[info1.outputs[out.value].second] = info2.outputs[out.index].second;
  }

  return abc_equivalence_check( c1, c2, sinput_mapping, soutput_mapping, settings, statistics );
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
