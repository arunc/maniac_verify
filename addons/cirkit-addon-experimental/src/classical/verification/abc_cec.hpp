/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file abc_cec.hpp
 *
 * @brief Combinational equivalance check with abc
 *
 * @author Mathias Soeken
 * @since  2.3
 */

#ifndef ABC_CEC_HPP
#define ABC_CEC_HPP

#include <map>
#include <vector>

#include <core/properties.hpp>
#include <classical/aig.hpp>

namespace cirkit
{

struct abc_equivalence_check_commands
{
  using type = std::tuple<std::string, std::string>;
  static const type cec;
};

/* If circuits are not of same size (# of PIs and POs) then c1 is
   assumed to be the larger one */
bool abc_equivalence_check( const aig_graph& c1, const aig_graph& c2,
                            const std::map<std::string, std::string>& input_mapping,
                            const std::map<std::string, std::string>& output_mapping,
                            const properties::ptr& settings = properties::ptr(),
                            const properties::ptr& statistics = properties::ptr() );

bool abc_equivalence_check( const aig_graph& c1, const aig_graph& c2,
                            const std::vector<unsigned>& input_mapping,
                            const std::vector<unsigned>& output_mapping,
                            const properties::ptr& settings = properties::ptr(),
                            const properties::ptr& statistics = properties::ptr() );

}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
