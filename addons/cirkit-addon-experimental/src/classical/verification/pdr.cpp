/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "pdr.hpp"

#include <core/utils/timer.hpp>

#include <boost/assign/std/vector.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/optional.hpp>

#include <queue>
#include <algorithm>

using namespace boost::assign;

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

using cube_t           = std::vector<int>;
using timed_cube_t     = std::pair<cube_t, int>;
using cube_vec_t       = std::vector<cube_t>;
using priority_queue_t = std::priority_queue<timed_cube_t>;

}

namespace std
{

template<>
struct less<cirkit::timed_cube_t>
{
  bool operator()( const cirkit::timed_cube_t& x, const cirkit::timed_cube_t& y ) const
  {
    return x.second > y.second; /* > is intended! */
  }
};

}

namespace cirkit
{

enum class solver_flag
{
  no_model, extract_model, no_induction
};

class pdr_sat_solver
{
public:
  pdr_sat_solver( const aig_graph& aig, const std::vector<cube_vec_t>& blocked_cubes )
    : aig( aig ), blocked_cubes( blocked_cubes )
  {
  }

  boost::optional<cube_t> get_bad_cube() { throw std::runtime_error("not implemented"); }
  bool is_blocked( const timed_cube_t& s ) { throw std::runtime_error("not implemented"); }
  bool is_initial( const cube_t& c ) { throw std::runtime_error("not implemented"); }
  timed_cube_t solve_relative( const timed_cube_t& s, solver_flag params = solver_flag::no_model ) { throw std::runtime_error("not implemented"); }

  void block_cube_in_solver( const timed_cube_t& s ) { throw std::runtime_error("not implemented"); } /* maybe not needed? */

private:
  const aig_graph&               aig;
  const std::vector<cube_vec_t>& blocked_cubes;
};

class pdr_manager
{
public:
  explicit pdr_manager( const aig_graph& aig )
    : aig( aig ), solver( aig, blocked_cubes ) {}

  bool start();

private:
  bool block_cube( const timed_cube_t& s0 );
  bool is_blocked( const timed_cube_t& s );
  void generalize( timed_cube_t& s );

  bool propagate_blocked_cubes();

  int depth();
  void new_frame();
  bool cond_assign( timed_cube_t& s, const timed_cube_t& t );
  void add_blocked_cube( const timed_cube_t& s );

private:
  const aig_graph&        aig;
  std::vector<cube_vec_t> blocked_cubes; /* F_∞ is blocked_cubes.end() */
  pdr_sat_solver          solver;
};

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

inline timed_cube_t next( const timed_cube_t& t ) { return {t.first, t.second + 1}; }

inline bool subsumes( const cube_t& c1, const cube_t& c2 )
{
  for ( auto l : c1 )
  {
    if ( boost::find( c2, l ) == c2.end() )
    {
      return false;
    }
  }

  return true;
}

bool pdr_manager::start()
{
  blocked_cubes += cube_vec_t(); /* F_∞ */
  new_frame();

  while ( true )
  {
    auto c = solver.get_bad_cube();
    if ( (bool)c )
    {
      if ( !block_cube( {*c, depth()} ) )
      {
        return false;
      }
    }
    else
    {
      new_frame();
      if ( propagate_blocked_cubes() )
      {
        return true;
      }
    }
  }
}

bool pdr_manager::block_cube( const timed_cube_t& s0 )
{
  priority_queue_t q;
  q.push( s0 );

  while ( !q.empty() )
  {
    auto s = q.top(); q.pop();
    if ( s.second == 0 )
    {
      return false;
    }

    if ( !is_blocked( s ) )
    {
      assert( !solver.is_initial( s.first ) );
      auto z = solver.solve_relative( s, solver_flag::extract_model );

      if ( z.second >= 0 )
      {
        generalize( z );

        while ( ( z.second < depth() - 1 ) &&
                cond_assign( z, solver.solve_relative( next( z ) ) ) ) {}

        add_blocked_cube( z );

        if ( s.second < depth() && z.second != depth() + 1u )
        {
          q.push( next( s ) );
        }
      }
      else
      {
        /* cube 's' was not blocked by image of predecessor */
        z.second = s.second - 1;
        q.push( z );
        q.push( s );
      }
    }
  }

  return true;
}

bool pdr_manager::is_blocked( const timed_cube_t& s )
{
  /* check syntactic subsumption (faster than SAT) */
  for ( auto d = s.second; d < blocked_cubes.size(); ++d )
  {
    for ( const auto& cube : blocked_cubes[d] )
    {
      if ( subsumes( cube, s.first ) )
      {
        return true;
      }
    }
  }

  /* semantic subsumption through SAT */
  return solver.is_blocked( s );
}

void pdr_manager::generalize( timed_cube_t& s )
{
  for ( int p : s.first )
  {
    cube_t tc( s.first.size() - 1u );
    std::copy_if( s.first.begin(), s.first.end(), tc.begin(), [&]( int l ) { return l != p; } );
    timed_cube_t t{ tc, s.second };

    if ( !solver.is_initial( t.first ) )
    {
      cond_assign( s, solver.solve_relative( t ) );
    }
  }
}

bool pdr_manager::propagate_blocked_cubes()
{
  for ( auto k = 1u; k < depth(); ++k )
  {
    //for ( const auto& c : blocked_cubes[k] )
    for ( auto i = 0u; i < blocked_cubes[k].size(); ++i ) /* NOTE: consider that blocked_cubes[k] may get updated, see paper remark */
    {
      auto c = blocked_cubes[k][i];
      auto s = solver.solve_relative( {c, k + 1}, solver_flag::no_induction );
      if ( s.second >= 0u )
      {
        add_blocked_cube( s );
      }
    }

    if ( blocked_cubes[k].empty() )
    {
      return true; /* invariant found */
    }
  }

  return false;
}

int pdr_manager::depth()
{
  return blocked_cubes.size() - 2;
}

void pdr_manager::new_frame()
{
  blocked_cubes.insert( blocked_cubes.end() - 1, cube_vec_t() );
}

bool pdr_manager::cond_assign( timed_cube_t& s, const timed_cube_t& t )
{
  if ( t.second >= 0 )
  {
    s = t;
    return true;
  }
  return false;
}

void pdr_manager::add_blocked_cube( const timed_cube_t& s )
{
  auto k = std::min( s.second, depth() + 1 );

  /* remove subsumed clauses */
  for ( auto d = 1u; d <= k; ++d )
  {
    for ( auto i = 0u; i < blocked_cubes[d].size(); )
    {
      if ( subsumes( s.first, blocked_cubes[d][i] ) )
      {
        blocked_cubes[d][i] = blocked_cubes[d].back();
        blocked_cubes[d].pop_back();
      }
      else
      {
        ++i;
      }
    }
  }

  /* store clause */
  blocked_cubes[k].push_back( s.first );
  solver.block_cube_in_solver( s );
}

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

bool pdr( const aig_graph& aig, const properties::ptr& settings, const properties::ptr& statistics )
{
  /* settings */

  /* statistics */
  properties_timer t( statistics );

  return false;
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
