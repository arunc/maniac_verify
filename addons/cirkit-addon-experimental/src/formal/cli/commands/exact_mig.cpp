/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "exact_mig.hpp"

#include <boost/format.hpp>

#include <classical/cli/stores.hpp>
#include <classical/utils/mig_utils.hpp>
#include <formal/synthesis/exact_mig.hpp>

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

exact_mig_command::exact_mig_command( const environment::ptr& env )
  : command( env, "Exact MIG synthesis" ),
    migs( env->store<mig_graph>() ),
    tts( env->store<tt>() )
{
  opts.add_options()
    ( "start,s",       value_with_default( &start ), "Start value for gate enumeration" )
    ( "incremental,i",                               "Incremental SAT solving" )
    ;
  be_verbose();
}

bool exact_mig_command::execute()
{
  using boost::format;

  auto settings = make_settings();
  settings->set( "start",       start );
  settings->set( "incremental", opts.is_set( "incremental" ) );
  auto statistics = std::make_shared<properties>();

  migs.extend();
  migs.current() = exact_mig_with_sat( tts.current(), settings, statistics );

  std::cout << format( "[i] run-time: %.2f seconds" ) % statistics->get<double>( "runtime" ) << std::endl;

  return true;
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
