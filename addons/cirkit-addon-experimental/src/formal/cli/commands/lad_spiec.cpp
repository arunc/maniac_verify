/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "lad_spiec.hpp"

#include <map>
#include <vector>

#include <boost/format.hpp>

#include <core/utils/program_options.hpp>
#include <core/utils/range_utils.hpp>
#include <core/utils/string_utils.hpp>
#include <classical/cli/stores.hpp>
#include <classical/functions/aig_cone.hpp>
#include <classical/functions/aig_rename.hpp>
#include <classical/functions/lad2.hpp>
#include <formal/revenge/find_component_candidate.hpp>
#include <formal/revenge/spiec.hpp>

using namespace boost::program_options;

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

lad_spiec_command::lad_spiec_command( const environment::ptr& env )
  : command( env, "Finds a component in a block using LAD" ),
    aigs( env->store<aig_graph>() )
{
  opts.add_options()
    ( "block,b",               value_with_default( &block ),         "Store-ID of block circuit" )
    ( "component,c",           value_with_default( &component ),     "Store-ID of component circuit" )
    ( "vectors",               value_with_default( &vectors ),       "Simulation vectors (comma separated):\nah: all-hot\n1h: one-hot\n2h: two-hot\nac: all-cold\n1c: one-cold\n2c: two-cold" )
    ( "functional,f",          value_with_default( &functional ),    "Whether to use functional support for domain restriction" )
    ( "support_edges,s",       value_with_default( &support_edges ), "Use support edges (only with non-native)" )
    ( "simulation_signatures", value( &simulation_signatures ),      "Use simulation signatures; value is max k" )
    ( "native_graph,n",                                              "Use native graph data structure" )
    ( "shrink",                                                      "Just shrink the block, perform no mapping" )
    ( "pruning_inputs",        value( &pruning_inputs ),             "Pruning inputs (only with non-native)" )
    ( "extract,e",                                                   "Extract component from block and rename I/Os" )
    ;
  be_verbose();

  boost::program_options::options_description new_spiec_options( "New SPIEC implementation" );
  new_spiec_options.add_options()
    ( "new",                       "Use new implementation" )
    ( "gac_no_simvectors",         "Ignore simulation vectors in GAC(AllDiff)" )
    ( "auto_symmetries",           "Detect symmetries using SAT for filtering" )
    ;

  opts.add( new_spiec_options );
}

command::rules_t lad_spiec_command::validity_rules() const
{
  return {
    {[&]() { return block < aigs.size(); }, "store-ID of block is invalid" },
    {[&]() { return component < aigs.size(); }, "store-ID of component is invalid" }
  };
}

bool lad_spiec_command::execute()
{
        auto& aig_block      = aigs[block];
  const auto& aig_component  = aigs[component];

  const auto& block_info     = aig_info( aig_block );
  const auto& component_info = aig_info( aig_component );

    std::vector<unsigned> types;
  foreach_string( vectors, ",", [&]( const std::string& s ) {
      if      ( s == "ah" ) types += 0u;
      else if ( s == "1h" ) types += 3u;
      else if ( s == "2h" ) types += 5u;
      else if ( s == "ac" ) types += 1u;
      else if ( s == "1c" ) types += 2u;
      else if ( s == "2c" ) types += 4u;
    } );

  std::vector<unsigned> input_mapping, output_mapping;

  auto settings   = make_settings();
  settings->set( "functional",            functional );
  settings->set( "support_edges",         support_edges );
  if ( opts.is_set( "simulation_signatures" ) )
  {
    settings->set( "simulation_signatures", boost::optional<unsigned>( simulation_signatures ) );
    settings->set( "signatures", boost::optional<unsigned>( simulation_signatures ) );
  }

  std::vector<std::string> vpruning_inputs;
  split_string( vpruning_inputs, pruning_inputs, "," );
  settings->set( "pruning_inputs", vpruning_inputs );
  auto statistics = std::make_shared<properties>();

  if ( opts.is_set( "shrink" ) )
  {
    aig_block = shrink_block( aig_block, aig_component, types, settings, statistics );
  }
  else
  {
    auto result = false;

    if ( opts.is_set( "new" ) )
    {
      settings->set( "types", types );
      settings->set( "gac_no_simvectors", opts.is_set( "gac_no_simvectors" ) );
      settings->set( "auto_symmetries", opts.is_set( "auto_symmetries" ) );
      spiec_with_lad( aig_component, aig_block, settings, statistics );
      return true;
    }
    else if ( opts.is_set( "native_graph" ) )
    {
      result = find_component_candidate_directed_lad3( input_mapping, output_mapping, aig_block, aig_component, types, settings, statistics );
    }
    else
    {
      result = find_component_candidate_directed_lad4( input_mapping, output_mapping, aig_block, aig_component, types, settings, statistics );
    }

    std::cout << "[i] is pattern contained in target: " << ( result ? "yes" : "no" ) << std::endl;

    if ( result && opts.is_set( "extract" ) )
    {
      std::vector<std::string> output_names;
      std::map<std::string, std::string> imap, omap;

      for ( const auto& i : index( input_mapping ) )
      {
        imap.insert( {block_info.node_names.at( block_info.inputs.at( i.value ) ),
                      component_info.node_names.at( component_info.inputs.at( i.index ) )} );
      }

      for ( const auto& i : index( output_mapping ) )
      {
        omap.insert( {block_info.outputs.at( i.value ).second, component_info.outputs.at( i.index ).second} );
        output_names += block_info.outputs.at( i.value ).second;
      }

      aig_block = aig_cone( aig_block, output_names, settings );
      aig_rename( aig_block, imap, omap );
    }
  }
  std::cout << boost::format( "[i] run-time: %.2f seconds" ) % statistics->get<double>( "runtime" ) << std::endl;

  return true;
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
