/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file lad_spiec.hpp
 *
 * @brief Finds a component in a block using LAD
 *
 * @author Mathias Soeken
 * @since  2.3
 */

#ifndef CLI_LAD_SPIEC_COMMAND_HPP
#define CLI_LAD_SPIEC_COMMAND_HPP

#include <string>

#include <core/cli/command.hpp>
#include <core/cli/environment.hpp>
#include <core/cli/store.hpp>
#include <classical/aig.hpp>

namespace cirkit
{

class lad_spiec_command : public command
{
public:
  lad_spiec_command( const environment::ptr& env );

protected:
  rules_t validity_rules() const;
  bool execute();

private:
  unsigned              block                 = 0u;
  unsigned              component             = 1u;
  std::string           vectors               = "ah,1h,2h";
  bool                  functional            = true;
  bool                  support_edges         = true;
  unsigned              simulation_signatures;
  std::string           pruning_inputs;
  cli_store<aig_graph>& aigs;
};

}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
