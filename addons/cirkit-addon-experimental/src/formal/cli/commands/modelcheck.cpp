/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "modelcheck.hpp"

#include <core/utils/program_options.hpp>
#include <classical/cli/stores.hpp>
#include <formal/verification/aig_modelcheck.hpp>

using namespace boost::program_options;

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

modelcheck_command::modelcheck_command( const environment::ptr & env )
  : aig_base_command( env, "Modelcheck AIG" ),
    cexs( env->store<counterexample_t>() )
{
  opts.add_options()
    ( "inputs,i",   value( &inputs ),   "Input constraints, e.g., \"00-11-0\"" )
    ( "outputs,o",  value( &outputs ),  "Output constraints" )
    ;
  be_verbose();
}

bool modelcheck_command::execute()
{
  const aig_graph& the_aig = aig();
  aig_modelcheck( the_aig, inputs, outputs );
  return true;
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
