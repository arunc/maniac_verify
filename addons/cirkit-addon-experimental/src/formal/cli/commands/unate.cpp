/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "unate.hpp"

#include <iostream>

#include <boost/format.hpp>

#include <formal/verification/unate.hpp>

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

unate_command::unate_command( const environment::ptr& env )
  : aig_base_command( env, "Checks function for unateness" )
{
  opts.add_options()
    ( "progress,p", "Show progress" )
    ;
  be_verbose();
}

bool unate_command::execute()
{
  const auto settings = make_settings();
  settings->set( "progress", opts.is_set( "progress" ) );
  const auto statistics = std::make_shared<properties>();

  const auto u = unateness( aig(), settings, statistics );
  for ( auto po = 0u; po < info().outputs.size(); ++po )
  {
    for ( auto pi = 0u; pi < info().inputs.size(); ++pi )
    {
      switch ( get_unateness_kind( u, po, pi, info() ) )
      {
      case unate_kind::binate:
        std::cout << ".";
        break;
      case unate_kind::unate_pos:
        std::cout << "p";
        break;
      case unate_kind::unate_neg:
        std::cout << "n";
        break;
      default:
        std::cout << " ";
        break;
      }
    }
    std::cout << std::endl;
  }

  std::cout << boost::format( "[i] run-time: %.2f secs" ) % statistics->get<double>( "runtime" ) << std::endl;

  return true;
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
