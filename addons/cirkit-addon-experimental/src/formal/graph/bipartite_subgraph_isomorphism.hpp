/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file bipartite_subgraph_isomorphism.hpp
 *
 * @brief Solves subgraph isomorphism using SAT
 *
 * @author Mathias Soeken
 * @since 2.0
 */

#ifndef BIPARTITE_SUBGRAPH_ISOMORPHISM_HPP
#define BIPARTITE_SUBGRAPH_ISOMORPHISM_HPP

#include <core/properties.hpp>
#include <formal/graph/kpartite_subgraph_isomorphism.hpp>

namespace cirkit
{

template<class Solver, class Graph>
bool bipartite_subgraph_isomorphism( const Graph& target, const Graph& pattern, unsigned n, unsigned m,
                                     properties::ptr settings = properties::ptr(), properties::ptr statistics = properties::ptr() )
{
  /* Number of vertices */
  unsigned q = boost::num_vertices( target ) - n;
  unsigned r = boost::num_vertices( pattern ) - m;

  assert( m <= n );
  assert( r <= q );

  return kpartite_subgraph_isomorphism<Solver>( target, pattern, {n, q}, {m, r}, settings, statistics );
}

}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
