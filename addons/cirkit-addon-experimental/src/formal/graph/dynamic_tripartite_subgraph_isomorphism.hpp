/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file dynamic_tripartite_subgraph_isomorphism.hpp
 *
 * @brief Tripartite subgraph isomorphism that creates graph implicitly using AIG
 *
 * @author Mathias Soeken
 * @since 2.0
 */

#ifndef DYNAMIC_TRIIPARTITE_SUBGRAPH_ISOMORPHISM_HPP
#define DYNAMIC_TRIIPARTITE_SUBGRAPH_ISOMORPHISM_HPP

#include <cstdlib>
#include <fstream>
#include <functional>
#include <vector>

#include <boost/assign/std/vector.hpp>
#include <boost/dynamic_bitset.hpp>
#include <boost/format.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/timer/timer.hpp>

#include <core/properties.hpp>
#include <classical/io/write_bench.hpp>
#include <core/utils/abc.hpp>
#include <core/utils/range_utils.hpp>
#include <core/utils/timer.hpp>
#include <classical/aig.hpp>
#include <formal/graph/kpartite_subgraph_isomorphism.hpp>
#include <formal/sat/operations/logic.hpp>
#include <formal/sat/utils/add_aig.hpp>
#include <formal/sat/utils/add_dimacs.hpp>
#include <formal/sat/utils/solver_executables.hpp>

using namespace boost::assign;

namespace cirkit
{

template<class Solver>
using encode_slice_func_t = std::function<int(Solver&, const aig_graph&, const std::vector<boost::dynamic_bitset<>>&,
                                              int, std::vector<int>&, std::vector<int>&, std::vector<std::vector<int>>&,
                                              const properties::ptr&)>;

struct encode_slice_abc_constants
{
  static constexpr const char* abc_cnf_command = "read_bench %s; strash; &get; &syn2; &write_cnf -K 8 -o %s";
};

template<class Solver>
int encode_slice_naive( Solver& solver, const aig_graph& aig, const std::vector<boost::dynamic_bitset<>>& sim_vectors,
                        int sid, std::vector<int>& i_neg_ids, std::vector<int>& o_neg_ids, std::vector<std::vector<int>>& target,
                        const properties::ptr& settings )
{
  const auto& aig_info = boost::get_property( aig, boost::graph_name );

  boost::copy( boost::irange( sid, (int)(sid + aig_info.inputs.size()) ), i_neg_ids.begin() );
  boost::copy( boost::irange( (int)(i_neg_ids.back() + 1u), (int)(i_neg_ids.back() + 1u + aig_info.outputs.size()) ), o_neg_ids.begin() );

  sid += aig_info.inputs.size() + aig_info.outputs.size();

  for ( const auto& sim : index( sim_vectors ) )
  {
    std::vector<int> piids( aig_info.inputs.size() );
    std::vector<int> poids( aig_info.outputs.size() );
    sid = add_aig( solver, aig, sid, piids, poids );

    /* constrain inputs */
    for ( unsigned i = 0u; i < sim.value.size(); ++i )
    {
      if ( sim.value[i] )
      {
        not_equals( solver, i_neg_ids[i], piids[i] );
      }
      else
      {
        equals( solver, i_neg_ids[i], piids[i] );
      }
    }

    /* duplicate outputs */
    for ( const auto& output : index( poids ) )
    {
      int noid = sid++;
      logic_xor( solver, output.value, o_neg_ids[output.index], noid );
      target[sim.index][output.index] = noid;
    }
  }

  return sid;
}

template<class Solver>
int encode_slice_abc( Solver& solver, const aig_graph& aig, const std::vector<boost::dynamic_bitset<>>& sim_vectors,
                      int sid, std::vector<int>& i_neg_ids, std::vector<int>& o_neg_ids, std::vector<std::vector<int>>& target,
                      const properties::ptr& settings )
{
  const auto& aig_info = boost::get_property( aig, boost::graph_name );

  std::ofstream os( "/tmp/slice.bench", std::ofstream::out );

  write_bench_settings wb_settings;
  wb_settings.write_input_declarations = false;
  wb_settings.write_output_declarations = false;

  /* inputs and outputs */
  for ( unsigned i = 0u; i < aig_info.inputs.size(); ++i )
  {
    os << boost::format( "INPUT(ii_%d)" ) % i << std::endl;
  }

  for ( unsigned i = 0u; i < aig_info.outputs.size(); ++i )
  {
    os << boost::format( "INPUT(oi_%d)" ) % i << std::endl;
  }

  for ( unsigned i = 0u; i < sim_vectors.size(); ++i )
  {
    for ( unsigned j = 0u; j < aig_info.outputs.size(); ++j )
    {
      os << boost::format( "OUTPUT(out_s%d_%d)" ) % i % j << std::endl;
      target[i][j] = sid + 1 + i * aig_info.outputs.size() + j;
    }
  }

  for ( auto sim_vector : index( sim_vectors ) )
  {
    wb_settings.prefix = boost::str( boost::format( "s%d_" ) % sim_vector.index );
    write_bench( aig, os, wb_settings );

    /* connect input inverters */
    for ( unsigned i = 0u; i < sim_vector.value.size(); ++i )
    {
      unsigned lut = sim_vector.value[i] ? 0x1 /* INV */ : 0x2 /* BUF */;
      os << boost::format( "s%d_%s = LUT 0x%x ( ii_%d )" ) % sim_vector.index % aig_info.node_names.find( aig_info.inputs[i] )->second % lut % i << std::endl;
    }

    /* connect output inverters and outputs */
    for ( unsigned i = 0u; i < aig_info.outputs.size(); ++i )
    {
      os << boost::format( "out_s%d_%d = LUT 0x6 ( s%d_%s, oi_%d )" ) % sim_vector.index % i % sim_vector.index % aig_info.outputs[i].second % i << std::endl;
    }
  }

  os.close();

  std::string abc_cnf_command = get( settings, "abc_cnf_command", std::string( encode_slice_abc_constants::abc_cnf_command ) );
  bool verbose = get( settings, "verbose", false );

  //abc( "read_bench /tmp/slice.bench; strash; csweep; refactor; rewrite; refactor -z; rewrite -z; write_cnf -for /tmp/slice.cnf" );
  //abc( "read_bench /tmp/slice.bench; strash; &get; &syn2; &write_cnf -K 8 -r /tmp/slice.cnf" );
  abc( boost::str( boost::format( abc_cnf_command ) % "/tmp/slice.bench" % "/tmp/slice.cnf" ), verbose );
  int new_sid = add_dimacs( solver, "/tmp/slice.cnf", sid );

  for ( unsigned i = 0u; i < aig_info.inputs.size(); ++i )
  {
    i_neg_ids[i] = new_sid - aig_info.inputs.size() - aig_info.outputs.size() + i - 1;
  }

  for ( unsigned i = 0u; i < aig_info.outputs.size(); ++i )
  {
    o_neg_ids[i] = new_sid - aig_info.outputs.size() + i - 1;
  }

  return new_sid;
}

template<class Solver, class Graph>
bool dynamic_tripartite_subgraph_isomorphism( const aig_graph& aig, const std::vector<boost::dynamic_bitset<>>& sim_vectors,
                                              const Graph& pattern, unsigned p_inputs, unsigned p_pattern,
                                              properties::ptr settings = properties::ptr(), properties::ptr statistics = properties::ptr() )
{
  using id_pair_vec_t = std::vector<std::pair<unsigned, unsigned>>;

  /* Settings */
  auto encode_slice     = get( settings, "encode_slice",     encode_slice_func_t<Solver>( encode_slice_abc<Solver> ) );
  auto blocking_vectors = get( settings, "blocking_vectors", id_pair_vec_t() ); /* target -> pattern */
  auto blocking_support = get( settings, "blocking_support", id_pair_vec_t() ); /* target -> pattern */
  auto symmetric_inputs = get( settings, "symmetric_inputs", id_pair_vec_t() ); /* small -> large */
  auto verbose          = get( settings, "verbose",          false );

  /* Timer */
  properties_timer t( statistics );

  auto solver = make_solver<Solver>( settings );

  const auto& graph_info = boost::get_property( aig, boost::graph_name );

  /* Number of vertices */
  auto t_inputs  = graph_info.inputs.size();
  auto t_pattern = sim_vectors.size();
  auto t_outputs = graph_info.outputs.size();
  auto p_outputs = boost::num_vertices( pattern ) - p_pattern - p_inputs;

  assert( p_pattern <= t_pattern );
  assert( p_outputs <= t_outputs );

  /* Functions v and a */
  auto f1 = injective_function( solver, 1u, p_inputs, t_inputs );
  auto f2 = injective_function( solver, f1.back() + 1u, p_pattern, t_pattern );
  auto f3 = injective_function( solver, f2.back() + 1u, p_outputs, t_outputs );

  /* Inverters */
  std::vector<int> i_invs( t_inputs ), o_invs( t_outputs );

  /* Target adjacency matrices */
  std::vector<std::vector<int>> target( t_pattern );
  boost::generate( target, [&t_outputs]() { return std::vector<int>( t_outputs ); } );

  /* Circuits */
  int sid = f3.back() + 1u;
  {
    print_timer t( "[i] run-time for encoding slice: %w secs\n", verbose );

    sid = encode_slice( solver, aig, sim_vectors, sid, i_invs, o_invs, target, settings );
  }

  {
    print_timer t( "[i] run-time for encoding subgraph isomorphism: %w secs\n", verbose );

    std::vector<boost::dynamic_bitset<>> p_in( p_inputs, boost::dynamic_bitset<>( p_pattern ) );
    for ( unsigned j = 0u; j < p_inputs; ++j )
    {
      adjacency_row_vector( pattern, p_inputs, j, p_in[j] );
    }

    std::vector<boost::dynamic_bitset<>> p( p_pattern, boost::dynamic_bitset<>( p_outputs ) );
    for ( unsigned j = 0u; j < p_pattern; ++j )
    {
      adjacency_row_vector( pattern, p_inputs + p_pattern, p_inputs + j, p[j] );
    }

    unsigned a, b, c, d;
    for ( a = 0u; a < t_pattern; ++a )
    {
      for ( b = 0u; b < p_pattern; ++b )
      {
        for ( c = 0u; c < t_inputs; ++c )
        {
          for ( d = 0u; d < p_inputs; ++d )
          {
            if ( sim_vectors[a][c] != p_in[d][b] )
            {
              add_clause( solver )( {-f1[d * t_inputs + c], -f2[b * t_pattern + a]} );
            }
          }
        }

        for ( c = 0u; c < t_outputs; ++c )
        {
          for ( d = 0u; d < p_outputs; ++d )
          {
            add_clause( solver )( {p[b][d] ? target[a][c] : -target[a][c], -f2[b * t_pattern + a], -f3[d * t_outputs + c]} );
          }
        }
      }
    }

    /* Blocking */
    for ( const auto& b : blocking_vectors )
    {
      add_clause( solver )( {-f2[b.second * t_pattern + b.first]} );
    }

    for ( const auto& b : blocking_support )
    {
      add_clause( solver )( {-f3[b.second * t_outputs + b.first]} );
    }

    /* Symmetric inputs */
    /*for ( const auto& si : symmetric_inputs )
    {
      for ( unsigned i = 1u; i < t_inputs; ++i )
      {
        add_clause( solver )( {-f1[si.first * t_inputs + i], -f1[si.second * t_inputs + 0u]} );
        add_clause( solver )( {-f1[si.first * t_inputs + i], -f1[si.second * t_inputs + 1u]} );
      }
      }*/
  }

  /* Solve */
  solver_execution_statistics solver_stats;
  solver_result_t result;
  {
    print_timer t( "[i] run-time to solve instance: %w secs", verbose );

    result = solve( solver, solver_stats );
  }

  if ( statistics )
  {
    statistics->set( "solving_runtime", solver_stats.runtime );
    statistics->set( "num_vars", solver_stats.num_vars );
    statistics->set( "num_clauses", solver_stats.num_clauses );
  }

  if ( (bool)result )
  {
    if ( statistics )
    {
      //assert( result->second.all() );

      boost::dynamic_bitset<>::size_type pos;
      std::vector<unsigned> mapping;

      get_vertex_mapping( mapping, result->first, p_inputs, t_inputs, pos, 0u, 0u );
      get_vertex_mapping( mapping, result->first, p_pattern, t_pattern, pos, p_inputs * t_inputs, t_inputs );
      get_vertex_mapping( mapping, result->first, p_outputs, t_outputs, pos, p_inputs * t_inputs + p_pattern * t_pattern, t_inputs + t_pattern );
      statistics->set( "mapping", mapping );

      boost::dynamic_bitset<> input_inverters( t_inputs );
      for ( auto it : index( i_invs ) )
      {
        input_inverters[it.index] = result->first[it.value - 1u];
      }
      statistics->set( "input_inverters", input_inverters );

      boost::dynamic_bitset<> output_inverters( t_outputs );
      for ( auto it : index( o_invs ) )
      {
        output_inverters[it.index] = result->first[it.value - 1u];
      }
      statistics->set( "output_inverters", output_inverters );
    }

    return true;
  }
  else
  {
    return false;
  }
}

}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
