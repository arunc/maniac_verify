/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file kpartite_subgraph_isomorphism.hpp
 *
 * @brief Solves subgraph isomorphism for k-partite (chain) graphs using SAT
 *
 * @author Mathias Soeken
 * @since 2.0
 */

#ifndef KPARTITE_SUBGRAPH_ISOMORPHISM_HPP
#define KPARTITE_SUBGRAPH_ISOMORPHISM_HPP

#include <vector>

#include <boost/assign/std/vector.hpp>
#include <boost/dynamic_bitset.hpp>
#include <boost/format.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/irange.hpp>
#include <boost/range/iterator_range.hpp>

#include <core/properties.hpp>
#include <core/utils/graph_utils.hpp>
#include <core/utils/timer.hpp>
#include <formal/sat/sat_solver.hpp>
#include <formal/sat/operations/cardinality.hpp>

using namespace boost::assign;

namespace cirkit
{

template<class Solver>
std::vector<int> injective_function( Solver& solver, int sid, unsigned domain, unsigned codomain )
{
  using boost::adaptors::transformed;

  std::vector<int> f( domain * codomain );
  boost::copy( boost::irange( sid, (int)( sid + domain * codomain ) ), f.begin() );

  for ( unsigned j = 0u; j < domain; ++j )
  {
    one_hot( solver, boost::irange( f[j * codomain], f[j * codomain] + (int)codomain ) );
  }
  for ( unsigned i = 0u; i < codomain; ++i )
  {
    atmost_one( solver, boost::irange( 0u, domain ) | transformed( [&f, &i, &codomain]( unsigned j ) { return f[j * codomain + i]; } ) );
  }

  return f;
}

void get_vertex_mapping( std::vector<unsigned>& mapping, const boost::dynamic_bitset<>& assignment,
                         unsigned domain, unsigned codomain, boost::dynamic_bitset<>::size_type& pos,
                         unsigned assignment_offset = 0u, unsigned vertex_offset = 0u );

template<class Graph>
void adjacency_row_vector( const Graph& g, unsigned n, vertex_t<Graph> v, boost::dynamic_bitset<>& vector )
{
  for ( const auto& w : boost::make_iterator_range( boost::adjacent_vertices( v, g ) ) )
  {
    vector.set( w - n );
  }
}

template<class Solver, class Graph>
bool kpartite_subgraph_isomorphism( const Graph& target, const Graph& pattern,
                                    const std::vector<unsigned>& target_partition, const std::vector<unsigned>& pattern_partition,
                                    properties::ptr settings = properties::ptr(), properties::ptr statistics = properties::ptr() )
{
  using blocking_tuple_vec_t = std::vector<std::tuple<unsigned, unsigned, unsigned>>;
  using symmetries_map_t     = std::map<unsigned, unsigned>;

  /* Settings */
  auto blocking   = get( settings, "blocking",   blocking_tuple_vec_t() ); /* (stage, target id, pattern id) */
  auto symmetries = get( settings, "symmetries", symmetries_map_t() );
  auto verbose    = get( settings, "verbose",    false );

  /* Timer */
  properties_timer t( statistics );

  auto solver = make_solver<Solver>( settings );

  /* Number of partitions */
  unsigned k = target_partition.size();
  assert( k == pattern_partition.size() );

  /* Injective functions */
  std::vector<std::vector<int>> funcs;
  int sid = 1;
  for ( auto stage = 0u; stage < k; ++stage )
  {
    print_timer t( boost::str( boost::format( "[i] run-time for adding injectivity clauses at stage %d: %%w secs" ) % stage ), verbose );

    funcs += injective_function<Solver>( solver, sid, pattern_partition[stage], target_partition[stage] );
    sid += pattern_partition[stage] * target_partition[stage];
  }

  /* For optimization */
  auto t_in_degrees = precompute_in_degrees( target );
  auto p_in_degrees = precompute_in_degrees( pattern );

  /* Adjacencies */
  auto t_offset = 0u;
  auto p_offset = 0u;
  for ( unsigned stage = 0u; stage < k - 1u; ++stage )
  {
    print_timer t( boost::str( boost::format( "[i] run-time for adding isomorphism clauses at stage %d: %%w secs" ) % stage ), verbose );

    auto n = target_partition[stage];
    auto m = pattern_partition[stage];
    auto q = target_partition[stage + 1u];
    auto r = pattern_partition[stage + 1u];

    std::vector<boost::dynamic_bitset<>> g( n, boost::dynamic_bitset<>( q ) ), p( m, boost::dynamic_bitset<>( r ) );

    /* build adjacency matrix */
    for ( auto i = 0u; i < n; ++i )
    {
      /* NOTE: we assume a particular implementation of the graph here */
      adjacency_row_vector( target, t_offset + n, t_offset + i, g[i] );
    }
    for ( auto j = 0u; j < m; ++j )
    {
      adjacency_row_vector( pattern, p_offset + m, p_offset + j, p[j] );
    }

    /* matching clauses */
    for ( auto i = 0u; i < n; ++i )
    {
      for ( auto j = 0u; j < m; ++j )
      {
        for ( auto k = 0u; k < q; ++k )
        {
          for ( auto l = 0u; l < r; ++l )
          {
            if ( g[i][k] != p[j][l] )
            {
              add_clause( solver )( {-funcs[stage][j * n + i], -funcs[stage + 1u][l * q + k]} );
            }
          }
        }
      }
    }

    /* blocking clause based on vertex degree */
    for ( auto i = 0u; i < n; ++i )
    {
      for ( auto j = 0u; j < m; ++j )
      {
        if ( boost::out_degree( t_offset + i, target ) < boost::out_degree( p_offset + j, pattern ) ||
             t_in_degrees[t_offset + i] < p_in_degrees[p_offset + j] )
        {
          add_clause( solver )( {-funcs[stage][j * n + i]} );
        }
      }
    }

    t_offset += n;
    p_offset += m;
  }

  /* More blocking */
  for ( const auto& b : blocking )
  {
    const auto& stage = std::get<0>( b );
    const auto& tid   = std::get<1>( b );
    const auto& pid   = std::get<2>( b );

    add_clause( solver )( {-funcs[stage][pid * target_partition[stage] + tid]} );
  }

  /* Input symmetries */
  if ( !symmetries.empty() )
  {
    auto stage = 0u;
    print_timer t( boost::str( boost::format( "[i] run-time for adding symmetry clauses at stage %d: %%.2f secs" ) % stage ), verbose );

    for ( const auto& p : symmetries )
    {
      unsigned m = target_partition[stage];
      for ( auto j2 = 0u; j2 < m; ++j2 )
      {
        for ( auto j1 = j2 + 1u; j1 < m; ++j1 )
        {
          add_clause( solver )( {-funcs[stage][p.first * m + j1], -funcs[stage][p.second * m + j2]} );
        }
      }
    }
  }

  /* Solve */
  solver_execution_statistics solver_stats;
  auto result = solve( solver, solver_stats );

  if ( statistics )
  {
    statistics->set( "solving_runtime", solver_stats.runtime );
    statistics->set( "num_vars", solver_stats.num_vars );
    statistics->set( "num_clauses", solver_stats.num_clauses );
  }

  if ( (bool)result )
  {
    if ( statistics )
    {
      //assert( result->second.all() );

      boost::dynamic_bitset<>::size_type pos;
      std::vector<unsigned> mapping;
      unsigned assignment_offset = 0u;
      unsigned vertex_offset = 0u;
      for ( unsigned stage = 0u; stage < k; ++stage )
      {
        get_vertex_mapping( mapping, result->first, pattern_partition[stage], target_partition[stage], pos, assignment_offset, vertex_offset );
        assignment_offset += pattern_partition[stage] * target_partition[stage];
        vertex_offset += target_partition[stage];
      }
      statistics->set( "mapping", mapping );
    }

    return true;
  }
  else
  {
    return false;
  }
}

}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
