/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file ceg_based_component_matching.hpp
 *
 * @brief Finds a subcomponent in a circuit using a counterexample guided search
 *
 * @author Mathias Soeken
 * @since 2.0
 */

#ifndef CEG_BASED_COMPONENT_MATCHING_HPP
#define CEG_BASED_COMPONENT_MATCHING_HPP

#include <boost/assign/std/vector.hpp>
#include <boost/format.hpp>
#include <boost/optional.hpp>

#include <core/properties.hpp>
#include <core/utils/range_utils.hpp>
#include <classical/aig.hpp>
#include <formal/revenge/component_equivalence_check.hpp>
#include <formal/revenge/find_component_candidate.hpp>
#include <formal/verification/equivalence_check.hpp>

using namespace boost::assign;


namespace cirkit
{

/**
 * @param circ                        Target circuit
 * @param component                   Pattern circuit
 * @param simulation_pattern_selector Simulation pattern types
 * @param settings The following settings are possible
 *                 +----------+------------------------------+-----------------+
 *                 | Name     | Type                         | Default         |
 *                 +----------+------------------------------+-----------------+
 *                 | max_runs | unsigned                     | 20u             |
 *                 | verbose  | bool                         | false           |
 *                 +----------+------------------------------+-----------------+
 * @param statistics The following statistics are given
 *                 +------------------------------+-------------------------+----------------------------------------------------------------+
 *                 | Name                         | Type                    | Description                                                    |
 *                 +------------------------------+-------------------------+----------------------------------------------------------------+
 *                 +------------------------------+-------------------------+----------------------------------------------------------------+
 */
template<class Solver>
boost::optional<bool> ceg_based_component_matching( const aig_graph& circ, const aig_graph& component, unsigned simulation_pattern_selector,
                                                    properties::ptr settings = properties::ptr(), properties::ptr statistics = properties::ptr() )
  {
  /* Settings */
  solver_executable_function_t solver              = get( settings, "solver",   solver_executable_function_t( execute_picosat ) );
  unsigned                     max_runs            = get( settings, "max_runs", 20u );
  bool                         verbose             = get( settings, "verbose",  false );

  auto target_info = boost::get_property( circ, boost::graph_name );

  properties::ptr fc_settings( new properties );
  fc_settings->set( "solver", solver );
  fc_settings->set( "verbose", verbose );
  fc_settings->set( "block_vectors", true );
  fc_settings->set( "block_support", true );

  properties::ptr fc_statistics( new properties );

  bitset_pair_vec_t additional_vectors;
  unsigned run = 0u;

  while ( run++ < max_runs )
  {
    if ( verbose )
    {
      std::cout << "[i] run " << run << std::endl;
    }

    fc_settings->set( "additional_vectors", additional_vectors );
    std::vector<unsigned> input_mapping, output_mapping;
    bool result = find_component_candidate_with_inverters<Solver>( input_mapping, output_mapping, circ, component,
                                                                   simulation_pattern_selector, fc_settings, fc_statistics );
    if ( verbose )
    {
      std::cout << "[i] is pattern contained in target: " << ( result ? "yes" : "no" ) << std::endl;
      std::cout << boost::format( "[i] run-time: %.2f seconds" ) % fc_statistics->get<double>( "runtime" ) << std::endl;
    }

    if ( !result )
    {
      if ( verbose )
      {
        std::cout << "[i] component is not contained" << std::endl;
      }
      return false;
    }

    bool equal = component_equivalence_check<Solver>( circ, component, input_mapping, output_mapping, fc_settings, fc_statistics );
    if ( verbose )
    {
      std::cout << "[i] component is equivalent: " << ( equal ? "yes" : "no" ) << std::endl;
      std::cout << boost::format( "[i] run-time: %.2f seconds" ) % fc_statistics->get<double>( "runtime" ) << std::endl;
    }
    if ( !equal )
    {
      auto cex = fc_statistics->get<assignment_t>( "assignment" );
      boost::dynamic_bitset<> target_vector( target_info.inputs.size() );
      for ( auto it : index( input_mapping ) )
      {
        target_vector[it.value] = cex.first[it.index];
      }
      if ( verbose )
      {
        std::cout << "[i] add vector " << target_vector << "/" << cex.first << std::endl;
      }
      additional_vectors += std::make_pair( target_vector, cex.first );
    }
    else
    {
      if ( verbose )
      {
        std::cout << "[i] found component" << std::endl;
      }
      return true;
    }
  }

  return boost::optional<bool>();
}


}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
