/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "component_equivalence_check.hpp"

#include <vector>

#include <boost/assign/std/vector.hpp>

#include <core/utils/range_utils.hpp>

using namespace boost::assign;

namespace cirkit
{

namespace detail
{

mapping_func_t make_mapping( const std::vector<unsigned>& mapping )
{
  return [&mapping]( const aig_graph& circ, const aig_graph& component ) {
    mapping_t m;
    for ( auto it : index( mapping ) )
    {
      m += std::make_pair( it.value, it.index );
    }
    return m;
  };
}

}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
