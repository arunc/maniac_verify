/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file component_equivalence_check.hpp
 *
 * @brief Checks whether component candidate is equivalent
 *
 * @author Mathias Soeken
 * @since 2.0
 */

#ifndef COMPONENT_EQUIVALENCE_CHECK_HPP
#define COMPONENT_EQUIVALENCE_CHECK_HPP

#include <vector>

#include <boost/dynamic_bitset.hpp>

#include <core/properties.hpp>
#include <classical/aig.hpp>
#include <formal/sat/operations/logic.hpp>
#include <formal/verification/equivalence_check.hpp>

namespace cirkit
{

namespace detail
{

mapping_func_t make_mapping( const std::vector<unsigned>& mapping );

}

/**
 * @param component      Circuit may be smaller than circ
 * @param input_mapping  Maps of indices from component to circ,
 *                       i.e. input i in component maps
 *                       to input input_mapping[i] in circ.
 * @param output_mapping Analogously to input_mapping
 *
 * @param statistics     double                                                       runtime         : Run-time for checking
 *                       std::tuple<boost::dynamic_bitset<>, boost::dynamic_bitset<>> counter_example : First pattern is assignment, second pattern is don't care mask (0 -> don't care)
 */
template<class Solver>
bool component_equivalence_check( const aig_graph& circ, const aig_graph& component,
                                  const std::vector<unsigned>& input_mapping, const std::vector<unsigned>& output_mapping,
                                  properties::ptr settings = properties::ptr(), properties::ptr statistics = properties::ptr() )
{
  /* AIG info */
  const auto& target_info = boost::get_property( circ, boost::graph_name );

  /* Settings */
  boost::dynamic_bitset<> input_inverters = get( settings, "input_inverters", boost::dynamic_bitset<>( target_info.inputs.size() ) );
  boost::dynamic_bitset<> output_inverters = get( settings, "output_inverters", boost::dynamic_bitset<>( target_info.outputs.size() ) );

  /* Equivalence check */
  if ( !settings )
  {
    settings = std::make_shared<properties>();
  }

  settings->set( "input_mapping", detail::make_mapping( input_mapping ) );
  settings->set( "output_mapping", detail::make_mapping( output_mapping ) );

  combine_ports_func_t<Solver> combine_inputs = [&input_inverters]( Solver& cnf, const std::pair<unsigned, int>& pi1, const std::pair<unsigned, int>& pi2, int sid ) {
    if ( input_inverters[pi1.first] )
    {
      not_equals( cnf, pi1.second, pi2.second );
    }
    else
    {
      equals( cnf, pi1.second, pi2.second );
    }
    return sid;
  };

  combine_ports_func_t<Solver> combine_outputs = [&output_inverters]( Solver& cnf, const std::pair<unsigned, int>& po1, const std::pair<unsigned, int>& po2, int sid ) {
    if ( output_inverters[po1.first] )
    {
      logic_xnor( cnf, po1.second, po2.second, sid );
    }
    else
    {
      logic_xor( cnf, po1.second, po2.second, sid );
    }
    return sid + 1;
  };

  settings->set( "combine_inputs", combine_inputs );
  settings->set( "combine_outputs", combine_outputs );

  return equivalence_check<Solver>( circ, component, settings, statistics );
}

}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
