/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file find_component_candidate.hpp
 *
 * @brief Finds a subcomponent in a circuit
 *
 * @author Mathias Soeken
 * @since 2.0
 */

#ifndef FIND_COMPONENT_CANDIDATE_HPP
#define FIND_COMPONENT_CANDIDATE_HPP

#include <vector>

#include <boost/dynamic_bitset.hpp>

#include <core/properties.hpp>
#include <core/utils/timer.hpp>

#include <core/utils/range_utils.hpp>
#include <classical/aig.hpp>
#include <classical/functions/aig_support.hpp>
#include <classical/functions/simulation_graph.hpp>
#include <classical/utils/aig_utils.hpp>
#include <formal/graph/dynamic_tripartite_subgraph_isomorphism.hpp>
#include <formal/graph/kpartite_subgraph_isomorphism.hpp>
#include <formal/sat/utils/solver_executables.hpp>

namespace cirkit
{

namespace detail
{

void blocking_nodes_from_partitions( const std::vector<unsigned>& partition_target,
                                     const std::vector<unsigned>& partition_pattern,
                                     const std::function<void(unsigned, unsigned)>& f );

template<typename Map>
void extract_mapping( std::vector<unsigned>& input_mapping, std::vector<unsigned>& output_mapping, const Map& mapping,
                      const aig_graph_info& target_info, unsigned target_vectors,
                      const aig_graph_info& pattern_info, unsigned pattern_vectors,
                      bool verbose )
{
  for ( auto i = 0u; i < pattern_info.inputs.size(); ++i )
  {
    if ( verbose )
    {
      const auto& pattern_input = pattern_info.node_names.find( pattern_info.inputs[i] )->second;
      const auto& target_input = target_info.node_names.find( target_info.inputs[mapping[i]] )->second;
      std::cout << boost::format( "[i] input '%s' maps with '%s'" ) % pattern_input % target_input << std::endl;
    }
    input_mapping += mapping[i];
  }
  for ( auto i = 0u; i < pattern_info.outputs.size(); ++i )
  {
    if ( verbose )
    {
      const auto& pattern_output = pattern_info.outputs[i].second;
      const auto& target_output = target_info.outputs[mapping[pattern_info.inputs.size() + pattern_vectors + i] - target_info.inputs.size() - target_vectors].second;
      std::cout << boost::format( "[i] output '%s' maps with '%s'" ) % pattern_output % target_output << std::endl;
    }
    output_mapping += mapping[pattern_info.inputs.size() + pattern_vectors + i] - target_info.inputs.size() - target_vectors;
  }
}

}

using id_pair_vec_t     = std::vector<std::pair<unsigned, unsigned>>;
using bitset_pair_vec_t = std::vector<std::pair<boost::dynamic_bitset<>, boost::dynamic_bitset<>>>;

bool find_component_candidate_directed_lad( std::vector<unsigned>& input_mapping, std::vector<unsigned>& output_mapping,
                                            const aig_graph& circ, const aig_graph& component, const std::vector<unsigned>& types,
                                            properties::ptr settings = properties::ptr(), properties::ptr statistics = properties::ptr() );

bool find_component_candidate_directed_lad2( std::vector<unsigned>& input_mapping, std::vector<unsigned>& output_mapping,
                                            const aig_graph& circ, const aig_graph& component, const std::vector<unsigned>& types,
                                            properties::ptr settings = properties::ptr(), properties::ptr statistics = properties::ptr() );

bool find_component_candidate_directed_lad3( std::vector<unsigned>& input_mapping, std::vector<unsigned>& output_mapping,
                                             const aig_graph& circ, const aig_graph& component, const std::vector<unsigned>& types,
                                             properties::ptr settings = properties::ptr(), properties::ptr statistics = properties::ptr() );

bool find_component_candidate_directed_lad4( std::vector<unsigned>& input_mapping, std::vector<unsigned>& output_mapping,
                                             const aig_graph& circ, const aig_graph& component, const std::vector<unsigned>& types,
                                             properties::ptr settings = properties::ptr(), properties::ptr statistics = properties::ptr() );

template<class Solver>
bool find_component_candidate( std::vector<unsigned>& input_mapping, std::vector<unsigned>& output_mapping,
                               const aig_graph& circ, const aig_graph& component, const std::vector<unsigned>& types,
                               properties::ptr settings = properties::ptr(), properties::ptr statistics = properties::ptr() )
{
  /* Settings */
  auto solver              = get( settings, "solver",              solver_executable_function_t( execute_minisat ) );
  auto circuit_dotname     = get( settings, "circuit_dotname",     std::string() );
  auto circuit_graphname   = get( settings, "circuit_graphname",   std::string() );
  auto component_dotname   = get( settings, "component_dotname",   std::string() );
  auto component_graphname = get( settings, "component_graphname", std::string() );
  auto block_vectors       = get( settings, "block_vectors",       true );
  auto block_support       = get( settings, "block_support",       true );
  auto symmetries          = get( settings, "symmetries",          std::string() );
  auto verbose             = get( settings, "verbose",             false );

  /* AIG info */
  const auto& target_info = boost::get_property( circ, boost::graph_name );
  const auto& pattern_info = boost::get_property( component, boost::graph_name );

  /* Create simulation graphs */
  std::vector<unsigned> vec_partition_target, vec_partition_pattern;

  auto vec_target = create_simulation_vectors( target_info.inputs.size(), types, &vec_partition_target );
  properties::ptr cs_settings_target = std::make_shared<properties>();
  cs_settings_target->set( "dotname", circuit_dotname );
  cs_settings_target->set( "graphname", circuit_graphname );
  auto sim_target = create_simulation_graph( circ, vec_target, cs_settings_target );

  auto vec_pattern = create_simulation_vectors( pattern_info.inputs.size(), types, &vec_partition_pattern );
  properties::ptr cs_settings_pattern = std::make_shared<properties>();
  cs_settings_pattern->set( "dotname", component_dotname );
  cs_settings_pattern->set( "graphname", component_graphname );
  auto sim_pattern = create_simulation_graph( component, vec_pattern, cs_settings_pattern );

  /* Blocking with vectors */
  std::vector<std::tuple<unsigned, unsigned, unsigned>> blocking;
  if ( block_vectors )
  {
    print_timer t( "[i] run-time for computing blocking clauses for vectors: %w secs", verbose );

    detail::blocking_nodes_from_partitions( vec_partition_target, vec_partition_pattern,
                                            [&]( unsigned tid, unsigned pid ) { blocking += std::make_tuple( 1u, tid, pid ); } );
  }

  /* Block with support */
  if ( block_support )
  {
    print_timer t( "[i] run-time for computing blocking clauses for support: %w secs", verbose );

    auto target_support = aig_structural_support( circ );
    auto pattern_support = aig_structural_support( component );

    for ( auto it : index( target_info.outputs ) )
    {
      for ( auto ip : index( pattern_info.outputs ) )
      {
        if ( pattern_support[ip.value.first] != target_support[it.value.first] )
        {
          blocking += std::make_tuple( 2u, it.index, ip.index );
        }
      }
    }
  }

  /* Input symmetries */
  std::map<unsigned, unsigned> input_symmetries;
  if ( !symmetries.empty() )
  {
    std::ifstream is( symmetries.c_str(), std::ifstream::in );

    for ( std::string line; std::getline( is, line ); )
    {
      boost::trim( line );
      if ( line.empty() ) { continue; }
      auto p = split_string_pair( line, " " );
      auto i1 = aig_input_index( pattern_info, aig_node_by_name( pattern_info, p.first ) );
      auto i2 = aig_input_index( pattern_info, aig_node_by_name( pattern_info, p.second ) );
      std::cout << "[i] add symmetry " << i1 << " " << i2 << std::endl;
      input_symmetries.insert( { i1, i2 } );
    }
  }

  /* Match? */
  properties::ptr si_settings = std::make_shared<properties>();
  si_settings->set( "solver",     solver );
  si_settings->set( "verbose",    verbose );
  si_settings->set( "blocking",   blocking );
  si_settings->set( "symmetries", input_symmetries );
  properties::ptr si_statistics = std::make_shared<properties>();
  bool result = kpartite_subgraph_isomorphism<Solver>( sim_target, sim_pattern,
                                               std::vector<unsigned>({(unsigned)target_info.inputs.size(), (unsigned)vec_target.size(), (unsigned)target_info.outputs.size()}),
                                               std::vector<unsigned>({(unsigned)pattern_info.inputs.size(), (unsigned)vec_pattern.size(), (unsigned)pattern_info.outputs.size()}), si_settings, si_statistics );

  if ( result )
  {
    const auto& mapping = si_statistics->get<std::vector<unsigned>>( "mapping" );
    detail::extract_mapping( input_mapping, output_mapping, mapping,
                             target_info, vec_target.size(), pattern_info, vec_pattern.size(),
                             verbose );
  }

  if ( statistics )
  {
    statistics->set( "runtime", si_statistics->get<double>( "runtime" ) );
    statistics->set( "solving_runtime", si_statistics->get<double>( "solving_runtime" ) );
    statistics->set( "num_vars", si_statistics->get<unsigned>( "num_vars" ) );
    statistics->set( "num_clauses", si_statistics->get<unsigned>( "num_clauses" ) );
  }

  return result;
}


/**
 * @param input_mapping   Input mapping
 * @param output_mapping  Output mapping
 * @param circ            Target circuit
 * @param component       Pattern circuit
 * @param types           Simulation pattern types
 * @param settings The following settings are possible
 *                 +---------------------+------------------------------+------------------------+
 *                 | Name                | Type                         | Default                |
 *                 +---------------------+------------------------------+------------------------+
 *                 | solver              | solver_executable_function_t | execute_picosat        |
 *                 | component_dotname   | std::string                  | std::string()          |
 *                 | component_graphname | std::string                  | std::string()          |
 *                 | block_vectors       | bool                         | true                   |
 *                 | block_support       | bool                         | true                   |
 *                 | symmetric_inputs    | id_pair_vec_t                | id_pair_vec_t()        |
 *                 | verbose             | bool                         | false                  |
 *                 +---------------------+------------------------------+------------------------+
 * @param statistics The following statistics are given
 *                 +------------------------------+-------------------------+----------------------------------------------------------------+
 *                 | Name                         | Type                    | Description                                                    |
 *                 +------------------------------+-------------------------+----------------------------------------------------------------+
 *                 | input_inverters              | boost::dynamic_bitset<> | Mask of inverted inputs                                        |
 *                 | output_inverters             | boost::dynamic_bitset<> | Mask of inverted outputs                                       |
 *                 | runtime                      | double                  | Runtime of the SAT solver                                      |
 *                 | num_vars                     | unsigned                | Number of SAT variables                                        |
 *                 | num_clauses                  | unsigned                | Number of SAT clauses                                          |
 *                 | num_blocking_clauses_vectors | unsigned                | Number of blocking clauses based on simulation vectors         |
 *                 | num_blocking_clauses_support | unsigned                | Number of blocking clauses based on output support             |
 *                 | num_circuit_vectors          | unsigned                | Number of simulation vectors generated for the target circuit  |
 *                 | num_component_vectors        | unsigned                | Number of simulation vectors generated for the pattern circuit |
 *                 +------------------------------+-------------------------+----------------------------------------------------------------+
 */
template<class Solver>
bool find_component_candidate_with_inverters( std::vector<unsigned>& input_mapping, std::vector<unsigned>& output_mapping,
                                              const aig_graph& circ, const aig_graph& component, const std::vector<unsigned>& types,
                                              properties::ptr settings = properties::ptr(), properties::ptr statistics = properties::ptr() )
{
  /* Settings */
  auto solver              = get( settings, "solver",              solver_executable_function_t( execute_picosat ) );
  auto component_dotname   = get( settings, "component_dotname",   std::string() );
  auto component_graphname = get( settings, "component_graphname", std::string() );
  auto block_vectors       = get( settings, "block_vectors",       true );
  auto block_support       = get( settings, "block_support",       true );
  auto symmetric_inputs    = get( settings, "symmetric_inputs",    id_pair_vec_t() );
  auto additional_vectors  = get( settings, "additional_vectors",  bitset_pair_vec_t() );
  auto verbose             = get( settings, "verbose",             false );
  auto abc_cnf_command     = get( settings, "abc_cnf_command",     std::string( encode_slice_abc_constants::abc_cnf_command ) );

  /* AIG info */
  const auto& target_info = boost::get_property( circ, boost::graph_name );
  const auto& pattern_info = boost::get_property( component, boost::graph_name );

  /* Create simulation graphs */
  simulation_graph sim_pattern;
  std::vector<boost::dynamic_bitset<>> vec_target, vec_pattern;
  std::vector<unsigned> vec_partition_target, vec_partition_pattern;

  /* Simulation vectors */
  {
    print_timer t( "[i] run-time for creating simulation vectors: %w secs", verbose );

    vec_target = create_simulation_vectors( target_info.inputs.size(), types, &vec_partition_target );
    vec_pattern = create_simulation_vectors( pattern_info.inputs.size(), types, &vec_partition_pattern );
  }

  for ( const auto& p : additional_vectors )
  {
    assert( p.first.size() == target_info.inputs.size() );
    assert( p.second.size() == pattern_info.inputs.size() );
    vec_target += p.first;
    vec_pattern += p.second;
  }

  properties::ptr cs_settings_pattern = std::make_shared<properties>();
  cs_settings_pattern->set( "dotname", component_dotname );
  cs_settings_pattern->set( "graphname", component_graphname );
  {
    print_timer t( "[i] run-time for creating pattern simulation graph: %w secs", verbose );

    sim_pattern = create_simulation_graph( component, vec_pattern, cs_settings_pattern );
  }

  /* Blocking with vectors */
  std::vector<std::pair<unsigned, unsigned>> blocking_vectors;
  if ( block_vectors )
  {
    print_timer t( "[i] run-time for computing blocking clauses for vectors: %w secs", verbose );

    detail::blocking_nodes_from_partitions( vec_partition_target, vec_partition_pattern,
                                            [&]( unsigned tid, unsigned pid ) { blocking_vectors += std::make_pair( tid, pid ); } );
  }

  /* Block with support */
  std::vector<std::pair<unsigned, unsigned>> blocking_support;
  if ( block_support )
  {
    print_timer t( "[i] run-time for computing blocking clauses for support: %w secs", verbose );

    auto target_support = aig_structural_support( circ );
    auto pattern_support = aig_structural_support( component );

    for ( auto it : index( target_info.outputs ) )
    {
      for ( auto ip : index( pattern_info.outputs ) )
      {
        if ( pattern_support[ip.value.first] > target_support[it.value.first] )
        {
          blocking_support += std::make_pair( it.index, ip.index );
        }
      }
    }
  }

  /* TODO move somewhere else */
  //for ( unsigned i = 0u; i < pattern_info.inputs.size() / 2; ++i )
  //{
  //  symmetric_inputs += std::make_pair( i, pattern_info.inputs.size() / 2 + i );
  //}

  /* Match? */
  properties::ptr si_settings = std::make_shared<properties>();
  si_settings->set( "solver", solver );
  si_settings->set( "verbose", verbose );
  si_settings->set( "blocking_vectors", blocking_vectors );
  si_settings->set( "blocking_support", blocking_support );
  si_settings->set( "symmetric_inputs", symmetric_inputs );
  si_settings->set( "abc_cnf_command", abc_cnf_command );
  properties::ptr si_statistics = std::make_shared<properties>();
  bool result = dynamic_tripartite_subgraph_isomorphism<Solver>( circ, vec_target, sim_pattern, pattern_info.inputs.size(), vec_pattern.size(), si_settings, si_statistics );

  if ( result )
  {
    const auto& mapping = si_statistics->get<std::vector<unsigned>>( "mapping" );
    auto input_inverters = si_statistics->get<boost::dynamic_bitset<>>( "input_inverters" );
    auto output_inverters = si_statistics->get<boost::dynamic_bitset<>>( "output_inverters" );
    if ( verbose )
    {
      std::cout << "[i] mapping: " << any_join( mapping, " " ) << std::endl;
    }

    for ( unsigned i = 0u; i < pattern_info.inputs.size(); ++i )
    {
      if ( verbose )
      {
        const std::string& pattern_input = pattern_info.node_names.find( pattern_info.inputs[i] )->second;
        const std::string& target_input = target_info.node_names.find( target_info.inputs[mapping[i]] )->second;
        std::cout << boost::format( "[i] input '%s' maps with %s'%s'" ) % pattern_input % ( input_inverters[mapping[i]] ? "~" : "" ) % target_input << std::endl;
      }
      input_mapping += mapping[i];
    }

    for ( unsigned i = 0u; i < pattern_info.outputs.size(); ++i )
    {
      unsigned output_index = mapping[pattern_info.inputs.size() + vec_pattern.size() + i] - target_info.inputs.size() - vec_target.size();
      if ( verbose )
      {
        const std::string& pattern_output = pattern_info.outputs[i].second;
        const std::string& target_output = target_info.outputs[output_index].second;
        std::cout << boost::format( "[i] output '%s' maps with %s'%s'" ) % pattern_output % ( output_inverters[output_index] ? "~" : "" ) % target_output << std::endl;
      }
      output_mapping += output_index;
    }

    if ( statistics )
    {
      statistics->set( "input_inverters", input_inverters );
      statistics->set( "output_inverters", output_inverters );
    }
  }

  if ( statistics )
  {
    statistics->set( "runtime", si_statistics->get<double>( "runtime" ) );
    statistics->set( "solving_runtime", si_statistics->get<double>( "solving_runtime" ) );
    statistics->set( "num_vars", si_statistics->get<unsigned>( "num_vars" ) );
    statistics->set( "num_clauses", si_statistics->get<unsigned>( "num_clauses" ) );
    statistics->set( "num_blocking_clauses_vectors", (unsigned)blocking_vectors.size() );
    statistics->set( "num_blocking_clauses_support", (unsigned)blocking_support.size() );
    statistics->set( "num_circuit_vectors", (unsigned)vec_target.size() );
    statistics->set( "num_component_vectors", (unsigned)vec_pattern.size() );
  }

  return result;
}

}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
