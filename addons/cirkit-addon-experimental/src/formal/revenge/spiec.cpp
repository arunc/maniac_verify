/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "spiec.hpp"

#include <functional>
#include <queue>
#include <set>
#include <stack>
#include <unordered_set>

#include <boost/assign/std/vector.hpp>
#include <boost/bimap.hpp>
#include <boost/container/flat_set.hpp>
#include <boost/graph/depth_first_search.hpp>
#include <boost/graph/properties.hpp>
#include <boost/graph/strong_components.hpp>
#include <boost/range/distance.hpp>
#include <boost/range/iterator_range.hpp>

#include <abc/functions/abc_cec.hpp>

#include <core/utils/graph_utils.hpp>
#include <core/utils/index.hpp>
#include <core/utils/range_utils.hpp>
#include <core/utils/timer.hpp>

#include <classical/functions/aig_cone.hpp>
#include <classical/functions/simulation_graph.hpp>
#include <classical/functions/strash.hpp>
#include <classical/utils/aig_utils.hpp>

using namespace boost::assign;

template<typename T>
using flat_set = boost::container::flat_set<T>;

/******************************************************************************
 * BGL properties                                                             *
 ******************************************************************************/

namespace boost
{

enum edge_used_t { edge_used };
BOOST_INSTALL_PROPERTY(edge, used);

enum edge_pair_t { edge_pair };
BOOST_INSTALL_PROPERTY(edge, pair);

}

namespace cirkit
{

/******************************************************************************
 * Macros                                                                     *
 ******************************************************************************/

#define L(x) { if ( verbose ) { std::cout << x; } }
#define LN(x) { if ( verbose ) { std::cout << x << std::endl; } }
#define LSIZE(x) { if ( verbose) { std::cout << " \033[1;33m" << ( x ) << "\033[0m"; } }

#define LROW(x) print_timer _timer( " \033[0;32m%w secs\033[0m", verbose ); L( "[i] " << x << ":" );
#define LFAIL L( " \033[1;31mfailed\033[0m" )

#define LAPPROACH( x, code ) { LROW( x ); if ( !code ) { LFAIL; return false; } LSIZE( ds.size() ); }
#define LAPPROACHNM( x, code ) { LROW( x ); if ( !code ) { LFAIL; return run_result::nomatch; } LSIZE( ds.size() ); }

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

struct pattern_index_tag;
using pattern_index = base_index<pattern_index_tag>;

struct target_index_tag;
using target_index = base_index<target_index_tag>;

}

namespace std
{

template<>
struct hash<cirkit::target_index>
{
  std::size_t operator()( const cirkit::target_index& index ) const
  {
    return index.index();
  }
};

}

namespace cirkit
{

// {{{ mark_used_visitor

template<typename UsedMap>
class mark_used_visitor : public boost::default_dfs_visitor
{
public:
  mark_used_visitor( UsedMap map ) : map( map ) {}

  template<typename Edge, typename Graph>
  void examine_edge( Edge e, const Graph& g ) const
  {
    put( map, e, true );
  }

private:
  UsedMap map;
};

template<typename UsedMap>
mark_used_visitor<UsedMap> make_mark_used_visitor( UsedMap map )
{
  return mark_used_visitor<UsedMap>( map );
}

// }}}

template<typename IndexType>
class set_domain
{
public:
  using const_iterator = typename std::unordered_set<IndexType>::const_iterator;

  bool insert( IndexType index )
  {
    return !values.insert( index ).second;
  }

  bool remove( IndexType index )
  {
    return values.erase( index ) == 1;
  }

  bool has( IndexType index ) const
  {
    return values.find( index ) != values.end();
  }

  unsigned size() const
  {
    return values.size();
  }

  bool empty() const
  {
    return values.empty();
  }

  IndexType front() const
  {
    assert( !empty() );
    return *( values.begin() );
  }

  IndexType back() const
  {
    assert( !empty() );
    return *( values.rbegin() );
  }

  /* iterators */
  const_iterator begin() const { return values.begin(); }
  const_iterator end()   const { return values.end();   }

private:
  std::unordered_set<IndexType> values;
};

template<typename T>
std::ostream& operator<<( std::ostream& os, const set_domain<T>& set )
{
  os << "[";

  auto begin = true;
  for ( auto i : set )
  {
    if ( !begin ) { os << ", "; }
    else { begin = false; }
    os << i;
  }
  os << "]";
  return os;
}

// {{{ domains

class domains
{
public:
  using domain = index_backtracking_set<target_index>;
  //using domain = set_domain<target_index>;

  const domain& operator[]( pattern_index u ) const
  {
    return ds[u];
  }

  bool insert( pattern_index u, target_index v )
  {
    us.insert( u );

    const auto ret = ds[u].insert( v );
    if ( !ret ) { ++total; }
    return ret;
  }

  bool remove( pattern_index u, target_index v )
  {
    if ( ds[u].remove( v ) ) { --total; }

    return !ds[u].empty();
  }

  bool in( pattern_index u, target_index v )
  {
    return ds[u].has( v );
  }

  const index_set<pattern_index>& pattern_indices() const
  {
    return us;
  }

  const unsigned size() const
  {
    return total;
  }

private:
  index_map<pattern_index, domain> ds;
  index_set<pattern_index>         us;

  unsigned                         total = 0u;
};

std::ostream& operator<<( std::ostream& os, const domains& ds )
{
  for ( auto u : ds.pattern_indices() )
  {
    os << "D[" << u << "] = " << ds[u] << std::endl;
  }
  return os;
}

// }}}

// {{{ domains_matching

class domains_matching
{
public:
  target_index operator[]( pattern_index u ) const { return pair_u[u]; }
  pattern_index operator[]( target_index v ) const { return pair_v[v]; }

  friend std::ostream& operator<<( std::ostream& os, const domains_matching& match )
  {
    return os << "pair_u:" << std::endl
              << match.pair_u << std::endl
              << "pair_v:" << std::endl
              << match.pair_v << std::endl;
  }

protected:
  bool match( const domains& ds )
  {
    while ( bfs( ds ) )
    {
      for ( auto u : ds.pattern_indices() )
      {
        if ( !pair_u[u] )
        {
          if ( dfs( ds, u ) )
          {
            ++matching_size;
          }
        }
      }
    }

    return matching_size == ds.pattern_indices().size();
  }

  bool bfs( const domains& ds )
  {
    auto queue = find_unmatched_pattern_indices( ds );
    dist[pattern_index::null()] = inf;

    while ( !queue.empty() )
    {
      auto u = queue.front(); queue.pop();
      if ( dist[u] < dist[pattern_index::null()] )
      {
        for ( auto v : ds[u] )
        {
          if ( dist[pair_v[v]] == inf )
          {
            dist[pair_v[v]] = dist[u] + 1u;
            queue.push( pair_v[v] );
          }
        }
      }
    }

    return dist[pattern_index::null()] != inf;
  }

  bool dfs( const domains& ds, pattern_index u )
  {
    if ( !u ) { return true; }

    for ( auto v : ds[u] )
    {
      if ( dist[pair_v[v]] == dist[u] + 1 )
      {
        if ( dfs( ds, pair_v[v] ) )
        {
          pair_v[v] = u;
          pair_u[u] = v;
          return true;
        }
      }
    }

    dist[u] = inf;
    return false;
  }

  std::queue<pattern_index> find_unmatched_pattern_indices( const domains& ds )
  {
    std::queue<pattern_index> queue;

    for ( auto u : ds.pattern_indices() )
    {
      if ( pair_u.has( u ) )
      {
        dist[u] = inf;
      }
      else
      {
        dist[u] = 0u;
        queue.push( u );
      }
    }

    return queue;
  }

private:
  const unsigned inf = std::numeric_limits<unsigned>::max();

protected:
  index_map<pattern_index, target_index> pair_u;
  index_map<target_index, pattern_index> pair_v;
  index_map<pattern_index, unsigned>     dist;
  unsigned                               matching_size = 0u;
};

// }}}

// {{{ global_matching

class global_matching : public domains_matching
{
public:
  explicit global_matching( const domains& ds ) : ds( ds ) {}

  bool match()
  {
    return domains_matching::match( ds );
  }

  bool remove( pattern_index u, target_index v )
  {
    if ( pair_u[u] == v )
    {
      //std::cout << "UPDATE MATCHING" << std::endl;
      //assert( false );
      // TODO
    }

    return true;
  }

private:
  const domains& ds;
};

// }}}

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

class spiec_manager
{
public:
  enum class run_result { match, nomatch, poreduction };

public:
  spiec_manager( const aig_graph& pattern, const aig_graph& target,
                 const std::vector<unsigned>& types,
                 const boost::optional<unsigned>& signatures,
                 bool verbose )
    : pattern( pattern ),
      target( target ),
      pg( simulation_graph_wrapper( pattern, types, true, signatures ) ),
      tg( simulation_graph_wrapper( target, types, true, signatures ) ),
      signatures( signatures ),
      verbose( verbose ),
      match( ds )
  {
    pg.write_dot( "/tmp/pattern.dot" );
    tg.write_dot( "/tmp/target.dot" );

    LN( boost::format( "[i] pattern #PIs: %d, #POs: %d, #S: %d" ) % pg.num_inputs() % pg.num_outputs() % pg.num_vectors() );
    LN( boost::format( "[i] target #PIs: %d, #POs: %d, #S: %d" ) % tg.num_inputs() % tg.num_outputs() % tg.num_vectors() );

    has_symmetries = !aig_info( pattern ).input_symmetries.empty();
  }

  run_result run()
  {
    /* initial size (theoretically) */
    L( "[i] max domain size:" );
    LSIZE( pg.num_inputs() * tg.num_inputs() + pg.num_vectors() * tg.num_vectors() + pg.num_outputs() * tg.num_outputs() );
    LN( "" );

    /* fill domains */
    LAPPROACHNM( "fill domains", fill_domains() );

    auto result = filter();

    if ( result )
    {
      LN( "[i] after first filtering" );
      //if ( verbose ) { debug_domain_sizes(); }
      if ( verbose ) { debug_domains(); }
    }

    /* TODO: we have to catch the "match" somehow else, we may have output reduction and still matched. */
    if ( !result )
    {
      return run_result::nomatch;
    }
    else if ( get_mapped_outputs().size() < tg.num_outputs() )
    {
      return run_result::poreduction;
    }
    else
    {
      return run_result::match;
    }
  }

private: /* domain management */
  inline pattern_index   pn2i( const simulation_node& u ) const { return pattern_index::from_index( u + 1u ); }
  inline target_index    tn2i( const simulation_node& v ) const { return target_index::from_index( v + 1u );  }
  inline simulation_node pi2n( pattern_index u ) const { return u.index() - 1u; }
  inline simulation_node ti2n( target_index  v ) const { return v.index() - 1u; }

  bool are_compatible( const simulation_node& u, const simulation_node& v,
                       const std::vector<std::vector<unsigned>>& pnds,
                       const std::vector<std::vector<unsigned>>& tnds )
  {
    if ( pg.in_degree( u ) > tg.in_degree( v ) )              return false;
    if ( pg.out_degree( u ) > tg.out_degree( v ) )            return false;
    if ( pg.label( u ) != tg.label( v ) )                     return false;
    if ( pg.support( u ).count() > tg.support( v ).count() ) return false; /* structural */
    //if ( pg.support( u ).count() != tg.support( v ).count() ) return false; /* functional */

    /* neighboring degrees */
    auto itv = tnds[v].begin();
    for ( auto du : pnds[u] )
    {
      while ( itv != tnds[v].end() && du > *itv )
      {
        ++itv;
      }

      if ( itv == tnds[v].end() ) return false;
      ++itv;
    }

    if ( (bool)signatures )
    {
      if ( !compatible_simulation_signatures( pg, tg, u, v, *signatures ) )
      {
        return false;
      }
    }

    return true;
  }

  bool fill_domains()
  {
    /* neighbor degree sequences */
    std::vector<std::vector<unsigned>> pnds( pg.size() ), tnds( tg.size() );
    for ( const auto& u : pg.vertices() )
    {
      pg.fill_neighbor_degree_sequence_all( u, pnds[u] );
    }
    for ( const auto& v : tg.vertices() )
    {
      tg.fill_neighbor_degree_sequence_all( v, tnds[v] );
    }

    for ( const auto& u : pg.vertices() )
    {
      auto empty = true;

      for ( const auto& v : tg.vertices() )
      {
        if ( are_compatible( u, v, pnds, tnds ) )
        {
          ds.insert( pn2i( u ), tn2i( v ) );
          empty = false;
        }
      }

      if ( empty )
      {
        L( " no compatible target vertices for " << pg.name( u ) );
        return false;
      }
    }

    return true;
  }

  void assign( pattern_index u, target_index v )
  {
    std::vector<target_index> to_remove;

    for ( auto v2 : ds[u] )
    {
      if ( v != v2 )
      {
        to_remove.push_back( v2 );
      }
    }

    for ( auto v2 : to_remove )
    {
      remove_value( u, v2 );
    }
  }

  bool remove_value( pattern_index u, target_index v )
  {
    if ( !ds.remove( u, v ) ) { return false; }
    if ( !match.remove( u, v ) ) { return false; }

    if ( ds[u].size() == 1u )
    {
      to_be_matched.push( u );
    }

    return true;
  }

private: /* graph */
  bool has_edge( pattern_index u, pattern_index u2 )
  {
    return pg.edge_direction( pi2n( u ), pi2n( u2 ) ) > 0u;
  }

  bool has_edge( target_index v, target_index v2 )
  {
    return tg.edge_direction( ti2n( v ), ti2n( v2 ) ) > 0u;
  }

  bool compatible_edges( pattern_index u, target_index v, pattern_index u2, target_index v2 )
  {
    return ( pg.edge_direction( pi2n( u ), pi2n( u2 ) ) == tg.edge_direction( ti2n( v ), ti2n( v2 ) ) )
      && ( pg.edge_label( pi2n( u ), pi2n( u2 ) ) == tg.edge_label( ti2n( v ), ti2n( v2 ) ) );
  }

private: /* filtering */
  bool filter()
  {
    LAPPROACH( "matching", match.match() );
    LAPPROACH( "GAC(AllDiff)", gac_all_diff() );
    LAPPROACH( "match vertices", match_vertices() );
    LAPPROACH( "symmetry filtering", symmetry_filtering() );
    LAPPROACH( "match vertices", match_vertices() );

    if ( auto_symmetries )
    {
      LAPPROACH( "auto input symmetries", auto_input_symmetry_filtering() );
      LAPPROACH( "match vertices", match_vertices() );
      LAPPROACH( "auto output symmetries", auto_output_symmetry_filtering() );
      LAPPROACH( "match vertices", match_vertices() );
    }

    //LAPPROACH( "output pruning", output_pruning() );
    //LAPPROACH( "match vertices", match_vertices() );

    //LAPPROACH( "vector pruning", vector_pruning() );
    //LAPPROACH( "match vertices", match_vertices() );

    return true;
  }

  bool gac_all_diff()
  {
    /* create Go graph */
    using go_graph_t = digraph_t<boost::property<boost::vertex_color_t, boost::default_color_type>, boost::property<boost::edge_used_t, bool, boost::property<boost::edge_pair_t, std::pair<pattern_index, target_index>>>>;
    go_graph_t go;

    auto used = get( boost::edge_used, go );
    auto pair = get( boost::edge_pair, go );

    index_map<pattern_index, unsigned> u_vertices;
    index_map<target_index, unsigned>  v_vertices;
    index_set<target_index>            free;

    L( " create graph" );
    for ( auto u : ds.pattern_indices() )
    {
      /* skip simulation vertices */
      if ( gac_no_simvectors && pg.is_vector( pi2n( u ) ) ) { continue; }

      assert( !ds[u].empty() );

      u_vertices[u] = add_vertex( go );

      for ( auto v : ds[u] )
      {
        free.insert( v );

        if ( !v_vertices.has( v ) )
        {
          v_vertices[v] = add_vertex( go );
          assert( v_vertices[v] != 0u );
        }

        edge_t<go_graph_t> e;
        if ( match[u] == v )
        {
          e = add_edge( u_vertices[u], v_vertices[v], go ).first;
        }
        else
        {
          e = add_edge( v_vertices[v], u_vertices[u], go ).first;
        }
        used[e] = false;
        pair[e] = {u, v};
      }
    }

    for ( auto u : ds.pattern_indices() )
    {
      free.remove( match[u] );
    }

    L( " DFS" );

    /* mark vertices as used on paths starting from free vertices */
    for ( auto v : free )
    {
      depth_first_visit( go, v_vertices[v], make_mark_used_visitor( used ), get( boost::vertex_color, go ) );
    }

    L( " SCCs" );

    /* SCCs */
    std::map<vertex_t<go_graph_t>, unsigned> comp;
    strong_components( go, boost::make_assoc_property_map( comp ) );

    for ( const auto& e : boost::make_iterator_range( edges( go ) ) )
    {
      if ( comp[boost::source( e, go )] == comp[boost::target( e, go )] )
      {
        used[e] = true;
      }
    }

    L( " remove edges" );

    for ( const auto& e : boost::make_iterator_range( edges( go ) ) )
    {
      if ( !used[e] )
      {
        auto u = pair[e].first;
        auto v = pair[e].second;

        // NOTE: else u->v is vital
        if ( match[u] != v )
        {
          if ( !remove_value( u, v ) ) { return false; }
        }
      }
    }

    return true;
  }

  bool symmetry_filtering()
  {
    /* don't do anything, if there are no input symmetries */
    if ( !has_symmetries ) { return true; }

    for ( const auto& p : aig_info( pattern ).input_symmetries )
    {
      const auto u1 = pn2i( pg.port_to_node( p.first ) );
      const auto u2 = pn2i( pg.port_to_node( p.second ) );

      auto& d1 = ds[u1];
      auto& d2 = ds[u2];
      const auto min = *boost::min_element( d1 );
      const auto max = *boost::max_element( d2 );

      if ( !remove_value( u1, max ) ) { return false; }
      if ( !remove_value( u2, min ) ) { return false; }
    }

    return true;
  }

  using symmetry_candidates_t = std::map<unsigned, std::vector<std::vector<pattern_index>>>;

  symmetry_candidates_t compute_port_symmetry_candidates( const simulation_graph_wrapper::index_range_t& range ) const
  {
    using port_domains_t = std::vector<std::pair<pattern_index, flat_set<target_index>>>;
    port_domains_t port_ds;

    for ( const auto& x : range )
    {
      const auto u = pn2i( x );
      if ( ds[u].size() == 1u ) { continue; }
      port_ds.push_back( {u, flat_set<target_index>( ds[u].begin(), ds[u].end() )} );
    }

    boost::sort( port_ds, []( const port_domains_t::value_type& d1, const port_domains_t::value_type& d2 ) {
        //std::cout << ".";
        const auto& s1 = d1.second;
        const auto& s2 = d2.second;

        if ( s1.size() < s2.size() ) { return true; }
        if ( s1.size() == s2.size() )
        {
          for ( auto it1 = s1.begin(), it2 = s2.begin(); it1 != s1.end(); ++it1, ++it2 )
          {
            if ( *it1 < *it2 ) { return true; }
            if ( *it1 > *it2 ) { return false; }
          }

          return d1.first < d2.first;
        }
        return false;
      } );

    /* move to map */
    symmetry_candidates_t result;
    auto x = 0u;

    while ( x < port_ds.size() )
    {
      /* not interested in singletons */
      const auto& vs = port_ds[x].second;
      const auto size = vs.size();

      /* not enough elements left to get group */
      if ( x + size > port_ds.size() ) { ++x; continue; }

      auto failed = false;
      for ( auto y = x + 1u; y < x + size; ++y )
      {
        if ( port_ds[y].second != vs ) { failed = true; break; }
      }
      if ( failed ) { ++x; continue; }

      std::vector<pattern_index> us;
      for ( auto y = x; y < x + size; ++y )
      {
        us.push_back( port_ds[y].first );
      }

      result[size].push_back( us );

      x += size;
    }

    return result;
  }

  inline symmetry_candidates_t compute_input_symmetry_candidates() const
  {
    return compute_port_symmetry_candidates( pg.input_indexes() );
  }

  inline symmetry_candidates_t compute_output_symmetry_candidates() const
  {
    return compute_port_symmetry_candidates( pg.output_indexes() );
  }

  bool auto_input_symmetry_filtering()
  {
    const auto input_symmetries = compute_input_symmetry_candidates();

    /* check for symmetries */
    for ( const auto& p : input_symmetries )
    {
      //LN( "[i] check for input symmetries of size " << p.first );

      for ( const auto& us : p.second )
      {
        // if ( verbose )
        // {
        //   std::cout << "[i] check possible symmetries in:";
        //   for ( const auto& u : us ) { std::cout << " " << pg.name( pi2n( u ) ); }
        //   std::cout << std::endl;
        // }
        auto symmetric = true;
        for ( auto x = 0u; x < p.first - 1u; ++x )
        {
          const auto strash_settings = std::make_shared<properties>();
          strash_settings->set( "reorder", std::map<unsigned, unsigned>{ {pi2n( us[x] ), pi2n( us[x + 1u] )}, {pi2n( us[x + 1u] ), pi2n( us[x] )} } );
          const auto pattern_copy = strash( pattern, strash_settings );

          const auto result = abc_cec( pattern, pattern_copy );

          if ( result == boost::none )
          {
            //LN( "[i] pair " << pg.name( pi2n( us[x] ) ) << " and " << pg.name( pi2n( us[x + 1u] ) ) << " is symmetric" );
          }
          else
          {
            //LN( "[i] pair " << pg.name( pi2n( us[x] ) ) << " and " << pg.name( pi2n( us[x + 1u] ) ) << " is not symmetric" );
            symmetric = false;
            break;
          }
        }

        if ( symmetric )
        {
          std::vector<target_index> vs( ds[us[0u]].begin(), ds[us[0u]].end() );

          for ( auto x = 0u; x < p.first; ++ x )
          {
            assign( us[x], vs[x] );
          }
        }
      }
    }

    return true;
  }

  bool auto_output_symmetry_filtering()
  {
    const auto output_symmetries = compute_output_symmetry_candidates();

    /* check for symmetries */
    for ( const auto& p : output_symmetries )
    {
      for ( const auto& us : p.second )
      {
        if ( verbose )
        {
          //std::cout << "[i] check possible symmetries in:";
          //for ( const auto& u : us ) { std::cout << " " << pg.name( pi2n( u ) ); }
          //std::cout << std::endl;
        }
        auto symmetric = true;
        for ( auto x = 0u; x < p.first - 1u; ++x )
        {
          const auto o1 = aig_cone( pattern, std::vector<unsigned>{pg.output_index( pi2n( us[x] ) )} );
          const auto o2 = aig_cone( pattern, std::vector<unsigned>{pg.output_index( pi2n( us[x + 1u] ) )} );
          const auto result = abc_cec( o1, o2 );

          if ( result == boost::none )
          {
            //LN( "[i] pair " << pg.name( pi2n( us[x] ) ) << " and " << pg.name( pi2n( us[x + 1u] ) ) << " is symmetric" );
          }
          else
          {
            //LN( "[i] pair " << pg.name( pi2n( us[x] ) ) << " and " << pg.name( pi2n( us[x + 1u] ) ) << " is not symmetric" );
            symmetric = false;
            break;
          }
        }

        if ( symmetric )
        {
          std::vector<target_index> vs( ds[us[0u]].begin(), ds[us[0u]].end() );

          for ( auto x = 0u; x < p.first; ++ x )
          {
            assign( us[x], vs[x] );
          }
        }
      }
    }

    return true;
  }

  bool output_pruning()
  {
    /* create union of support */
    boost::dynamic_bitset<> support( tg.num_inputs() );
    for ( auto v : get_mapped_outputs() )
    {
      support |= tg.support( ti2n( v ) );
    }

    /* remove inputs that are not in the support */
    for ( const auto& u : pg.input_indexes() )
    {
      for ( auto v : ds[pn2i( u )] )
      {
        if ( !support[ti2n( v )] )
        {
          if ( !remove_value( pn2i( u ), v ) ) { return false; }
        }
      }
    }

    return true;
  }

  bool vector_pruning()
  {
    const auto mapped_pis = get_mapped_inputs();

    for ( auto u : pg.vector_indexes() )
    {
      for ( auto v : ds[pn2i( u )] )
      {
        const auto& simvector = tg.simvector( ti2n( v ) );

        if ( !simvector.none() ) { continue; } // quick hack

        auto pos = simvector.find_first();
        while ( pos != boost::dynamic_bitset<>::npos )
        {
          if ( !mapped_pis.has( tn2i( pos ) ) )
          {
            if ( !remove_value( pn2i( u ), v ) ) { return false; }
            break;
          }

          pos = simvector.find_next( pos );
        }
      }
    }

    return true;
  }

  bool match_vertices()
  {
    while ( !to_be_matched.empty() )
    {
      const auto u = to_be_matched.top();
      to_be_matched.pop();

      assert( ds[u].size() == 1u );
      const auto v = ds[u].front();

      for ( auto u2 : ds.pattern_indices() )
      {
        if ( u == u2 ) { continue; }

        /* propagate FC(diff) */
        if ( ds[u2].has( v ) )
        {
          if ( !remove_value( u2, v ) ) { return false; }
        }

        /* propagate FC(edges) */
        if ( has_edge( u, u2 ) )
        {
          index_set<target_index> to_be_removed;
          for ( auto v2 : ds[u2] )
          {
            if ( !compatible_edges( u, v, u2, v2 ) )
            {
              to_be_removed.insert( v2 );
            }
          }
          for ( auto v2 : to_be_removed )
          {
            if ( !remove_value( u2, v2 ) ) { return false; }
          }
        }
        else
        {
          index_set<target_index> to_be_removed;
          for ( auto v2 : ds[u2] )
          {
            if ( has_edge( v, v2 ) )
            {
              to_be_removed.insert( v2 );
            }
          }
          for ( auto v2 : to_be_removed )
          {
            if ( !remove_value( u2, v2 ) ) { return false; }
          }
        }
      }
    }

    return true;
  }

private: /* utility functions for mapping */
  index_set<target_index> get_mapped_inputs() const
  {
    return get_mapped( pg.input_indexes() );
  }

  index_set<target_index> get_mapped_vectors() const
  {
    return get_mapped( pg.vector_indexes() );
  }

  index_set<target_index> get_mapped_outputs() const
  {
    return get_mapped( pg.output_indexes() );
  }

  index_set<target_index> get_mapped( const simulation_graph_wrapper::index_range_t& range ) const
  {
    index_set<target_index> mapped;
    for ( const auto& u : range )
    {
      for ( auto v : ds[pn2i( u )] )
      {
        mapped.insert( v );
      }
    }
    return mapped;
  }

public: /* target manipulation */
  aig_graph reduce_target()
  {
    std::vector<unsigned> indexes;

    for ( auto v : get_mapped_outputs() )
    {
      indexes.push_back( tg.output_index( ti2n( v ) ) );
    }

    return aig_cone( target, indexes );
  }

private: /* debug */
  void debug_domains()
  {
    std::stringstream hist;
    hist << "[i] histogram (inputs):";

    for ( auto i = 0u; i < pg.size(); )
    {
      std::cout << "D[" << pg.name( i ) << "] =";

      for ( auto v : ds[pn2i( i )] )
      {
        std::cout << " " << tg.name( ti2n( v ) );
      }

      std::cout << std::endl;

      hist << " " << ds[pn2i( i )].size();

      /* advance i (skip sim vectors) */
      if ( ++i == pg.num_inputs() )
      {
        i = pg.num_inputs() + pg.num_vectors();
        hist << std::endl << "[i] histogram (outputs):";
      }
    }

    std::cout << hist.str() << std::endl;
  }

  void debug_domain_sizes()
  {
    for ( auto i = 0u; i < pg.size(); ++i )
    {
      std::cout << "D[" << pg.name( i ) << "] = " << ds[pn2i( i )].size() << std::endl;
    }
  }

private:
  const aig_graph&          pattern;
  const aig_graph&          target;
  simulation_graph_wrapper  pg;
  simulation_graph_wrapper  tg;
  bool                      verbose;

  domains                   ds;
  global_matching           match;
  std::stack<pattern_index> to_be_matched;

  bool                      has_symmetries;

public: /* settings */
  boost::optional<unsigned> signatures;
  bool                      gac_no_simvectors;
  bool                      auto_symmetries;
};

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

bool spiec_with_lad( const aig_graph& pattern, const aig_graph& target,
                     const properties::ptr& settings,
                     const properties::ptr& statistics )
{
  const auto types              = get( settings, "types",             std::vector<unsigned>{ 0u, 3u, 5u } ); // 0c 1h 2h
  const auto signatures         = get( settings, "signatures",        boost::optional<unsigned>() );
  const auto gac_no_simvectors  = get( settings, "gac_no_simvectors", false );
  const auto auto_symmetries    = get( settings, "auto_symmetries",   false );
  const auto verbose            = get( settings, "verbose",           false );

  /* some information */
  LN( "[i] types: " << any_join( types, " " ) );

  if ( verbose && (bool)signatures )
  {
    std::cout << "[i] signatures: " << *signatures << std::endl;
  }

  /* copy target */
  auto target_copy = target;

  /* run algorithm */
  while ( true )
  {
    spiec_manager mgr( pattern, target_copy, types, signatures, verbose );
    mgr.gac_no_simvectors = gac_no_simvectors;
    mgr.auto_symmetries   = auto_symmetries;

    auto result = mgr.run();

    if ( result == spiec_manager::run_result::poreduction )
    {
      target_copy = mgr.reduce_target();
    }
    else
    {
      break;
    }
  }

  return true;
}


}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
