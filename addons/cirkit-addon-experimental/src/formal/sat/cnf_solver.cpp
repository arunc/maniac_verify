/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cnf_solver.hpp"

#include <cstdlib>
#include <fstream>

#include <boost/assign/std/vector.hpp>
#include <boost/range/algorithm.hpp>

using namespace boost::assign;

namespace cirkit
{

cnf_solver::cnf_solver( const solver_executable_function_t& solver_func )
  : solver_func( solver_func ) {}

void cnf_solver::push_clause( const std::vector<int>& clause )
{
  assert( boost::find( clause, 0 ) == clause.end() );

  clauses += clause;

  for ( int v : clause )
  {
    if ( abs( v ) > num_variables ) num_variables = abs( v );
  }
}

void cnf_solver::write_dimacs( std::ostream& os )
{
  os << "p cnf " << num_variables << " " << clauses.size() << std::endl;

  for ( const auto& c : clauses )
  {
    for ( const auto& l : c )
    {
      os << l << " ";
    }
    os << 0 << std::endl;
  }
}

solver_result_t cnf_solver::solve( solver_execution_statistics& statistics )
{
  std::ofstream os( "/tmp/test.cnf", std::ofstream::out );
  write_dimacs( os );
  os.close();

  return solver_func( "/tmp/test.cnf", statistics );
}

template<>
cnf_solver make_solver<cnf_solver>( properties::ptr settings )
{
  auto solver = get( settings, "solver", solver_executable_function_t( execute_minisat ) );
  return cnf_solver( solver );
}

template<>
solver_traits<cnf_solver>::clause_adder add_clause( cnf_solver& solver )
{
  return solver_traits<cnf_solver>::clause_adder( solver );
}

template<>
solver_result_t solve<cnf_solver>( cnf_solver& solver, solver_execution_statistics& statistics, const std::vector<int>& assumptions )
{
  if ( assumptions.size() > 0u )
  {
    std::cerr << "[e] cnf_solver does not support assumptions" << '\n';
    assert( false && "cnf_solver does not support assumptions" );
  }
  return solver.solve( statistics ); // TODO assumptions
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
