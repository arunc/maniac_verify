/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file cnf_solver.hpp
 *
 * @brief Solver based on executables
 *
 * @author Mathias Soeken
 * @since 2.0
 */

#ifndef CNF_SOLVER
#define CNF_SOLVER

#include <vector>

#include <core/properties.hpp>
#include <classical/aig.hpp>
#include <formal/sat/sat_solver.hpp>
#include <formal/sat/utils/solver_executables.hpp>

namespace cirkit
{

class cnf_solver
{
public:
  cnf_solver( const solver_executable_function_t& solver_func );

  void push_clause( const std::vector<int>& clause );

  void write_dimacs( std::ostream& os );
  solver_result_t solve( solver_execution_statistics& statistics );

private:
  solver_executable_function_t solver_func;
  std::vector<std::vector<int>> clauses;
  unsigned num_variables = 0u;
};

struct cnf_clause_adder
{
  explicit cnf_clause_adder( cnf_solver& solver ) : solver( solver ) {}

  template<typename C>
  bool add( const C& clause )
  {
    clause_t c( boost::distance( clause ) );
    boost::copy( clause, c.begin() );
    solver.push_clause( c );
    return true;
  }

private:
  cnf_solver& solver;
};

template<>
class solver_traits<cnf_solver>
{
public:
  using clause_adder = base_clause_adder<cnf_clause_adder>;
};

template<>
cnf_solver make_solver<cnf_solver>( properties::ptr settings );

template<>
solver_traits<cnf_solver>::clause_adder add_clause( cnf_solver& solver );

template<>
solver_result_t solve<cnf_solver>( cnf_solver& solver, solver_execution_statistics& statistics, const std::vector<int>& assumptions );

}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
