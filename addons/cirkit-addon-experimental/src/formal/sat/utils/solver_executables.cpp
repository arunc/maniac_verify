/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "solver_executables.hpp"

#include <cmath>
#include <cstdlib>
#include <fstream>
#include <functional>
#include <iostream>
#include <regex>

#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <boost/config/warning_disable.hpp>
#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/range/algorithm_ext/push_back.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/regex.hpp>

#include <core/utils/system_utils.hpp>
#include <core/utils/string_utils.hpp>

namespace cirkit
{

solver_result_t execute_minisat( const std::string& dimacs, solver_execution_statistics& statistics )
{
  auto sresult = system( boost::str( boost::format( "(minisat %s /tmp/result; echo -n) > /tmp/log" ) % dimacs ).c_str() );

  /* Read log */
  line_parser( "/tmp/log", {
      { boost::regex( "Number of variables: *(\\d+) " ), [&statistics]( const boost::smatch& m ) { statistics.num_vars = boost::lexical_cast<unsigned>( m[1u] ); } },
      { boost::regex( "Number of clauses: *(\\d+) " ), [&statistics]( const boost::smatch& m ) { statistics.num_clauses = boost::lexical_cast<unsigned>( m[1u] ); } },
      { boost::regex( "Parse time: *(\\d+(\\.\\d+)?) " ), [&statistics]( const boost::smatch& m ) { statistics.parse_time = boost::lexical_cast<double>( m[1u] ); } },
      { boost::regex( "CPU time *: *(\\d+(\\.\\d+)?) " ), [&statistics]( const boost::smatch& m ) { statistics.runtime = boost::lexical_cast<double>( m[1u] ); } }
    });

  /* Read result */
  std::ifstream is( "/tmp/result", std::ifstream::in );
  std::string line;

  getline( is, line );

  if ( line == "SAT" )
  {
    std::vector<int> assignments;
    boost::dynamic_bitset<> values( statistics.num_vars ), mask( statistics.num_vars );

    getline( is, line );
    parse_string_list( assignments, line, " " );

    for ( int l : boost::make_iterator_range( assignments.begin(), assignments.end() - 1 ) )
    {
      unsigned pos = abs( l ) - 1u;
      values[pos] = l > 0;
      mask.set( pos );
    }

    return solver_result_t( {values, mask} );
  }
  else
  {
    assert( line == "UNSAT" );
    is.close();
    return solver_result_t();
  }
}

solver_result_t execute_picosat( const std::string& dimacs, solver_execution_statistics& statistics )
{
  auto sresult = system( boost::str( boost::format( "(picosat -v %s; echo -n) > /tmp/result" ) % dimacs ).c_str() );

  /* Read result (with log) */
  std::string result, assignment;

  line_parser( "/tmp/result", {
      { boost::regex( "c parsed header 'p cnf (\\d+) (\\d+)'" ), [&statistics]( const boost::smatch& m )
        {
          statistics.num_vars = boost::lexical_cast<unsigned>(m [1u] );
          statistics.num_clauses = boost::lexical_cast<unsigned>(m [2u] );
        }
      },
      { boost::regex( "c *(\\d+(\\.\\d+)?) seconds total run time" ), [&statistics]( const boost::smatch& m ) { statistics.runtime = boost::lexical_cast<double>( m[1u] ); } },
      { boost::regex( "^s (\\w+)$" ), [&result]( const boost::smatch& m ) { result = m[1u]; } },
      { boost::regex( "^v (.*)$" ), [&assignment]( const boost::smatch& m ) { assignment += " "; assignment += m[1u]; } }
    } );

  if ( result == "SATISFIABLE" )
  {
    std::vector<int> assignments;
    boost::dynamic_bitset<> values( statistics.num_vars ), mask( statistics.num_vars );

    parse_string_list( assignments, assignment, " " );

    for ( int l : boost::make_iterator_range( assignments.begin(), assignments.end() - 1 ) )
    {
      unsigned pos = abs( l ) - 1u;
      values[pos] = l > 0;
      mask.set( pos );
    }

    return solver_result_t( {values, mask} );
  }
  else
  {
    assert( result == "UNSATISFIABLE" );
    return solver_result_t();
  }
}

solver_result_t execute_sat13( const std::string& dimacs, solver_execution_statistics& statistics )
{
  auto sresult = system( boost::str( boost::format( "(cat %s | dimacs-to-sat > /tmp/test.sat ; echo -n) > /dev/null 2>&1" ) % dimacs ).c_str() );
  unsigned buffer_length = boost::lexical_cast<unsigned>( execute_and_return( "wc -L /tmp/test.sat | awk '{print $1}'" ).second[0] ) + 2u;
  sresult = system( boost::str( boost::format( "(cat /tmp/test.sat | sat13 b%d m30 h13; echo -n) > /tmp/result 2> /tmp/log" ) % buffer_length ).c_str() );

  /* Read log */
  std::string result;
  line_parser( "/tmp/log", {
      { boost::regex( "\\((\\d+) variables?, (\\d+) clauses?" ), [&statistics]( const boost::smatch& m )
        {
          statistics.num_vars = boost::lexical_cast<unsigned>( m[1u] );
          statistics.num_clauses = boost::lexical_cast<unsigned>( m[2u] );
        }
      },
      { boost::regex( "^!SAT!$" ), [&result]( const boost::smatch& m ) { result = "SAT"; } },
      { boost::regex( "UNSAT" ), [&result]( const boost::smatch& m ) { result = "UNSAT"; } }
    });

  solver_result_t res;

  std::ifstream in( "/tmp/result", std::ifstream::in );
  std::string line;

  if ( result == "SAT" )
  {
    using boost::adaptors::transformed;


    getline( in, line );
    boost::trim( line );

    std::vector<std::string> str_list;
    std::vector<int> assignments;
    boost::dynamic_bitset<> values( statistics.num_vars ), mask( statistics.num_vars );

    boost::split( str_list, line, boost::is_any_of( " " ), boost::algorithm::token_compress_on );
    boost::push_back( assignments, str_list | transformed( []( const std::string& s ) { return s[0] == '~' ? -boost::lexical_cast<int>( s.substr( 1u ) ) : boost::lexical_cast<int>( s ); } ) );

    for ( int l : assignments )
    {
      unsigned pos = abs( l ) - 1u;
      values[pos] = l > 0;
      mask.set( pos );
    }

    res = {values, mask};
  }
  else
  {
    getline( in, line );
    assert( line == "~" );
    assert( result == "UNSAT" );
  }

  getline( in, line );
  std::smatch m;
  assert( std::regex_match( line, m, std::regex( "Elapsed time: (\\d+(\\.\\d+)?)." ) ) );
  statistics.runtime = boost::lexical_cast<double>( m[1u] );

  return res;
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
