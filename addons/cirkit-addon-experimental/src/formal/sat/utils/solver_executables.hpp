/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file solver_executables.hpp
 *
 * @brief Interface for solver execution
 *
 * @author Mathias Soeken
 * @since 2.0
 */

#ifndef SOLVER_EXECUTABLES_HPP
#define SOLVER_EXECUTABLES_HPP

#include <functional>
#include <string>

#include <boost/dynamic_bitset.hpp>
#include <boost/optional.hpp>

#include <formal/sat/sat_solver.hpp>

namespace cirkit
{

using solver_executable_function_t = std::function<solver_result_t(const std::string&, solver_execution_statistics&)>;

solver_result_t execute_minisat( const std::string& dimacs, solver_execution_statistics& statistics );
solver_result_t execute_picosat( const std::string& dimacs, solver_execution_statistics& statistics );
solver_result_t execute_sat13( const std::string& dimacs, solver_execution_statistics& statistics );

}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
