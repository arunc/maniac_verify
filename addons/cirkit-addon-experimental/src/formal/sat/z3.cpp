/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "z3.hpp"

#ifdef ADDON_FORMAL

#include <cmath>

#include <boost/assign/std/vector.hpp>
#include <boost/format.hpp>

#include <core/utils/range_utils.hpp>
#include <formal/utils/z3_utils.hpp>

using namespace boost::assign;

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

template<>
z3_solver make_solver<z3_solver>( properties::ptr settings )
{
  std::unique_ptr<z3::context> ctx( new z3::context );
  std::unique_ptr<z3::solver> solver( new z3::solver( *ctx ) );
  return std::make_tuple( std::move( ctx ), std::move( solver ), std::vector<z3::expr>() );
}

template<>
solver_traits<z3_solver>::clause_adder add_clause( z3_solver& solver )
{
  return solver_traits<z3_solver>::clause_adder( solver );
}

template<>
solver_result_t solve<z3_solver>( z3_solver& solver, solver_execution_statistics& statistics, const std::vector<int>& assumptions ) // TODO assumptions
{
  auto& s = std::get<1>( solver );

  if ( s->check() == z3::sat )
  {
    const auto& vars = std::get<2>( solver );

    boost::dynamic_bitset<> bits( vars.size() );
    boost::dynamic_bitset<> care( vars.size() );

    auto m = s->get_model();

    for ( auto v : index( vars ) )
    {
      bits[v.index] = expr_to_bool( m.eval( v.value ) );
      care.set( v.index );
    }

    return solver_result_t( {bits, care} );
  }
  else
  {
    return solver_result_t();
  }
}

}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
