/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file z3.hpp
 *
 * @brief Generic SAT solver implementation based on Z3
 *
 * @author Mathias Soeken
 * @since  2.2
 */

#ifndef Z3_SOLVER_HPP
#define Z3_SOLVER_HPP

#ifdef ADDON_FORMAL

#include <vector>

#include <boost/format.hpp>

#include <z3++.h>

#include <formal/sat/sat_solver.hpp>

namespace cirkit
{

using z3_solver = std::tuple<std::unique_ptr<z3::context>, std::unique_ptr<z3::solver>, std::vector<z3::expr>>;

struct z3_clause_adder
{
  explicit z3_clause_adder( z3_solver& solver ) : solver( solver ) {}

  template<typename C>
  bool add( const C& clause )
  {
    using boost::format;
    using boost::str;

    auto& ctx  = std::get<0>( solver );
    auto& s    = std::get<1>( solver );
    auto& vars = std::get<2>( solver );

    int var;
    z3::expr c = ctx->bool_val( false );

    for ( auto lit : clause )
    {
      var = abs( lit ) - 1;
      while ( var >= vars.size() )
      {
        vars += ctx->bool_const( str( format( "v%d" ) % vars.size() ).c_str() );
      }
      c = c || ( ( lit > 0 ) ? vars[var] : !vars[var] );
    }

    s->add( c );

    return true;
  }

private:
  z3_solver& solver;
};

template<>
class solver_traits<z3_solver>
{
public:
  using clause_adder = base_clause_adder<z3_clause_adder>;
};

template<>
z3_solver make_solver<z3_solver>( properties::ptr settings );

template<>
solver_traits<z3_solver>::clause_adder add_clause( z3_solver& solver );

template<>
solver_result_t solve<z3_solver>( z3_solver& solver, solver_execution_statistics& statistics, const std::vector<int>& assumptions );

}

#endif

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
