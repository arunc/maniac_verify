/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "exact_mig.hpp"

#include <cmath>
#include <vector>

#include <boost/assign/std/vector.hpp>
#include <boost/format.hpp>

#include <core/utils/timer.hpp>
#include <formal/utils/z3_utils.hpp>

#include <z3++.h>

using namespace boost::assign;

using boost::format;
using boost::str;

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

struct exact_mig_instance
{
  exact_mig_instance( unsigned num_vars ) :
    num_vars( num_vars ),
    solver( ctx ),
    out( 1u << num_vars ),
    in( 3u, std::vector<std::vector<z3::expr>>( 1u << num_vars ) ),
    sel( 3u ),
    neg( 3u ),
    out_neg( ctx.bool_const( "out_neg" ) )
  {}

  void add_level()
  {
    const auto bw = (unsigned)ceil( log( 2u + num_vars + 7u ) / log( 2u ) );

    unsigned level = sel[0u].size();

    for ( auto x = 0u; x < 3u; ++x )
    {
      sel[x] += ctx.bv_const( str( format( "sel%d_%d" ) % x % level ).c_str(), bw );
      neg[x] += ctx.bool_const( str( format( "neg%d_%d" ) % x % level ).c_str() );

      /* assertion for sel[level] <= n + k */
      solver.add( z3::ule( sel[x][level], ctx.bv_val( num_vars + level, bw ) ) );
    }

    solver.add( sel[0][level] < sel[1][level] );
    solver.add( sel[1][level] < sel[2][level] );

    for ( auto i = 0u; i < level; ++i )
    {
      solver.add( sel[0][level] != sel[0][i] || sel[1][level] != sel[1][i] || sel[2][level] != sel[2][i] ||
                  neg[0][level] != neg[0][i] || neg[1][level] != neg[1][i] || neg[2][level] != neg[2][i] );
    }

    for ( auto j = 0u; j < ( 1u << num_vars ); ++j )
    {
      out[j] += ctx.bool_const( str( format( "out_%d_%d" ) % j % level ).c_str() );
      in[0u][j] += ctx.bool_const( str( format( "in1_%d_%d" ) % j % level ).c_str() );
      in[1u][j] += ctx.bool_const( str( format( "in2_%d_%d" ) % j % level ).c_str() );
      in[2u][j] += ctx.bool_const( str( format( "in3_%d_%d" ) % j % level ).c_str() );

      /* assertion for out[j][level] = M(in1[j][level],in2[j][level],in3[j][level] */
      solver.add( out[j][level] == ( ( in[0u][j][level] && in[1u][j][level] ) || ( in[0u][j][level] && in[2u][j][level] ) || ( in[1u][j][level] && in[2u][j][level] ) ) );

      /* assertions for in[x][j][level] = neg[level] ^ ite( sel[level], ... ) */
      boost::dynamic_bitset<> val( num_vars, j );
      for ( auto x = 0u; x < 3u; ++x )
      {
        solver.add( implies( sel[x][level] == ctx.bv_val( 0u, bw ),
                             in[x][j][level] == ( neg[x][level] != ctx.bool_val( false ) ) ) );

        for ( auto l = 0u; l < num_vars; ++l )
        {
          solver.add( implies( sel[x][level] == ctx.bv_val( l + 1u, bw ),
                               in[x][j][level] == ( neg[x][level] != ctx.bool_val( val[l] ) ) ) );
        }
        for ( auto l = 0u; l < level; ++l )
        {
          solver.add( implies( sel[x][level] == ctx.bv_val( l + num_vars + 1u, bw ),
                               in[x][j][level] == ( neg[x][level] != out[j][l] ) ) );
        }
      }
    }
  }

  void constrain( const tt& spec )
  {
    for ( auto j = 0u; j < ( 1u << num_vars ); ++j )
    {
      solver.add( out[j].back() == ctx.bool_val( spec[j] ) );
    }
  }

  z3::expr_vector constrain_assumptions( const tt& spec )
  {
    z3::expr_vector assumptions( ctx );

    for ( auto j = 0u; j < ( 1u << num_vars ); ++j )
    {
      if ( spec[j] )
      {
        assumptions.push_back( out[j].back() );
      }
      else
      {
        assumptions.push_back( !( out[j].back() ) );

      }
    }

    return assumptions;
  }

  mig_graph extract_solution( const std::string& model_name, const std::string& output_name, bool verbose )
  {
    mig_graph mig;
    mig_initialize( mig, model_name );

    std::vector<mig_function> inputs, nodes;
    for ( auto i = 0u; i < num_vars; ++i )
    {
      inputs += mig_create_pi( mig, str( format( "x%d" ) % i ) );
    }

    const auto m = solver.get_model();

    for ( auto i = 0u; i < sel[0].size(); ++i )
    {
      mig_function children[3];

      for ( auto x = 0u; x < 3u; ++x )
      {
        auto sel_val = to_bitset( m.eval( sel[x][i] ) ).to_ulong();

        if ( sel_val == 0u )
        {
          children[x] = mig_get_constant( mig, false );
        }
        else if ( sel_val > 0 && sel_val <= num_vars )
        {
          children[x] = inputs[sel_val - 1u];
        }
        else
        {
          children[x] = nodes[sel_val - num_vars - 1u];
        }

        if ( expr_to_bool( m.eval( neg[x][i] ) ) )
        {
          children[x] = !children[x];
        }
      }

      nodes += mig_create_maj( mig, children[0], children[1], children[2] );

      if ( verbose )
      {
        std::cout << format( "added node (%d,%d) for i = %d" ) % nodes.back().node % nodes.back().complemented % i << std::endl;
        for ( auto x = 0u; x < 3u; ++x )
        {
          std::cout << format( "  - child %d = (%d,%d)" ) % x % children[x].node % children[x].complemented << std::endl;
        }
      }
    }

    auto f = nodes.back();

    /*if ( expr_to_bool( m.eval( out_neg ) ) )
    {
      f = !f;
      }*/

    mig_create_po( mig, f, output_name );

    return mig;
  }

  unsigned num_vars;
  z3::context ctx;
  z3::solver solver;

  std::vector<std::vector<z3::expr>> out;
  std::vector<std::vector<std::vector<z3::expr>>> in;

  std::vector<std::vector<z3::expr>> sel;
  std::vector<std::vector<z3::expr>> neg;

  z3::expr out_neg;
};

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

mig_graph exact_mig_with_sat_explicit( const tt& spec, unsigned start, const std::string& model_name, const std::string& output_name, bool verbose )
{
  auto k = start;
  while ( true )
  {
    if ( verbose )
    {
      std::cout << boost::format( "[i] check for realization with %d gates" ) % k << std::endl;
    }

    exact_mig_instance inst( tt_num_vars( spec ) );

    for ( auto i = 0u; i < k; ++i )
    {
      inst.add_level();
    }

    inst.constrain( spec );

    if ( inst.solver.check() == z3::sat )
    {
      return inst.extract_solution( model_name, output_name, verbose );
    }

    ++k;
  }
}

mig_graph exact_mig_with_sat_incremental( const tt& spec, unsigned start, const std::string& model_name, const std::string& output_name, bool verbose )
{
  exact_mig_instance inst( tt_num_vars( spec ) );

  for ( unsigned i = 1u; i < start; ++i )
  {
    inst.add_level();
  }

  while ( true )
  {
    inst.add_level();

    if ( verbose )
    {
      std::cout << boost::format( "[i] check for realization with %d gates" ) % inst.sel[0].size() << std::endl;
    }

    /* solve */
    if (inst.solver.check( inst.constrain_assumptions( spec ) ) == z3::sat)
    {
      return inst.extract_solution( model_name, output_name, verbose );
    }
  }
}

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

mig_graph exact_mig_with_sat( const tt& spec,
                              const properties::ptr& settings,
                              const properties::ptr& statistics )
{
  /* settings */
  const auto start       = get( settings, "start",       1u );
  const auto model_name  = get( settings, "model_name",  std::string( "exact" ) );
  const auto output_name = get( settings, "output_name", std::string( "f" ) );
  const auto incremental = get( settings, "incremental", false );
  const auto verbose     = get( settings, "verbose",     false );

  /* timing */
  properties_timer t( statistics );

  auto n = tt_num_vars( spec );
  assert( !spec.empty() && n <= 6u );

  /* terminal cases */
  if ( ( ~spec ).none() || spec.none() )
  {
    mig_graph mig;
    mig_initialize( mig, model_name );
    mig_create_po( mig, mig_get_constant( mig, spec.test( 0u ) ), output_name );
    return mig;
  }

  /* single variable */
  tt spec_copy = spec;
  tt_extend( spec_copy, 6u );

  for ( auto i = 0u; i < 6u; ++i )
  {
    if ( spec_copy == tt_store::i()( i ) || ~spec_copy == tt_store::i()( i ) )
    {
      mig_graph mig;
      mig_initialize( mig, model_name );
      auto f = mig_create_pi( mig, str( format( "x%d" ) % i ) );
      if ( spec_copy.test( 0u ) )
      {
        f = !f;
      }
      mig_create_po( mig, f, output_name );
      return mig;
    }
  }

  if ( incremental )
  {
    return exact_mig_with_sat_incremental( spec, start, model_name, output_name, verbose );
  }
  else
  {
    return exact_mig_with_sat_explicit( spec, start, model_name, output_name, verbose );
  }
}

mig_graph exact_mig_with_bdds( const tt& spec,
                               const properties::ptr& settings,
                               const properties::ptr& statistics )
{
  properties_timer t( statistics );

  assert( false && "not yet implemented" );

  return mig_graph();
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
