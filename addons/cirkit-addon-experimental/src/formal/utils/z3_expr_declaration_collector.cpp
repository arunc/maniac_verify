/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if ADDON_FORMAL

#include <formal/utils/z3_expr_declaration_collector.hpp>

namespace cirkit
{

z3_expr_declaration_collector::z3_expr_declaration_collector( std::vector< z3::sort >& sorts,
                                                              std::vector< z3::func_decl >& funs )
  : sorts(sorts)
  , funs(funs)
{}

void z3_expr_declaration_collector::visit_application( const z3::expr &e )
{
  collect_fun( e.decl() );
}

void z3_expr_declaration_collector::visit_quantifier( const z3::expr &e )
{
  const unsigned nb = Z3_get_quantifier_num_bound( e.ctx(), e );
  for ( unsigned i = 0u; i < nb; ++i )
  {
    z3::sort srt( e.ctx(), Z3_get_quantifier_bound_sort( e.ctx(), e, i ) );
    collect_sort( srt );
  }
}

void z3_expr_declaration_collector::visit_variable( const z3::expr &e )
{
  collect_sort( e.get_sort() );
}

void z3_expr_declaration_collector::collect_sort( const z3::sort& s )
{
  const unsigned id = Z3_get_sort_id( s.ctx(), s );
  if ( s.sort_kind() == Z3_UNINTERPRETED_SORT &&
       !seen(id) )
  {
    seen_ids.insert( id );
    sorts.push_back( s );
  }
}

void z3_expr_declaration_collector::collect_fun( const z3::func_decl& f )
{
  const unsigned id = Z3_get_func_decl_id( f.ctx(), f );
  if ( seen(id ) )
  {
    return;
  }
  seen_ids.insert( id );
  if ( f.decl_kind() == Z3_OP_UNINTERPRETED )
  {
    funs.push_back( f );
  }
  for ( unsigned i = 0u; i < f.arity(); ++i )
  {
    collect_sort( f.domain(i) );
  }
  collect_sort( f.range() );
}

}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
