/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file z3_expr_declaration_collector.hpp
 *
 * @brief Recursively collects all sorts and function declarations in
 * an z3::expr.
 *
 * Based on
 * http://z3.codeplex.com/SourceControl/latest#examples/tptp/tptp5.cpp
 * (branch: unstable).
 *
 * @author Heinz Riener
 * @since 2.0
 */

#if ADDON_FORMAL

#ifndef Z3_EXPR_DECLARATION_COLLECTOR_HPP
#define Z3_EXPR_DECLARATION_COLLECTOR_HPP

#include <formal/utils/z3_expr_visitor.hpp>

#include <vector>

namespace cirkit
{

class z3_expr_declaration_collector : public z3_expr_visitor
{
public:
  z3_expr_declaration_collector( std::vector< z3::sort >& sorts,
                                 std::vector< z3::func_decl >& funs );

  void visit_application( const z3::expr &e );
  void visit_quantifier( const z3::expr &e );
  void visit_variable( const z3::expr &e );

protected:
  void collect_fun( const z3::func_decl& f );
  void collect_sort( const z3::sort& s );

  std::vector< z3::sort >& sorts;
  std::vector< z3::func_decl >& funs;
};

}

#endif

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
