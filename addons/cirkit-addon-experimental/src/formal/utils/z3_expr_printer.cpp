/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if ADDON_FORMAL

#include <formal/utils/z3_expr_printer.hpp>
#include <formal/utils/z3_expr_declaration_collector.hpp>

namespace cirkit
{

z3_expr_printer::z3_expr_printer( std::ostream& os, style st )
  : os(os)
  , st(st)
{}

std::ostream& z3_expr_printer::operator<<( const z3::expr& e )
{
  std::vector< z3::sort > sorts;
  std::vector< z3::func_decl > funs;

  z3_expr_declaration_collector collector( sorts, funs );
  collector.visit( e );
  print_sorts( sorts );
  print_function_decls( funs );
  if ( e.decl().decl_kind() == Z3_OP_AND )
  {
    const unsigned num_args = e.num_args();
    for ( unsigned i = 0u; i < num_args; ++i )
    {
      os << "(assert " << e.arg(i) << ")";
      if ( i + 1 < num_args )
      {
        os << std::endl;
      }
    }
  }
  else
  {
    os << "(assert " << e << ")";
  }
  return os;
}

void z3_expr_printer::print_sorts( const std::vector< z3::sort > &sorts )
{
  for ( const auto& s : sorts )
  {
    os << "(declare-sort " << s << ")" << std::endl;
  }
}

void z3_expr_printer::print_function_decls( const std::vector< z3::func_decl > &funs )
{
  for ( const auto& f : funs )
  {
    print_function_decl( f );
  }
}

void z3_expr_printer::print_function_decl( const z3::func_decl &f )
{
  const std::string name = f.name().str();
  if ( f.is_const() )
  {
    if ( st == Z3 )
    {
      os << "(declare-const " << name << ' ';
    }
    else
    {
      os << "(declare-fun " << name << " () ";
    }
    z3::sort srt( f.range() );
    os << srt;
    os << ")" << std::endl;
    return;
  }

  os << "(declare-fun " << name << " (";

  const unsigned na = f.arity();
  switch ( na )
  {
  case 0:
    break;
  case 1:
    {
      z3::sort srt( f.domain(0) );
      os << srt;
      break;
    }
  default:
    {
      for ( unsigned i = 0u; i < na; ++i )
      {
        z3::sort srt( f.domain(i) );
        os << srt;
        if ( i + 1 < na )
        {
          os << ' ';
        }
      }
    }
  }
  os << ") ";

  z3::sort srt( f.range() );
  os << srt << ")" << std::endl;
}

}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
