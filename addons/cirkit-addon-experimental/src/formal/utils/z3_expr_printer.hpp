/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file z3_expr_printer.hpp
 *
 * @brief Pretty printer for z3::expr.
 *
 * Based on
 * http://z3.codeplex.com/SourceControl/latest#examples/tptp/tptp5.cpp
 * (branch: unstable).
 *
 * @author Heinz Riener
 * @since 2.0
 */

#if ADDON_FORMAL

#ifndef Z3_EXPR_PRINTER_HPP
#define Z3_EXPR_PRINTER_HPP

#include <z3++.h>

#include <ostream>
#include <vector>

namespace cirkit
{

struct z3_expr_printer
{
public:
  enum style
  {
    Z3        /* use Z3 specific commands */
  , SMTLIB2   /* use only standard SMT-LIB2 */
  }; // style

  z3_expr_printer( std::ostream& os, style st = SMTLIB2 );
  std::ostream& operator<<( const z3::expr& e );

protected:
  void print_sorts( const std::vector< z3::sort > &sorts );
  void print_function_decls( const std::vector< z3::func_decl > &funs );
  void print_function_decl( const z3::func_decl &f );

  std::ostream& os;
  style st;
};

}

#endif

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
