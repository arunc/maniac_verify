/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if ADDON_FORMAL

#include <formal/utils/z3_expr_to_aig.hpp>
#include <formal/utils/z3_utils.hpp>

namespace cirkit
{

z3_expr_to_aig::z3_expr_to_aig( aig_graph& aig )
  : aig(aig)
{}

z3_expr_to_aig::~z3_expr_to_aig() {}

const aig_function& z3_expr_to_aig::get_aig_function( const z3::expr& e )
{
  return to_aig_function( get_aig_word( e ) );
}

const aig_word& z3_expr_to_aig::get_aig_word( const z3::expr& e )
{
  const unsigned id = Z3_get_ast_id( e.ctx(), e );
  // std::cerr << "Get id " << id << " --- " << e << std::endl;
  const auto& it = strash.find( id );
  assert( it != strash.end() );
  return it->second;
}

void z3_expr_to_aig::visit_application( const z3::expr& e )
{
  /* handle nullary functions, that are, constant values or variables */
  const z3::func_decl& decl = e.decl();
  const unsigned na = decl.arity();
  if ( na != 0 ) return;

  /* strashing */
  auto it = strash.find( e );
  if ( it != strash.end() ) return;

  const Z3_decl_kind& decl_kind = decl.decl_kind();
  const std::string name = decl.name().str();
  aig_word w;

  /* constant value */
  if ( decl_kind == Z3_OP_TRUE || decl_kind == Z3_OP_FALSE )
  {
    assert( decl.range().sort_kind() == Z3_BOOL_SORT );
    const bool value = expr_to_bool( e );
    w = to_aig_word( aig_get_constant(aig, value) );
  }
  else if ( decl_kind == Z3_OP_BNUM )
  {
    assert( decl.range().sort_kind() == Z3_BV_SORT );
    const std::string value = expr_to_bin( e );
    w = aig_create_bvbin( aig, value );
  }
  /* variable */
  else
  {
    const z3::sort& srt = decl.range();
    const Z3_sort_kind& sort_kind = srt.sort_kind();
    if ( sort_kind == Z3_BOOL_SORT )
    {
      w = to_aig_word( aig_create_pi( aig, name ) );
    }
    else if ( sort_kind == Z3_BV_SORT )
    {
      w = aig_create_wi( aig, decl.range().bv_size(), name );
    }
    else
    {
      assert( false && "Unsupported domain finitizing for expression type" );
      exit(1);
    }
  }

  const unsigned id = Z3_get_ast_id( e.ctx(), e );
  // std::cerr << name << " --- register id = " << id << std::endl;
  strash.insert( {id,w} );
}

void z3_expr_to_aig::visit_quantifier( const z3::expr& e )
{
  assert( false && "Quantifiers are not supported" );
}

void z3_expr_to_aig::visit_expression_after( const z3::expr& e )
{
  if ( !e.is_app() ) return;

  const z3::func_decl& decl = e.decl();
  const unsigned na = decl.arity();
  if ( na <= 0u ) return;

  const unsigned id = Z3_get_ast_id( e.ctx(), e );
  const std::string& op = decl.name().str();

  const Z3_decl_kind decl_kind = decl.decl_kind();

  if ( decl_kind == Z3_OP_NOT )
  {
    assert( e.num_args() == 1u );
    const aig_word& w = get_aig_word( e.arg(0) );
    aig_function n = !to_aig_function( w );
    strash.insert( {id,to_aig_word(n)} );
  }
  else if ( decl_kind == Z3_OP_EQ )
  {
    assert( e.num_args() == 2u );
    const aig_word& left_op = get_aig_word( e.arg(0) );
    const aig_word& right_op = get_aig_word( e.arg(1) );
    aig_function cmp = aig_create_equal( aig, left_op, right_op );
    strash.insert( {id,to_aig_word(cmp)} );
  }
  else if ( decl_kind == Z3_OP_AND )
  {
    assert( e.num_args() >= 2u );
    aig_function res;
    if ( e.num_args() >= 2u )
    {
      const aig_function& left_op = get_aig_function( e.arg(0) );
      const aig_function& right_op = get_aig_function( e.arg(1) );
      res = aig_create_and( aig, left_op, right_op );
    }
    unsigned i = 2u;
    while ( i < e.num_args() )
    {
      const aig_function& another_op = get_aig_function( e.arg(i) );
      res = aig_create_and( aig, res, another_op );
      ++i;
    }
    strash.insert( {id,to_aig_word(res)} );
  }
  else if ( decl_kind == Z3_OP_OR )
  {
    assert( e.num_args() >= 2u );
    aig_function res;
    if ( e.num_args() >= 2u )
    {
      const aig_function& left_op = get_aig_function( e.arg(0) );
      const aig_function& right_op = get_aig_function( e.arg(1) );
      res = aig_create_or( aig, left_op, right_op );
    }
    unsigned i = 2u;
    while ( i < e.num_args() )
    {
      const aig_function& another_op =  get_aig_function( e.arg(i) );
      res = aig_create_or( aig, res, another_op );
      ++i;
    }
    strash.insert( {id,to_aig_word(res)} );
  }
  else if ( decl_kind == Z3_OP_ULEQ )
  {
    assert( e.num_args() == 2u );
    const aig_word& left_op = get_aig_word( e.arg(0) );
    const aig_word& right_op = get_aig_word( e.arg(1) );
    aig_function cmp = aig_create_bvule( aig, left_op, right_op );
    strash.insert( {id,to_aig_word(cmp)} );
  }
  else if ( decl_kind == Z3_OP_SLEQ )
  {
    assert( e.num_args() == 2u );
    const aig_word& left_op = get_aig_word( e.arg(0) );
    const aig_word& right_op = get_aig_word( e.arg(1) );
    aig_function cmp = aig_create_bvsle( aig, left_op, right_op );
    strash.insert( {id,to_aig_word(cmp)} );
  }
  else if ( decl_kind == Z3_OP_UGEQ )
  {
    assert( e.num_args() == 2u );
    const aig_word& left_op = get_aig_word( e.arg(0) );
    const aig_word& right_op = get_aig_word( e.arg(1) );
    aig_function cmp = aig_create_bvuge( aig, left_op, right_op );
    strash.insert( {id,to_aig_word(cmp)} );
  }
  else if ( decl_kind == Z3_OP_SGEQ )
  {
    assert( e.num_args() == 2u );
    const aig_word& left_op = get_aig_word( e.arg(0) );
    const aig_word& right_op = get_aig_word( e.arg(1) );
    aig_function cmp = aig_create_bvsge( aig, left_op, right_op );
    strash.insert( {id,to_aig_word(cmp)} );
  }
  else if ( decl_kind == Z3_OP_ULT )
  {
    assert( e.num_args() == 2u );
    const aig_word& left_op = get_aig_word( e.arg(0) );
    const aig_word& right_op = get_aig_word( e.arg(1) );
    aig_function cmp = aig_create_bvult( aig, left_op, right_op );
    strash.insert( {id,to_aig_word(cmp)} );
  }
  else if ( decl_kind == Z3_OP_SLT )
  {
    assert( e.num_args() == 2u );
    const aig_word& left_op = get_aig_word( e.arg(0) );
    const aig_word& right_op = get_aig_word( e.arg(1) );
    aig_function cmp = aig_create_bvslt( aig, left_op, right_op );
    strash.insert( {id,to_aig_word(cmp)} );
  }
  else if ( decl_kind == Z3_OP_UGT )
  {
    assert( e.num_args() == 2u );
    const aig_word& left_op = get_aig_word( e.arg(0) );
    const aig_word& right_op = get_aig_word( e.arg(1) );
    aig_function cmp = aig_create_bvugt( aig, left_op, right_op );
    strash.insert( {id,to_aig_word(cmp)} );
  }
  else if ( decl_kind == Z3_OP_SGT )
  {
    assert( e.num_args() == 2u );
    const aig_word& left_op = get_aig_word( e.arg(0) );
    const aig_word& right_op = get_aig_word( e.arg(1) );
    aig_function cmp = aig_create_bvsgt( aig, left_op, right_op );
    strash.insert( {id,to_aig_word(cmp)} );
  }
  else if ( decl_kind == Z3_OP_ITE )
  {
    assert( e.num_args() == 3u );
    const aig_function& cond = get_aig_function( e.arg(0) );
    const aig_word& left_op = get_aig_word( e.arg(1) );
    const aig_word& right_op = get_aig_word( e.arg(2) );
    aig_word res = aig_create_ite( aig, cond, left_op, right_op );
    strash.insert( {id,res} );
  }
  else if ( decl_kind == Z3_OP_BADD )
  {
    const aig_word& left_op = get_aig_word( e.arg(0) );
    const aig_word& right_op = get_aig_word( e.arg(1) );
    aig_word res = aig_create_bvadd( aig, left_op, right_op );
    strash.insert( {id,res} );
  }
  else
  {
    assert( false && "Yet not supported SMT operators" );
  }
}

}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
