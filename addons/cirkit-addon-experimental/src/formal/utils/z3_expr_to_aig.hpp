/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file z3_expr_to_aig.hpp
 *
 * @brief Converter from z3::expr to cirkit::aig_graph.
 *
 * @author Heinz Riener
 * @since 2.0
 */

#if ADDON_FORMAL

#ifndef Z3_EXPR_TO_AIG_HPP
#define Z3_EXPR_TO_AIG_HPP

#include <formal/utils/z3_expr_visitor.hpp>

#include <classical/aig_word.hpp>

#include <z3++.h>

namespace cirkit
{

struct z3_expr_to_aig : public z3_expr_visitor
{
public:
  z3_expr_to_aig( aig_graph& aig );
  virtual ~z3_expr_to_aig();

  const aig_function& get_aig_function( const z3::expr& e );
  const aig_word& get_aig_word( const z3::expr& e );

  virtual void visit_application( const z3::expr& e );
  virtual void visit_quantifier( const z3::expr& e );
  virtual void visit_expression_after( const z3::expr& e );

protected:
  aig_graph& aig;
  std::map< unsigned, aig_word > strash;
};

}

#endif

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
