/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#if ADDON_FORMAL

#include <formal/utils/z3_expr_visitor.hpp>

namespace cirkit
{

z3_expr_visitor::z3_expr_visitor() {}
z3_expr_visitor::~z3_expr_visitor() {}

void z3_expr_visitor::visit_application( const z3::expr& e ) {}
void z3_expr_visitor::visit_quantifier( const z3::expr& e ) {}
void z3_expr_visitor::visit_variable( const z3::expr& e ) {}
void z3_expr_visitor::visit_expression_after( const z3::expr& e ) {}
void z3_expr_visitor::visit_expression_before( const z3::expr& e ) {}

void z3_expr_visitor::visit( const z3::expr& e )
{
  const unsigned id = Z3_get_ast_id( e.ctx(), e );
  if ( seen( id ) ) {
    return;
  }

  visit_expression_before( e );

  if ( e.is_app() )
  {
    visit_application( e );
    const unsigned size = e.num_args();
    for ( unsigned i = 0u; i < size; ++i )
    {
      visit( e.arg(i) );
    }
  }
  else if ( e.is_quantifier() )
  {
    visit_quantifier( e );
    visit( e.body() );
  }
  else if ( e.is_var() )
  {
    visit_variable( e );
  }
  else
  {
    assert( false && "Expression type is not supported" );
  }

  visit_expression_after( e );
}

const bool z3_expr_visitor::seen( const unsigned id )
{
  if ( seen_ids.find(id) != seen_ids.end() )
  {
    return true;
  }
  seen_ids.insert( id );
  return false;
}

}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
