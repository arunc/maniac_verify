/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file z3_expr_visitor.hpp
 *
 * @brief Recursive visitor for z3::expr.
 *
 * @author Heinz Riener
 * @since 2.0
 */

#if ADDON_FORMAL

#ifndef Z3_EXPR_VISITOR_HPP
#define Z3_EXPR_VISITOR_HPP

#include <z3++.h>

#include <set>
#include <stack>

namespace cirkit
{

class z3_expr_visitor
{
public:
  z3_expr_visitor();
  virtual ~z3_expr_visitor();

  void visit( const z3::expr& e );

  virtual void visit_application( const z3::expr& e );
  virtual void visit_quantifier( const z3::expr& e );
  virtual void visit_variable( const z3::expr& e );
  virtual void visit_expression_before( const z3::expr& e );
  virtual void visit_expression_after( const z3::expr& e );

protected:
  const bool seen( unsigned id );
  std::set< unsigned > seen_ids;
};

}

#endif

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
