/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "aig_modelcheck.hpp"

#include <classical/utils/aig_structural_visitor.hpp>
#include <classical/utils/aig_utils.hpp>
#include <core/utils/range_utils.hpp>

#include <formal/sat/minisat.hpp>
#include <formal/sat/sat_solver.hpp>

#include <boost/format.hpp>

#include <map>

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

template< typename Solver >
class aig_encoding_visitor : public aig_structural_visitor
{
public:
  aig_encoding_visitor( const aig_graph& aig, Solver& solver, int& sid, std::map< aig_node, int >& node_map )
    : aig_structural_visitor( aig )
    , solver( solver )
    , sid( sid )
    , node_map( node_map )
  {}

public:
  void visit_constant( const aig_node& node ) const
  {
    // std::cout << "[const] 0u --> 0u" << '\n';
    node_map.insert( {aig_to_literal(aig, 0u),0u} );
  }

  void visit_input( const aig_node& node ) const
  {
    const int id = sid++;
    // std::cout << ( boost::format("[in] %s --> %s") % aig_to_literal(aig, node) % id ).str() << '\n';
    node_map.insert( {node,id} );
  }

  void visit_output( const aig_node& node ) const
  {
    // std::cout << "out " << node << '\n';
  }

  void visit_latch( const aig_node& node ) const
  {
    assert( false && "Latches are not supported" );
  }

  void visit_and( const aig_node& node, const aig_function& left, const aig_function& right ) const
  {
    const int id = sid++;
    // std::cout << ( boost::format("[and] %s,%s,%s --> %s") % aig_to_literal(aig, node) % aig_to_literal(aig, left) % aig_to_literal(aig, right) % id ).str() << '\n';

    /* lookup left and right */
    auto l_var = node_map.find( left.node );
    auto r_var = node_map.find( right.node );
    assert( l_var != node_map.end() );
    assert( r_var != node_map.end() );

    auto l = left.complemented  ? -l_var->second : l_var->second;
    auto r = right.complemented ? -r_var->second : r_var->second;
    node_map.insert( {node,id} );

    add_clause( solver )( { -id,  l } );
    add_clause( solver )( { -id,  r } );
    add_clause( solver )( {  id, -l, -r } );
  }

private:
  Solver &solver;
  int& sid;
  std::map< aig_node, int >& node_map;
};

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

void aig_modelcheck( const aig_graph& aig, const std::string inputs, const std::string outputs,
                     const std::vector< std::string >& blocked_inputs )
{
  const auto& info = aig_info( aig );

  assert( inputs.size() <= info.inputs.size() );
  assert( outputs.size() <= info.outputs.size() );

  auto solver = make_solver< minisat_solver >();
  std::map< aig_node, int > node_map;

  /* a pointer to the next free variable */
  int sid = 1u;

  /* encode AIG */
  aig_visit_nodes( aig, aig_encoding_visitor<minisat_solver>( aig, solver, sid, node_map ) );

  /* blocked inputs */
  for ( const auto& blocked_input : blocked_inputs )
  {
    std::vector< int > clause;
    for ( const auto &i : index( info.inputs ) )
    {
      assert( blocked_input[ i.index ] == '0' || blocked_input[ i.index ] == '1' );
      const auto it = node_map.find( i.value );
      assert( it != node_map.end() );
      const int var = it->second;
      clause += ( blocked_input[ i.index ] == '1' ? -var : var );
    }
    add_clause( solver )( clause );
  }

  /* input constraint */
  for ( const auto &i : index( info.inputs ) )
  {
    if ( i.index >= inputs.size() ) break;
    assert( inputs[ i.index ] == '0' || inputs[ i.index ] == '1' || inputs[ i.index ] == '-' );
    if ( inputs[ i.index ] == '-' ) continue;
    const auto it = node_map.find( i.value );
    assert( it != node_map.end() );
    const int var = it->second;
    add_clause( solver )( { inputs[ i.index ] == '1' ? var : -var } );
  }

  /* output constraint */
  for ( const auto &o : index( info.outputs ) )
  {
    if ( o.index >= outputs.size() ) break;
    assert( outputs[ o.index ] == '0' || outputs[ o.index ] == '1' || outputs[ o.index ] == '-' );
    if ( outputs[ o.index ] == '-' ) continue;
    const auto it = node_map.find( o.value.first.node );
    assert( it != node_map.end() );
    const int var = o.value.first.complemented ? -it->second : it->second;
    add_clause( solver )( { outputs[ o.index ] == '1' ? var : -var } );
  }

  solver_execution_statistics stats;
  auto result = solve( solver, stats );
  std::cout << "SAT = " << (bool)result << '\n';

  if ( result )
  {
    // std::cout << result->first << '\n';
    // std::cout << result->second << '\n';

    for ( const auto &i : info.inputs )
    {
      const auto it = node_map.find( i );
      assert( it != node_map.end() );
      const unsigned var = it->second;
      std::cout << ( result->first[ var - 1u ] );
    }
    std::cout << '-';
    for ( const auto &o : info.outputs )
    {
      const auto it = node_map.find( o.first.node );
      assert( it != node_map.end() );
      const unsigned var = it->second;
      std::cout << ( o.first.complemented ? ~result->first[ var - 1u ] : result->first[ var - 1u ] );
    }
    std::cout << '\n';
  }
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
