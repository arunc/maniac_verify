/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "equivalence_check.hpp"

#include <boost/assign/std/vector.hpp>

using namespace boost::assign;

namespace cirkit
{

mapping_t map_inputs_by_order( const aig_graph& c1, const aig_graph& c2 )
{
  const auto& c1_info = boost::get_property( c1, boost::graph_name );
  const auto& c2_info = boost::get_property( c2, boost::graph_name );

  assert( c1_info.inputs.size() == c2_info.inputs.size() );

  mapping_t m;
  for ( unsigned i = 0u; i < c1_info.inputs.size(); ++i )
  {
    m += std::make_pair( i, i );
  }

  return m;
}

mapping_t map_outputs_by_order( const aig_graph& c1, const aig_graph& c2 )
{
  const auto& c1_info = boost::get_property( c1, boost::graph_name );
  const auto& c2_info = boost::get_property( c2, boost::graph_name );

  assert( c1_info.outputs.size() == c2_info.outputs.size() );

  mapping_t m;
  for ( unsigned i = 0u; i < c1_info.outputs.size(); ++i )
  {
    m += std::make_pair( i, i );
  }

  return m;
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
