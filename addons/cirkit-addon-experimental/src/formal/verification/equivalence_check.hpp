/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file equivalence_check.hpp
 *
 * @brief Equivalence check of two AIGs
 *
 * @author Mathias Soeken
 * @since 2.0
 */

#ifndef EQUIVALENCE_CHECK_HPP
#define EQUIVALENCE_CHECK_HPP

#include <functional>
#include <vector>

#include <boost/assign/std/vector.hpp>
#include <boost/dynamic_bitset.hpp>

#include <core/properties.hpp>
#include <core/utils/range_utils.hpp>
#include <classical/aig.hpp>
#include <formal/sat/sat_solver.hpp>
#include <formal/sat/operations/logic.hpp>
#include <formal/sat/utils/add_aig.hpp>
#include <formal/sat/utils/solver_executables.hpp>

namespace cirkit
{

using mapping_t = std::vector<std::pair<unsigned, unsigned>>;
using mapping_func_t = std::function<mapping_t( const aig_graph& c1, const aig_graph& c2 )>;

/**
 * @brief Functions to combine inputs or outputs
 *
 * 1st parameter: Reference to the CNF generator
 * 2nd paramater: Pair of (Input index,Variable index) of the port of c1
 * 3rd parameter: Pair of (Output index,Variable index) of the port of c2
 * 4th parameter: First free variable index
 * Return value: New free variable index
 *
 * Remark:
 *   When used for the output port combining, if r is the return
 *   value then (r-1) is the variable index of the signal that
 *   should feed into the OR gate of the miter.
 */
template<class Solver>
using combine_ports_func_t = std::function<int(Solver&, const std::pair<unsigned, int>&, const std::pair<unsigned, int>&, int)>;
using assignment_t = std::pair<boost::dynamic_bitset<>, boost::dynamic_bitset<>>;

mapping_t map_inputs_by_order( const aig_graph& c1, const aig_graph& c2 );
mapping_t map_outputs_by_order( const aig_graph& c1, const aig_graph& c2 );

template<class Solver>
int default_combine_inputs( Solver& cnf, const std::pair<unsigned, int>& pi1, const std::pair<unsigned, int>& pi2, int sid )
{
  equals( cnf, pi1.second, pi2.second );
  return sid;
}

template<class Solver>
int default_combine_outputs( Solver& cnf, const std::pair<unsigned, int>& po1, const std::pair<unsigned, int>& po2, int sid )
{
  logic_xor( cnf, po1.second, po2.second, sid );
  return sid + 1;
}

/**
 * @param c1 First circuit
 * @param c2 Second circuit
 * @param settings The following settings are possible
 *                 +-----------------+------------------------------+------------------------+
 *                 | Name            | Type                         | Default                |
 *                 +-----------------+------------------------------+------------------------+
 *                 | input_mapping   | mapping_func_t               | map_inputs_by_order    |
 *                 | output_mapping  | mapping_func_t               | map_outputs_by_order   |
 *                 | combine_inputs  | combine_ports_func_t         | default_combine_inputs |
 *                 | combine_outputs | combine_ports_func_t         | default_combine_inputs |
 *                 +-----------------+------------------------------+------------------------+
 * @param statistics The following statistics are given
 *                 +----------------+----------------+-----------------------------------------+
 *                 | Name           | Type           | Description                             |
 *                 +----------------+----------------+-----------------------------------------+
 *                 | runtime        | double         | Runtime of the SAT solver               |
 *                 | assignment     | assignment_t   | Input assignment with bit- and care-set |
 *                 +----------------+----------------+-----------------------------------------+
 */
template<class Solver>
bool equivalence_check( const aig_graph& c1, const aig_graph& c2,
                        properties::ptr settings = properties::ptr(), properties::ptr statistics = properties::ptr() )
{
  using namespace boost::assign;

  /* Settings */
  auto input_mapping   = get( settings, "input_mapping",   mapping_func_t( map_inputs_by_order ) );
  auto output_mapping  = get( settings, "output_mapping",  mapping_func_t( map_outputs_by_order ) );
  auto combine_inputs  = get( settings, "combine_inputs",  combine_ports_func_t<Solver>( default_combine_inputs<Solver> ) );
  auto combine_outputs = get( settings, "combine_outputs", combine_ports_func_t<Solver>( default_combine_outputs<Solver> ) );

  /* AIG info */
  const auto& c1_info = boost::get_property( c1, boost::graph_name );
  const auto& c2_info = boost::get_property( c2, boost::graph_name );

  /* Create miter */
  auto cnf = make_solver<Solver>( settings );

  std::vector<int> c1_piids( c1_info.inputs.size() ), c1_poids( c1_info.outputs.size() ),
                   c2_piids( c2_info.inputs.size() ), c2_poids( c2_info.outputs.size() );
  int sid = add_aig( cnf, c1, 1u, c1_piids, c1_poids );
  sid = add_aig( cnf, c2, sid, c2_piids, c2_poids );

  auto im = input_mapping( c1, c2 );
  for ( const auto& it : im )
  {
    sid = combine_inputs( cnf, {it.first, c1_piids[it.first]}, {it.second, c2_piids[it.second]}, sid );
  }

  std::vector<int> orids;
  auto om = output_mapping( c1, c2 );
  for ( const auto& it : om )
  {
    sid = combine_outputs( cnf, {it.first, c1_poids[it.first]}, {it.second, c2_poids[it.second]}, sid );
    orids += sid - 1;
  }

  logic_or( cnf, orids, sid );
  add_clause( cnf )( {sid} );

  /* Solve */
  solver_execution_statistics solver_stats;
  auto result = solve( cnf, solver_stats );

  if ( statistics )
  {
    statistics->set( "runtime", solver_stats.runtime );
    if ( (bool)result )
    {
      /* Distinguishing inputs */
      boost::dynamic_bitset<> cex_bits( im.size() ), cex_mask( im.size() );
      for ( const auto& it : index( im ) )
      {
        cex_bits[it.index] = result->first[ c1_piids[it.value.first]-1 ];
        cex_mask[it.index] = result->second[ c1_piids[it.value.first]-1 ];
      }
      statistics->set( "assignment", std::make_pair( cex_bits, cex_mask ) );

      /* Output of c1 */
      boost::dynamic_bitset<> out_bits( om.size() ), out_mask( om.size() );
      for ( const auto& it : index( om ) )
      {
        out_bits[it.index] = result->first[ c1_poids[it.value.first]-1 ];
        out_mask[it.index] = result->second[ c1_poids[it.value.first]-1 ];
      }
      statistics->set( "output_c1", std::make_pair( out_bits, out_mask ) );

      /* Output of c2 */
      for ( const auto& it : index( om ) )
      {
        out_bits[it.index] = result->first[ c2_poids[it.value.second]-1 ];
        out_mask[it.index] = result->second[ c2_poids[it.value.second]-1 ];
      }
      statistics->set( "output_c2", std::make_pair( out_bits, out_mask ) );
    }
  }

  return !(bool)result;
}

}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
