/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "unate.hpp"

#include <boost/assign/std/vector.hpp>
#include <boost/range/algorithm.hpp>

#include <core/utils/terminal.hpp>
#include <core/utils/timer.hpp>
#include <classical/utils/aig_utils.hpp>
#include <formal/sat/minisat.hpp>
#include <formal/sat/sat_solver.hpp>
#include <formal/sat/operations/logic.hpp>
#include <formal/sat/utils/add_aig.hpp>

#define timer timer_class
#include <boost/progress.hpp>
#undef timer

using namespace boost::assign;

namespace cirkit
{

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

boost::dynamic_bitset<> unateness( const aig_graph& aig,
                                   const properties::ptr& settings,
                                   const properties::ptr& statistics )
{
  /* settings */
  const auto progress = get( settings, "progress", false );
  const auto verbose  = get( settings, "verbose", false );

  /* timer */
  properties_timer t( statistics );

  /* info */
  const auto& info = aig_info( aig );

  /* create solver */
  auto solver = make_solver<minisat_solver>();
  solver.no_model = true;
  solver_execution_statistics stats;
  auto sid = 1;

  /* build miter */
  std::vector<int> piids1, piids2, poids1, poids2;
  sid = add_aig( solver, aig, sid, piids1, poids1 );
  sid = add_aig( solver, aig, sid, piids2, poids2 );

  /* connect inputs */
  const auto n = info.inputs.size();
  std::vector<int> input_xnors( n );
  for ( auto i = 0u; i < n; ++i )
  {
    logic_xnor( solver, piids1[i], piids2[i], sid );
    input_xnors[i] = sid++;
  }

  /* connect outputs */
  const auto m = info.outputs.size();
  std::vector<int> output_xors( n ), output_ors_neg( n ), output_ors_pos( n );
  for ( auto j = 0u; j < m; ++j )
  {
    logic_xor( solver, poids1[j], poids2[j], sid );
    output_xors[j] = sid++;

    logic_or( solver, -poids1[j], poids2[j], sid );
    output_ors_pos[j] = sid++;

    logic_or( solver, poids1[j], -poids2[j], sid );
    output_ors_neg[j] = sid++;
  }

  /* iterate over output/input pairs */
  null_stream ns;
  std::ostream null_out( &ns );
  boost::progress_display show_progress( m * n, progress ? std::cout : null_out );

  boost::dynamic_bitset<> result( ( m * n ) << 1u );
  auto pos = 0u;
  for ( auto j = 0u; j < m; ++j )
  {
    for ( auto i = 0u; i < n; ++i )
    {
      ++show_progress;

      /* prepare assumptions */
      std::vector<int> assumptions( n - 1u );
      boost::remove_copy( input_xnors, assumptions.begin(), input_xnors[i] );

      /* assume different values for x_i */
      assumptions += -piids1[i],piids2[i];

      /* check for support */
      assumptions += output_xors[j];

      if ( solve( solver, stats, assumptions ) == boost::none ) /* unsat */
      {
        result[pos++] = 1; result[pos++] = 1;
        continue;
      }

      /* check for negative unate */
      assumptions.back() = -output_ors_neg[j];

      if ( solve( solver, stats, assumptions ) == boost::none ) /* unsat */
      {
        result[pos++] = 1; result[pos++] = 0;
        continue;
      }

      /* check for positive unate */
      assumptions.back() = -output_ors_pos[j];

      if ( solve( solver, stats, assumptions ) == boost::none ) /* unsat */
      {
        result[pos++] = 0; result[pos++] = 1;
        continue;
      }

      result[pos++] = 0; result[pos++] = 0;
    }
  }

  assert( pos == ( m * n ) << 1u );

  return result;
}

unate_kind get_unateness_kind( const boost::dynamic_bitset<>& u, unsigned po, unsigned pi, unsigned num_pis )
{
  const auto pos = po * ( num_pis << 1u ) + ( pi << 1u );

  return u[pos] ? ( u[pos + 1u] ? unate_kind::independent : unate_kind::unate_neg ) : ( u[pos + 1u] ? unate_kind::unate_pos : unate_kind::binate );
}

unate_kind get_unateness_kind( const boost::dynamic_bitset<>& u, unsigned po, unsigned pi, const aig_graph_info& info )
{
  return get_unateness_kind( u, po, pi, info.inputs.size() );
}

unate_kind get_unateness_kind( const boost::dynamic_bitset<>& u, unsigned po, unsigned pi, const aig_graph& aig )
{
  return get_unateness_kind( u, po, pi, aig_info( aig ) );
}

}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
