/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "pprm_synthesis.hpp"

#if ADDON_REVERSIBLE

#include <map>
#include <queue>
#include <set>
#include <utility>
#include <vector>

#include <boost/algorithm/string/join.hpp>
#include <boost/assign/std/vector.hpp>
#include <boost/date_time.hpp>
#include <boost/format.hpp>
#include <boost/optional.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/unordered_map.hpp>
#include <boost/unordered_set.hpp>

#include <core/functor.hpp>
#include <core/utils/timer.hpp>

#include <reversible/functions/add_gates.hpp>
#include <reversible/functions/copy_metadata.hpp>
#include <reversible/truth_table.hpp>

namespace cirkit {

using boost::format;
using boost::str;

/******************************************************************************
 * Types                                                                      *
 ******************************************************************************/

using variable_t = std::string;
using product_t  = std::set<variable_t>;

/******************************************************************************
 * Constants                                                                  *
 ******************************************************************************/

const float ALPHA = 0.3f;
const float BETA  = 0.6f;
const float GAMMA = 0.1f;

/******************************************************************************
 * Private functions                                                          *
 ******************************************************************************/

std::ostream& operator<<( std::ostream& os, const product_t& p )
{
  return os << ( p.empty() ? "1" : boost::join( p, "*" ) );
}

namespace pprm {

/**
 * @brief positive-polarity Reed-Muller-expansion of a reversible function
 */
class pprm_expansion
{
public:
  /**
   * map to map a variable name to its position in the table
   */
  using var_to_idx_map_type = std::map<variable_t, unsigned>;

  /**
   * base type for esop (exor-sum of products)
   */
  using esop_base_type = boost::unordered_set<product_t>;

  /**
   * @brief EXOR sum of products
   *
   * overwrites the set's insert method
   */
  class esop : public esop_base_type
  {
  public:
    esop() : esop_base_type() {}

    /**
     * @brief insert new product to the exor sum of products
     *
     * the product is added to the sum of products, if the product is
     * already present in the sum of products, the product is removed instead
     * of being added
     *
     * @param p the product to add, or remove if already present
     */
    void insert( const product_t& p )
    {
      // try to find product p, if p is in the set, remove it
      iterator it = find( p );
      if (it != end())
      {
        erase( it );
      }
      else
      {
        // if p is not in set, add it to set
        esop_base_type::insert( p );
      }
    }

    /**
     * @brief print the sum of products to a stream
     *
     * @param os
     *        the stream to be used for printing
     */
    void print( std::ostream& os ) const
    {
      std::string prefix = "";
      for ( const auto& p : *this )
      {
        os << prefix << p;
        prefix = " + ";
      }
    }

  };

  /**
   * map that maps output variable to esop
   */
  using esop_map_t = std::map<variable_t, esop>;

  /**
   * @brief Constructs pprm-expansion for a given truth table
   * @param table
   *          the truth table defining a reversible function
   */
  pprm_expansion( const binary_truth_table& table )
  {
    //fill the var_idx map
    for ( auto out_idx = 0u; out_idx < table.num_outputs(); ++out_idx )
    {
      m_var_to_idx[str( format( "x%d" ) % out_idx )] = out_idx;
    }

    //go trough all lines of the truth table
    for(binary_truth_table::const_iterator it=table.begin(); it!=table.end();it++)
    {
      unsigned int idx=0;
      //go trough the variables
      //if the output variable is set to 1 a new product (or more than one)
      //has to be added
      for ( const auto& out_bit : boost::make_iterator_range( it->second ) )
      {

        if(out_bit.get())
        {
          std::vector<product_t> new_products;
          new_products.push_back(product_t());

          unsigned int input_index=0;
          for ( const auto& in_bit : boost::make_iterator_range( it->first ) )
          {
            if(in_bit.get())
            {
              for (product_t& p : new_products)
              {
                p.insert( str( format( "x%d" ) % input_index ) );
              }
            }
            else
            {
              //input is negative, add (input +1), which means adding all
              //new_products + all new_products * input
              std::vector<product_t> additional_products;
              for (product_t& p : new_products)
              {
                product_t new_product=p;
                new_product.insert( str( format( "x%d" ) % input_index ) );
                additional_products.push_back(new_product);
              }

              new_products.insert(new_products.end(),additional_products.begin(),additional_products.end());
            }

            input_index++;
          }

          for (product_t& p : new_products)
          {
            m_esop_map[str( format( "x%d" ) % idx )].insert( p );
          }
        }


        idx++;
      }
    }
  }

  /**
   * @brief substitute all occurences of v with v XOR factor
   *
   *
   * @param v the variable to be substituted
   * @param v will be substituted by v XOR factor
   */
  void substitute( const variable_t& v, const product_t& factor )
  {
    //go through all pprm expansions for each output var
    for (esop_map_t::iterator map_it = m_esop_map.begin();
        map_it != m_esop_map.end(); map_it++)
    {
      //substitution of v by v XOR factor means, that all occurences of
      //v have to be found first, these occurences are collected in the
      //vector occurences
      std::vector<product_t> occurences;

      for(esop::const_iterator products_it=map_it->second.begin();
          products_it!=map_it->second.end();products_it++)
      {
        if(products_it->find(v)!=products_it->end())
        {
          occurences.push_back(*products_it);
        }
      }

      //now each of the collected products, has the form: v*rest
      //substitution by v + factor means
      // (v+factor)*rest = v*rest + rest*factor
      //this means that just rest*factor has to be added to the esop
      for(std::vector<product_t>::iterator ocurrence_it=occurences.begin();
          ocurrence_it!=occurences.end();ocurrence_it++)
      {
        ocurrence_it->erase(v);//=rest
        ocurrence_it->insert(factor.begin(),factor.end());//=rest*factor

        map_it->second.insert(*ocurrence_it);
      }
    }
  }

  friend std::ostream& operator<<( std::ostream& os, const pprm_expansion& exp )
  {
    exp.print(os);
    return os;
  }

  /**
   * @brief print the pprm expansion to a stream
   *
   * @param os
   *          the stream to be used for printing
   */
  void print( std::ostream& os ) const
  {
    for(esop_map_t::const_iterator it=m_esop_map.begin();
        it!=m_esop_map.end();it++)
    {
      os<<it->first<<" = ";
      it->second.print(os);
      os<<std::endl;
    }
  }

  /**
   * returns the number of terms of the pprm expansion
   *
   * @return the number of terms of the pprm expansion
   */
  unsigned int no_terms() const
  {
    unsigned int result = 0;
    for ( const esop_map_t::value_type& v : m_esop_map )
    {
     result += v.second.size();
    }
    return result;
  }

  /**
   * @brief check whether pprm expansion is a solution
   *
   * pprm expansion is a solution if for all variables v, v_out= v_in
   *
   * @return true if pprm expansion is a solution
   */
  bool is_solution() const
  {
    for (esop_map_t::value_type v : m_esop_map)
    {
      if((v.second.size()!=1)||(v.second.begin()->size()!=1)||(*(v.second.begin()->begin())!=v.first))
      {
        return false;
      }
    }
    return true;
  }

  /**
   * return the index of a variable
   *
   * @param var
   *          the variable to get the index of
   * @return the index of the variable var
   */
  unsigned var_to_idx( const variable_t& var )
  {
    var_to_idx_map_type::iterator it=m_var_to_idx.find(var);
    if(it==m_var_to_idx.end())
    {
      throw std::runtime_error("Cannot get index of unknown variable.");
    }
    return it->second;
  }

  /**
   * @return returns the esop map of this pprm expansion
   */
  const esop_map_t& esop_map() const
  {
    return m_esop_map;
  }

private:
  esop_map_t m_esop_map;
  var_to_idx_map_type m_var_to_idx;
};

std::ostream& operator<<( std::ostream& os, const pprm_expansion& exp );

} // namespace pprm

class pprm_node;
typedef std::shared_ptr<pprm_node> pprm_node_ptr;

class pprm_node
{
public:
  /**
   * pair of variable v and factor f, used to represent a substitution v = v + f;
   */
  typedef std::pair<variable_t, product_t> substitution_type;

  /**
   * maps a variable to 1, if it was negated in a previous step,
   * the value is reset to 0 as soon as the variable is used as target or
   * control state in a next step
   */
  typedef boost::unordered_map<variable_t, unsigned int> negations_map;

  pprm_node(const pprm::pprm_expansion& pprm)
      : depth( 0 ),
        pprm( pprm ),
        terms( pprm.esop_map().size() ),
        elim( 0 ),
        priority( 0.f )
  {
      for ( const pprm::pprm_expansion::esop_map_t::value_type& v : this->pprm.esop_map() )
      {
        negations[v.first] = 0;
      }
  }

  /**
   * pointer to parent node
   */
	pprm_node_ptr parent;
	int depth;
	/**
	 * optional substition, not present for root node
	 */
	boost::optional<substitution_type> factor;
	/**
	 * pprm expansion of this node
	 */
	pprm::pprm_expansion pprm;
	int terms;
	int elim;
	float priority;
	negations_map negations;
};

class compare_pprm_node {
public:
	bool operator()(pprm_node_ptr t1, pprm_node_ptr t2) {
		if (t1->priority < t2->priority)
			return true;
		return false;
	}
};

typedef std::priority_queue<pprm_node_ptr, std::vector<pprm_node_ptr>,  compare_pprm_node> pprm_priority_queue;

pprm_node_ptr pprm_synthesis_alg(circuit& circ,
    const pprm::pprm_expansion& pprm_exp, const unsigned timeout,
    const int max_depth, const unsigned int additional_substitutions)
{
	int bestDepth = max_depth+1;
	std::cout<<"Max depth for synthesis algorithm is: "<<max_depth<<"."<<std::endl;
	pprm_node_ptr bestSolNode;
	int initTerms = pprm_exp.no_terms();
	boost::posix_time::ptime init=boost::posix_time::microsec_clock::local_time();

	std::cout<<"PPRM expansion: ";
	pprm_exp.print(std::cout);
	std::cout<<std::endl;

	pprm_node_ptr rootNode(new pprm_node(pprm_exp));
	rootNode->depth = 0;
	rootNode->pprm = pprm_exp;
	rootNode->terms = initTerms;
	rootNode->elim = 0;
	rootNode->priority = INT_MAX;

	pprm_priority_queue pq;
	pq.push(rootNode);


	do {
		pprm_node_ptr parentNode = pq.top();
		pq.pop();
	  //avoid memory overflows
	  BOOST_ASSERT(pq.size()<200000);
	  //BOOST_ASSERT(parentNode->depth<1000);

		if (parentNode->depth >= (bestDepth - 1))
			continue;

		for (pprm::pprm_expansion::esop_map_t::const_iterator it =
				parentNode->pprm.esop_map().begin();
				it != parentNode->pprm.esop_map().end(); it++) {
		  bool skipAdditionalSubst2=false;

			const variable_t& v_i=it->first;

			//make sure the esop has the form v_i=v_i + factor (or ignore if additional_substitutions is greater 0)
      product_t prod;
      prod.insert(v_i);
      if(!it->second.count(prod))
      {
        if(additional_substitutions<1)
        {
          continue;
        }
      }

		  for (const product_t& factor : it->second)
			{
		    if(factor.count(v_i)<=0){
		      pprm_node_ptr childNode(new pprm_node(parentNode->pprm));

		      childNode->parent = parentNode;
          childNode->depth = parentNode->depth + 1;
          childNode->factor=std::make_pair(v_i, factor);

          if(factor.empty())
          {
            //v_i=v_i+1, substitution 2 is not necessary any more
            skipAdditionalSubst2=true;
          }

          childNode->pprm.substitute(v_i,factor);
          childNode->terms = childNode->pprm.no_terms();
          childNode->elim = parentNode->terms - childNode->terms;
          //reset negations map
          for (const variable_t& v : factor)
          {
            childNode->negations[v]=0;
          }

          if(childNode->pprm.is_solution() && childNode->depth < bestDepth)
          {
            bestDepth = childNode->depth;
            bestSolNode = childNode;
          }
          else if(childNode->elim > 0)
          {
            childNode->priority = ALPHA * childNode->depth + (BETA * childNode->elim) / childNode->depth - GAMMA* (std::max((unsigned)1,(unsigned)factor.size()));
            pq.push(childNode);
          }

//          std::cout<<std::endl<<"---------------------------------------"<<
//              "Parentnode pprm: "<<std::endl<<parentNode->pprm<<std::endl
//              <<"replace: "<<childNode->factor->first<<" = "<<childNode->factor->first<<" + ";
//              pprm::pprm_expansion::print_product(childNode->factor->second,std::cout);
//              std::cout<<std::endl
//              <<"Childnode pprm: "<<std::endl<<childNode->pprm<<std::endl
//          <<"-------------------------------------"<<std::endl;

		    }
		  }
		  //additional substitution 2
		  /**
		   * use the negations map to avoid that a variable is negated twice,
		   * once a variable is negated by this additional substitution the value
		   * of the negations map is set, and it is only reset,
		   * when the variable is involved in another
		   * substitution as control or target state
		   */
      if ((additional_substitutions >= 2) && (!skipAdditionalSubst2)
          && (parentNode->negations[v_i] == 0))
      {
        pprm_node_ptr childNode(new pprm_node(parentNode->pprm));

        childNode->parent = parentNode;
        childNode->depth = parentNode->depth + 1;
        product_t factor;
        childNode->factor = std::make_pair(v_i, factor);
        childNode->pprm = pprm::pprm_expansion(parentNode->pprm);
        childNode->pprm.substitute(v_i, factor);
        childNode->terms = childNode->pprm.no_terms();
        childNode->elim = parentNode->terms - childNode->terms;
        childNode->negations[v_i]=1;

        if (childNode->pprm.is_solution() && childNode->depth < bestDepth)
        {
          bestDepth = childNode->depth;
          bestSolNode = childNode;
        }
        else
        {
          childNode->priority = ALPHA * childNode->depth
              + (BETA * childNode->elim) / childNode->depth
              - GAMMA * (std::max((unsigned) 1,(unsigned) factor.size()));
          pq.push(childNode);
        }
		  }
		}
	} while (!pq.empty() && (boost::posix_time::microsec_clock::local_time()-init) < boost::posix_time::milliseconds(timeout));
	if(!pq.empty()){
	  std::cerr<<"Timer expired!"<<std::endl;
	}

	return bestSolNode;
}

/******************************************************************************
 * Public functions                                                           *
 ******************************************************************************/

bool pprm_synthesis( circuit& circ, const binary_truth_table& spec,
                     properties::ptr settings, properties::ptr statistics )
{
  using namespace boost::assign;

	// Settings parsing
  auto timeout                  = get( settings, "timeout",                  3000u );
  auto additional_substitutions = get( settings, "additional_substitutions", 2u );
	auto max_depth_str            = get( settings, "max_depth",                std::string( "max" ) );

	// Run-time measuring
  properties_timer t( statistics );

	pprm::pprm_expansion pprm_exp = pprm::pprm_expansion(spec);

	int max_depth = 0;
	if ( max_depth_str == "max" )
	{
	  max_depth = INT_MAX-1; // -1 because best depth will be set to max_depth+1
	}
	else if ( max_depth_str == "terms" )
	{
	  max_depth = pprm_exp.no_terms();
	}
	else
	{
	  max_depth = boost::lexical_cast<int>( max_depth_str );
	}

  pprm_node_ptr solution = pprm_synthesis_alg( circ, pprm_exp, timeout,
                                               max_depth, additional_substitutions );

	if ( solution )
	{
	  // a solution has been found, create circuit
	  circ.set_lines( spec.num_inputs() );
	  copy_metadata( spec, circ );

	  pprm_node_ptr cur_pprm_node = solution;
	  while (cur_pprm_node && cur_pprm_node->factor )
	  {
	    gate::control_container control_lines;
      for ( const std::string& control_var : cur_pprm_node->factor->second )
	    {
        control_lines += make_var( pprm_exp.var_to_idx( control_var ) );
	    }
	    prepend_toffoli( circ, control_lines, pprm_exp.var_to_idx( cur_pprm_node->factor->first ) );
      cur_pprm_node = cur_pprm_node->parent;
	  }
	  return true;
	}
	else
	{
	  std::cerr << "No solution found!" << std::endl;
	  return false;
	}
}

truth_table_synthesis_func pprm_synthesis_func(properties::ptr settings, properties::ptr statistics)
{
  truth_table_synthesis_func f = [&settings, &statistics]( circuit& circ, const binary_truth_table& spec ) {
    return pprm_synthesis( circ, spec, settings, statistics );
  };
  f.init( settings, statistics );
  return f;
}

}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
