/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file pprm_synthesis.hpp
 *
 * @brief PPRM-based synthesis algorithm
 *
 * @author Felix Hübner
 * @author Alexander Scharf
 *
 * @since  2.0
 */

#ifndef PPRM_SYNTHESIS_HPP
#define PPRM_SYNTHESIS_HPP

#if ADDON_REVERSIBLE

#include <core/properties.hpp>

#include <reversible/synthesis/synthesis.hpp>
#include <reversible/circuit.hpp>
#include <reversible/truth_table.hpp>

namespace cirkit
{

/**
 * @brief
 *
 * An Algorithm for Synthesis of Reversible Logic Circuits by positive-polarity Reed-Muller expansion as presented in IEEE TRANSACTIONS ON COMPUTER-AIDED DESIGN OF INTEGRATED CIRCUITS AND SYSTEMS, VOL. 25 NO 11, NOVEMBER 2006 by Gupta et al.
 *
 * @param settings
 *        the synthesis algorithm accepts the following settings:
 *
 *        timeout: timeout for the algorithm in ms, default 3000
 *
 *        random_lines: If this parameter is set, a specification with the given number of lines and filled with a random permutation
 *
 *        max_depth: max depth for the algorithm. Give a positive integer, or \"terms\" for number of terms of the pprm expansion, or \"max\" for INT_MAX"
 *
 *        additional_substitutions: 0= algorithm just substitutes v_i = v_i + factor, if v_i is in v_out,i, 1 = v_i is always replaced by v_i = v_i + factor, 2 = the substitution v_i = v_i + 1 is additionally considered, and the number of terms is allowed to increase in this case
 */
bool pprm_synthesis(circuit& circ, const binary_truth_table& spec,
		properties::ptr settings = properties::ptr(),
		properties::ptr statistics = properties::ptr());

/**
 * @brief Functor for the pprm_synthesis algorithm
 *
 * @param settings Settings (see pprm_synthesis)
 * @param statistics Statistics (see pprm_synthesis)
 *
 * @return A functor which complies with the truth_table_synthesis_func interface
 */
truth_table_synthesis_func pprm_synthesis_func( properties::ptr settings = std::make_shared<properties>(),
                                                properties::ptr statistics = std::make_shared<properties>() );

}

#endif /* PPRM_SYNTHESIS_HPP */

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
