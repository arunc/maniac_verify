/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE aig_to_bench

#include <cstdio>
#include <iostream>
#include <vector>

#include <boost/range/algorithm.hpp>
#include <boost/test/unit_test.hpp>

#include <core/utils/system_utils.hpp>
#include <classical/aig.hpp>
#include <classical/io/write_bench.hpp>
#include <classical/utils/aig_from_bdd.hpp>

#include <cuddObj.hh>

BOOST_AUTO_TEST_CASE(simple)
{
  using namespace cirkit;

  Cudd mgr;

  std::vector<BDD> xs( 3u );
  boost::generate( xs, [&mgr]() { return mgr.bddVar(); } );

  std::vector<BDD> fs( {xs[0] & ( xs[1] | ~xs[2] ), ( xs[0] | xs[1] ) & ( xs[2] | ~xs[0] ), ( xs[0] ^ xs[1] ) & xs[2],
        (xs[0] & xs[1]) | (xs[0] & xs[2]) | (xs[1] & xs[2]),
        (xs[0] | xs[1]) & (xs[0] | xs[2]) & (xs[1] | xs[2])} );


  for ( const auto& f : fs )
  {
    aig_graph aig;
    aig_initialize( aig );
    aig_create_po( aig, aig_from_bdd( aig, f ), "f0" );

    const char * inames[] = { "x0", "x1", "x2" };
    const char * onames[] = { "f0" };
    FILE * fp = fopen( "/tmp/test.blif", "w" );
    mgr.DumpBlif( {f}, (char**)inames, (char**)onames, 0, fp );
    fclose( fp );

    write_bench( aig, "/tmp/test.bench" );

    std::vector<std::string> result = execute_and_return( "abc -c \"cec /tmp/test.bench /tmp/test.blif\"" ).second;
    BOOST_CHECK( result.size() == 3u );
    BOOST_CHECK( result[2u] == "Networks are equivalent after structural hashing." );
  }
}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
