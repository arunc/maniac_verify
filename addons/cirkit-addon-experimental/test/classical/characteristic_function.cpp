/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE characteristic_function

#include <iostream>

#include <boost/test/unit_test.hpp>

#include <classical/dd/bdd.hpp>
#include <classical/dd/bdd_to_truth_table.hpp>
#include <classical/dd/characteristic.hpp>
#include <classical/dd/copy.hpp>

using namespace cirkit;

BOOST_AUTO_TEST_CASE(simple)
{
  bdd_manager mgr1( 2u, 10u );
  bdd_manager mgr2( 3u, 10u );

  BOOST_CHECK( mgr1.num_vars() == 2u );
  BOOST_CHECK( mgr2.num_vars() == 3u );

  bdd f1 = mgr1[0] && mgr1[1];

  bdd f2 = bdd_copy( f1, mgr2 );

  BOOST_CHECK( f2.equals( mgr2[1] && mgr2[2] ) );

  bdd chi = characteristic_function( {f1}, mgr2 );

  BOOST_CHECK( bdd_to_truth_table( f1 ) == tt( std::string( "1000100010001000100010001000100010001000100010001000100010001000" ) ) );
  BOOST_CHECK( bdd_to_truth_table( chi ) == tt( std::string( "1001010110010101100101011001010110010101100101011001010110010101" ) ) );
}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
