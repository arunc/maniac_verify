/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file cikit_bdd.cpp
 *
 * @brief Unit tests for cirkit BDD package
 *
 * @author Stefan Frehse
 * @author Mathias Soeken
 * @since  2.3
 */

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE cirkit_bdd

#include <classical/dd/bdd.hpp>
#include <classical/dd/arithmetic.hpp>
#include <classical/dd/count_solutions.hpp>
#include <classical/dd/visit_solutions.hpp>

#include <boost/dynamic_bitset.hpp>
#include <boost/format.hpp>
#include <boost/range/counting_range.hpp>
#include <boost/test/unit_test.hpp>

#include <vector>

using namespace cirkit;

namespace
{

class simple_boolean_functions
{
public:
  simple_boolean_functions ()
  {
    manager = bdd_manager::create( 2u, 10u, false );
  }

  bdd left()
  {
    return manager->bdd_var ( 0 );
  }

  bdd right()
  {
    return manager->bdd_var ( 1 );
  }

  bdd zero()
  {
    return manager->bdd_bot();
  }

  bdd one ()
  {
    return manager->bdd_top();
  }

protected:
  bdd_manager_ptr manager;
};

template<int N>
class boolean_functions
{
public:
  boolean_functions()
  {
    manager = bdd_manager::create( N, 10u, false );
  }

  void print_solutions( const bdd& func, const std::string& name )
  {
    std::cout << name << ":" << std::endl;
    visit_solutions( func, []( boost::dynamic_bitset<> bs ) { std::cout << bs << std::endl; } );
  }

  bdd cube( const std::string& s )
  {
    auto _cube = manager->bdd_top();
    auto invert = false;

    for ( char ch : s )
    {
      if ( ch == '!' ) { invert = !invert; }
      else
      {
        assert( ch >= 'a' && ch <= 'a' + N );
        if ( invert ) { _cube = _cube && !manager->bdd_var( ch - 'a' ); }
        else          { _cube = _cube &&  manager->bdd_var( ch - 'a' ); }
        invert = false;
      }
    }

    return _cube;
  }

  bdd func( const std::vector<std::string>& v )
  {
    auto _func = manager->bdd_bot();
    for ( const auto& s : v )
    {
      _func = _func || cube( s );
    }
    return _func;
  }

  bdd a() const { return manager->bdd_var( 0u ); }
  bdd b() const { return manager->bdd_var( 1u ); }
  bdd c() const { return manager->bdd_var( 2u ); }
  bdd d() const { return manager->bdd_var( 3u ); }
  bdd e() const { return manager->bdd_var( 4u ); }
  bdd f() const { return manager->bdd_var( 5u ); }

protected:
  bdd_manager_ptr manager;
};

} // anonymous namespace

visit_solutions_func push_visitor( std::vector<boost::dynamic_bitset<>>& v )
{
  return [&]( const boost::dynamic_bitset<>& x ) { v.push_back( x ); };
}

BOOST_FIXTURE_TEST_SUITE( And, simple_boolean_functions )
BOOST_AUTO_TEST_CASE(Simple)
{
  auto conj = left() && right();

  BOOST_CHECK ( !conj.is_bot() );
  BOOST_CHECK ( !conj.is_top() );

  BOOST_CHECK ( count_solutions( conj ) == 1 );

  /* visit */
  std::vector<boost::dynamic_bitset<>> solutions;
  visit_solutions( conj, push_visitor( solutions ) );

  BOOST_CHECK ( solutions.size() == 1u );
  BOOST_CHECK ( solutions[0] == boost::dynamic_bitset<>( 2u, 3u ) );
}

BOOST_AUTO_TEST_CASE(SimpleTrue)
{
  BOOST_CHECK ( count_solutions( left()  && zero() ) == 0 );
  BOOST_CHECK ( count_solutions( zero() && right() ) == 0 );
}

BOOST_AUTO_TEST_SUITE_END();

BOOST_FIXTURE_TEST_SUITE ( Or, simple_boolean_functions)
BOOST_AUTO_TEST_CASE(Simple)
{
  auto conj = left() || right();

  BOOST_CHECK ( !conj.is_bot() );
  BOOST_CHECK ( !conj.is_top() );

  BOOST_CHECK ( count_solutions( conj ) == 3 );

  /* visit */
  std::vector<boost::dynamic_bitset<>> solutions;
  visit_solutions( conj, push_visitor( solutions ) );

  BOOST_CHECK ( solutions.size() == 3u );
  BOOST_CHECK ( solutions[0] == boost::dynamic_bitset<>( 2u, 2u ) );
  BOOST_CHECK ( solutions[1] == boost::dynamic_bitset<>( 2u, 1u ) );
  BOOST_CHECK ( solutions[2] == boost::dynamic_bitset<>( 2u, 3u ) );
}

BOOST_AUTO_TEST_CASE(SimpleTrue)
{
  BOOST_CHECK ( count_solutions( left() || one () ) == 4u );
  BOOST_CHECK ( count_solutions( one() || right() ) == 4u );
}

BOOST_AUTO_TEST_SUITE_END();

BOOST_FIXTURE_TEST_SUITE ( Xor, simple_boolean_functions)
BOOST_AUTO_TEST_CASE(Simple)
{
  auto conj = left() ^ right();

  BOOST_CHECK ( !conj.is_bot() );
  BOOST_CHECK ( !conj.is_top() );

  BOOST_CHECK ( count_solutions( conj ) == 2 );

  /* visit */
  std::vector<boost::dynamic_bitset<>> solutions;
  visit_solutions( conj, push_visitor( solutions ) );

  BOOST_CHECK ( solutions.size() == 2u );
  BOOST_CHECK ( solutions[0] == boost::dynamic_bitset<>( 2u, 2u ) );
  BOOST_CHECK ( solutions[1] == boost::dynamic_bitset<>( 2u, 1u ) );
}

BOOST_AUTO_TEST_CASE(SimpleXnor)
{
  auto conj = !left() ^ right();

  BOOST_CHECK ( !conj.is_bot() );
  BOOST_CHECK ( !conj.is_top() );

  BOOST_CHECK ( count_solutions( conj ) == 2 );

  /* visit */
  std::vector<boost::dynamic_bitset<>> solutions;
  visit_solutions( conj, push_visitor( solutions ) );

  BOOST_CHECK ( solutions.size() == 2u );
  BOOST_CHECK ( solutions[0] == boost::dynamic_bitset<>( 2u, 0u ) );
  BOOST_CHECK ( solutions[1] == boost::dynamic_bitset<>( 2u, 3u ) );
}

BOOST_AUTO_TEST_CASE(SimpleXnor2)
{
  auto conj = !(left() ^ right());

  BOOST_CHECK ( !conj.is_bot() );
  BOOST_CHECK ( !conj.is_top() );

  BOOST_CHECK ( count_solutions( conj ) == 2 );

  /* visit */
  std::vector<boost::dynamic_bitset<>> solutions;
  visit_solutions( conj, push_visitor( solutions ) );

  BOOST_CHECK ( solutions.size() == 2u );
  BOOST_CHECK ( solutions[0] == boost::dynamic_bitset<>( 2u, 0u ) );
  BOOST_CHECK ( solutions[1] == boost::dynamic_bitset<>( 2u, 3u ) );
}

BOOST_AUTO_TEST_CASE(SimpleTrue)
{
  BOOST_CHECK ( count_solutions( left() ^  one() ) == 2u );
  BOOST_CHECK ( count_solutions( one() ^ right() ) == 2u );
}

BOOST_AUTO_TEST_SUITE_END();

BOOST_FIXTURE_TEST_SUITE( ComplexOperations, boolean_functions<6> )

BOOST_AUTO_TEST_CASE(Exists)
{
  auto f1 = cube( "a!bc" ) || cube( "b!cd" );
  auto f2 = cube( "ac" ) || cube( "!cd" );
  auto f3 = f1.exists( manager->bdd_var( 1 ) );

  BOOST_CHECK( f2.index == f3.index );
}

BOOST_AUTO_TEST_CASE(Constrain)
{
  auto f1 = func( { "abcdef", "a!bc!def", "!a!b!c!d!ef" } );
  auto f2 = f1.cof0( 1 ).cof1( 2 ).cof0( 3 ).cof1( 4 );
  auto f3 = f1.constrain( cube( "!bc!de" ) );

  BOOST_CHECK( f2.index == f3.index );
}

BOOST_AUTO_TEST_SUITE_END();

BOOST_FIXTURE_TEST_SUITE( ArithmeticOperations, boolean_functions<6> )

BOOST_AUTO_TEST_CASE(Subtract)
{
  auto f1 = func( { "a!bc", "b!cd", "a!c" } );
  auto f2 = func( { "bd", "!ae", "ce" } );
  auto f3 = func( { "abcd", "cd!e", "d!f" } );

  auto diff = bdd_subtract( {f1, f2, f3}, {f1, f2, f3} );

  BOOST_CHECK( diff.size() == 3u );
  for ( const auto& d : diff )
  {
    BOOST_CHECK( d.index == 0u );
  }
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_FIXTURE_TEST_SUITE( ArithmeticOperations2, boolean_functions<2> )

BOOST_AUTO_TEST_CASE(Subtract2)
{
  std::vector<bdd> binary_functions =
    { manager->bdd_bot(), a() && b(), !a() && b(), b(),
      a() && !b(), a(), a() ^ b(), a() || b(),
      !a() && !b(), !a() ^ b(), !a(), !a() || b(),
      !b(), a() || !b(), !a() || !b(), manager->bdd_top() };

  for ( auto i : boost::counting_range( 0u, 16u ) )
  {
    boost::dynamic_bitset<> bs( 4u );
    visit_solutions( binary_functions[i], [&]( boost::dynamic_bitset<> sol ) { bs.set( 3ul - sol.to_ulong() ); } );
    BOOST_CHECK( i == bs.to_ulong() );
  }

  auto from_ids = [&]( const std::vector<unsigned>& ids ) {
    std::vector<bdd> fs;
    for ( auto id : ids ) { fs.push_back( binary_functions[id] ); }
    return fs;
  };

  std::vector<unsigned> fids = {4u, 14u, 3u, 9u, 4u};
  auto ff = sign_extend( from_ids( fids ), 6u );

  std::vector<unsigned> gids = {13u, 2u, 3u, 0u, 12u};
  auto gg = sign_extend( from_ids( gids ), 6u );

  std::vector<unsigned> dids = {9u, 5u, 1u, 8u, 8u, 0u};

  /* difference */
  auto diff = bdd_subtract( ff, gg );

  BOOST_CHECK( diff.size() == dids.size() );
  for ( auto i : boost::counting_range( 0u, (unsigned)dids.size() ) )
  {
    BOOST_CHECK( ff[i].index == binary_functions[fids[std::min( i, (unsigned)fids.size() - 1 )]].index );
    BOOST_CHECK( gg[i].index == binary_functions[gids[std::min( i, (unsigned)fids.size() - 1 )]].index );
    BOOST_CHECK( diff[i].index == binary_functions[dids[i]].index );
  }

  diff = bdd_abs( diff );
  BOOST_CHECK( diff.size() == dids.size() );

  for ( auto i : boost::counting_range( 0u, (unsigned)dids.size() ) )
  {
    BOOST_CHECK( diff[i].index == binary_functions[dids[i]].index );
  }

  /* absolute of F */
  ff = bdd_abs( ff );
  BOOST_CHECK( ff.size() == 6u );

  std::vector<unsigned> new_fids = {4u, 10u, 7u, 13u, 0u, 0u};

  for ( auto i : boost::counting_range( 0u, 6u ) )
  {
    BOOST_CHECK( ff[i].index == binary_functions[new_fids[i]].index );
  }
}

BOOST_AUTO_TEST_SUITE_END();

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
