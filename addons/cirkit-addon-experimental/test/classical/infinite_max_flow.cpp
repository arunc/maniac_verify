/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE infinite_max_flow

#define BOOST_ENABLE_ASSERT_HANDLER

#include <fstream>
#include <iostream>
#include <string>

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/boykov_kolmogorov_max_flow.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>
#include <boost/graph/read_dimacs.hpp>
#include <boost/graph/graph_utility.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/test/execution_monitor.hpp>
#include <boost/test/unit_test.hpp>

using namespace boost;

void boost::assertion_failed(char const * expr, char const * function, char const * file, long line)
{
  throw std::runtime_error( "assertion_failed" );
}

/**
 * @brief Checks whether there is a path in `g' that contains
 *        only edges with infinity capacity.
 */
template<typename Graph>
bool has_infinity_path( const Graph& g,
                        const typename graph_traits<Graph>::vertex_descriptor& s,
                        const typename graph_traits<Graph>::vertex_descriptor& t )
{
  typedef typename graph_traits<Graph>::vertex_descriptor vertex_t;
  typedef typename graph_traits<Graph>::edge_descriptor edge_t;

  auto capacity = get( edge_capacity, g );
  std::map<edge_t, double> weight;
  std::map<vertex_t, double> distance;

  for ( const auto& e : boost::make_iterator_range( edges( g ) ) )
  {
    if ( capacity[e] == 0.0 )
    {
      weight[e] = std::numeric_limits<double>::infinity();
    }
    else if ( capacity[e] == 1.0 )
    {
      weight[e] = 1.0;
    }
    else if ( capacity[e] == std::numeric_limits<double>::infinity() )
    {
      weight[e] = 0.0;
    }
    else
    {
      assert( false );
    }
  }

  dijkstra_shortest_paths( g, s, weight_map( make_assoc_property_map( weight ) ).distance_map( make_assoc_property_map( distance ) ) );

  return distance[t] == 0.0;
}

BOOST_AUTO_TEST_CASE(simple)
{
  using boost::unit_test::framework::master_test_suite;

  if ( master_test_suite().argc != 2u ) return;

  typedef adjacency_list_traits <vecS, vecS, directedS> traits_t;
  typedef adjacency_list <vecS, vecS, directedS,
    property<vertex_name_t, std::string,
    property<vertex_index_t, long,
    property<vertex_color_t, boost::default_color_type,
    property<vertex_distance_t, long,
    property<vertex_predecessor_t, traits_t::edge_descriptor>>>>>,
    property<edge_capacity_t, double,
    property<edge_residual_capacity_t, double,
    property<edge_reverse_t, traits_t::edge_descriptor>>>> graph_t;

  graph_t g;
  auto capacity          = get( edge_capacity, g );
  auto residual_capacity = get( edge_residual_capacity, g );
  auto rev               = get( edge_reverse, g );

  std::map<traits_t::edge_descriptor, long> file_capacity;

  std::ifstream in( master_test_suite().argv[1] );

  traits_t::vertex_descriptor s, t;
  read_dimacs_max_flow( g, make_assoc_property_map( file_capacity ), rev, s, t, in );

  for ( const auto& p : file_capacity )
  {
    capacity[p.first] = p.second == 0l ? 0.0 : std::numeric_limits<double>::infinity();
  }

  BOOST_CHECK( has_infinity_path( g, s, t ) == true );

  BOOST_REQUIRE_THROW( boykov_kolmogorov_max_flow( g, s, t ), std::runtime_error );
}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
