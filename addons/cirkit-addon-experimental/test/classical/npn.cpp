/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE npn

#include <fstream>
#include <iostream>
#include <random>

#include <boost/format.hpp>
#include <boost/test/unit_test.hpp>

#include <core/utils/bitset_utils.hpp>
#include <core/utils/timer.hpp>
#include <classical/functions/npn_canonization.hpp>
#include <classical/utils/truth_table_utils.hpp>

BOOST_AUTO_TEST_CASE(simple)
{
  using namespace cirkit;

  /* Parameters */
  auto num_tables = 1000u;
  auto bitwidth = 4u;

  /* Random number generator */
  auto seed = 42u;
  std::default_random_engine generator( seed );

  auto runtime_npn = 0.0;
  auto runtime_npn_exact = 0.0;

  std::ofstream os( "/tmp/test.npn", std::ofstream::out );

  for ( auto i = 0u; i < num_tables; ++i )
  {
    auto t = random_bitset( 1 << bitwidth, generator );
    os << "0x" << tt_to_hex( t ) << std::endl;

    BOOST_CHECK( t.size() == 16u );

    tt npn, npn_exact;
    boost::dynamic_bitset<> phase, phase_exact;
    std::vector<unsigned> perm, perm_exact;
    {
      increment_timer tim( &runtime_npn );
      npn = npn_canonization( t, phase, perm );
      BOOST_CHECK( npn.size() == 16u );
      BOOST_CHECK( perm.size() == 4u );
      BOOST_CHECK( phase.size() == 5u );
    }
    {
      increment_timer tim( &runtime_npn_exact );
      npn_exact = exact_npn_canonization( t, phase_exact, perm_exact );
      BOOST_CHECK( npn_exact.size() == 16u );
      BOOST_CHECK( perm_exact.size() == 4u );
      BOOST_CHECK( phase_exact.size() == 5u );
    }

    auto tcopy = tt_from_npn( npn, phase, perm );
    auto tcopy_exact = tt_from_npn( npn_exact, phase_exact, perm_exact );

    BOOST_CHECK( tcopy.size() == 16u );
    BOOST_CHECK( tcopy_exact.size() == 16u );

    BOOST_CHECK( t == tcopy );
    BOOST_CHECK( t == tcopy_exact );
  }

  os.close();

  std::cout << boost::format( "Runtime (canonization): %.2f secs" ) % runtime_npn << std::endl;
  std::cout << boost::format( "Runtime (exact canonization): %.2f secs" ) % runtime_npn_exact << std::endl;
}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
