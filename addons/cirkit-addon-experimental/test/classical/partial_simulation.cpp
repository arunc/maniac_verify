/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE partial_simulation

#include <iostream>

#include <vector>

#include <boost/test/unit_test.hpp>

#include <classical/aig.hpp>
#include <classical/functions/simulate_aig.hpp>
#include <classical/utils/aig_from_bdd.hpp>

#include <cuddObj.hh>

BOOST_AUTO_TEST_CASE(simple)
{
  using namespace cirkit;

  Cudd mgr;

  std::vector<BDD> xs( 3u );
  boost::generate( xs, [&mgr]() { return mgr.bddVar(); } );

  /* majority function */
  BDD f = (xs[0] & xs[1]) | (xs[0] & xs[2]) | (xs[1] & xs[2]);

  aig_graph aig;
  aig_initialize( aig );
  const auto& graph_info = boost::get_property( aig, boost::graph_name );

  aig_function af = aig_from_bdd( aig, f );

  bdd_simulator bs( mgr );
  tt_simulator  ts;

  BOOST_CHECK( simulate_aig_function( aig, af, bs ) == f );
  BOOST_CHECK( simulate_aig_function( aig, af, ts ) == boost::dynamic_bitset<>( std::string( "11101000" ) ) );

  std::map<aig_node, BDD> bassignment( {{graph_info.inputs[0], mgr.bddZero()}} );
  partial_simulator<BDD> pbs( bs, bassignment, aig );

  std::map<aig_node, tt> tassignment( {{graph_info.inputs[0], tt_const0()}} );
  partial_simulator<tt> pts( ts, tassignment, aig );

  BOOST_CHECK( simulate_aig_function( aig, af, pbs ) == ( xs[0u] & xs[1u] ) );
  BOOST_CHECK( simulate_aig_function( aig, af, pts ) == boost::dynamic_bitset<>( std::string( "1000" ) ) );
}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
