/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @file dd_manager.hpp
 *
 * @brief Unit tests for build cirkit BDD from pla file
 *
 * @author Stefan Frehse
 * @since  2.3
 */

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE read_pla_into_cirkit_bdd

#include <classical/io/read_pla_to_cirkit_bdd.hpp>
#include <classical/dd/count_solutions.hpp>

#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_CASE(FileDoesNotExistReturnsFalse)
{
  using namespace cirkit;
  auto filename = "there/is/no/file";

  auto res = read_pla_into_cirkit_bdd( filename );
  BOOST_CHECK ( !res );
}

BOOST_AUTO_TEST_CASE(FileExist)
{
  using namespace cirkit;
  auto filename = boost::filesystem::path ( DATA_PATH ) / "test/example.pla";

  auto res = read_pla_into_cirkit_bdd( filename );
  BOOST_CHECK ( res );

  auto manager = res->manager ();
  std::cout << *manager.get() << std::endl;
}

BOOST_AUTO_TEST_CASE(BuiltUpBdd)
{
  using namespace cirkit;
  auto filename = boost::filesystem::path ( DATA_PATH ) / "test/apex5.pla";

  auto res = read_pla_into_cirkit_bdd( filename );
  BOOST_CHECK ( res );
}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
