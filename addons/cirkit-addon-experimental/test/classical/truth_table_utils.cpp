/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE truth_table_utils

#include <iostream>

#include <boost/test/unit_test.hpp>

#include <classical/utils/truth_table_utils.hpp>

BOOST_AUTO_TEST_CASE(simple)
{
  using namespace cirkit;

  tt a = tt_nth_var( 0u );
  tt b = tt_nth_var( 1u );
  tt c = tt_nth_var( 2u );

  BOOST_CHECK( a.size() == 64u );
  BOOST_CHECK( b.size() == 64u );
  BOOST_CHECK( c.size() == 64u );

  BOOST_CHECK( a == tt( std::string( "1010101010101010101010101010101010101010101010101010101010101010" ) ) );
  BOOST_CHECK( b == tt( std::string( "1100110011001100110011001100110011001100110011001100110011001100" ) ) );
  BOOST_CHECK( c == tt( std::string( "1111000011110000111100001111000011110000111100001111000011110000" ) ) );
  BOOST_CHECK( tt_to_hex( a ) == "AAAAAAAAAAAAAAAA" );
  BOOST_CHECK( tt_to_hex( b ) == "CCCCCCCCCCCCCCCC" );
  BOOST_CHECK( tt_to_hex( c ) == "F0F0F0F0F0F0F0F0" );

  BOOST_CHECK( tt_has_var( a, 0u ) == true );
  BOOST_CHECK( tt_has_var( a, 1u ) == false );
  BOOST_CHECK( tt_has_var( a, 2u ) == false );

  BOOST_CHECK( tt_has_var( b, 0u ) == false );
  BOOST_CHECK( tt_has_var( b, 1u ) == true );
  BOOST_CHECK( tt_has_var( b, 2u ) == false );

  BOOST_CHECK( tt_has_var( c, 0u ) == false );
  BOOST_CHECK( tt_has_var( c, 1u ) == false );
  BOOST_CHECK( tt_has_var( c, 2u ) == true );

  BOOST_CHECK( ( a & c ) == tt( std::string( "1010000010100000101000001010000010100000101000001010000010100000" ) ) );
  BOOST_CHECK( ( a & b ) == tt( std::string( "1000100010001000100010001000100010001000100010001000100010001000" ) ) );
  BOOST_CHECK( ( a | b | c ) == tt( std::string( "1111111011111110111111101111111011111110111111101111111011111110" ) ) );
  BOOST_CHECK( ~(a & b) == tt( std::string( "0111011101110111011101110111011101110111011101110111011101110111" ) ) );
  BOOST_CHECK( tt_to_hex( a & c ) == "A0A0A0A0A0A0A0A0" );
  BOOST_CHECK( tt_to_hex( a & b ) == "8888888888888888" );
  BOOST_CHECK( tt_to_hex( a | b | c ) == "FEFEFEFEFEFEFEFE" );
  BOOST_CHECK( tt_to_hex( ~(a & b) ) == "7777777777777777" );

  BOOST_CHECK( tt_support( a & c ) == boost::dynamic_bitset<>( std::string( "000101" ) ) );
  BOOST_CHECK( tt_support( a & b ) == boost::dynamic_bitset<>( std::string( "000011" ) ) );
  BOOST_CHECK( tt_support( a | b | c ) == boost::dynamic_bitset<>( std::string( "000111" ) ) );
  BOOST_CHECK( tt_support( ~(a & b) ) == boost::dynamic_bitset<>( std::string( "000011" ) ) );

  BOOST_CHECK( tt_has_var( a & c, 0u ) == true );
  BOOST_CHECK( tt_has_var( a & c, 1u ) == false );
  BOOST_CHECK( tt_has_var( a & c, 2u ) == true );

  BOOST_CHECK( tt_cofs_opposite( a & b, 0u ) == false );
  BOOST_CHECK( tt_cofs_opposite( a ^ b, 0u ) == true );

  /* x < y */
  tt x0 = tt_nth_var( 3u );
  tt x1 = tt_nth_var( 2u );
  tt y0 = tt_nth_var( 1u );
  tt y1 = tt_nth_var( 0u );

  tt_extend( x1, 4u );
  tt_extend( y0, 4u );
  tt_extend( y1, 4u );

  tt f = ( ~x0 & y0 ) | ( ~(x0 ^ y0) & ~x1 & y1 );

  BOOST_CHECK( f == tt( std::string( "0000100011001110000010001100111000001000110011100000100011001110" ) ) );

  BOOST_CHECK( tt_cof0( f, 3u ) == tt( std::string( "1100111011001110110011101100111011001110110011101100111011001110" ) ) );
  BOOST_CHECK( tt_cof1( f, 3u ) == tt( std::string( "0000100000001000000010000000100000001000000010000000100000001000" ) ) );
  BOOST_CHECK( tt_cof0( f, 2u ) == tt( std::string( "1000100011101110100010001110111010001000111011101000100011101110" ) ) );
  BOOST_CHECK( tt_cof1( f, 2u ) == tt( std::string( "0000000011001100000000001100110000000000110011000000000011001100" ) ) );
  BOOST_CHECK( tt_cof0( f, 1u ) == tt( std::string( "0000000000001010000000000000101000000000000010100000000000001010" ) ) );
  BOOST_CHECK( tt_cof1( f, 1u ) == tt( std::string( "0000101011111111000010101111111100001010111111110000101011111111" ) ) );
  BOOST_CHECK( tt_cof0( f, 0u ) == tt( std::string( "0000000011001100000000001100110000000000110011000000000011001100" ) ) );
  BOOST_CHECK( tt_cof1( f, 0u ) == tt( std::string( "0000110011001111000011001100111100001100110011110000110011001111" ) ) );

  /* Permute */
  BOOST_CHECK( tt_permute( a | c, 1u, 2u ) == ( a | b ) );
  BOOST_CHECK( tt_permute( a | c, 0u, 1u ) == ( b | c ) );
  BOOST_CHECK( tt_permute( tt_permute( f, 0u, 2u ), 1u, 3u ) == ( ( ~y0 & x0 ) | ( ~(y0 ^ x0) & ~y1 & x1 ) ) );

  tt mb1 = a & c;    tt_to_minbase( mb1 );
  tt mb2 = b & c;    tt_to_minbase( mb2 );
  tt mb3 = y0 & x0;  tt_to_minbase( mb3 );
  BOOST_CHECK( mb1 == tt( std::string( "1000" ) ) );
  BOOST_CHECK( mb2 == tt( std::string( "1000" ) ) );
  BOOST_CHECK( mb3 == tt( std::string( "1000" ) ) );

  tt_from_minbase( mb1, boost::dynamic_bitset<>( std::string( "101" ) ) );
  tt_from_minbase( mb2, boost::dynamic_bitset<>( std::string( "110" ) ) );
  tt_from_minbase( mb3, boost::dynamic_bitset<>( std::string( "1010" ) ) );
  BOOST_CHECK( mb1 == ( a & c ) );
  BOOST_CHECK( mb2 == ( b & c ) );
  BOOST_CHECK( mb3 == ( y0 & x0 ) );

  /* Co-factors */
  BOOST_CHECK( tt_exists( a & c, 0u ) == c );
  BOOST_CHECK( tt_exists( a & c, 1u ) == ( a & c ) );
  BOOST_CHECK( tt_exists( a & c, 2u ) == a );

  /* Some tests */
  typedef boost::dynamic_bitset<> pattern_t;
  auto func = []( const pattern_t& a, const pattern_t& b ) {
    pattern_t p( a.count() );
    unsigned bpos = 0u;
    pattern_t::size_type pos = a.find_first();
    while ( pos != pattern_t::npos )
    {
      p[bpos++] = b[pos];
      pos = a.find_next( pos );
    }
    return p;
  };

  BOOST_CHECK( func( pattern_t( std::string( "01011010" ) ), pattern_t( std::string( "00011000" ) ) ) == pattern_t( std::string( "0110" ) ) );
  BOOST_CHECK( func( pattern_t( std::string( "11011100" ) ), pattern_t( std::string( "01010100" ) ) ) == pattern_t( std::string( "01101" ) ) );
}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
