/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE word_level_operations

#include <boost/test/unit_test.hpp>

#if ADDON_EXPERIMENTAL

#include <classical/aig_word.hpp>
#include <classical/utils/simulate_aig.hpp>
// #include <classical/io/write_aiger.hpp>

#include <boost/format.hpp>

#include <iostream>

namespace cirkit
{

boost::dynamic_bitset<> tt_word( const std::vector< tt >& v, const unsigned index )
{
  assert( v.size() > 0 );
  tt r(v[0].size());
  for ( unsigned i = 0u; i < v.size(); ++i )
  {
    r[i] = v[i][index];
  }
  return r;
}

}

BOOST_AUTO_TEST_CASE(equal)
{
  using namespace cirkit;

  const unsigned char width = 8u;
  for ( unsigned char w = 1u; w <= width; ++w ) {
    aig_graph aig;
    aig_initialize( aig );

    aig_word x = aig_create_wi( aig, w, "x" );
    aig_word y = aig_create_wi( aig, w, "y" );

    aig_function eq = aig_create_equal( aig, x, y );
    aig_create_po( aig, eq, "out" );

    // write_aiger( aig, std::cerr );

    auto values = simulate_aig( aig, tt_simulator() );

    /* make vars */
    std::vector< tt > x_vars, y_vars;
    for ( unsigned i = 0u; i < w; ++i )
    {
      x_vars.push_back( tt_nth_var(i) );
      y_vars.push_back( tt_nth_var(w+i) );
    }

    /* align */
    for ( unsigned i = 0u; i < w; ++i )
    {
      tt_align( x_vars[i], y_vars[w-1u] );
      tt_align( y_vars[i], y_vars[w-1u] );
    }

    /* compare bitwise */
    for ( unsigned i = 0u; i < (2u << w); ++i )
    {
      BOOST_CHECK( ( tt_word( x_vars, i ).to_ulong() == tt_word( y_vars, i ).to_ulong() ) == values[eq][i] );
    }
  }
}

BOOST_AUTO_TEST_CASE(bvult)
{
  using namespace cirkit;

  const unsigned char width = 8u;
  for ( unsigned char w = 1u; w < width; ++w )
  {
    aig_graph aig;
    aig_initialize( aig );

    aig_word x = aig_create_wi( aig, w, "x" );
    aig_word y = aig_create_wi( aig, w, "y" );

    aig_function ult = aig_create_bvult( aig, x, y );
    aig_create_po( aig, ult, "out" );

    // write_aiger( aig, std::cerr );

    auto values = simulate_aig( aig, tt_simulator() );

    /* make vars */
    std::vector< tt > x_vars, y_vars;
    for ( unsigned i = 0u; i < w; ++i )
    {
      x_vars.push_back( tt_nth_var(i) );
      y_vars.push_back( tt_nth_var(w+i) );
    }

    /* align */
    for ( unsigned i = 0u; i < w; ++i )
    {
      tt_align( x_vars[i], y_vars[w-1u] );
      tt_align( y_vars[i], y_vars[w-1u] );
    }

    /* compare bitwise */
    for ( unsigned i = 0u; i < (2u << w); ++i )
    {
      BOOST_CHECK( ( tt_word( x_vars, i ).to_ulong() < tt_word( y_vars, i ).to_ulong() ) == values[ult][i] );
    }
  }
}

BOOST_AUTO_TEST_CASE(bvule)
{
  using namespace cirkit;

  const unsigned char width = 8u;
  for ( unsigned char w = 1u; w < width; ++w )
  {
    aig_graph aig;
    aig_initialize( aig );

    aig_word x = aig_create_wi( aig, w, "x" );
    aig_word y = aig_create_wi( aig, w, "y" );

    aig_function ule = aig_create_bvule( aig, x, y );
    aig_create_po( aig, ule, "out" );

    // write_aiger( aig, std::cerr );

    auto values = simulate_aig( aig, tt_simulator() );

    /* make vars */
    std::vector< tt > x_vars, y_vars;
    for ( unsigned i = 0u; i < w; ++i )
    {
      x_vars.push_back( tt_nth_var(i) );
      y_vars.push_back( tt_nth_var(w+i) );
    }

    /* align */
    for ( unsigned i = 0u; i < w; ++i )
    {
      tt_align( x_vars[i], y_vars[w-1u] );
      tt_align( y_vars[i], y_vars[w-1u] );
    }

    /* compare bitwise */
    for ( unsigned i = 0u; i < (2u << w); ++i )
    {
      BOOST_CHECK( ( tt_word( x_vars, i ).to_ulong() <= tt_word( y_vars, i ).to_ulong() ) == values[ule][i] );
    }
  }
}

BOOST_AUTO_TEST_CASE(bvuge)
{
  using namespace cirkit;

  const unsigned char width = 8u;
  for ( unsigned char w = 1u; w < width; ++w )
  {
    aig_graph aig;
    aig_initialize( aig );

    aig_word x = aig_create_wi( aig, w, "x" );
    aig_word y = aig_create_wi( aig, w, "y" );

    aig_function uge = aig_create_bvuge( aig, x, y );
    aig_create_po( aig, uge, "out" );

    // write_aiger( aig, std::cerr );

    auto values = simulate_aig( aig, tt_simulator() );

    /* make vars */
    std::vector< tt > x_vars, y_vars;
    for ( unsigned i = 0u; i < w; ++i )
    {
      x_vars.push_back( tt_nth_var(i) );
      y_vars.push_back( tt_nth_var(w+i) );
    }

    /* align */
    for ( unsigned i = 0u; i < w; ++i )
    {
      tt_align( x_vars[i], y_vars[w-1u] );
      tt_align( y_vars[i], y_vars[w-1u] );
    }

    /* compare bitwise */
    for ( unsigned i = 0u; i < (2u << w); ++i )
    {
      BOOST_CHECK( ( tt_word( x_vars, i ).to_ulong() >= tt_word( y_vars, i ).to_ulong() ) == values[uge][i] );
    }
  }
}

BOOST_AUTO_TEST_CASE(bvugt)
{
  using namespace cirkit;

  const unsigned char width = 8u;
  for ( unsigned char w = 1u; w < width; ++w )
  {
    aig_graph aig;
    aig_initialize( aig );

    aig_word x = aig_create_wi( aig, w, "x" );
    aig_word y = aig_create_wi( aig, w, "y" );

    aig_function ugt = aig_create_bvugt( aig, x, y );
    aig_create_po( aig, ugt, "out" );

    // write_aiger( aig, std::cerr );

    auto values = simulate_aig( aig, tt_simulator() );

    /* make vars */
    std::vector< tt > x_vars, y_vars;
    for ( unsigned i = 0u; i < w; ++i )
    {
      x_vars.push_back( tt_nth_var(i) );
      y_vars.push_back( tt_nth_var(w+i) );
    }

    /* align */
    for ( unsigned i = 0u; i < w; ++i )
    {
      tt_align( x_vars[i], y_vars[w-1u] );
      tt_align( y_vars[i], y_vars[w-1u] );
    }

    /* compare bitwise */
    for ( unsigned i = 0u; i < (2u << w); ++i )
    {
      BOOST_CHECK( ( tt_word( x_vars, i ).to_ulong() > tt_word( y_vars, i ).to_ulong() ) == values[ugt][i] );
    }
  }
}

BOOST_AUTO_TEST_CASE(bvadd)
{
  using namespace cirkit;

  const unsigned char width = 8u;
  for ( unsigned char w = 1u; w < width; ++w )
  {
    aig_graph aig;
    aig_initialize( aig );

    aig_word x = aig_create_wi( aig, w, "x" );
    aig_word y = aig_create_wi( aig, w, "y" );

    aig_word sum = aig_create_bvadd( aig, x, y );
    aig_create_wo( aig, sum, "out" );

    // write_aiger( aig, std::cerr );

    auto values = simulate_aig( aig, tt_simulator() );

    /* make vars */
    std::vector< tt > x_vars, y_vars;
    for ( unsigned i = 0u; i < w; ++i )
    {
      x_vars.push_back( tt_nth_var(i) );
      y_vars.push_back( tt_nth_var(w+i) );
    }

    /* align */
    for ( unsigned i = 0u; i < w; ++i )
    {
      tt_align( x_vars[i], y_vars[w-1u] );
      tt_align( y_vars[i], y_vars[w-1u] );
      tt_align( values[ sum[i] ], y_vars[w-1u] );
    }

    // foreach assignment
    for ( unsigned j = 0u; j < ( 2u << w ); ++j )
    {
      // foreach aig_function
      const unsigned tt_result = ( ( tt_word( x_vars, j ).to_ulong() + tt_word( y_vars, j ).to_ulong() ) % ( (2u << (w-1)) ) );

      boost::dynamic_bitset<> bs( w );
      for ( unsigned i = 0u; i < w; ++i )
      {
        bs[i] = values[ sum[i] ][ j ];
      }

      BOOST_CHECK( bs.to_ulong() == tt_result );
    }
  }
}

BOOST_AUTO_TEST_CASE(bvsub)
{
  using namespace cirkit;

  const unsigned char width = 8u;
  for ( unsigned char w = 1u; w < width; ++w )
  {
    aig_graph aig;
    aig_initialize( aig );

    aig_word x = aig_create_wi( aig, w, "x" );
    aig_word y = aig_create_wi( aig, w, "y" );

    aig_word sum = aig_create_bvsub( aig, x, y );
    aig_create_wo( aig, sum, "out" );

    // write_aiger( aig, std::cerr );

    auto values = simulate_aig( aig, tt_simulator() );

    /* make vars */
    std::vector< tt > x_vars, y_vars;
    for ( unsigned i = 0u; i < w; ++i )
    {
      x_vars.push_back( tt_nth_var(i) );
      y_vars.push_back( tt_nth_var(w+i) );
    }

    /* align */
    for ( unsigned i = 0u; i < w; ++i )
    {
      tt_align( x_vars[i], y_vars[w-1u] );
      tt_align( y_vars[i], y_vars[w-1u] );
      tt_align( values[ sum[i] ], y_vars[w-1u] );
    }

    // foreach assignment
    for ( unsigned j = 0u; j < ( 2u << w ); ++j )
    {
      // foreach aig_function
      const unsigned tt_result = ( ( tt_word( x_vars, j ).to_ulong() - tt_word( y_vars, j ).to_ulong() ) % ( (2u << (w-1)) ) );

      boost::dynamic_bitset<> bs( w );
      for ( unsigned i = 0u; i < w; ++i )
      {
        bs[i] = values[ sum[i] ][ j ];
      }

      BOOST_CHECK( bs.to_ulong() == tt_result );
    }
  }
}

BOOST_AUTO_TEST_CASE(bvmul)
{
  using namespace cirkit;

  const unsigned char width = 8u;
  for ( unsigned char w = 1u; w < width; ++w )
  {
    aig_graph aig;
    aig_initialize( aig );

    aig_word x = aig_create_wi( aig, w, "x" );
    aig_word y = aig_create_wi( aig, w, "y" );

    aig_word mul = aig_create_bvmul( aig, x, y );
    aig_create_wo( aig, mul, "out" );

    // write_aiger( aig, std::cerr );

    auto values = simulate_aig( aig, tt_simulator() );

    /* make vars */
    std::vector< tt > x_vars, y_vars;
    for ( unsigned i = 0u; i < w; ++i )
    {
      x_vars.push_back( tt_nth_var(i) );
      y_vars.push_back( tt_nth_var(w+i) );
    }

    /* align */
    for ( unsigned i = 0u; i < w; ++i )
    {
      tt_align( x_vars[i], y_vars[w-1u] );
      tt_align( y_vars[i], y_vars[w-1u] );
      tt_align( values[ mul[i] ], y_vars[w-1u] );
    }

    // foreach assignment
    for ( unsigned j = 0u; j < ( 2u << w ); ++j )
    {
      // foreach aig_function
      const unsigned tt_result = ( ( tt_word( x_vars, j ).to_ulong() * tt_word( y_vars, j ).to_ulong() ) % ( (2u << (w-1)) ) );

      boost::dynamic_bitset<> bs( w );
      for ( unsigned i = 0u; i < w; ++i )
      {
        bs[i] = values[ mul[i] ][ j ];
      }

      BOOST_CHECK( bs.to_ulong() == tt_result );
    }
  }
}

#else

BOOST_AUTO_TEST_CASE(equal) {}
BOOST_AUTO_TEST_CASE(bvult) {}
BOOST_AUTO_TEST_CASE(bvule) {}
BOOST_AUTO_TEST_CASE(bvuge) {}
BOOST_AUTO_TEST_CASE(bvugt) {}
BOOST_AUTO_TEST_CASE(bvadd) {}
BOOST_AUTO_TEST_CASE(bvsub) {}
BOOST_AUTO_TEST_CASE(bvmul) {}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
