/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE zdd

#include <iostream>

#include <boost/test/unit_test.hpp>

#include <classical/dd/zdd.hpp>
#include <classical/dd/zdd_from_sets.hpp>

using namespace cirkit;

BOOST_AUTO_TEST_CASE(simple)
{
  zdd_manager mgr( 3u, 10u );

  auto x0 = mgr.zdd_var( 0u );
  auto x1 = mgr.zdd_var( 1u );
  auto x2 = mgr.zdd_var( 2u );

  auto f1 = zdd_from_sets( mgr, set_family<unsigned>( {{0u, 2u}, {1u}} ) );
  auto f2 = f1 && x1;

  BOOST_CHECK( f2.index == x1.index );

  std::cout << f1 << std::endl;

  std::cout << mgr << std::endl;
}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
