/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE graphs

#include <boost/test/unit_test.hpp>
#include <boost/graph/bipartite.hpp>
#include <boost/graph/graphviz.hpp>

#if ADDON_EXPERIMENTAL

#include <iostream>
#include <fstream>

#include <boost/graph/adjacency_list.hpp>

#include <core/io/read_graph_file.hpp>
#include <formal/graph/bipartite_subgraph_isomorphism.hpp>
#include <formal/sat/cnf_solver.hpp>

BOOST_AUTO_TEST_CASE(simple)
{
  using namespace cirkit;

  typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS> graph_t;

  graph_t target, pattern;
  read_graph_file( target, "../addons/cirkit-addon-experimental/examples/target.txt" );
  read_graph_file( pattern, "../addons/cirkit-addon-experimental/examples/pattern.txt" );

  BOOST_CHECK( boost::num_vertices( target ) == 7u );
  BOOST_CHECK( boost::num_edges( target ) == 7u );
  BOOST_CHECK( boost::is_bipartite( target ) );

  BOOST_CHECK( boost::num_vertices( pattern ) == 5u );
  BOOST_CHECK( boost::num_edges( pattern ) == 5u );
  BOOST_CHECK( boost::is_bipartite( pattern ) );

  bipartite_subgraph_isomorphism<cnf_solver>( target, pattern, 4u, 3u );
}

#else

BOOST_AUTO_TEST_CASE(simple) {}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
