/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE executables

#include <fstream>

#include <boost/test/unit_test.hpp>

#include <formal/sat/utils/solver_executables.hpp>

BOOST_AUTO_TEST_CASE(simple)
{
  using namespace cirkit;

  std::ofstream os( "/tmp/test.cnf", std::ofstream::out );
  os << "p cnf 3 7" << std::endl
     << "-1 -2 -3 0" << std::endl
     << "-1 -2 3 0" << std::endl
     << "-1 2 -3 0" << std::endl
     << "-1 2 3 0" << std::endl
     << "1 -2 -3 0" << std::endl
     << "1 2 -3 0" << std::endl
     << "1 2 3 0" << std::endl;
  os.close();

  std::ofstream os2( "/tmp/test-unsat.cnf", std::ofstream::out );
  os2 << "p cnf 1 2" << std::endl
      << "1 0" << std::endl
      << "-1 0" << std::endl;
  os2.close();

  {
    solver_execution_statistics statistics;
    auto result = execute_minisat( "/tmp/test.cnf", statistics );
    BOOST_CHECK( statistics.num_vars == 3u );
    BOOST_CHECK( statistics.num_clauses == 7u );
    BOOST_CHECK( result->first == boost::dynamic_bitset<>( std::string( "010" ) ) );
    //BOOST_CHECK( result->second.all() );
  }

  {
    solver_execution_statistics statistics;
    auto result = execute_picosat( "/tmp/test.cnf", statistics );
    BOOST_CHECK( statistics.num_vars == 3u );
    BOOST_CHECK( statistics.num_clauses == 7u );
    BOOST_CHECK( result->first == boost::dynamic_bitset<>( std::string( "010" ) ) );
    //BOOST_CHECK( result->second.all() );
  }

  {
    solver_execution_statistics statistics;
    auto result = execute_sat13( "/tmp/test.cnf", statistics );
    BOOST_CHECK( statistics.num_vars == 3u );
    BOOST_CHECK( statistics.num_clauses == 7u );
    BOOST_CHECK( result->first == boost::dynamic_bitset<>( std::string( "010" ) ) );
    //BOOST_CHECK( result->second.all() );
  }

  {
    solver_execution_statistics statistics;
    auto result = execute_minisat( "/tmp/test-unsat.cnf", statistics );
    BOOST_CHECK( statistics.num_vars == 1u );
    BOOST_CHECK( statistics.num_clauses == 0u );
    BOOST_CHECK( !((bool)result) );
  }

  {
    solver_execution_statistics statistics;
    auto result = execute_picosat( "/tmp/test-unsat.cnf", statistics );
    BOOST_CHECK( statistics.num_vars == 1u );
    BOOST_CHECK( statistics.num_clauses == 2u );
    BOOST_CHECK( !((bool)result) );
  }

  {
    solver_execution_statistics statistics;
    auto result = execute_sat13( "/tmp/test-unsat.cnf", statistics );
    BOOST_CHECK( statistics.num_vars == 1u );
    BOOST_CHECK( statistics.num_clauses == 2u );
    BOOST_CHECK( !((bool)result) );
  }
}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
