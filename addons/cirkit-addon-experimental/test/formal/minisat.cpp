/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE minisat

#include <iostream>

#include <boost/mpl/list.hpp>
#include <boost/test/test_case_template.hpp>
#include <boost/test/unit_test.hpp>

#include <formal/sat/minisat.hpp>
#include <formal/sat/sat_solver.hpp>

#ifdef ADDON_FORMAL
#include <formal/sat/z3.hpp>
#endif

using namespace cirkit;

#if defined ( ADDON_FORMAL )
using solvers = boost::mpl::list<minisat_solver, z3_solver>;
#else
using solvers = boost::mpl::list<minisat_solver>;
#endif

BOOST_AUTO_TEST_CASE_TEMPLATE(simple, T, solvers)
{
  auto solver = make_solver<T>();

  add_clause( solver )( {1, 2} );
  add_clause( solver )( { -1} );

  solver_execution_statistics statistics;
  //auto result = solve( solver, statistics );

  //BOOST_CHECK( (bool)result );
  //BOOST_CHECK( !result->first[0] && result->first[1] );
  //BOOST_CHECK( result->second[0] && result->second[1] );
}

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
