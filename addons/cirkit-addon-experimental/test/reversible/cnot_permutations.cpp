/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE cnot_permutations

#include <boost/test/unit_test.hpp>

#if ADDON_REVERSIBLE

#include <iostream>

#include <boost/assign/std/vector.hpp>
#include <boost/dynamic_bitset.hpp>
#include <boost/range/irange.hpp>
#include <boost/range/algorithm/permutation.hpp>
#include <boost/range/algorithm_ext/push_back.hpp>

#include <core/utils/bitset_utils.hpp>
#include <core/utils/range_utils.hpp>
#include <reversible/utils/permutation.hpp>

using namespace boost::assign;
using namespace cirkit;

permutation_t create_permutation( unsigned a, unsigned b, unsigned c, unsigned d )
{
  permutation_t p;
  boost::push_back( p, boost::irange( 0u, 8u ) );

  p[a] = b;
  p[b] = a;
  p[c] = d;
  p[d] = c;

  return p;
}

permutation_t compute_commutator( const cycles_t& cf, const cycles_t& ch, const boost::dynamic_bitset<>& mask, const std::vector<unsigned>& fpperm )
{
  permutation_t g( 8u );

  for ( auto itc : index( cf ) )
  {
    for ( auto ite : index( itc.value ) )
    {
      int a = itc.index;
      int b = ite.index;

      if ( itc.value.size() == 2u ) /* apply mask? */
      {
        if ( mask[0] )
        {
          a = 1u - a;
        }
        if ( ( itc.index == 0u && mask[1] ) || ( itc.index == 1u && mask[2] ) )
        {
          b = 1u - b;
        }
      }
      else
      {
        a = fpperm[a - 2u] + 2u;
      }
      g[ite.value] = ch[a][b];
    }
  }

  return g;
}

/* compute g's such that f = g * h * g^-1 */
std::vector<permutation_t> compute_commutators( const permutation_t& f, const permutation_t& h )
{
  auto cf = permutation_to_cycles( f );
  auto ch = permutation_to_cycles( h );

  std::vector<permutation_t> gs;
  boost::dynamic_bitset<> mask( 3u );

  do
  {
    std::vector<unsigned> fpperm = {0,1,2,3};

    do
    {
      gs += compute_commutator( cf, ch, mask, fpperm );
    } while ( boost::next_permutation( fpperm ) );
    inc( mask );
  } while ( mask.any() );

  return gs;
}

BOOST_AUTO_TEST_CASE(simple)
{
  std::vector<permutation_t> cnots = {
    // (0 1)(2 3)(4 5)(6 7)
    { 1, 0, 3, 2, 4, 5, 6, 7 }, // (0 1)(2 3)
    { 1, 0, 2, 3, 5, 4, 6, 7 }, // (0 1)(4 5)
    { 0, 1, 3, 2, 4, 5, 7, 6 }, // (2 3)(6 7)
    { 0, 1, 2, 3, 5, 4, 7, 6 }, // (4 5)(6 7)
    // (0 2)(1 3)(4 6)(5 7)
    { 2, 3, 0, 1, 4, 5, 6, 7 }, // (0 2)(1 3)
    { 2, 1, 0, 3, 6, 5, 4, 7 }, // (0 2)(4 6)
    { 0, 3, 2, 1, 4, 7, 6, 5 }, // (1 3)(5 7)
    { 0, 1, 2, 3, 6, 7, 4, 5 }, // (4 6)(5 7)
    // (0 4)(1 5)(2 6)(3 7)
    { 4, 5, 2, 3, 0, 1, 6, 7 }, // (0 4)(1 5)
    { 4, 1, 6, 3, 0, 5, 2, 7 }, // (0 4)(2 6)
    { 0, 5, 2, 7, 4, 1, 6, 3 }, // (1 5)(3 7)
    { 0, 1, 6, 7, 4, 5, 2, 3 }  // (2 6)(3 7)
  };

  /* iterate over (a b)(c d) */
  for ( unsigned a = 0u; a < 7u; ++a )
  {
    for ( unsigned b = a + 1u; b < 8u; ++b )
    {
      for ( unsigned c = 0u; c < 7u; ++c )
      {
        for ( unsigned d = c + 1u; d < 8u; ++d )
        {
          if ( c <= a || c == b || d == b ) continue;

          auto perm = create_permutation( a, b, c, d );
          unsigned num_compatible_gates = 0u;

          for ( const auto& gate : cnots )
          {
            bool found = false;
            for ( const auto& comm : compute_commutators( perm, gate ) )
            {
              if ( is_involution( comm ) )
              {
                found = true;
                break;
              }
            }

            if ( found )
            {
              ++num_compatible_gates;
            }
          }
          std::cout << "[i] permutation: " << cycles_to_string( perm ) << std::endl;
          std::cout << "[i] compatible gates: " << num_compatible_gates << std::endl;
        }
      }
    }
  }
}

#else

BOOST_AUTO_TEST_CASE(simple) {}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
