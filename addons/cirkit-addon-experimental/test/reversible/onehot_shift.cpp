/* CirKit: A circuit toolkit
 * Copyright (C) 2009-2015  University of Bremen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE onehot_shift

#include <boost/test/unit_test.hpp>

#if ADDON_REVERSIBLE

#include <boost/assign/std/vector.hpp>
#include <boost/format.hpp>

#include <reversible/circuit.hpp>
#include <reversible/truth_table.hpp>
#include <reversible/functions/add_gates.hpp>
#include <reversible/functions/circuit_to_truth_table.hpp>
#include <reversible/io/create_image.hpp>
#include <reversible/io/print_circuit.hpp>
#include <reversible/io/write_specification.hpp>
#include <reversible/simulation/simple_simulation.hpp>
#include <reversible/utils/permutation.hpp>

using namespace boost::assign;
using namespace cirkit;

circuit create_onehot_shift( unsigned n )
{
  circuit c( n );

  for ( unsigned i = 0u; i < n - 1u; ++i )
  {
    prepend_cnot( c, i + 1u, i );
    append_cnot( c, i + 1u, i );
  }

  for ( unsigned i = 1u; i < n; ++i )
  {
    gate::control_container controls;
    controls += make_var( 0u );

    for ( unsigned j = 1u; j < i; ++j )
    {
      controls += make_var( j );
    }

    for ( unsigned j = i + 1u; j < n; ++j )
    {
      controls += make_var( j, false );
    }

    insert_toffoli( c, n - 2u + i, controls, i );
  }

  return c;
}

circuit create_onehot_shift_small( unsigned n )
{
  circuit c( n );

  for ( int i = n; i >= 0; --i )
  {
    gate::control_container controls;
    unsigned target = i % n;

    for ( unsigned j = 0u; j < n; ++j )
    {
      if ( j != target )
      {
        controls += make_var( j, false );
      }
    }

    append_toffoli( c, controls, target );
  }

  return c;
}

BOOST_AUTO_TEST_CASE(simple)
{
  for ( unsigned n = 3u; n < 9u; ++n )
  {
    std::cout << "n = " << n << std::endl;

    circuit c = create_onehot_shift_small( n );
    std::cout << c << std::endl;

    permutation_t perm = circuit_to_permutation( c );
    cycles_t cycles = permutation_to_cycles( perm );
    std::cout << cycles_to_string( cycles ) << std::endl;

    /* write *.spec file */
    binary_truth_table spec;
    circuit_to_truth_table( c, spec, simple_simulation_func() );
    write_specification( spec, boost::str( boost::format( "/tmp/onehot_shift%d.spec" ) % n ) );

    /* write *.tikz file */
    create_tikz_settings settings;
    create_image( boost::str( boost::format( "/tmp/onehot_shift%d.tikz" ) % n ), c, settings );
  }
}

#else

BOOST_AUTO_TEST_CASE(simple)
{
}

#endif

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
