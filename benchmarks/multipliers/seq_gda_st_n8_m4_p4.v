
//-----------------------ORIGINAL DESIGN BEGIN- ----------------------------
module GDA_St_N8_M4_P4(
    input  [7:0] in1,
    input  [7:0] in2,
    output [8:0] res
    );

wire [2:0] 	temp1, temp2, temp3, temp4;
wire 			carry_pred_1,carry_pred_2;

//C2
wire 			g0,g1,p0,p1;
wire			p1g0;
wire 			c2;

and and_0(g0,in1[0],in2[0]);
and and_1(g1,in1[1],in2[1]);
xor xor_0(p0,in1[0],in2[0]);
xor xor_1(p1,in1[1],in2[1]);

and and_2(p1g0,p1,g0);
or  or_1 (c2,g1,p1g0);

//C4
wire 			g2,g3,p2,p3;
wire			p3g2;
wire 			c4,p3p2,p3p2c2;

and and_3(g2,in1[2],in2[2]);
and and_4(g3,in1[3],in2[3]);
xor xor_2(p2,in1[2],in2[2]);
xor xor_3(p3,in1[3],in2[3]);

and and_5(p3g2,p3,g2);
or  or_2 (c4,g3,p3g2);

and and_6(p3p2,p3,p2);
and and_7(p3p2c2,p3p2,c2);
or  or_3 (carry_pred_1,c4,p3p2c2);

//C8
wire 			g4,g5,p4,p5;
wire			p5g4;
wire 			c6,p5p4,p5p4c4;

and and_8(g4,in1[4],in2[4]);
and and_9(g5,in1[5],in2[5]);
xor xor_4(p4,in1[4],in2[4]);
xor xor_5(p5,in1[5],in2[5]);

and and_10(p5g4,p5,g4);
or  or_4  (c6,g5,p5g4);

and and_11(p5p4,p5,p4);
and and_12(p5p4c4,p5p4,c4);
or  or_5 (carry_pred_2,c6,p5p4c4);

// Results

assign temp1[2:0] = in1[1:0] + in2[1:0];
assign temp2[2:0] = in1[3:2] + in2[3:2] + c2;
assign temp3[2:0] = in1[5:4] + in2[5:4] + carry_pred_1;
assign temp4[2:0] = in1[7:6] + in2[7:6] + carry_pred_2;
assign res[8:0] = {temp4[2:0],temp3[1:0],temp2[1:0],temp1[1:0]};

endmodule

//-----------------------ORIGINAL DESIGN END- ----------------------------
/*
 @file       :  seq_gda_st_n8_m4_p4.v
 @author     :  Arun <arun@uni-bremen.de>
 @brief      :  Seq Multiplier with Approximate Adder
 
 Modifications :
   Instead of the "+" operator explicit adder is used to faciliate the
   study of approximate adders.
 
 */
   module adder (
                output [8:0] res, 
                input [7:0] in1, 
                input [7:0] in2
                );

   GDA_St_N8_M4_P4 approx_adder_inst (.res(res), 
       		             .in1(in1), .in2(in2) );

   endmodule





module multiplier (a, b, out, clk, rst, oe);
   output [7:0] out;
   output 	 oe;
   input [3:0] 	 a;
   input [3:0] 	 b;
   input 	 clk;
   input 	 rst;

   reg [7:0] 	 out;
   reg 		 oe;
   wire [3:0] 	 a;
   wire [3:0] 	 b;
   wire 	 clk;
   wire 	 rst;


   wire [7:0] 	 p0, p1, p2, p3; // 4 Partial Products

   reg [3:0] 	 a_reg;
   reg [3:0] 	 b_reg;
   
   
   assign p0 = b_reg[0] ? {4'b0, a_reg} : 8'b0;
   assign p1 = b_reg[1] ? {3'b0, a_reg, 1'b0} : 8'b0;
   assign p2 = b_reg[2] ? {2'b0, a_reg, 2'b0} : 8'b0;
   assign p3 = b_reg[3] ? {1'b0, a_reg, 3'b0} : 8'b0;
   

   // Now each cycle the partial products are simply added.
   reg [7:0] 	 in1;
   reg [7:0] 	 in2;
   wire [8:0] 	 res;
   
   adder adder_inst1 (.in1(in1), .in2(in2), .res(res));

   reg [5:0] 	 state;
   
   always @(posedge clk)
     begin
	if (rst)
	  begin
	     state <= 6'd0; oe <= 0; out <= 8'd0; in1 <= 8'd0; in2 <= 8'd0;
	     a_reg <= 4'b0; b_reg <= 4'b0;
	  end

	else
	  begin
	     case (state)
	       0:
		 if (b == 4'd0)
		   begin
		      state <= 6'd0; out <= 8'd0; oe <= 1; 
		   end
		 else if (b == 4'b1)
		   begin
		      state <= 6'd0; out <= {4'd0, a}; oe <= 1; 
		   end
		 else
		   begin
		      a_reg <= a;
		      b_reg <= b;
		      oe <= 0; out <= 8'd0;
		      state <= 6'd1;
		   end
	       1:
		 begin
		    in1 <= p0; in2 <= p1; state <= 2; oe <= 0; out <= 8'd0;
		 end
	       2:
		 begin
		    in1 <= res[7:0]; in2 <= p2; state <= 3; oe <= 0; out <= 8'd0;
		 end
	       3:
		 begin
		    in1 <= res[7:0]; in2 <= p3; state <= 4; oe <= 0; out <= 8'd0;
		 end
	       4:
		 begin
		    out <= res[7:0];
		    state <= 0; oe <= 1;
		 end
	       default:
		 begin
		    state <= 0; oe <= 0;
		 end  
	     endcase // case (state)
	  end // else: !if(rst)
     end // always @ (posedge clk)
      
   
   


endmodule // multiplier



