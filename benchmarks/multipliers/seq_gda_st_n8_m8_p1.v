
//-----------------------ORIGINAL DESIGN BEGIN- ----------------------------
module GDA_St_N8_M8_P1(
    input  [7:0] in1,
    input  [7:0] in2,
    output [8:0] res
    );

wire [2:0] 	temp1, temp2, temp3, temp4, temp5, temp6, temp7, temp8;
wire  		p0,p1,p2,p3,p4,p5,p6,g0,g1,g2,g3,g4,g5,g6,c1,c2,c3,c4,c5,c6,c7;

and and_0(g0,in1[0],in2[0]);
and and_1(g1,in1[1],in2[1]);
and and_2(g2,in1[2],in2[2]);
and and_3(g3,in1[3],in2[3]);
and and_4(g4,in1[4],in2[4]);
and and_5(g5,in1[5],in2[5]);
and and_6(g6,in1[6],in2[6]);

xor xor_0(p0,in1[0],in2[0]);
xor xor_1(p1,in1[1],in2[1]);
xor xor_2(p2,in1[2],in2[2]);
xor xor_3(p3,in1[3],in2[3]);
xor xor_4(p4,in1[4],in2[4]);
xor xor_5(p5,in1[5],in2[5]);
xor xor_6(p6,in1[6],in2[6]);

assign c1 = g0;

assign c2 = g1;

assign c3 = g2;

assign c4 = g3;

assign c5 = g4;

assign c6 = g5;

assign c7 = g6;

// Results

assign temp1[1:0] = in1[0] + in2[0];
assign temp2[1:0] = in1[1] + in2[1] + c1;
assign temp3[1:0] = in1[2] + in2[2] + c2;
assign temp4[1:0] = in1[3] + in2[3] + c3;
assign temp5[1:0] = in1[4] + in2[4] + c4;
assign temp6[1:0] = in1[5] + in2[5] + c5;
assign temp7[1:0] = in1[6] + in2[6] + c6;
assign temp8[1:0] = in1[7] + in2[7] + c7;
assign res[8:0] = {temp8[1:0],temp7[0],temp6[0],temp5[0],temp4[0],temp3[0],temp2[0],temp1[0]};

endmodule

//-----------------------ORIGINAL DESIGN END- ----------------------------
/*
 @file       :  seq_gda_st_n8_m8_p1.v
 @author     :  Arun <arun@uni-bremen.de>
 @brief      :  Seq Multiplier with Approximate Adder
 
 Modifications :
   Instead of the "+" operator explicit adder is used to faciliate the
   study of approximate adders.
 
 */
   module adder (
                output [8:0] res, 
                input [7:0] in1, 
                input [7:0] in2
                );

   GDA_St_N8_M8_P1 approx_adder_inst (.res(res), 
       		             .in1(in1), .in2(in2) );

   endmodule





module multiplier (a, b, out, clk, rst, oe);
   output [7:0] out;
   output 	 oe;
   input [3:0] 	 a;
   input [3:0] 	 b;
   input 	 clk;
   input 	 rst;

   reg [7:0] 	 out;
   reg 		 oe;
   wire [3:0] 	 a;
   wire [3:0] 	 b;
   wire 	 clk;
   wire 	 rst;


   wire [7:0] 	 p0, p1, p2, p3; // 4 Partial Products

   reg [3:0] 	 a_reg;
   reg [3:0] 	 b_reg;
   
   
   assign p0 = b_reg[0] ? {4'b0, a_reg} : 8'b0;
   assign p1 = b_reg[1] ? {3'b0, a_reg, 1'b0} : 8'b0;
   assign p2 = b_reg[2] ? {2'b0, a_reg, 2'b0} : 8'b0;
   assign p3 = b_reg[3] ? {1'b0, a_reg, 3'b0} : 8'b0;
   

   // Now each cycle the partial products are simply added.
   reg [7:0] 	 in1;
   reg [7:0] 	 in2;
   wire [8:0] 	 res;
   
   adder adder_inst1 (.in1(in1), .in2(in2), .res(res));

   reg [5:0] 	 state;
   
   always @(posedge clk)
     begin
	if (rst)
	  begin
	     state <= 6'd0; oe <= 0; out <= 8'd0; in1 <= 8'd0; in2 <= 8'd0;
	     a_reg <= 4'b0; b_reg <= 4'b0;
	  end

	else
	  begin
	     case (state)
	       0:
		 if (b == 4'd0)
		   begin
		      state <= 6'd0; out <= 8'd0; oe <= 1; 
		   end
		 else if (b == 4'b1)
		   begin
		      state <= 6'd0; out <= {4'd0, a}; oe <= 1; 
		   end
		 else
		   begin
		      a_reg <= a;
		      b_reg <= b;
		      oe <= 0; out <= 8'd0;
		      state <= 6'd1;
		   end
	       1:
		 begin
		    in1 <= p0; in2 <= p1; state <= 2; oe <= 0; out <= 8'd0;
		 end
	       2:
		 begin
		    in1 <= res[7:0]; in2 <= p2; state <= 3; oe <= 0; out <= 8'd0;
		 end
	       3:
		 begin
		    in1 <= res[7:0]; in2 <= p3; state <= 4; oe <= 0; out <= 8'd0;
		 end
	       4:
		 begin
		    out <= res[7:0];
		    state <= 0; oe <= 1;
		 end
	       default:
		 begin
		    state <= 0; oe <= 0;
		 end  
	     endcase // case (state)
	  end // else: !if(rst)
     end // always @ (posedge clk)
      
   
   


endmodule // multiplier



