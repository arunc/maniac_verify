
//-----------------------ORIGINAL DESIGN BEGIN- ----------------------------
module GeAr_N8_R1_P4(
    input [7:0] in1,
    input [7:0] in2,
    output [8:0] res
    );

wire [5:0] temp1,temp2,temp3,temp4;

assign temp1[5:0] = in1[4:0] + in2[4:0];
assign temp2[5:0] = in1[5:1] + in2[5:1];
assign temp3[5:0] = in1[6:2] + in2[6:2];
assign temp4[5:0] = in1[7:3] + in2[7:3];

assign res[8:0] = {temp4[5:4],temp3[4],temp2[4],temp1[4:0]};

endmodule

//-----------------------ORIGINAL DESIGN END- ----------------------------
/*
 @file       :  seq_gear_n8_r1_p4.v
 @author     :  Arun <arun@uni-bremen.de>
 @brief      :  Seq Multiplier with Approximate Adder
 
 Modifications :
   Instead of the "+" operator explicit adder is used to faciliate the
   study of approximate adders.
 
 */
   module adder (
                output [8:0] res, 
                input [7:0] in1, 
                input [7:0] in2
                );

   GeAr_N8_R1_P4 approx_adder_inst (.res(res), 
       		             .in1(in1), .in2(in2) );

   endmodule





module multiplier (a, b, out, clk, rst, oe);
   output [7:0] out;
   output 	 oe;
   input [3:0] 	 a;
   input [3:0] 	 b;
   input 	 clk;
   input 	 rst;

   reg [7:0] 	 out;
   reg 		 oe;
   wire [3:0] 	 a;
   wire [3:0] 	 b;
   wire 	 clk;
   wire 	 rst;


   wire [7:0] 	 p0, p1, p2, p3; // 4 Partial Products

   reg [3:0] 	 a_reg;
   reg [3:0] 	 b_reg;
   
   
   assign p0 = b_reg[0] ? {4'b0, a_reg} : 8'b0;
   assign p1 = b_reg[1] ? {3'b0, a_reg, 1'b0} : 8'b0;
   assign p2 = b_reg[2] ? {2'b0, a_reg, 2'b0} : 8'b0;
   assign p3 = b_reg[3] ? {1'b0, a_reg, 3'b0} : 8'b0;
   

   // Now each cycle the partial products are simply added.
   reg [7:0] 	 in1;
   reg [7:0] 	 in2;
   wire [8:0] 	 res;
   
   adder adder_inst1 (.in1(in1), .in2(in2), .res(res));

   reg [5:0] 	 state;
   
   always @(posedge clk)
     begin
	if (rst)
	  begin
	     state <= 6'd0; oe <= 0; out <= 8'd0; in1 <= 8'd0; in2 <= 8'd0;
	     a_reg <= 4'b0; b_reg <= 4'b0;
	  end

	else
	  begin
	     case (state)
	       0:
		 if (b == 4'd0)
		   begin
		      state <= 6'd0; out <= 8'd0; oe <= 1; 
		   end
		 else if (b == 4'b1)
		   begin
		      state <= 6'd0; out <= {4'd0, a}; oe <= 1; 
		   end
		 else
		   begin
		      a_reg <= a;
		      b_reg <= b;
		      oe <= 0; out <= 8'd0;
		      state <= 6'd1;
		   end
	       1:
		 begin
		    in1 <= p0; in2 <= p1; state <= 2; oe <= 0; out <= 8'd0;
		 end
	       2:
		 begin
		    in1 <= res[7:0]; in2 <= p2; state <= 3; oe <= 0; out <= 8'd0;
		 end
	       3:
		 begin
		    in1 <= res[7:0]; in2 <= p3; state <= 4; oe <= 0; out <= 8'd0;
		 end
	       4:
		 begin
		    out <= res[7:0];
		    state <= 0; oe <= 1;
		 end
	       default:
		 begin
		    state <= 0; oe <= 0;
		 end  
	     endcase // case (state)
	  end // else: !if(rst)
     end // always @ (posedge clk)
      
   
   


endmodule // multiplier



