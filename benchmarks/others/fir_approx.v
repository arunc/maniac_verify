

// N,M are pixel widths. N=16 and M=17
module fir(clk, rst, x, y);
   parameter N = 16;
   parameter M = 17;
   input clk, rst;
   input [N-1:0] x;
   output [M-1:0] y;
   wire signed [M-1:0] y;
   parameter b0 = 5;
   parameter b1 = 6;
   parameter b2 = -7;
   parameter b3 = 1;
   reg signed [N-1:0]  d0, d1, d2, d3;
   wire signed [N-1:0] w0, w1, w2, w3;
   wire signed [N-1:0] w4, w5;
   
   
   always @(posedge clk) begin
      if (rst == 1) begin
	 d0 <= 0;
	 d1 <= 0;
	 d2 <= 0;
	 d3 <= 0;
      end else  begin
	 d0 <= x;
	 d1 <= d0;
	 d2 <= d1;
	 d3 <= d2;
      end // else: !if(rst == 1)
   end // always @ (posedge clk)

   
   assign w0 = x <<< 2; 
   assign w1 = d1 <<< 3;
   assign w2 = b2 * d2;
   assign w3 = d3 * 3;

   assign w4 = w0 + w1;
   assign w5 = w1 + w2;
   assign y = w3 + w5;

endmodule // fir

   
