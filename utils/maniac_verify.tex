\documentclass[notitlepage, a4paper,10pt]{report} \usepackage[utf8]{inputenc}
\renewcommand\thesubsection{\roman{subsection}.}
\renewcommand\thesection{\arabic{section}.}
\renewcommand\thesubsubsection{\roman{subsubsection}.}
\usepackage{graphicx}
\usepackage{url}
\usepackage{listings}
\usepackage{upquote}
\usepackage{marvosym}
%\usepackage{MinionPro}
\usepackage{xcolor}

\makeatletter
\newcommand*{\toccontents}{\@starttoc{toc}}
\makeatother

\setcounter{tocdepth}{4}
\setcounter{secnumdepth}{4}

% Title Page
\title{CirKit - Maniac Verify\\
  \large { \textit{
      ( How to Build  \& Run cirkit::mc\_verify 
      )}}
} \author{Arun \\ (arun@uni-bremen.de)}

\usepackage{vmargin} \setpapersize{A4}
\setmarginsrb{30mm}{30mm}{20mm}{22mm}{3mm}{12mm}{3mm}{10mm}

\begin{document}
\maketitle
%\tableofcontents
%\textcolor{gray}{\rule{\linewidth}{1.5pt}}
%\textcolor{gray}{\rule{\linewidth}{1.5pt}}

\textcolor{gray}{\noindent\hrulefill}

\textcolor{gray}{\noindent\hrulefill} 
Contents
\textcolor{gray}{\noindent\hrulefill}

\toccontents


%\textcolor[RGB]{220,220,220}{\rule{\textwidth}{2pt}}

%\textcolor{gray}{\rule{\textwidth}{1.5pt}}
%\textcolor{gray}{\rule{\textwidth}{1.5pt}}

\textcolor{gray}{\noindent\hrulefill} 

\textcolor{gray}{\noindent\hrulefill}

%\noindent\hrulefill

%\noindent\hrulefill

\section{Acknowledgments}

\textmd{MANIAC} is a project funded by the German Research Foundation, DFG (DR
287/29-1) that investigates suitable data structures for Approximate Computing.  The
project is carried out under the guidance of Prof. Rolf Drechsler in the
\textit{Group of Computer Architecture} (AGRA), Dept. of Computer Science, University
of Bremen, Germany - \url{http://www.informatik.uni-bremen.de/agra/eng/index.php}.
\vspace{0.2cm} \newline
Project url : 
\url{http://www.informatik.uni-bremen.de/agra/eng/maniac.php} (continuously updated)
However, at the time of writing this document, Maniac-Verify is not updated
here. Instead you may clone it from : \url{git@gitlab.com:arunc/maniac_verify.git}
\vspace{0.2cm} \newline
\textmd{CirKit} is a software library for logic synthesis and formal
verification. CirKit is maintained primarily by Dr. Mathias Soeken, and contributed
by several others. This application is developed as an algorithm (invoked as a
command) in CirKit.
\vspace{0.2cm} \newline
Further credits to German Academic Exchange Service (DAAD) for supporting my PhD
studies. 
\vspace{0.2cm} \newline
Related publication : Arun Chandrasekharan, Mathias Soeken, Daniel Grosse, Rolf
Drechsler, ``Precise Error Determination of Approximated Components in Sequential
Circuits with Model Checking'', in 53$^{rd}$ ACM/EDAC/IEEE Design Automation
Conference \& Exhibition (DAC), 2016 \label{label:pub}.


\section{Introduction}
\textsc{ManiacVerify} is a program that compares two verilog designs of similar
functionality and formally verifies if they conform to a given
quality metrics. The primary application of this tool is in \textit{Approximate
  Computing}. 
\subsection{Approximate Computing}
\textsc{Approximate Computing} refers to hardware and software techniques where 
\textit{implementation} is allowed to differ from \textit{specification}, but within
acceptable \textit{range}. In other words, it trades off \textit{accuracy} for design
goals such as speed, power and area.
There are several applications where
these techniques could be employed such as web-search, data mining, signal
processing, recognition etc. These class of applications do not have the concept of
an \textit{exact} answer or are computationally very
expensive to arrive at an exact answer. Moreover in practice, many of these are
dependent on 
input data sets that relies on assumptions, and the results may be subjective to human
perceptual limitations. Here, the key to faster and efficient results is to introduce
\underline{\textit{safe}} approximations in computations.
In short, Approximate Computing is a very promising
energy efficient computing paradigm that can address the ever increasing demand for speed
and performance in today's world. 
For an academic overview and recent updates in this area
refer \url{https://github.com/sampsyo/approxbib} \footnote{Need NodeJs
  and NPM to build the documents in \textit{approxbib}.}.


In this work, we concern ourselves with hardware level approximations
only. Furthermore we deal only with \textit{functional} (as opposed to timing)
approximations.  
In functional approximation, a circuit is implemented in a slightly different
functionality to improve the frequency, area and power. A common example is
approximation adders, where the addition of two inputs \textit{very likely} result in
the actual sum. Of course, the most important question in such optimization is
\textbf{errors}. By 
doing this we introduce a whole range of computational errors and we have to ensure
that the errors introduced are (i) negligible \textit{enough} for the application and
(ii) the 
errors do not have any \textit{side effects}. i.e. they do not accumulate or affect the
control logic or propagate further to disrupt other functions. 
In this work, we \textit{guarantee} the error behavior using \underline{formal
  verification} 
techniques. \newline
As a side note, timing approximations allow \textit{predictable} timing errors (for
e.g. setup-time violation) to
occur in circuit operation. These errors need to be verified, whether they could be
ignored or can be corrected (else metastability and circuit failure will result), and
the advantages should be compelling enough to go for this scheme.
%Also the potential dangers and 
% Overclocking could be seen as technique that may introduce timing approximations.

\subsection{Formal Verification}
Verification is complementary to design for successful product releases. In hardware,
verification mainly takes 
 two forms - simulations and formal. Simulation based approach
uses a test-vector with test-bench, where a set of inputs
is applied and the output is monitored for possible bugs. Tools such as
\textit{Cadence NCSim}
, \textit{Synopsys VCS} do this. However simulations do not give the
\textit{guarantee} 
that the implementation is functionally correct as the number of input patterns
are  essentially infinite\footnote {Even for a simple 16-bit combinational adder, there
  are $2^{16+16+1}$ = 8,589,934,591 input patterns to be verified.}. In practice, only a
small subset of important functional patterns are simulated.

Compared to simulations, formal verification does not need any input vector. Rather
the design is exhaustively verified for all possible scenarios.
Formal verification as used in the industry may be subdivided into two classes,
Logical Equivalence Checking {LEC} and Model Checking. 
I will briefly explain LEC, as this is the main technique used in our work.

\begin{figure}[htp] \centering{                                      
    \includegraphics[scale=0.6]{FormalVerification.pdf}}                             
  \caption{Conventional Design and Verification}                                         
\end{figure}                                                         



In LEC, two designs (usually called
golden and revised) are exhaustively checked to be \textit{equivalent}.
\textit{Cadence
Conformal LEC}, \textit{Synopsys Formality} are industry tools in this
category. However an 
inherent problem with formal verification is \textit{state-explosion}, which
increases the algorithmic complexity,  memory requirements and run time
drastically. This is  especially severe when
sequential elements (flip-flops and latches) are present. One approach (usually used
in the industry tools) to tackle this
is to identify and map the \underline{corresponding} \underline{sequential} elements
in both the 
designs, and 
in the subsequent step apply combinational equivalence
checking\footnote{Combinational Equiv Checking (CEC) scale far better compared to
  sequential counterpart. \newline
 Besides, in data-structures such as \textit{Binary
    Decision Diagram}, CEC is trivial due to canonical nature. }. 
A drawback to this 
approach is that flip-flops must have a corresponding mapping in both golden and
revised.
Situations where this fail (e.g. clock-gating latches, power-gating cells) are
identified and accommodated for, in a separate pre-processing step before applying
equivalence checking algorithm.
Yet another failure scenario is different \textit{state encoding} in golden and
revised. For e.g. a state machine may be synthesised with \textit{one-hot} encoding
in golden and \textit{grey encoding} in revised. Here even the number of flip flops
do not match, and identifying the sequential correspondence is tricky and complex.

Another LEC technique, which do not suffer from the latter problem is
\textit{state-unrolling}. This is an active research area and the state-of-art
techniques do not (yet) scale beyond a few thousands of registers. Therefore it
cannot be directly applied to commercial designs with millions of components.
 Despite these, we use state
unrolling with \textit{property directed reachability (PDR)} in this work
\Smiley{}. For further information refer the publication.

For the sake of completeness of this article, a superficial overview of Model Checking
is given next. In model checking, a design is formally checked against a
\textit{specification}. Industry level tools such as \textit{OneSpin 360 DV}
can do model 
checking. The \textit{specification} may look like a set of mathematical
formulas written in a specification language like PSL\footnote{
  \url{https://en.wikipedia.org/wiki/Property_Specification_Language} } 
or SVA\footnote{ \url{https://en.wikipedia.org/wiki/SystemVerilog#Assertions}}, or in
proprietary 
language (e.g. OneSpin ITL). The verification itself is exhaustive,
i.e. provides a guarantee for working; but capturing the design properties to
a specification language is a bit involved. 

\subsection{Maniac-Verify}
Maniac-Verify (\texttt{mc\_verify}) is a routine in \textit{CirKit} package to
precisely determine error behavior in sequential circuits that contain approximated
combinational components.
\texttt{mc\_verify} reads in \textit{golden circuit} and \textit{approximated
  circuit} descriptions in Verilog RTL and reports the error status of the
approximated one w.r.t the golden. The error
behavior in terms of \textit{accumulated error}, \textit{worst-case error}, 
\textit{worst case bit-flip error} and \textit{cumulative bit-flip error} are reported.
The worst-case error and accumulated error denote the absolute error magnitudes. 
If we consider N clock cycles of circuit operation, worst-case error is the maximum
possible error introduced in any of these N cycles. In other words, the approximated
circuit does not produce an error above this limit in any clock cycle. 
Errors tend to add up (can nullify each other also), and accumulate over a period of
circuit 
operation. The accumulated
error is the maximum sum of the errors introduced in N clock cycles. 
Similarly in many applications, rather than the magnitude of the error, the number of
output bits changed due to approximation is of interest. For e.g, in error detection
and corrections schemes, the magnitude of the error itself is unimportant, as long as
the number of bits flipped is within a limit. The maximum bit-flip error gives this
number and as before this is evaluated for accumulated bit-flips also.
If in an approximated circuit, the number of cumulative bit flips is guaranteed to be
within a \textit{correctable-limit} in N clock cycles, then the error detection and
correction circuitry need to 
act only once in N cycles. 

The accumulated error and accumulated bit-flip error are verified against a user
input threshold where as worst-case error and max bit-flip error are reported in
absolute numbers.
All these numbers are limits and the circuit is \underline{guaranteed} to work within
these error limits. It has to be noted that conventional LEC and simulations do not
verify a circuit in the presence of errors. In particular, finding the absolute limit
of errors and bit-flips is \textit{combinatorial optimization}\footnote{Not to be
  confused with combina-\textit{tional} and sequential (type of circuits)
  \newline Combinatorics is a branch of
  computer science \url{https://en.wikipedia.org/wiki/Combinatorial_optimization}
  } problem, which cannot 
be obtained by 
either of these techniques taken alone.
\begin{figure}[htp] \centering{                                      
    \includegraphics[scale=0.6]{maniac.pdf}}                             
  \caption{Maniac-Verify}
\end{figure}                                                         

\texttt{mc\_verify} uses formal techniques (SAT, PDR) with \textit{And-Invert Graph}
(AIG) as the underlying data structure. The application is coded in C++11.
 Maniac-Verify does a modified algorithm for LEC in the presence of errors.
For a detailed explanation of the methodology refer to the publication.


\section{Build Maniac-Verify}
The project is hosted here : \url{git@gitlab.com:arunc/maniac_verify.git}
\subsection{How to build in a nutshell (Ninja)}
\begin{lstlisting}[language=csh]
 $git clone git@gitlab.com:arunc/maniac_verify.git ./top
 $cd top/build
 $cmake .. -G Ninja
 $ccmake .. # turn on enable_cirkit-addon-experimental, enable_cirkit-addon-formal
 $ninja-build external   # Build external libs, takes about 30-40 min.
 $ninja-build cirkit     # Takes about 15 min.
\end{lstlisting}

\subsection{How to build in a nutshell (Make)} \label{label:make}
\begin{lstlisting}[language=csh]
 $git clone git@gitlab.com:arunc/maniac.git ./top
 $cd top/build
 $cmake .. -G 
 $ccmake .. # turn on enable_cirkit-addon-experimental, enable_cirkit-addon-formal
 $make external   # Build external libs, takes about 30-40 min.
 $make cirkit     # Takes about 15 min.
\end{lstlisting}


\subsection{How to Build - detailed}
\subsubsection{Dependencies}
  \begin{enumerate}
  \item A \textsc{Linux} OS.
  \item CMake version 3.0 or above
  \item GnuMake or Ninja-build
  \item GCC Compiler (at least version 4.9.0)
  \item GNU MP, and its C++ interface GMP++
  \item GNU readline
  \item GIT (For repository management)
  \item Boost Library (at least 1.56 version)
  \item In Ubunutu
    \begin{itemize}
    \item sudo apt-get install build-essential git g++ cmake
    \item sudo apt-get install libgmp3-dev libxml2-dev
    \item sudo apt-get install libboost-all-dev
    \item \textit{etc... (list may not complete. Please mail me in case of issues)} 
    \end{itemize}
  \end{enumerate}

\subsubsection{Build Flow}
Build time could easily be more than half an hour. CirKit does many things in
addition to \texttt{mc\_verify}. More important, it can be extended with
\textit{addons}. \texttt{mc\_verify} is initially developed as an addon in the
cirkit-addon-experimental project, and later merged with the main stream.
\begin{itemize}
\item Build time is about 30 min in Arch-Linux 12GB Core-i5 system.
\end{itemize}

\begin{itemize}
\item Step - 1, setup repository and initialize the build system.
  \begin{itemize}
  \item Choose either Ninja or Make for building. (Ninja is just another build
    system similar to Make). Here make flow is explained. 
  \end{itemize}
\begin{lstlisting}[language=bash]
 $git clone git@gitlab.com:arunc/maniac.git ./top
 $cd top/build
\end{lstlisting}
CMake is the build manager used and CCMake provides some higher
level build options. In the \texttt{ccmake} step, turn on the following entries,
\texttt{enable\_cirkit-addon-experimental} and \newline
\texttt{enable\_cirkit-addon-formal}. To turn these options on, navigate to
these two entries (the last 2), and
\begin{enumerate}
\item press \texttt{ENTER} in both entries first
\item press the letter \texttt{c} (configure) in the keyboard and then
\item press the letter \texttt{g} (generate) in the keyboard.
\end{enumerate}
For Make flow.
\begin{lstlisting}[language=bash]
$cmake .. -G 
$ccmake .. 
# turn on enable_cirkit-addon-experimental, enable_cirkit-addon-formal
\end{lstlisting}

\item Step - 2, Build the external libraries and dependencies. (in the top/build
  directory)
  \begin{lstlisting}[language=bash]
    $make external ##  Note:: Going to take a loooong time.
  \end{lstlisting}

\item Step - 3, Build the program itself. (in the top/build
  directory). Ignore the warnings thrown out. \Smiley{}
  \begin{lstlisting}[language=bash]
    $make cirkit 
  \end{lstlisting}

\end{itemize}

\subsubsection{Sample Log}
The whole build process will be something similar to the one given below.
\begin{verbatim}
-------------------------------------------------------------
kavitha: approxComputing -> git clone git@gitlab.com:arunc/maniac_verify.git ./top
Cloning into './top'...
...
kavitha: approxComputing -> cd top/build 
kavitha: top/build (master)-> 
kavitha: top/build (master)-> cmake -G Ninja ..
-- The CXX compiler identification is GNU 4.9.3
...
-- Boost version: 1.57.0
-- Found the following Boost libraries:
--   unit_test_framework
--   regex
--   filesystem
--   graph
--   program_options
--   system
--   timer
--   thread
-- Configuring done
-- Generating done
-- Build files have been written to: ...
kavitha: top/build (master)-> ccmake ..


kavitha: top/build (master)-> ninja external
...
[12/49] Performing download step (download, verify and extract) ...
-- minisat download command succeeded. ...
...
-- The C compiler identification is GNU 4.9.3
...
...
-- [download 21% complete]
-- [download 30% complete]
-- downloading... done
-- verifying file...
...
...
make: Leaving directory ...
[49/49] Build external dependencies

kavitha: top/build (master)-> 
kavitha: top/build (master)-> ninja cirkit  
[7/239] Building CXX object 
....
....
[239/239] Linking CXX executable programs/cirkit

kavitha: top/build (master)-> 
kavitha: top/build (master)-> l ./programs/cirkit 
-rwxrwxr-x 1 arun arun 6,5M Apr  4 23:28 ./programs/cirkit

kavitha: top/build (master)-> uname -i -s -v -o  
Linux #49~14.04.1-Ubuntu SMP Thu Dec 31 15:44:49 UTC 2015 x86_64 GNU/Linux
kavitha: top/build (master)-> 
-------------------------------------------------------------
\end{verbatim}


\subsection{Benchmarks and How to Run}
Benchmark circuits have been provided in top/benchmarks directory. 
Invoke the \texttt{cirkit} program in the build/programs directory and type
\texttt{help}. This will list the commands integrated to this version. 
Next type \texttt{mc\_verify -h}. This will show the Maniac-Verify program
options. Note that \texttt{mc\_verify}  does not share the common cirkit database,
and is always run as an independent program in this version. \newline
A linux terminal copy-paste that shows how to run the application is given below
(self explanatory) 

\begin{verbatim}
-------------------------------------------------------------
kavitha: approxComputing/top (master)-> ./build/programs/cirkit 
cirkit> help
[i] Available commands:
    ...
    * bdd                  : BDD manipulation
    * cec                  : Combinatorial equivalence checking ...
    ...
    * mc_combo_metrics     : MANIAC Simple Error Metrics...
    * mc_verify            : MANIAC Formal Verifier for ...
    ...
    * quit                 : Quits the program
    ...
    * write_verilog        : Writes circuit to Verilog
cirkit> 
cirkit> mc_verify -h
MANIAC Formal Verifier for Approximate circuits :: ...
  -h [ --help ]                       produce help message
  -g [ --golden ] arg                 Golden Design file (verilog)
  -a [ --approx ] arg                 Approximated Design file (verilog)
  -1 [ --golden_module ] arg          Golden Module to be verified
  -2 [ --approx_module ] arg          Approx Module to be verified
  -p [ --port ] arg                   Output to be verified
  --avg_error arg                     Max average error to verify
  --accum_error_rate arg              Accum error-rate [count (bit_flips()]
  --accum_error arg                   Max accumulated error 
  -c [ --cycle_limit ] arg (=100)     Number of clock cycles to check for 
                                      accumulated error. [Default = 100]
  ...
  -l [ --log ] arg                    Log file [Default = cirkit_maniac.log]
  -d [ --debug ] arg (=1)             Extra debug information [Default = 1 
                                      (true), Recommended to keep true]
  --clock arg (=clock)                Clock signal name [Default = clock]
  --reset arg (=rst)                  Reset signal name [Default = rst]
  --oe arg (=__NA__)                  Output-Enable signal, 1bit output signal 
                                      [Default = __NA__ (No output-enable 
                                      signal)]
  --report arg (=maniac.rpt)          Output report [Default = maniac.rpt]
  --csv arg (=maniac.csv)             Output csv [Default = maniac.csv]
  --signed_outputs arg (=0)           If the outputs are signed numbers 
                                      [Default = false]
  ...
  --opt arg (=0)                      heavy optimization procedures 
  ...
cirkit> 
cirkit> mc_verify -g ./benchmarks/adders/seq_aca_ii_n8_q4.v -1 multiplier 
	          -a ./benchmarks/adders/seq_rca_n8.v -2 multiplier -p out 
          --accum_error 1000 --accum_error_rate 5 
          --clock clk --reset rst --oe oe
[i] Maniac Circuit Verifier - started on Mon Apr  4 22:54:38 2016
[w] Directory "work" exists. Contents will be overwritten
...
----------------- Checking WORST CASE ----------------- :: ...
[w] Directory "work/worst_case" exists. Contents will be overwritten
[i] BinSearch PDR on output :: out  [width = 8]
[i] Step 1 Started  creating max-error miter :: Mon Apr  4 22:54:38 2016
...
Invariant F[7] : 353 clauses with 76 flops (out of 76)
Verification of invariant with 353 clauses was successful.  Time =     0.01 s
Property proved.  Time =     0.35 sec
[i] Abc Info ends 
[i] Step 8 Finished PDR :: BinSearch bounds [128,129] :: out >= 129?   ::  ...
----------------- Checking ACCUM ERROR ----------------- :: Mon Apr  4 22:54:..
[w] Directory "work/accum_case" exists. Contents will be overwritten
[i] Started  creating accum-error miter :: Mon Apr  4 22:54:49 2016
...
----------------- Checking WORST ERROR RATE ----------------- :: Mon Apr  4...
[w] Directory "work/worst_er" exists. Contents will be overwritten
[i] BinSearch PDR on output :: out  [width = 8]
[i] Step 1 Started  creating max-error-rate miter :: Mon Apr  4 22:54:52 2016
[i] Step 1 Finished creating max-error-rate miter :: Mon Apr  4 22:54:53 2016
[i] Step 1 Starting PDR :: BinSearch bounds [0,8] :: ##bit_flip(out) >= 4?   
...
[i] Step 3 Finished PDR :: BinSearch bounds [2,3] :: ##bit_flip(out) >= 3?   
----------------- Checking ACCUM ERROR RATE ----------------- :: Mon Apr  4 
[w] Directory "work/accum_er" exists. Contents will be overwritten
[i] Started  creating accum-error-rate miter :: Mon Apr  4 22:54:56 2016
...
----------------- Checking AVG CASE ----------------- :: Mon Apr  4 22:54:57 
NOT YET READY TO DEAL WITH AVG-ERROR

RESULT: SEQ WORST CASE = 128
RESULT: CYCLES FOR MAX ACCUMULATED ERROR  1000 = 41
RESULT: SEQ WORST ERROR RATE = 3
RESULT: CYCLES FOR ACCUMULATED ERROR RATE 5 = 11
cirkit> quit

kavitha: approxComputing/top (master)-> cat maniac.rpt 
Maniac Approximation Verifier Report 
Start Time  :: Mon Apr  4 22:54:38 2016
------------------------------------------------
Program Options
golden,g         :: ./benchmarks/adders/seq_aca_ii_n8_q4.v
approx,a         :: ./benchmarks/adders/seq_rca_n8.v
golden_module,1  :: multiplier
approx_module,2  :: multiplier
...
report   :: maniac.rpt
...
cex_en          :: 0
cross_verify    :: 0
------------------------------------------------
Results
Worst Case   :: 128
Cycles for Max Accumulated Error of 1000  :: 41
Worst Error Rate   :: 3
Cycles for Accumulated Error Rate of 5  :: 11
...
...
-------------------------------------------------------------
\end{verbatim}

\section{A brief intro to CMake} \label{sec:cmake-flow}
CMake is a build \textit{manager} (build automation system). CMake can be
\textit{directed} (using CMakeLists.txt configurations) to include files from
specific directories, build external 
libraries etc.. Internally it generates the Makefile for \textit{make} or
corresponding build-configuration for \textit{Ninja} etc 
to build the system.

You need to explicitly give the \textit{make} or
\textit{ninja-build} command to build the system. As such in this flow, CMake
will not do that for you.

Main advantage for using CMake is that, it allows to script the build process
and automate heavily. Takes very little effort to understand the  syntax
(\textit{with some interesting conventions}) if
experienced in scripting and Makeflow. 

There are three items in this flow, \texttt{cmake} (c-make), \texttt{ccmake}
(c-c-make) 
and \texttt{make} (or \texttt{ninja-build})

\begin{enumerate}
\item \texttt{cmake} (c-make)
  \begin{itemize}
  \item \texttt{cmake} is the program that tracks all the source files (.cpp,
    .hpp) etc and write out build configuration.
    \begin{itemize}
    \item What this means is, every time you \textbf{add a new} C++ file, you must
      run \verb|'cmake ..'| in the \verb|'./build'| directory to reflect the
      new file you added.
    \item cmake configuration files are CMakeLists.txt
    \end{itemize}
  \end{itemize}
\item \texttt{ccmake} (c-c-make)
  \begin{itemize}
  \item \texttt{ccmake} is a simple interface for \texttt{cmake}. It gives some
    high level options (for e.g. build shared library or static library) for
    cmake. 
    \begin{itemize}
    \item Invoke it once, opens a terminal based menu kind of system
      (curses). You can 
      move around with arrow keys and press enter to change the value. Once this
      is over press letter ``c''  to configure and ``g'' to generate and exit.
      \begin{itemize}
      \item All these options and what to press, when etc are displayed in the
        ccmake screen itself.
      \item Normally need to do only once at the beginning of the project.
      \item There will be better GUI replacements for ccmake (But I use this
        one, for now).
      \item Need to do this \verb|'ccmake ..'| from the \verb|'./build'|
        directory. 
      \end{itemize}
    \end{itemize}
  \end{itemize}
\item The actual build - \texttt{Make, Ninja}  etc
  \begin{itemize}
  \item Use normal \verb|'make <optional-arg>'| or
    \verb|'ninja-build <optional-arg>'| for building the executable.
  \item As always, invoke it from the \texttt{./build} directory.
  \end{itemize}
\end{enumerate}

\section{C++ source files}
To figure out the related C++ source files could very intimidating in
cirkit \Frowny{}. There are over 300 source files organized into many different
directories. 
The top level \texttt{src}, \texttt{programs} directories are used
by the base routines of cirkit.  The \texttt{mc\_verify.cpp} and \texttt{hpp} are in
the 
directory : \newline 
 \texttt{addons/cirkit-addon-experimental/src/classical/cli/commands}.  \newline
This is integrated to cirkit with an \texttt{ADD\_COMMAND} macro in : \newline
\texttt{addons/cirkit-addon-experimental/programs/classical/cirkit.cpp} \newline
As a starting point, go through these files; best of luck \Smiley{}. \newline
Note: everything in the build directory is for build purpose only and are not the
actual source files. CMake/Ninja might copy the source files to the build directories
first. Any modifications in the build directories will be completely
ignored. Normally user need not bother about the contents of top/build except for the
executable. 

\vspace{1cm}
\center{********************}

\end{document}          

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% mode: reftex
%%% mode: auto-fill
%%% TeX-master: t
%%% End:


